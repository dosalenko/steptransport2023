import Vue from "vue";
import DataTable from 'laravel-vue-datatable';

import ClientDataTable from "./components/ClientDataTable";
import ClientCreate from "./components/ClientCreate";
import ClientEdit from "./components/ClientEdit";
import AgreementCreate from "./components/AgreementCreate";
import AgreementEdit from "./components/AgreementEdit";
import ProductDataTable from "./components/ProductDataTable";
import ProductCreate from "./components/ProductCreate";
import ServiceCreate from "./components/ServiceCreate";
import BookingCreate from "./components/BookingCreate";
import RecurringDataTable from "./components/RecurringDataTable";
import RecurringCreate from "./components/RecurringCreate";
import RecurringEdit from "./components/RecurringEdit";
import TripDataTable from "./components/TripDataTable";
import TripEdit from "./components/TripEdit";
import BookingDataTable from "./components/BookingDataTable";
import BookingEdit from "./components/BookingEdit";
import UserCreate from "./components/UserCreate";
import Settings from "./components/Settings";
import StepAddressCreate from "./components/StepAddressCreate";
import StepAddresses from "./components/StepAddresses";
import LocationDataTable from "./components/LocationDataTable";
import DriverDataTable from "./components/DriverDataTable";
import ColliDataTable from "./components/ColliDataTable";
import LocationEdit from "./components/LocationEdit";
import DriverEdit from "./components/DriverEdit";
import ReceiveGoods from "./components/ReceiveGoods";
import ReceiveGoodsMobile from "./components/ReceiveGoodsMobile";
import ReceiveColli from "./components/ReceiveColli";
import MoveGoods from "./components/MoveGoods";
import PackGoods from "./components/PackGoods";
import CreateBarcode from "./components/CreateBarcode";
import LockedProducts from "./components/LockedProducts";
import BookingRecalculatePrices from "./components/BookingRecalculatePrices";
import Route from "./components/Route";
import Rate from "./components/Rate";
import CheckDelivery from "./components/CheckDelivery";
import FlashMessage from '@smartweb/vue-flash-message';
import VueProgressBar from 'vue-progressbar'
import vSelect from "vue-select";
import ProgressBar from 'vuejs-progress-bar'
import DatePicker from 'vue2-datepicker';
import VueTimepicker from 'vue2-timepicker';
import 'vue2-timepicker/dist/VueTimepicker.css';
import 'vue-select/dist/vue-select.css';
import 'vue2-datepicker/index.css';
import {BootstrapVue} from 'bootstrap-vue'
import vTitle from 'vuejs-title'

require('./bootstrap');

Vue.use(BootstrapVue)
Vue.use(DatePicker)
Vue.use(VueTimepicker)
Vue.use(ProgressBar)
Vue.use(DataTable);
Vue.use(FlashMessage, {});
Vue.use(VueProgressBar, {
    color: 'rgb(143, 255, 199)',
    failedColor: 'red',
    height: '20px'
})
Vue.use(vTitle, {
    cssClass: "my-title-bubble",
    fontSize: "14px",
    maxHeight: "150px",
    maxWidth: "380px"
})
Vue.component("v-select", vSelect);

Vue.directive('number', {
    inserted(el, binding, vnode) {
        var event = new Event("input", {bubbles: true});
        el.value = Number(el.value).toLocaleString('da-DK');
        el.dispatchEvent(event);
    },
});

const app = new Vue({
    el: '#app',
    components: {
        ClientDataTable,
        ProductDataTable,
        ClientCreate,
        ClientEdit,
        AgreementCreate,
        AgreementEdit,
        ProductCreate,
        ServiceCreate,
        BookingCreate,
        BookingDataTable,
        BookingEdit,
        RecurringDataTable,
        RecurringCreate,
        RecurringEdit,
        TripDataTable,
        TripEdit,
        UserCreate,
        Settings,
        StepAddresses,
        StepAddressCreate,
        LocationDataTable,
        DriverDataTable,
        ColliDataTable,
        LocationEdit,
        DriverEdit,
        ReceiveGoods,
        ReceiveGoodsMobile,
        ReceiveColli,
        CreateBarcode,
        MoveGoods,
        PackGoods,
        LockedProducts,
        BookingRecalculatePrices,
        Route,
        Rate,
        CheckDelivery,
    }
});
