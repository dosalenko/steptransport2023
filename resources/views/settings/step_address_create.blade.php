@extends('layouts.app')

@section('content')
    <div class="flex-center position-ref full-height">
        <div class="container-fluide mx-3 px-4 py-4 bg-light rounded-lg">
            <div class="row">
                 <step-address-create></step-address-create>
            </div>
        </div>
    </div>
@endsection
