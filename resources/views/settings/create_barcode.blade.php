@extends('layouts.app')

@section('content')
    <div class="flex-center position-ref full-height">
        <div class="container-fluide mx-3 px-4 py-4 bg-light rounded-lg">
            <div class="row">
                <create-barcode url="{{ route('collis.getAll') }}" code="{{$code}}" warehouse="{{$warehouse}}"></create-barcode>
            </div>
        </div>
    </div>
@endsection
