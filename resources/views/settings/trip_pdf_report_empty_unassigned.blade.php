@php
    $generator = new Picqer\Barcode\BarcodeGeneratorHTML();
@endphp
<html>
<head>
    <title> Report </title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
</head>
<body>
@if ($showAllTrips || $showSingleDriverTrips)
    <?php
    $i = 0;
    ?>
    @foreach($routesStopsFormatted as $driver=>$batch)
        @foreach($batch as $k=>$stop)
            @if(count($stop)>0)
                @if ($k!=0)
                    <div class="page-break"></div>
                @endif
                <table class="table table-borderless table-sm" style="border: none;">
                    <tbody>
                    <tr>
                        <td valign="top">
                            <img src="https://my.steptransport.dk/logo.png" width="128" alt=""> <br/>
                            <strong>Driver: {{$driver}}</strong>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td rowspan="2">
                            <div class="picklist">
                                <h4>Picklist - {{$pickListDate}} </h4>
                            </div>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                            <div class="barcode">
                                @if (array_key_exists($driver,$barcodeData))
                                    {!! $generator->getBarcode($barcodeData[$driver], $generator::TYPE_CODE_128) !!}
                                    {{$barcodeData[$driver]}}_picklist
                                @endif
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <table class="table table-borderless table-sm data-table" style="font-size:10px;">
                    <thead>
                    <tr>
                        <td class="text-left"><b> Stop </b></td>
                        <td class="text-left" style="width:10%"><b> Trip </b></td>
                        <td class="text-left"><b> Client </b></td>
                        <td class="text-left" style="width:20%"><b> Address </b></td>
                        <td class="text-left"><b> Reference </b></td>
                        <td class="text-left"><b> Colli </b></td>
                        <td class="text-left"><b> Location </b></td>
                        <td class="text-left" style="width:20%"><b> Note </b></td>
                        <td class="text-left" style="width:20%"><b> Services </b></td>
                        <td class="text-left"><b> &nbsp;&nbsp;W </b></td>
                        <td class="text-left"><b> &nbsp;&nbsp;D </b></td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($stop as $k=>$trip)
                        <tr class="border-bottom <?=$trip['trip']['type'] === 'depot' ? 'depot-background' : '' ?>">
                            <td class="text-left">
                                @if ($trip['trip']!==null && $trip['trip']['type']==='depot')
                                    depot
                                @else
                                    {{$k+1}}
                                @endif
                            </td>
                            <td class="text-left">
                                @if ($trip['trip']!==null && $trip['trip']['type']!=='depot' && $trip['trip']['type']!=='unknown')
                                    B-{{$trip['trip']['booking']['id']}}
                                    {{strpos($trip['orderNo'], 'T') !== false ? $trip['orderNo']: 'T-'.$trip['orderNo']}}
                                @endif
                                @if ($trip['trip']['type']==='unknown')
                                    {{$trip['trip']['id']}}
                                @endif
                            </td>
                            <td class="text-left">
                                @if ($trip['trip']!==null && $trip['trip']['type']!=='depot' && $trip['trip']['type']!=='unknown')
                                    {{$trip['trip']['client']['name']}} <br>
                                @endif
                                @if ($trip['trip']['type']==='unknown')
                                    {{$trip['trip']['client']}}
                                @endif
                            </td>
                            <td class="text-left" style="min-width:50%">
                                @if ($trip['trip']!==null)
                                    @if ($trip['trip']['type']==1)
                                        {{$trip['trip']['booking']['pickupAddress']['name']}},
                                        {{$trip['trip']['booking']['pickupAddress']['street']}}
                                        {{$trip['trip']['booking']['pickupAddress']['zip']}}
                                        {{$trip['trip']['booking']['pickupAddress']['city']}}
                                    @elseif ($trip['trip']['type']==3)
                                        {{$trip['trip']['booking']['dropoffAddress']['name']}},
                                        {{$trip['trip']['booking']['dropoffAddress']['street']}}
                                        {{$trip['trip']['booking']['dropoffAddress']['zip']}}
                                        {{$trip['trip']['booking']['dropoffAddress']['city']}}
                                    @elseif ($trip['trip']['type']=='depot')
                                        {{$trip['address']}}
                                    @endif
                                @endif
                                @if ($trip['trip']['type']==='unknown')
                                    {{$trip['trip']['address']}}
                                @endif
                            </td>
                            <td class="text-left">
                                @if ($trip['trip']!==null && $trip['trip']['type']!=='depot' && $trip['trip']['type']!=='unknown')
                                    {{--
                                                                        {{$trip['trip']['booking']['reference']}} <br> -
                                    --}}
                                    @if(count($trip['trip']['booking']['collis'])>0)
                                            @foreach ($trip['trip']['booking']['collis'] as $colli)
                                                {{$colli->reference}} ({{$colli->colli}}) <br/>
                                            @endforeach
                                    @endif
                                @endif
                            </td>
                            <td class="text-left">
                                @if ($trip['trip']!==null && $trip['trip']['type']!=='depot' && $trip['trip']['type']!=='unknown')
                                    {{$trip['trip']['total_colli']}} <br>
                                @endif
                            </td>
                            <td>
                                @if ($trip['trip']!==null && $trip['trip']['type']!=='depot' && $trip['trip']['type']!=='unknown')
                                    @foreach($trip['trip']['colli_location'] as $k=>$location)
                                        {{$k}} ({{$location}}) <br/>
                                    @endforeach
                                @endif
                            </td>
                            <td class="text-left">
                                @if ($trip['trip']!==null && $trip['trip']['type']!=='depot' && $trip['trip']['type']!=='unknown')
                                    {{$trip['trip']['booking']['note']}} <br>
                                @endif
                            </td>
                            <td class="text-left">
                                @if ($trip['trip']!==null && $trip['trip']['type']!=='depot' && $trip['trip']['type']!=='unknown')
                                    @if ($trip['trip']['type']===1)
                                        AFHENTNING
                                    @else
                                        @foreach($trip['trip']['services'] as $service)
                                            {{$service}} <br/>
                                        @endforeach
                                    @endif
                                @endif
                            </td>
                            <td class="text-left">
                                <div>
                                    <input id="checkbox-1" class="checkbox-custom" name="checkbox-1" type="checkbox">
                                    <label for="checkbox-1" class="checkbox-custom-label"></label>
                                </div>
                            </td>
                            <td class="text-left">
                                <div>
                                    <input id="checkbox-1" class="checkbox-custom" name="checkbox-1" type="checkbox">
                                    <label for="checkbox-1" class="checkbox-custom-label"></label>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        @endforeach
        <?php
        $i++;
        ?>
        @if (!$showSingleDriverTrips && $i!==count($routesStopsFormatted))
            <div class="page-break"></div>
        @endif
    @endforeach
@endif
<script type="text/php">
	if ( isset($pdf) ) {
	    $pdf->page_script('
	            $font = $fontMetrics->get_font("Arial, Helvetica, sans-serif", "normal");
	            $size = 10;
	            $pageText = "Page " . $PAGE_NUM . " of " . $PAGE_COUNT;
	            $y = 550;
	            $x = 400;
	            $pdf->text($x, $y, $pageText, $font, $size);
	    ');
	}
</script>
</body>
</html>
<style>
    .checkbox-custom {
        display: none;
    }

    .checkbox-custom-label {
        display: inline-block;
        position: relative;
        vertical-align: middle;
        margin: 5px;
        cursor: pointer;
    }

    .checkbox-custom + .checkbox-custom-label:before {
        content: '';
        background: #fff;
        border-radius: 5px;
        border: 2px solid #ddd;
        display: inline-block;
        vertical-align: middle;
        width: 10px;
        height: 10px;
        padding: 2px;
        margin-right: 10px;
    }

    .checkbox-custom:checked + .checkbox-custom-label:after {
        content: "";
        padding: 0;
        position: absolute;
        width: 1px;
        height: 5px;
        border: solid blue;
        border-width: 0 3px 3px 0;
        transform: rotate(45deg);
        top: 2px;
        left: 0;
    }

    .picklist {
        position: absolute;
        left: 35%;
    }

    .barcode {
        position: absolute;
        right: 0;
    }

    body {
        margin: 0 !important;
        padding: 0 !important;
    }

    /*    .data-table td {
            border-bottom: 1px solid rgba(0, 0, 0, 0.39);
        }*/
    .data-table td {
        border-right: 0.5px solid grey;
    }

    tr td {
        padding: 3px !important;
        margin: 0 !important;
    }

    .page-break {
        page-break-after: always;
    }

    .depot-background {
        background-color: yellow;
    }
</style>
