<html>
<head>
    <title> Report </title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
</head>
<body>
<table class="table table-borderless table-sm" style="border: none;">
    <tbody>
    <tr>
        <td valign="top" style="width:60%">
            <img src="logo.png" width="128" alt="">
        </td>
        <td>
            <h4> {{$colliRequest['receivedFrom']}} - {{$colliRequest['receivedTo']}} </h4>
        </td>
    </tr>
    </tbody>
</table>
<table class="table table-bordered table-striped" style="font-size:10px;">
    <thead>
    <tr>
        <td class="text-left"><b>Reference</b></td>
        <td class="text-left"><b>Colli</b></td>
        <td class="text-left"><b>Volume</b></td>
        <td class="text-left"><b>Weight</b></td>
    </tr>
    </thead>
    <tbody>
    @foreach($collis as $colli)
        <tr>
            <td class="text-left">{{$colli->reference}}</td>
            <td class="text-left">{{\App\Services\Core\Helpers\Helper::getTotalColliAmountByColliBatch($colli->colli_batch)}}</td>
            <td class="text-left">{{number_format(\App\Services\Core\Helpers\Helper::getTotalColliMeasureByColliBatch($colli->colli_batch, 'volume'), 1)}}
                m3
            </td>
            <td class="text-left">{{number_format(\App\Services\Core\Helpers\Helper::getTotalColliMeasureByColliBatch($colli->colli_batch, 'weight'), 0)}}
                kg
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<script type="text/php">
	if ( isset($pdf) ) {
	    $pdf->page_script('
	            $font = $fontMetrics->get_font("Arial, Helvetica, sans-serif", "normal");
	            $size = 10;
	            $pageText = "Page " . $PAGE_NUM . " of " . $PAGE_COUNT;
	            $y = 550;
	            $x = 400;
	            $pdf->text($x, $y, $pageText, $font, $size);
	    ');
	}
</script>
</body>
</html>
<style>
    body {
        margin: 0 !important;
        padding: 0 !important;
    }

    tr td {
        padding: 0 !important;
        margin: 0 !important;
    }

    .page-break {
        page-break-after: always;
    }
</style>
