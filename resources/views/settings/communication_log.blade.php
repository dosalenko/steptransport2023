@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <table class="table table-striped">
                                    <th>Trip</th>
                                    <th>Date assigned</th>
                                    <th>Date of sending</th>
                                    <th>Message type</th>
                                    <th>Communication type</th>
                                    <th>Message</th>
                                    @foreach($communications as $communication)
                                        <tr>
                                            <td>
                                                <a href="/trips/{{$communication->trip_id}}/edit">
                                                    #T-{{$communication->trip_id}}</a>
                                            </td>
                                            <td>{{$communication->date_assigned}}</td>
                                            <td>{{$communication->created_at}}</td>
                                            <td>{{$communication->message}}</td>
                                            <td>
                                                @if ($communication->message_type==0)
                                                    -
                                                @else
                                                    {{$communication->communication_type === 1 ? 'SMS' : 'Email' }}
                                                @endif


                                            </td>
                                            <td>{!!$communication->text!!}</td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
