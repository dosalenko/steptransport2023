@extends('layouts.app')

@section('content')
    <div class="flex-center position-ref full-height">
        <div class="container-fluide mx-3 px-4 py-4 bg-light rounded-lg">
            <div class="row">
                <locked-products url="{{ route('settings.getLockedProducts') }}"></locked-products>
            </div>
        </div>
    </div>
@endsection
