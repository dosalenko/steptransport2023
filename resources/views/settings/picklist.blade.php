@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <table class="table table-striped">
                                    @foreach($picklist as $v)
                                        <tr>
                                            <td>{{$v->driver}}</td>
                                            <td><a href="/settings/download-file/{{$v->file}}"> {{$v->file}}</a>
                                            </td>
                                            <td>{{$v->date}} </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
