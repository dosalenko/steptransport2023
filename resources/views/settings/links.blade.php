@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <table class="table table-striped">
                                    <th>booking id</th>
                                    <th>
                                        unique link  <Wbr/>
                                        http://steptransport.youwe.dk/mobile/rate/
                                    </th>
                                    <th>trip <br/>
                                        http://steptransport.youwe.dk/mobile/check-delivery/
                                    </th>
                                    @foreach($bookings as $v)
                                        <tr>
                                            <td>{{$v->id}}</td>
                                            <td>{{$v->rate_link}}</td>
                                            <td>
                                                @foreach($v->trips as $trip)
                                                    {{ $trip->id }}:  {{ $trip->unique_link }} <br/>
                                                @endforeach
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
