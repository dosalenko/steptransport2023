<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}"
      class="{{ \Request::is('login') || \Request::is('password/*') ? 'h-100' : ''}}">
<link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/site.css') }}" rel="stylesheet">
    <link href="{{ asset('open-iconic/font/css/open-iconic-bootstrap.css') }}" rel="stylesheet">

    <!-- https://material.io/resources/icons/?style=baseline -->
    <link href="https://fonts.googleapis.com/css2?family=Material+Icons"
          rel="stylesheet">

    <!-- https://material.io/resources/icons/?style=outline -->
    <link href="https://fonts.googleapis.com/css2?family=Material+Icons+Outlined"
          rel="stylesheet">

    <!-- https://material.io/resources/icons/?style=round -->
    <link href="https://fonts.googleapis.com/css2?family=Material+Icons+Round"
          rel="stylesheet">

    <!-- https://material.io/resources/icons/?style=sharp -->
    <link href="https://fonts.googleapis.com/css2?family=Material+Icons+Sharp"
          rel="stylesheet">

    <!-- https://material.io/resources/icons/?style=twotone -->
    <link href="https://fonts.googleapis.com/css2?family=Material+Icons+Two+Tone"
          rel="stylesheet">
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDSSw89RnoedD7ChwJE6p2ACQ3GfMDqS1A&libraries=places">
    </script>
    @yield('css')
</head>
<body class="bg-page pb-5 {{ \Request::is('login') || \Request::is('password/*') ? 'h-100 ' : ''}}" lang="da">

<div id="app" class="{{ \Request::is('login') || \Request::is('password/*') ? 'h-100' : ''}}">

    <nav class="navbar navbar-expand-md navbar-light mt-2">
        <div class="container-fluid bg-header rounded-large">
            <a class="navbar-brand text-white d-flex align-items-right" href="{{ url('/') }}">
                <span class="px-2">Book Transport</span>
            </a>
            <a class="navbar-brand text-white d-flex align-items-right" href="{{ url('/') }}">
                <img height="32" src="/images/steptransport-logo.png" class="bg-white rounded-lg" alt="logo">
            </a>
        </div>
    </nav>

    @if (session('alert'))
        <div class="alert alert-danger">
            {{ session('alert') }}
        </div>
    @endif

    @if (session('success'))
        <div class="alert alert-success" role="alert">
            {{ session('success') }}
        </div>
    @endif
    @if (session('warning'))
        <div class="alert alert-warning" role="alert">
            {{ session('warning') }}
        </div>
    @endif
    @if (!empty($alert))
        <div class="alert alert-danger" role="alert">
            {{ $alert }}
        </div>
    @endif

    <main
        class="{{ \Request::is('login') || \Request::is('password/*') ? 'h-100 d-flex align-items-center' : 'pt-2 pb-max'}}">
        @yield('content')
    </main>
</div>
<div class="col-md-12 fixed-bottom footer text-white">
    <span>© {{ now()->year }} Step Transport</span>
    <div class="d-inline-block float-right">
        <span>Built with </span>
        <span class="text-danger oi oi-heart"></span> by
        <a href="http://youwe.dk/" target="_blank" class="text-white font-weight-bold">YouWe</a>
    </div>

</div>

</body>
</html>
