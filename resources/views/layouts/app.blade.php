<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}"
      class="{{ \Request::is('login') || \Request::is('password/*') ? 'h-100' : ''}}">
<link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/site.css') }}" rel="stylesheet">
    <link href="{{ asset('open-iconic/font/css/open-iconic-bootstrap.css') }}" rel="stylesheet">

    <!-- https://material.io/resources/icons/?style=baseline -->
    <link href="https://fonts.googleapis.com/css2?family=Material+Icons"
          rel="stylesheet">

    <!-- https://material.io/resources/icons/?style=outline -->
    <link href="https://fonts.googleapis.com/css2?family=Material+Icons+Outlined"
          rel="stylesheet">

    <!-- https://material.io/resources/icons/?style=round -->
    <link href="https://fonts.googleapis.com/css2?family=Material+Icons+Round"
          rel="stylesheet">

    <!-- https://material.io/resources/icons/?style=sharp -->
    <link href="https://fonts.googleapis.com/css2?family=Material+Icons+Sharp"
          rel="stylesheet">

    <!-- https://material.io/resources/icons/?style=twotone -->
    <link href="https://fonts.googleapis.com/css2?family=Material+Icons+Two+Tone"
          rel="stylesheet">
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDSSw89RnoedD7ChwJE6p2ACQ3GfMDqS1A&libraries=places&callback=Function.prototype">
    </script>
    @yield('css')
</head>
<body class="bg-page pb-5 {{ \Request::is('login') || \Request::is('password/*') ? 'h-100 ' : ''}}" lang="da">

<div id="app" class="{{ \Request::is('login') || \Request::is('password/*') ? 'h-100' : ''}}">

    <nav class="navbar navbar-expand-md navbar-light mt-2">
        <div class="container-fluid bg-header rounded-large">
            <a class="navbar-brand text-white d-flex align-items-center" href="{{ url('/') }}">
                <img height="32" src="/images/steptransport-logo.png" class="bg-white rounded-lg" alt="logo">
                <span class="px-2">Step Transport</span>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav mx-auto">
                    <!-- Authentication Links -->
                    @guest
                        <li class="nav-item" style="margin-right: 170px;">
                            <a class="nav-link h6 my-0 mx-2 px-4 py-3 {{ \Request::is('login') ? 'border-bottom border-width-2 text-white' : 'text-secondary'}}"
                               href="{{ route('login') }}"> <span class="oi oi-account-login pr-2"
                                                                  aria-hidden="true"></span> {{ __('Login') }}</a>
                        </li>
                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @endif
                    @else
                        @if((new \Jenssegers\Agent\Agent())->isMobile())
                            <li class="nav-item dropdown">
                                <a class="nav-link  d-flex align-items-center ropdown-toggle  h6 my-0 mx-2 py-3 px-4 dropdown-toggle {{ \Request::route()->getName()=='settings.index' || \Request::route()->getName()=='settings.stepAddresses'? 'border-bottom border-width-2 text-white' : 'text-secondary'}}"
                                   data-toggle="dropdown" href="#" role="button"
                                   aria-haspopup="true" aria-expanded="false">
                                  <span
                                      class="vertical-align-middle pr-2 material-icons-outlined">settings</span> {{ __('Settings') }}
                                </a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="{{ route('settings.moveGoods') }}">
                                      <span class="vertical-align-middle pr-2 material-icons-outlined">
                                      @if(Request::is('settings') && !Request::is('settings/move'))
                                              article @else
                                              article @endif</span>
                                        {{ __('Move goods') }}
                                    </a>
                                    <a class="dropdown-item" href="{{ route('settings.packGoods') }}">
                                      <span class="vertical-align-middle pr-2 material-icons-outlined">
                                      @if(Request::is('settings') && !Request::is('settings/pack'))
                                              article @else
                                              article @endif</span>
                                        {{ __('Packing goods') }}
                                    </a>
                                    <a class="dropdown-item" href="{{ route('mobile.receiveGoods') }}">
                                      <span class="vertical-align-middle pr-2 material-icons-outlined">
                                      @if(Request::is('mobile') && !Request::is('mobile/receiveGoods'))
                                              article @else
                                              article @endif</span>
                                        {{ __('Receive goods') }}
                                    </a>
                                </div>
                            </li>

                        @else
                            <li class="nav-item dropdown">
                                <a class="nav-link  d-flex align-items-center ropdown-toggle  h6 my-0 mx-2 py-3 px-4 dropdown-toggle {{ \Request::route()->getName()=='bookings.create' || \Request::is('bookings/*') || \Request::route()->getName()=='bookings.index'? 'border-bottom border-width-2 text-white' : 'text-secondary'}}"
                                   data-toggle="dropdown" href="#" role="button"
                                   aria-haspopup="true" aria-expanded="false">
                                  <span
                                      class="vertical-align-middle pr-2 material-icons-outlined">article</span> {{ __('Bookings') }}
                                </a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item d-flex align-items-center "
                                       href="{{ route('bookings.create') }}">
                                      <span class="vertical-align-middle pr-2 material-icons-outlined">
                                          post_add
                                      </span>
                                        <span class="d-none vertical-align-middle pr-2 material-icons-outlined">
                                          home
                                      </span>
                                        {{ __('Create booking') }}
                                    </a>
                                    <a class="dropdown-item" href="{{ route('bookings.index') }}">
                                      <span class="vertical-align-middle pr-2 material-icons-outlined">
                                      article
                                      </span>
                                        {{ __('Booking overview') }}
                                    </a>
                                    @userCan(\App\Models\UserRole::ROLE_ADMIN)
                                    <a class="dropdown-item" href="{{ route('recurrings.index') }}">
                                      <span class="vertical-align-middle pr-2 material-icons-outlined">
                                      article
                                      </span>
                                        {{ __('Recurring overview') }}
                                    </a>
                                    @endUserCan
                                </div>
                            </li>

                            @if (in_array('user', Auth::user()->roles)  && !in_array('admin', Auth::user()->roles))
                            <!--wms clients-->
                                <li class="nav-item">
                                    <a class="nav-link  d-flex align-items-center h6 my-0 mx-2 py-3 px-4 {{  \Request::is('collis/*')  || \Request::route()->getName()=='collis.index'? 'border-bottom border-width-2 text-white' : 'text-secondary'}}"
                                       href="{{ route('collis.index') }}">
                                                                <span
                                                                    class="vertical-align-middle pr-2 material-icons-outlined">warehouse
                                                                </span> {{ __('Warehouse') }}
                                    </a>
                                </li>
                            @endif

                            @userCan(\App\Models\UserRole::ROLE_ADMIN)

                            <!--trips-->
                            <li class="nav-item">
                                <a class="nav-link  d-flex align-items-center h6 my-0 mx-2 py-3 px-4 {{  \Request::is('trips/*')  || \Request::route()->getName()=='trips.index'? 'border-bottom border-width-2 text-white' : 'text-secondary'}}"
                                   href="{{ route('trips.index') }}"><span
                                        class="vertical-align-middle pr-2 material-icons-outlined">local_shipping</span> {{ __('Trips') }}
                                </a>
                            </li>

                            <!--WMS-->
                            <li class="nav-item dropdown">
                                <a class="nav-link  d-flex align-items-center ropdown-toggle  h6 my-0 mx-2 py-3 px-4 dropdown-toggle {{ \Request::route()->getName()=='collis.index' || \Request::route()->getName()=='settings.receiveGoods' || \Request::route()->getName()=='settings.createBarcode' || \Request::route()->getName()=='settings.moveGoods' || \Request::route()->getName()=='settings.packGoods' || \Request::route()->getName()=='locations.index' ? 'border-bottom border-width-2 text-white' : 'text-secondary'}}"
                                   data-toggle="dropdown" href="#" role="button"
                                   aria-haspopup="true" aria-expanded="false">
                                  <span
                                      class="vertical-align-middle pr-2 material-icons-outlined">warehouse</span> {{ __('WMS') }}
                                </a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="{{ route('collis.index') }}">
                                      <span class="vertical-align-middle pr-2 material-icons-outlined">
                                      @if(Request::is('collis'))
                                              article @else
                                              article @endif</span>
                                        {{ __('Overview') }}
                                    </a>

                                    <a class="dropdown-item" href="{{ route('settings.receiveGoods') }}">
                                      <span class="vertical-align-middle pr-2 material-icons-outlined">
                                      @if(Request::is('settings') && !Request::is('settings/receive'))
                                              move_to_inbox @else
                                              move_to_inbox @endif</span>
                                        {{ __('Receive') }}
                                    </a>
                                    <a class="dropdown-item" href="{{ route('settings.moveGoods') }}">
                                      <span class="vertical-align-middle pr-2 material-icons-outlined">
                                      @if(Request::is('settings') && !Request::is('settings/move'))
                                              exit_to_app @else
                                              exit_to_app @endif</span>
                                        {{ __('Move') }}
                                    </a>

                                    <a class="dropdown-item" href="{{ route('settings.packGoods') }}">
                                      <span class="vertical-align-middle pr-2 material-icons-outlined">
                                      @if(Request::is('settings') && !Request::is('settings/pack'))
                                              widgets @else
                                              widgets @endif</span>
                                        {{ __('Pack') }}
                                    </a>

                                    <a class="dropdown-item" href="{{ route('locations.index') }}">
                                      <span class="vertical-align-middle pr-2 material-icons-outlined">
                                      @if(Request::is('locations') && !Request::is('locations/index'))
                                              edit_location_alt @else
                                              article @endif</span>
                                        {{ __('Locations') }}
                                    </a>

                                </div>
                            </li>

                            <!--Settings-->
                            <li class="nav-item dropdown">
                                <a class="nav-link  d-flex align-items-center ropdown-toggle  h6 my-0 mx-2 py-3 px-4 dropdown-toggle {{ \Request::route()->getName()=='printer'  || \Request::is('products/*') || \Request::is('clients/*') || \Request::is('settings.index') || \Request::is('settings.stepAddresses') ? 'border-bottom border-width-2 text-white' : 'text-secondary'}}"
                                   data-toggle="dropdown" href="#" role="button"
                                   aria-haspopup="true" aria-expanded="false">
                                  <span
                                      class="vertical-align-middle pr-2 material-icons-outlined">settings</span> {{ __('Settings') }}
                                </a>
                                <div class="dropdown-menu">

                                    <a class="dropdown-item" href="{{ route('products.index') }}">
                                      <span class="vertical-align-middle pr-2 material-icons-outlined">
                                      inventory_2</span>
                                        {{ __('Products') }}
                                    </a>

                                    <a class="dropdown-item" href="{{ route('clients.index') }}">
                                      <span class="vertical-align-middle pr-2 material-icons-outlined">
                                      business</span>
                                        {{ __('Clients') }}
                                    </a>

                                    <a class="dropdown-item" href="{{ route('settings.index') }}">
                                      <span class="vertical-align-middle pr-2 material-icons-outlined">
                                      post_add</span>
                                        {{ __('Reporting') }}
                                    </a>

                                    <a class="dropdown-item" href="{{ route('settings.stepAddresses') }}">
                                      <span class="vertical-align-middle pr-2 material-icons-outlined">
                                      @if(Request::is('settings') && !Request::is('settings/step-addresses'))
                                              home_work @else
                                              home_work @endif</span>
                                        {{ __('Addresses') }}
                                    </a>

                                    <a class="dropdown-item" href="{{ route('printer') }}">
                                      <span class="vertical-align-middle pr-2 material-icons-outlined">
                                      @if(Request::is('settings') && !Request::is('settings/step-addresses'))
                                              print @else
                                              print @endif</span>
                                        {{ __('Printers') }}
                                    </a>

                                    <a class="dropdown-item" href="{{ route('drivers.index') }}">
                                      <span class="vertical-align-middle pr-2 material-icons-outlined">
                                      groups</span>
                                        {{ __('Drivers') }}
                                    </a>

                                    <a class="dropdown-item" href="{{ route('settings.showLockedProducts') }}">
                                      <span class="vertical-align-middle pr-2 material-icons-outlined">
                                      inventory_2</span>
                                        {{ __('Locked products') }}
                                    </a>

                                    <a class="dropdown-item" href="{{ route('routes.index') }}">
                                      <span class="vertical-align-middle pr-2 material-icons-outlined">
                                      home_work</span>
                                        {{ __('Routes') }}
                                    </a>
                                </div>
                            </li>

                            @endUserCan
                            @if (!Auth::guest())
                                <li class="nav-item dropdown">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        {{ Auth::user()->name }}
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                       document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                              class="d-none">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                            @endif
                        @endif

                    @endguest
                </ul>
            </div>
            <div>
                @if (!Auth::guest())
                    <a class="text-white h6 m-0" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                        <span class="vertical-align-middle material-icons pr-2">logout</span>
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                @endif
            </div>
        </div>
    </nav>


    @if (session('alert'))
        <div class="alert alert-danger">
            {{ session('alert') }}
        </div>
    @endif

    @if (session('success'))
        <div class="alert alert-success" role="alert">
            {{ session('success') }}
        </div>
    @endif
    @if (session('warning'))
        <div class="alert alert-warning" role="alert">
            {{ session('warning') }}
        </div>
    @endif
    @if (!empty($alert))
        <div class="alert alert-danger" role="alert">
            {{ $alert }}
        </div>
    @endif

    <main
        class="{{ \Request::is('login') || \Request::is('password/*') ? 'h-100 d-flex align-items-center' : 'pt-2 pb-max'}}">
        @yield('content')
    </main>
</div>
<div class="col-md-12 fixed-bottom footer text-white">
    <span>© {{ now()->year }} Step Transport</span>
    <div class="d-inline-block float-right">
        <span>Built with </span>
        <span class="text-danger oi oi-heart"></span> by
        <a href="http://youwe.dk/" target="_blank" class="text-white font-weight-bold">YouWe</a>
    </div>

</div>

</body>
</html>
