<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml"
      xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <!--[if gte mso 9]>
    <xml>
    <o:OfficeDocumentSettings>
        <o:AllowPNG/>
        <o:PixelsPerInch>96</o:PixelsPerInch>
    </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="x-apple-disable-message-reformatting">
    <!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge"><!--<![endif]-->
    <title></title>

    <style type="text/css">
        @media only screen and (min-width: 620px) {
            .u-row {
                width: 600px !important;
            }

            .u-row .u-col {
                vertical-align: top;
            }

            .u-row .u-col-100 {
                width: 600px !important;
            }

        }

        @media (max-width: 620px) {
            .u-row-container {
                max-width: 100% !important;
                padding-left: 0px !important;
                padding-right: 0px !important;
            }

            .u-row .u-col {
                min-width: 320px !important;
                max-width: 100% !important;
                display: block !important;
            }

            .u-row {
                width: calc(100% - 40px) !important;
            }

            .u-col {
                width: 100% !important;
            }

            .u-col > div {
                margin: 0 auto;
            }
        }

        body {
            margin: 0;
            padding: 0;
        }

        table,
        tr,
        td {
            vertical-align: top;
            border-collapse: collapse;
        }

        p {
            margin: 0;
        }

        .ie-container table,
        .mso-container table {
            table-layout: fixed;
        }

        * {
            line-height: inherit;
        }

        a[x-apple-data-detectors='true'] {
            color: inherit !important;
            text-decoration: none !important;
        }

        @media (max-width: 480px) {
            .hide-mobile {
                max-height: 0px;
                overflow: hidden;
                display: none !important;
            }
        }

        table, td {
            color: #000000;
        }

        #u_body a {
            color: #0000ee;
            text-decoration: underline;
        }

        @media (max-width: 480px) {
            #u_content_image_1 .v-src-width {
                width: auto !important;
            }

            #u_content_image_1 .v-src-max-width {
                max-width: 82% !important;
            }

            #u_content_heading_2 .v-container-padding-padding {
                padding: 6px 10px 10px !important;
            }

            #u_content_heading_2 .v-line-height {
                line-height: 110% !important;
            }

            #u_content_text_2 .v-container-padding-padding {
                padding: 40px 20px 10px !important;
            }

            #u_content_text_1 .v-container-padding-padding {
                padding: 10px 20px !important;
            }

            #u_content_text_1 .v-text-align {
                text-align: justify !important;
            }

            #u_column_3 .v-col-padding {
                padding: 50px 0px !important;
            }

            #u_content_text_3 .v-container-padding-padding {
                padding: 10px 20px !important;
            }

            #u_content_text_3 .v-text-align {
                text-align: center !important;
            }

            #u_content_text_3 .v-line-height {
                line-height: 160% !important;
            }

            #u_content_divider_1 .v-container-padding-padding {
                padding: 20px 10px !important;
            }

            #u_content_menu_1 .v-padding {
                padding: 5px !important;
            }

            #u_content_text_5 .v-container-padding-padding {
                padding: 10px 10px 0px !important;
            }

            #u_content_text_5 .v-text-align {
                text-align: center !important;
            }

            #u_content_text_5 .v-line-height {
                line-height: 160% !important;
            }
        }
    </style>
    <!--[if !mso]><!-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700&display=swap" rel="stylesheet"
          type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,700&display=swap" rel="stylesheet" type="text/css">
    <!--<![endif]-->
</head>

<body class="clean-body u_body"
      style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #e7e7e7;color: #000000">
<!--[if IE]>
<div class="ie-container"><![endif]-->
<!--[if mso]>
<div class="mso-container"><![endif]-->
<table id="u_body"
       style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: #e7e7e7;width:100%"
       cellpadding="0" cellspacing="0">
    <tbody>
    <tr style="vertical-align: top">
        <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
            <!--[if (mso)|(IE)]>
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td align="center" style="background-color: #e7e7e7;"><![endif]-->

            <div class="u-row-container" style="padding: 0px;background-color: transparent">
                <div class="u-row"
                     style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;">
                    <div
                        style="border-collapse: collapse;display: table;width: 100%;height: 100%;background-color: transparent;">
                        <!--[if (mso)|(IE)]>
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td style="padding: 0px;background-color: transparent;" align="center">
                                <table cellpadding="0" cellspacing="0" border="0" style="width:600px;">
                                    <tr style="background-color: transparent;"><![endif]-->

                        <!--[if (mso)|(IE)]>
                        <td align="center" width="600" class="v-col-padding"
                            style="background-color: #ffffff;width: 600px;padding: 0px 0px 50px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;"
                            valign="top"><![endif]-->
                        <div class="u-col u-col-100"
                             style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                            <div style="background-color: #ffffff;height: 100%;width: 100% !important;">
                                <!--[if (!mso)&(!IE)]><!-->
                                <div class="v-col-padding"
                                     style="height: 100%; padding: 0px 0px 50px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                                    <!--<![endif]-->

                                    <table id="u_content_text_2" style="font-family:'Montserrat',sans-serif;"
                                           role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
                                        <tbody>
                                        <tr>
                                            <td class="v-container-padding-padding"
                                                style="overflow-wrap:break-word;word-break:break-word;padding:40px 10px 10px 40px;font-family:'Montserrat',sans-serif;"
                                                align="left">
                                                <div class="v-text-align v-line-height"
                                                     style="line-height: 140%; text-align: left; word-wrap: break-word;">
                                                    <p style="font-size: 14px; line-height: 140%;"><strong><span
                                                                style="font-size: 18px; line-height: 25.2px;">Dear {{ array_key_exists('toOwnerName', $colliRequest) ? $colliRequest['toOwnerName'] : $collis[0]->client->name}}!</span></strong>
                                                    </p>
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                    <table id="u_content_text_1" style="font-family:'Montserrat',sans-serif;"
                                           role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
                                        <tbody>
                                        <tr>
                                            <td class="v-container-padding-padding"
                                                style="overflow-wrap:break-word;word-break:break-word;padding:10px 40px;font-family:'Montserrat',sans-serif;"
                                                align="left">

                                                <div class="v-text-align v-line-height"
                                                     style="line-height: 160%; text-align: left; word-wrap: break-word;">
                                                    <p style="font-size: 14px; line-height: 160%;">
                                                        Vi har d. {{$colliRequest['receivedFrom']}}
                                                        - {{$colliRequest['receivedTo']}}
                                                        modtaget jeres varer på vores varehus. <br/>
                                                        Se venligst vedhæftede liste for komplet overblik over hvilke
                                                        varer der er
                                                        modtaget. <br/>
                                                        I kan altid få et fuldt overblik over hvad I har
                                                        på lager på siden:
                                                        <a href="https://my.steptransport.dk/collis">https://my.steptransport.dk/collis</a>.
                                                    </p>
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                    <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
                            </div>
                        </div>
                        <!--[if (mso)|(IE)]></td><![endif]-->
                        <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->
                    </div>
                </div>
            </div>


            <div class="u-row-container" style="padding: 0px;background-color: transparent">
                <div class="u-row"
                     style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;">
                    <div
                        style="border-collapse: collapse;display: table;width: 100%;height: 100%;background-color: transparent;">
                        <!--[if (mso)|(IE)]>
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td style="padding: 0px;background-color: transparent;" align="center">
                                <table cellpadding="0" cellspacing="0" border="0" style="width:600px;">
                                    <tr style="background-color: transparent;"><![endif]-->

                        <!--[if (mso)|(IE)]>
                        <td align="center" width="600" class="v-col-padding"
                            style="background-color: #f0f0f0;width: 600px;padding: 50px 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;border-radius: 0px;-webkit-border-radius: 0px; -moz-border-radius: 0px;"
                            valign="top"><![endif]-->
                        <div id="u_column_3" class="u-col u-col-100"
                             style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                            <div
                                style="background-color: #f0f0f0;height: 100%;width: 100% !important;border-radius: 0px;-webkit-border-radius: 0px; -moz-border-radius: 0px;">
                                <!--[if (!mso)&(!IE)]><!-->
                                <div class="v-col-padding"
                                     style="height: 100%; padding: 50px 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;border-radius: 0px;-webkit-border-radius: 0px; -moz-border-radius: 0px;">
                                    <!--<![endif]-->

                                    <table id="u_content_text_3" style="font-family:'Montserrat',sans-serif;"
                                           role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
                                        <tbody>
                                        <tr>
                                            <td class="v-container-padding-padding"
                                                style="overflow-wrap:break-word;word-break:break-word;padding:0px 10px;font-family:'Montserrat',sans-serif;"
                                                align="left">

                                                <div class="v-text-align v-line-height"
                                                     style="line-height: 140%; text-align: center; word-wrap: break-word;">
                                                    <p style="font-size: 14px; line-height: 140%;">

                                                    <table height="0px" align="center" border="0" cellpadding="0"
                                                           cellspacing="0" width="100%"
                                                           style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;border-bottom: 1px solid #BBBBBB;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%">
                                                        <tbody>
                                                        <tr>
                                                            <td>
                                                                Med  venlig  hilsen / Best  regards​
                                                                <br/> <br/>
                                                            </td>
                                                        </tr>
                                                        <tr style="vertical-align: top">
                                                            <td style="padding-left:10px;width:20%;border-right:1px solid #BBBBBB;">
                                                                <img
                                                                    src="https://my.steptransport.dk/images/goods/image962440.jpg"
                                                                    alt=""
                                                                    title="" width="250px"
                                                                    style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;max-width: 350px !important">
                                                            </td>
                                                            <td style="width:80%; padding-left: 10px;" align="left">
                                                                <b> <span
                                                                        style="color:#843468">Joakim R. Sennicksen</span></b>
                                                                <br/>
                                                                Terminal Manager<br/> <br/>
                                                                Direct :  +45 50245050  <br/>
                                                                Office :  +45 50245050<br/> <br/>
                                                                <a rel="noopener" href="mailto:jrs@steptransport.dk "
                                                                   target="_blank"> jrs@steptransport.dk
                                                                </a>| <a href="www.steptransport.dk" title="">www.steptransport.dk </a>
                                                                 
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>

                                                </div>

                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                    <table id="u_content_text_3" style="font-family:'Montserrat',sans-serif;"
                                           role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
                                        <tbody>
                                        <tr>
                                            <td class="v-container-padding-padding"
                                                style="overflow-wrap:break-word;word-break:break-word;padding:0px 10px;font-family:'Montserrat',sans-serif;"
                                                align="left">

                                                <div class="v-text-align v-line-height"
                                                     style="line-height: 140%; text-align: center; word-wrap: break-word;">
                                                    <br/>
                                                    <p style="font-size: 14px; line-height: 140%;">

                                                    <table height="0px" align="center" border="0" cellpadding="0"
                                                           cellspacing="0" width="100%"
                                                           style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;border-bottom: 1px solid #BBBBBB;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%">
                                                        <tbody>
                                                        <tr>
                                                            <td>
                                                                <img
                                                                    src="https://my.steptransport.dk/images/goods/Signature_1.jpg"
                                                                    alt=""
                                                                    title=""
                                                                    style="width:100% !important; outline: none;text-decoration:
                                                            none;-ms-interpolation-mode: bicubic;clear: both;display:
                                                            block !important;border: none;height: auto;float:
                                                            none;max-width: 100% !important">
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>

                                                </div>

                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>


                                    <!--Alle opgaver-->
                                    <table id="u_content_text_5" style="font-family:'Montserrat',sans-serif;"
                                           role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
                                        <tbody>
                                        <tr>
                                            <td class="v-container-padding-padding"
                                                style="overflow-wrap:break-word;word-break:break-word;padding:10px 70px 0px;font-family:'Montserrat',sans-serif;"
                                                align="left">

                                                <div class="v-text-align v-line-height"
                                                     style="line-height: 140%; text-align: center; word-wrap: break-word;">
                                                    <p style="font-size: 14px; line-height: 140%;">
                                                        Alle opgaver og tilbud er i henhold til:
                                                        Nordisk Speditørforbunds Almindelige Bestemmelser (NSAB 2015)
                                                        ​ Se vores generelle forretningsbetingelser
                                                        <a href="https://steptransport.dk/wp-content/uploads/2019/09/nsab_2015_dk_pr._16-12-2015.pdf">HER</a>
                                                    </p>
                                                </div>

                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                    <!--social-->
                                    <table style="font-family:'Montserrat',sans-serif;" role="presentation"
                                           cellpadding="0" cellspacing="0" width="100%" border="0">
                                        <tbody>
                                        <tr>
                                            <td class="v-container-padding-padding"
                                                style="max-width: 100% !important;overflow-wrap:break-word;word-break:break-word;padding:10px;font-family:'Montserrat',sans-serif;"
                                                align="left">

                                                <div align="center">
                                                    <div style="display: table; max-width:187px;">
                                                        <!--[if (mso)|(IE)]>
                                                        <table width="187" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td style="border-collapse:collapse;" align="center">
                                                                <table width="100%" cellpadding="0" cellspacing="0"
                                                                       border="0"
                                                                       style="border-collapse:collapse; mso-table-lspace: 0pt;mso-table-rspace: 0pt; width:187px;">
                                                                    <tr><![endif]-->


                                                        <!--[if (mso)|(IE)]>
                                                        <td width="32" style="width:32px; padding-right: 15px;"
                                                            valign="top"><![endif]-->
                                                        <table align="left" border="0" cellspacing="0" cellpadding="0"
                                                               width="32" height="32"
                                                               style="width: 32px !important;height: 32px !important;display: inline-block;border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;margin-right: 15px">
                                                            <tbody>
                                                            <tr style="vertical-align: top">
                                                                <td align="left" valign="middle"
                                                                    style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                                                                    <a href="https://www.facebook.com/www.steptransport.dk/"
                                                                       title="Facebook"
                                                                       target="_blank">
                                                                        <img
                                                                            src="https://my.steptransport.dk/images/goods/signature_fb.png"
                                                                            alt="facebook"
                                                                            title="Facebook" width="32"
                                                                            style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;max-width: 32px !important">
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                        <!--[if (mso)|(IE)]></td><![endif]-->

                                                        <!--[if (mso)|(IE)]>
                                                        <td width="32" style="width:32px; padding-right: 15px;"
                                                            valign="top"><![endif]-->
                                                        <table align="left" border="0" cellspacing="0" cellpadding="0"
                                                               width="32" height="32"
                                                               style="width: 32px !important;height: 32px !important;display: inline-block;border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;margin-right: 15px">
                                                            <tbody>
                                                            <tr style="vertical-align: top">
                                                                <td align="left" valign="middle"
                                                                    style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                                                                    <a href="https://www.linkedin.com/company/step-transport/"
                                                                       title="linkedin"
                                                                       target="_blank">
                                                                        <img
                                                                            src="https://my.steptransport.dk/images/goods/signature_li.png"
                                                                            alt="linkedin"
                                                                            title="Linkedin" width="32"
                                                                            style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;max-width: 32px !important">
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                        <!--[if (mso)|(IE)]></td><![endif]-->

                                                        <!--[if (mso)|(IE)]>
                                                        <td width="32" style="width:32px; padding-right: 15px;"
                                                            valign="top"><![endif]-->
                                                        <table align="left" border="0" cellspacing="0" cellpadding="0"
                                                               width="32" height="32"
                                                               style="width: 32px !important;height: 32px !important;display: inline-block;border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;margin-right: 15px">
                                                            <tbody>
                                                            <tr style="vertical-align: top">
                                                                <td align="left" valign="middle"
                                                                    style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                                                                    <a href="https://twitter.com/transportstep"
                                                                       title="Twitter"
                                                                       target="_blank">
                                                                        <img
                                                                            src="https://my.steptransport.dk/images/goods/signature_tw.png"
                                                                            alt="twitter"
                                                                            title="Twitter" width="32"
                                                                            style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;max-width: 32px !important">
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                        <!--[if (mso)|(IE)]></td><![endif]-->

                                                        <!--[if (mso)|(IE)]>
                                                        <td width="32" style="width:32px; padding-right: 0px;"
                                                            valign="top"><![endif]-->
                                                        <table align="left" border="0" cellspacing="0" cellpadding="0"
                                                               width="32" height="32"
                                                               style="width: 32px !important;height: 32px !important;display: inline-block;border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;margin-right: 0px">
                                                            <tbody>
                                                            <tr style="vertical-align: top">
                                                                <td align="left" valign="middle"
                                                                    style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                                                                    <a href="https://www.instagram.com/steptransport/"
                                                                       title="Instagram"
                                                                       target="_blank">
                                                                        <img
                                                                            src="https://my.steptransport.dk/images/goods/signature_ig.png"
                                                                            alt="instagram"
                                                                            title="Instagram" width="32"
                                                                            style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;max-width: 32px !important">
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                        <!--[if (mso)|(IE)]></td><![endif]-->


                                                        <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->
                                                    </div>
                                                </div>

                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                    <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
                            </div>
                        </div>
                        <!--[if (mso)|(IE)]></td><![endif]-->
                        <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->
                    </div>
                </div>
            </div>

            <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
        </td>
    </tr>
    </tbody>
</table>
<!--[if mso]></div><![endif]-->
<!--[if IE]></div><![endif]-->
</body>

</html>

