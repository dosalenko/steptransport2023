@extends('layouts.app')

@section('content')

 <div class="flex-center position-ref full-height">
    <div class="container-fluide mx-3 px-4 py-4 bg-light rounded-lg">
        <div class="row">
             <agreement-edit url="{{ route('agreement.getSingle',$agreement->id) }}" client="{{$clientData}}"></agreement-edit>
        </div>
    </div>
</div>

@endsection
