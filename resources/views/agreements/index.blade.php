@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <client-data-table url="{{ route('clients.getAll') }}"></client-data-table>
        </div>
    </div>
@endsection
