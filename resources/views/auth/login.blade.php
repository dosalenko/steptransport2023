@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-5">
            <div class="card  mb-5">
                <div class="p-5">
                    <div class="d-flex align-items-center">
                        <img width="32" src="images/steptransport-logo.png" alt="logo">
                        <span class="ml-2 h6 m-0 font-weight-semi-bold">Step Transport</span>
                    </div>
                    <div class="mb-3 mt-2 h1">
                        Welcome
                    </div>
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-12 col-form-label text-md-left pb-0 font-weight-semi-bold h6 mb-0">{{ __('Email Address') }}</label>

                            <div class="col-md-12">
                                <div class="d-flex align-items-center py-1 border rounded div-focus border-2">
                                    <span class="oi oi-person pl-3 text-primary" title="person" aria-hidden="true"></span>
                                      <input id="email" type="email" class="form-control border-0  @error('email') is-invalid @enderror shadow-none" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                </div>
                                 @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-12 col-form-label text-md-left pb-0 font-weight-semi-bold h6 mb-0">{{ __('Password') }}</label>

                            <div class="col-md-12">
                                <div class="d-flex align-items-center py-1 border rounded div-focus border-2">
                                    <span class="oi oi-key pl-3 text-primary" title="key" aria-hidden="true"></span>
                                    <input id="password" placeholder="Your password" type="password" autocomplete="off" class="form-control border-0 @error('password') is-invalid @enderror shadow-none" name="password" required autocomplete="current-password">
                                </div>
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12 mt-2">
                                <button type="submit" class="btn btn-lg btn-primary w-100 font-weight-semi-bold h6 mb-0">
                                    Log in
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
