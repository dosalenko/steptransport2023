@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-5  mb-5">
            <div class="card  mb-5">
                <div class="h4 px-3 mb-0 pt-3 border-bottom pb-3">{{ __('Reset Password') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="row mb-3">
                            <label for="email" class="col-md-12 col-form-label text-md-left pb-0 font-weight-semi-bold h6 mb-0">{{ __('Email Address') }}</label>

                            <div class="col-md-12">

                                <div class="d-flex align-items-center py-1 border rounded div-focus border-2">
                                    <span class="oi oi-ellipses pl-3 text-primary" title="key" aria-hidden="true"></span>
                                     <input id="email" type="email" class="form-control border-0 shadow-none @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                   
                                </div>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary  w-100  font-weight-semi-bold h6 mb-0">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
