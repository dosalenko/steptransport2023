@extends('layouts.communication')

@section('content')
    <table id="u_content_text_1" style="font-family:'Montserrat',sans-serif;"
           role="presentation" cellpadding="0" cellspacing="0" width="100%"
           border="0">
        <tbody>
        <tr>
            <td class="v-container-padding-padding"
                style="overflow-wrap:break-word;word-break:break-word;padding:10px 40px;font-family:'Montserrat',sans-serif;"
                align="left">

                <div class="v-text-align v-line-height"
                     style="line-height: 160%; text-align: left; word-wrap: break-word;">
                    <p style="font-size: 14px; line-height: 160%;">
                    <h2> Hej, {{$trip->booking->dropoffAddress->name}} </h2>

                    Hermed bekræftelse på dine ændrede leveringsinformationer
                    på levering fra {{$trip->client->name}} <br/>
                    @if ($from==='00:00' || $to==='00:00')
                        Vi ankommer d. {{$date}} på
                    @else
                        Vi ankommer d. {{$date}} mellem {{$from}} og {{$to}} på
                    @endif
                    {{$address}}.<br/>
                    Du får kommunikeret et 4-timers interval senest 24 timer før levering.<br/>
                    @if (count($trip->booking->bookingServices)>0)
                        <br/>  <b> Leveringsservice er: </b> <br/>
                        @foreach($trip->booking->bookingServices as $bookingService)
                            {{$bookingService->service->name}} <br/>
                        @endforeach
                        <br/>
                    @endif


                    Hvis du ønsker at ændre leverancen, kan du klikke her:
                    <a href="http://steptransport.youwe.dk/mobile/check-delivery/{{$trip->unique_link}}">LINK</a>
                    <br/> <br/>

                    Mvh <br/>
                    Step Transport
                    </p>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
@endsection
