Hej, {{$trip->booking->dropoffAddress->name}}

Husk din leverance fra {{$trip->client->name}}
Vi ankommer {{$date}}.
Du kan ændre leverancen frem til kl. 12.00 i dag på følgende link:
{{route('site.checkDelivery',$trip->unique_link)}}

Mvh
Step Transport
