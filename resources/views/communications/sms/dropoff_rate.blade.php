Hej, {{$trip->booking->dropoffAddress->name}}

Vi har leveret dine varer fra {{$trip->client->name}}, og vi kunne godt tænke os at høre hvad du tænker om leveringsoplevelsen.
Klik her for at bedømme din oplevelse:
http://my.steptransport.dk/mobile/rate/{{$trip->booking->rate_link}}

Mvh
Step Transport
