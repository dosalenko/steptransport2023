Hej, {{$trip->booking->dropoffAddress->name}}

Vi har afhentet dine varer til {{$trip->booking->dropoffAddress->name}}, og vi kunne godt tænke os at høre hvad du tænker om oplevelsen.
Klik her for at bedømme din oplevelse:
http://my.steptransport.dk/mobile/rate/{{$trip->booking->rate_link}}

Mvh
Step Transport
