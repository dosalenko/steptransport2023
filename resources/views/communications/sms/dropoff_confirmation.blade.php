Hej, {{$trip->booking->dropoffAddress->name}}

Vi har modtaget dine varer fra {{$trip->client->name}}
Din levering vil ske d. {{$date}} @if ($from==='00:00' || $to==='00:00') @else mellem {{$from}} og {{$to}} @endif på {{$trip->address->street}} {{$trip->address->zip}}, {{$trip->address->city}}.<br/>
Du får kommunikeret et 4-timers interval senest 24 timer før levering.<br/>
@if (count($trip->booking->bookingServices)>0)
Leveringsservice er:
@foreach($trip->booking->bookingServices as $bookingService)
{{$bookingService->service->name}}
@endforeach
@endif


Hvis du ønsker at ændre leverancen, kan du klikke her:
{{route('site.checkDelivery',$trip->unique_link)}}
Mvh <br/>
Step Transport
