Hej, {{$trip->booking->dropoffAddress->name}}

Hermed bekræftelse på dine ændrede afhentningsinformationer på afhentning til {{$trip->booking->dropoffAddress->name}}
Vi ankommer d. {{$date}} @if ($from==='00:00' || $to==='00:00') @else mellem {{$from}} og {{$to}} @endif på {{$address}}.
Du får kommunikeret et 4-timers interval senest 24 timer før levering.
@if (count($trip->booking->bookingServices)>0)
Afhentningsservice er:
@foreach($trip->booking->bookingServices as $bookingService)
{{$bookingService->service->name}}
@endforeach
@endif

Hvis du ønsker at ændre leverancen, kan du klikke her:
{{route('site.checkDelivery',$trip->unique_link)}}
Mvh
Step Transport

