@if ($trip->type===3)
    <!--do confirmation-->
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <!--[if gte mso 9]>
        <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
        </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="x-apple-disable-message-reformatting">
        <!--[if !mso]><!-->
        <meta http-equiv="X-UA-Compatible" content="IE=edge"><!--<![endif]-->
        <title></title>

        <style type="text/css">
            @media only screen and (min-width: 620px) {
                .u-row {
                    width: 600px !important;
                }

                .u-row .u-col {
                    vertical-align: top;
                }

                .u-row .u-col-100 {
                    width: 600px !important;
                }

            }

            @media (max-width: 620px) {
                .u-row-container {
                    max-width: 100% !important;
                    padding-left: 0px !important;
                    padding-right: 0px !important;
                }

                .u-row .u-col {
                    min-width: 320px !important;
                    max-width: 100% !important;
                    display: block !important;
                }

                .u-row {
                    width: calc(100% - 40px) !important;
                }

                .u-col {
                    width: 100% !important;
                }

                .u-col > div {
                    margin: 0 auto;
                }
            }

            body {
                margin: 0;
                padding: 0;
            }

            table,
            tr,
            td {
                vertical-align: top;
                border-collapse: collapse;
            }

            p {
                margin: 0;
            }

            .ie-container table,
            .mso-container table {
                table-layout: fixed;
            }

            * {
                line-height: inherit;
            }

            a[x-apple-data-detectors='true'] {
                color: inherit !important;
                text-decoration: none !important;
            }

            @media (max-width: 480px) {
                .hide-mobile {
                    max-height: 0px;
                    overflow: hidden;
                    display: none !important;
                }
            }

            table, td {
                color: #000000;
            }

            #u_body a {
                color: #0000ee;
                text-decoration: underline;
            }

            @media (max-width: 480px) {
                #u_content_image_1 .v-src-width {
                    width: auto !important;
                }

                #u_content_image_1 .v-src-max-width {
                    max-width: 82% !important;
                }

                #u_content_heading_2 .v-container-padding-padding {
                    padding: 6px 10px 10px !important;
                }

                #u_content_heading_2 .v-line-height {
                    line-height: 110% !important;
                }

                #u_content_text_2 .v-container-padding-padding {
                    padding: 40px 20px 10px !important;
                }

                #u_content_text_1 .v-container-padding-padding {
                    padding: 10px 20px !important;
                }

                #u_content_text_1 .v-text-align {
                    text-align: justify !important;
                }

                #u_column_3 .v-col-padding {
                    padding: 50px 0px !important;
                }

                #u_content_text_3 .v-container-padding-padding {
                    padding: 10px 20px !important;
                }

                #u_content_text_3 .v-text-align {
                    text-align: center !important;
                }

                #u_content_text_3 .v-line-height {
                    line-height: 160% !important;
                }

                #u_content_divider_1 .v-container-padding-padding {
                    padding: 20px 10px !important;
                }

                #u_content_menu_1 .v-padding {
                    padding: 5px !important;
                }

                #u_content_text_5 .v-container-padding-padding {
                    padding: 10px 10px 0px !important;
                }

                #u_content_text_5 .v-text-align {
                    text-align: center !important;
                }

                #u_content_text_5 .v-line-height {
                    line-height: 160% !important;
                }
            }
        </style>
        <!--[if !mso]><!-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700&display=swap" rel="stylesheet"
              type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Raleway:400,700&display=swap" rel="stylesheet"
              type="text/css">
        <!--<![endif]-->
    </head>
    <body class="clean-body u_body"
          style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #e7e7e7;color: #000000">
    <!--[if IE]>
    <div class="ie-container"><![endif]-->
    <!--[if mso]>
    <div class="mso-container"><![endif]-->
    <table id="u_body"
           style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: #e7e7e7;width:100%"
           cellpadding="0" cellspacing="0">
        <tbody>
        <tr style="vertical-align: top">
            <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                <!--[if (mso)|(IE)]>
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td align="center" style="background-color: #e7e7e7;"><![endif]-->

                <div class="u-row-container" style="padding: 0px;background-color: transparent">
                    <div class="u-row"
                         style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;">
                        <div
                            style="border-collapse: collapse;display: table;width: 100%;height: 100%;background-color: transparent;">
                            <!--[if (mso)|(IE)]>
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td style="padding: 0px;background-color: transparent;" align="center">
                                    <table cellpadding="0" cellspacing="0" border="0" style="width:600px;">
                                        <tr style="background-color: transparent;"><![endif]-->

                            <!--[if (mso)|(IE)]>
                            <td align="center" width="600" class="v-col-padding"
                                style="background-color: #ffffff;width: 600px;padding: 0px 0px 50px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;"
                                valign="top"><![endif]-->
                            <div class="u-col u-col-100"
                                 style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                                <div style="background-color: #ffffff;height: 100%;width: 100% !important;">
                                    <!--[if (!mso)&(!IE)]><!-->
                                    <div class="v-col-padding"
                                         style="height: 100%; padding: 0px 0px 50px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                                        <!--<![endif]-->

                                        <table id="u_content_text_2" style="font-family:'Montserrat',sans-serif;"
                                               role="presentation" cellpadding="0" cellspacing="0" width="100%"
                                               border="0">
                                            <tbody>
                                            <tr>
                                                <td class="v-container-padding-padding"
                                                    style="overflow-wrap:break-word;word-break:break-word;padding:40px 10px 10px 40px;font-family:'Montserrat',sans-serif;"
                                                    align="left">
                                                    <div class="v-text-align v-line-height"
                                                         style="line-height: 140%; text-align: left; word-wrap: break-word;">
                                                        <p style="font-size: 14px; line-height: 140%;"><strong><span
                                                                    style="font-size: 18px; line-height: 25.2px;">Hej, {{$trip->booking->dropoffAddress->name}}</span></strong>
                                                        </p>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>

                                        <table id="u_content_text_1" style="font-family:'Montserrat',sans-serif;"
                                               role="presentation" cellpadding="0" cellspacing="0" width="100%"
                                               border="0">
                                            <tbody>
                                            <tr>
                                                <td class="v-container-padding-padding"
                                                    style="overflow-wrap:break-word;word-break:break-word;padding:10px 40px;font-family:'Montserrat',sans-serif;"
                                                    align="left">

                                                    <div class="v-text-align v-line-height"
                                                         style="line-height: 160%; text-align: left; word-wrap: break-word;">
                                                        <p style="font-size: 14px; line-height: 160%;">
                                                            Vi har modtaget dine varer fra {{$trip->client->name}} <br/>
                                                            Din levering vil ske d. {{$date}} mellem 08.00 og 16.00 på
                                                            {{$trip->address->street}}  {{$trip->address->zip}}
                                                            , {{$trip->address->city}}.<br/>
                                                            Du får kommunikeret et 4-timers interval senest 24 timer før
                                                            levering.<br/>

                                                            @if (count($trip->booking->bookingServices)>0)
                                                                <br/> <b> Leveringsservice er: </b> <br/>
                                                                @foreach($trip->booking->bookingServices as $bookingService)
                                                                    {{$bookingService->service->name}} <br/>
                                                                @endforeach
                                                                <br/>
                                                            @endif

                                                            Hvis du ønsker at ændre leverancen, kan du klikke her:
                                                            <a href="http://steptransport.youwe.dk/mobile/check-delivery/{{$trip->unique_link}}">LINK</a>
                                                            <br/> <br/>

                                                            Mvh <br/>
                                                            Step Transport
                                                        </p>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
                                </div>
                            </div>
                            <!--[if (mso)|(IE)]></td><![endif]-->
                            <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->
                        </div>
                    </div>
                </div>


                <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
            </td>
        </tr>
        </tbody>
    </table>
    <!--[if mso]></div><![endif]-->
    <!--[if IE]></div><![endif]-->
    </body>
    </html>
    <br/>

    <!--do reconfirmation-->
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <!--[if gte mso 9]>
        <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
        </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="x-apple-disable-message-reformatting">
        <!--[if !mso]><!-->
        <meta http-equiv="X-UA-Compatible" content="IE=edge"><!--<![endif]-->
        <title></title>

        <style type="text/css">
            @media only screen and (min-width: 620px) {
                .u-row {
                    width: 600px !important;
                }

                .u-row .u-col {
                    vertical-align: top;
                }

                .u-row .u-col-100 {
                    width: 600px !important;
                }

            }

            @media (max-width: 620px) {
                .u-row-container {
                    max-width: 100% !important;
                    padding-left: 0px !important;
                    padding-right: 0px !important;
                }

                .u-row .u-col {
                    min-width: 320px !important;
                    max-width: 100% !important;
                    display: block !important;
                }

                .u-row {
                    width: calc(100% - 40px) !important;
                }

                .u-col {
                    width: 100% !important;
                }

                .u-col > div {
                    margin: 0 auto;
                }
            }

            body {
                margin: 0;
                padding: 0;
            }

            table,
            tr,
            td {
                vertical-align: top;
                border-collapse: collapse;
            }

            p {
                margin: 0;
            }

            .ie-container table,
            .mso-container table {
                table-layout: fixed;
            }

            * {
                line-height: inherit;
            }

            a[x-apple-data-detectors='true'] {
                color: inherit !important;
                text-decoration: none !important;
            }

            @media (max-width: 480px) {
                .hide-mobile {
                    max-height: 0px;
                    overflow: hidden;
                    display: none !important;
                }
            }

            table, td {
                color: #000000;
            }

            #u_body a {
                color: #0000ee;
                text-decoration: underline;
            }

            @media (max-width: 480px) {
                #u_content_image_1 .v-src-width {
                    width: auto !important;
                }

                #u_content_image_1 .v-src-max-width {
                    max-width: 82% !important;
                }

                #u_content_heading_2 .v-container-padding-padding {
                    padding: 6px 10px 10px !important;
                }

                #u_content_heading_2 .v-line-height {
                    line-height: 110% !important;
                }

                #u_content_text_2 .v-container-padding-padding {
                    padding: 40px 20px 10px !important;
                }

                #u_content_text_1 .v-container-padding-padding {
                    padding: 10px 20px !important;
                }

                #u_content_text_1 .v-text-align {
                    text-align: justify !important;
                }

                #u_column_3 .v-col-padding {
                    padding: 50px 0px !important;
                }

                #u_content_text_3 .v-container-padding-padding {
                    padding: 10px 20px !important;
                }

                #u_content_text_3 .v-text-align {
                    text-align: center !important;
                }

                #u_content_text_3 .v-line-height {
                    line-height: 160% !important;
                }

                #u_content_divider_1 .v-container-padding-padding {
                    padding: 20px 10px !important;
                }

                #u_content_menu_1 .v-padding {
                    padding: 5px !important;
                }

                #u_content_text_5 .v-container-padding-padding {
                    padding: 10px 10px 0px !important;
                }

                #u_content_text_5 .v-text-align {
                    text-align: center !important;
                }

                #u_content_text_5 .v-line-height {
                    line-height: 160% !important;
                }
            }
        </style>
        <!--[if !mso]><!-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700&display=swap" rel="stylesheet"
              type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Raleway:400,700&display=swap" rel="stylesheet"
              type="text/css">
        <!--<![endif]-->
    </head>
    <body class="clean-body u_body"
          style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #e7e7e7;color: #000000">
    <!--[if IE]>
    <div class="ie-container"><![endif]-->
    <!--[if mso]>
    <div class="mso-container"><![endif]-->
    <table id="u_body"
           style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: #e7e7e7;width:100%"
           cellpadding="0" cellspacing="0">
        <tbody>
        <tr style="vertical-align: top">
            <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                <!--[if (mso)|(IE)]>
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td align="center" style="background-color: #e7e7e7;"><![endif]-->

                <div class="u-row-container" style="padding: 0px;background-color: transparent">
                    <div class="u-row"
                         style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;">
                        <div
                            style="border-collapse: collapse;display: table;width: 100%;height: 100%;background-color: transparent;">
                            <!--[if (mso)|(IE)]>
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td style="padding: 0px;background-color: transparent;" align="center">
                                    <table cellpadding="0" cellspacing="0" border="0" style="width:600px;">
                                        <tr style="background-color: transparent;"><![endif]-->

                            <!--[if (mso)|(IE)]>
                            <td align="center" width="600" class="v-col-padding"
                                style="background-color: #ffffff;width: 600px;padding: 0px 0px 50px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;"
                                valign="top"><![endif]-->
                            <div class="u-col u-col-100"
                                 style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                                <div style="background-color: #ffffff;height: 100%;width: 100% !important;">
                                    <!--[if (!mso)&(!IE)]><!-->
                                    <div class="v-col-padding"
                                         style="height: 100%; padding: 0px 0px 50px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                                        <!--<![endif]-->

                                        <table id="u_content_text_2" style="font-family:'Montserrat',sans-serif;"
                                               role="presentation" cellpadding="0" cellspacing="0" width="100%"
                                               border="0">
                                            <tbody>
                                            <tr>
                                                <td class="v-container-padding-padding"
                                                    style="overflow-wrap:break-word;word-break:break-word;padding:40px 10px 10px 40px;font-family:'Montserrat',sans-serif;"
                                                    align="left">
                                                    <div class="v-text-align v-line-height"
                                                         style="line-height: 140%; text-align: left; word-wrap: break-word;">
                                                        <p style="font-size: 14px; line-height: 140%;"><strong><span
                                                                    style="font-size: 18px; line-height: 25.2px;">Hej, {{$trip->booking->dropoffAddress->name}}</span></strong>
                                                        </p>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>

                                        <table id="u_content_text_1" style="font-family:'Montserrat',sans-serif;"
                                               role="presentation" cellpadding="0" cellspacing="0" width="100%"
                                               border="0">
                                            <tbody>
                                            <tr>
                                                <td class="v-container-padding-padding"
                                                    style="overflow-wrap:break-word;word-break:break-word;padding:10px 40px;font-family:'Montserrat',sans-serif;"
                                                    align="left">

                                                    <div class="v-text-align v-line-height"
                                                         style="line-height: 160%; text-align: left; word-wrap: break-word;">
                                                        <p style="font-size: 14px; line-height: 160%;">
                                                            Hermed bekræftelse på dine ændrede leveringsinformationer
                                                            på levering fra {{$trip->client->name}} <br/>
                                                            Vi ankommer d. {{$date}} mellem 08.00 og 16.00 på
                                                            {{$trip->address->street}}  {{$trip->address->zip}}
                                                            , {{$trip->address->city}}.<br/>
                                                            Du får kommunikeret et 4-timers interval senest 24 timer før
                                                            levering.<br/>

                                                            @if (count($trip->booking->bookingServices)>0)
                                                                <br/>  <b> Leveringsservice er: </b> <br/>
                                                                @foreach($trip->booking->bookingServices as $bookingService)
                                                                    {{$bookingService->service->name}} <br/>
                                                                @endforeach
                                                                <br/>
                                                            @endif


                                                            Hvis du ønsker at ændre leverancen, kan du klikke her:
                                                            <a href="http://steptransport.youwe.dk/mobile/check-delivery/{{$trip->unique_link}}">LINK</a>
                                                            <br/> <br/>

                                                            Mvh <br/>
                                                            Step Transport
                                                        </p>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>

                                        <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
                                </div>
                            </div>
                            <!--[if (mso)|(IE)]></td><![endif]-->
                            <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->
                        </div>
                    </div>
                </div>


                <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
            </td>
        </tr>
        </tbody>
    </table>
    <!--[if mso]></div><![endif]-->
    <!--[if IE]></div><![endif]-->
    </body>
    </html>
    <br/>

    <!--do tomorrow-->
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <!--[if gte mso 9]>
        <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
        </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="x-apple-disable-message-reformatting">
        <!--[if !mso]><!-->
        <meta http-equiv="X-UA-Compatible" content="IE=edge"><!--<![endif]-->
        <title></title>

        <style type="text/css">
            @media only screen and (min-width: 620px) {
                .u-row {
                    width: 600px !important;
                }

                .u-row .u-col {
                    vertical-align: top;
                }

                .u-row .u-col-100 {
                    width: 600px !important;
                }

            }

            @media (max-width: 620px) {
                .u-row-container {
                    max-width: 100% !important;
                    padding-left: 0px !important;
                    padding-right: 0px !important;
                }

                .u-row .u-col {
                    min-width: 320px !important;
                    max-width: 100% !important;
                    display: block !important;
                }

                .u-row {
                    width: calc(100% - 40px) !important;
                }

                .u-col {
                    width: 100% !important;
                }

                .u-col > div {
                    margin: 0 auto;
                }
            }

            body {
                margin: 0;
                padding: 0;
            }

            table,
            tr,
            td {
                vertical-align: top;
                border-collapse: collapse;
            }

            p {
                margin: 0;
            }

            .ie-container table,
            .mso-container table {
                table-layout: fixed;
            }

            * {
                line-height: inherit;
            }

            a[x-apple-data-detectors='true'] {
                color: inherit !important;
                text-decoration: none !important;
            }

            @media (max-width: 480px) {
                .hide-mobile {
                    max-height: 0px;
                    overflow: hidden;
                    display: none !important;
                }
            }

            table, td {
                color: #000000;
            }

            #u_body a {
                color: #0000ee;
                text-decoration: underline;
            }

            @media (max-width: 480px) {
                #u_content_image_1 .v-src-width {
                    width: auto !important;
                }

                #u_content_image_1 .v-src-max-width {
                    max-width: 82% !important;
                }

                #u_content_heading_2 .v-container-padding-padding {
                    padding: 6px 10px 10px !important;
                }

                #u_content_heading_2 .v-line-height {
                    line-height: 110% !important;
                }

                #u_content_text_2 .v-container-padding-padding {
                    padding: 40px 20px 10px !important;
                }

                #u_content_text_1 .v-container-padding-padding {
                    padding: 10px 20px !important;
                }

                #u_content_text_1 .v-text-align {
                    text-align: justify !important;
                }

                #u_column_3 .v-col-padding {
                    padding: 50px 0px !important;
                }

                #u_content_text_3 .v-container-padding-padding {
                    padding: 10px 20px !important;
                }

                #u_content_text_3 .v-text-align {
                    text-align: center !important;
                }

                #u_content_text_3 .v-line-height {
                    line-height: 160% !important;
                }

                #u_content_divider_1 .v-container-padding-padding {
                    padding: 20px 10px !important;
                }

                #u_content_menu_1 .v-padding {
                    padding: 5px !important;
                }

                #u_content_text_5 .v-container-padding-padding {
                    padding: 10px 10px 0px !important;
                }

                #u_content_text_5 .v-text-align {
                    text-align: center !important;
                }

                #u_content_text_5 .v-line-height {
                    line-height: 160% !important;
                }
            }
        </style>
        <!--[if !mso]><!-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700&display=swap" rel="stylesheet"
              type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Raleway:400,700&display=swap" rel="stylesheet"
              type="text/css">
        <!--<![endif]-->
    </head>
    <body class="clean-body u_body"
          style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #e7e7e7;color: #000000">
    <!--[if IE]>
    <div class="ie-container"><![endif]-->
    <!--[if mso]>
    <div class="mso-container"><![endif]-->
    <table id="u_body"
           style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: #e7e7e7;width:100%"
           cellpadding="0" cellspacing="0">
        <tbody>
        <tr style="vertical-align: top">
            <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                <!--[if (mso)|(IE)]>
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td align="center" style="background-color: #e7e7e7;"><![endif]-->

                <div class="u-row-container" style="padding: 0px;background-color: transparent">
                    <div class="u-row"
                         style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;">
                        <div
                            style="border-collapse: collapse;display: table;width: 100%;height: 100%;background-color: transparent;">
                            <!--[if (mso)|(IE)]>
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td style="padding: 0px;background-color: transparent;" align="center">
                                    <table cellpadding="0" cellspacing="0" border="0" style="width:600px;">
                                        <tr style="background-color: transparent;"><![endif]-->

                            <!--[if (mso)|(IE)]>
                            <td align="center" width="600" class="v-col-padding"
                                style="background-color: #ffffff;width: 600px;padding: 0px 0px 50px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;"
                                valign="top"><![endif]-->
                            <div class="u-col u-col-100"
                                 style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                                <div style="background-color: #ffffff;height: 100%;width: 100% !important;">
                                    <!--[if (!mso)&(!IE)]><!-->
                                    <div class="v-col-padding"
                                         style="height: 100%; padding: 0px 0px 50px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                                        <!--<![endif]-->

                                        <table id="u_content_text_2" style="font-family:'Montserrat',sans-serif;"
                                               role="presentation" cellpadding="0" cellspacing="0" width="100%"
                                               border="0">
                                            <tbody>
                                            <tr>
                                                <td class="v-container-padding-padding"
                                                    style="overflow-wrap:break-word;word-break:break-word;padding:40px 10px 10px 40px;font-family:'Montserrat',sans-serif;"
                                                    align="left">
                                                    <div class="v-text-align v-line-height"
                                                         style="line-height: 140%; text-align: left; word-wrap: break-word;">
                                                        <p style="font-size: 14px; line-height: 140%;"><strong><span
                                                                    style="font-size: 18px; line-height: 25.2px;">Hej, {{$trip->booking->dropoffAddress->name}}</span></strong>
                                                        </p>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>

                                        <table id="u_content_text_1" style="font-family:'Montserrat',sans-serif;"
                                               role="presentation" cellpadding="0" cellspacing="0" width="100%"
                                               border="0">
                                            <tbody>
                                            <tr>
                                                <td class="v-container-padding-padding"
                                                    style="overflow-wrap:break-word;word-break:break-word;padding:10px 40px;font-family:'Montserrat',sans-serif;"
                                                    align="left">

                                                    <div class="v-text-align v-line-height"
                                                         style="line-height: 160%; text-align: left; word-wrap: break-word;">
                                                        <p style="font-size: 14px; line-height: 160%;">
                                                            Husk din leverance fra {{$trip->client->name}} <br/>
                                                            Vi ankommer {{$date}} .<br/>
                                                            Du kan ikke længere ændre leverancen.
                                                            <br/> <br/>

                                                            Mvh <br/>
                                                            Step Transport
                                                        </p>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>

                                        <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
                                </div>
                            </div>
                            <!--[if (mso)|(IE)]></td><![endif]-->
                            <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->
                        </div>
                    </div>
                </div>


                <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
            </td>
        </tr>
        </tbody>
    </table>
    <!--[if mso]></div><![endif]-->
    <!--[if IE]></div><![endif]-->
    </body>
    </html>
    <br/>

    <!--do rate-->
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <!--[if gte mso 9]>
        <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
        </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="x-apple-disable-message-reformatting">
        <!--[if !mso]><!-->
        <meta http-equiv="X-UA-Compatible" content="IE=edge"><!--<![endif]-->
        <title></title>

        <style type="text/css">
            @media only screen and (min-width: 620px) {
                .u-row {
                    width: 600px !important;
                }

                .u-row .u-col {
                    vertical-align: top;
                }

                .u-row .u-col-100 {
                    width: 600px !important;
                }

            }

            @media (max-width: 620px) {
                .u-row-container {
                    max-width: 100% !important;
                    padding-left: 0px !important;
                    padding-right: 0px !important;
                }

                .u-row .u-col {
                    min-width: 320px !important;
                    max-width: 100% !important;
                    display: block !important;
                }

                .u-row {
                    width: calc(100% - 40px) !important;
                }

                .u-col {
                    width: 100% !important;
                }

                .u-col > div {
                    margin: 0 auto;
                }
            }

            body {
                margin: 0;
                padding: 0;
            }

            table,
            tr,
            td {
                vertical-align: top;
                border-collapse: collapse;
            }

            p {
                margin: 0;
            }

            .ie-container table,
            .mso-container table {
                table-layout: fixed;
            }

            * {
                line-height: inherit;
            }

            a[x-apple-data-detectors='true'] {
                color: inherit !important;
                text-decoration: none !important;
            }

            @media (max-width: 480px) {
                .hide-mobile {
                    max-height: 0px;
                    overflow: hidden;
                    display: none !important;
                }
            }

            table, td {
                color: #000000;
            }

            #u_body a {
                color: #0000ee;
                text-decoration: underline;
            }

            @media (max-width: 480px) {
                #u_content_image_1 .v-src-width {
                    width: auto !important;
                }

                #u_content_image_1 .v-src-max-width {
                    max-width: 82% !important;
                }

                #u_content_heading_2 .v-container-padding-padding {
                    padding: 6px 10px 10px !important;
                }

                #u_content_heading_2 .v-line-height {
                    line-height: 110% !important;
                }

                #u_content_text_2 .v-container-padding-padding {
                    padding: 40px 20px 10px !important;
                }

                #u_content_text_1 .v-container-padding-padding {
                    padding: 10px 20px !important;
                }

                #u_content_text_1 .v-text-align {
                    text-align: justify !important;
                }

                #u_column_3 .v-col-padding {
                    padding: 50px 0px !important;
                }

                #u_content_text_3 .v-container-padding-padding {
                    padding: 10px 20px !important;
                }

                #u_content_text_3 .v-text-align {
                    text-align: center !important;
                }

                #u_content_text_3 .v-line-height {
                    line-height: 160% !important;
                }

                #u_content_divider_1 .v-container-padding-padding {
                    padding: 20px 10px !important;
                }

                #u_content_menu_1 .v-padding {
                    padding: 5px !important;
                }

                #u_content_text_5 .v-container-padding-padding {
                    padding: 10px 10px 0px !important;
                }

                #u_content_text_5 .v-text-align {
                    text-align: center !important;
                }

                #u_content_text_5 .v-line-height {
                    line-height: 160% !important;
                }
            }
        </style>
        <!--[if !mso]><!-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700&display=swap" rel="stylesheet"
              type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Raleway:400,700&display=swap" rel="stylesheet"
              type="text/css">
        <!--<![endif]-->
    </head>
    <body class="clean-body u_body"
          style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #e7e7e7;color: #000000">
    <!--[if IE]>
    <div class="ie-container"><![endif]-->
    <!--[if mso]>
    <div class="mso-container"><![endif]-->
    <table id="u_body"
           style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: #e7e7e7;width:100%"
           cellpadding="0" cellspacing="0">
        <tbody>
        <tr style="vertical-align: top">
            <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                <!--[if (mso)|(IE)]>
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td align="center" style="background-color: #e7e7e7;"><![endif]-->

                <div class="u-row-container" style="padding: 0px;background-color: transparent">
                    <div class="u-row"
                         style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;">
                        <div
                            style="border-collapse: collapse;display: table;width: 100%;height: 100%;background-color: transparent;">
                            <!--[if (mso)|(IE)]>
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td style="padding: 0px;background-color: transparent;" align="center">
                                    <table cellpadding="0" cellspacing="0" border="0" style="width:600px;">
                                        <tr style="background-color: transparent;"><![endif]-->

                            <!--[if (mso)|(IE)]>
                            <td align="center" width="600" class="v-col-padding"
                                style="background-color: #ffffff;width: 600px;padding: 0px 0px 50px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;"
                                valign="top"><![endif]-->
                            <div class="u-col u-col-100"
                                 style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                                <div style="background-color: #ffffff;height: 100%;width: 100% !important;">
                                    <!--[if (!mso)&(!IE)]><!-->
                                    <div class="v-col-padding"
                                         style="height: 100%; padding: 0px 0px 50px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                                        <!--<![endif]-->

                                        <table id="u_content_text_2" style="font-family:'Montserrat',sans-serif;"
                                               role="presentation" cellpadding="0" cellspacing="0" width="100%"
                                               border="0">
                                            <tbody>
                                            <tr>
                                                <td class="v-container-padding-padding"
                                                    style="overflow-wrap:break-word;word-break:break-word;padding:40px 10px 10px 40px;font-family:'Montserrat',sans-serif;"
                                                    align="left">
                                                    <div class="v-text-align v-line-height"
                                                         style="line-height: 140%; text-align: left; word-wrap: break-word;">
                                                        <p style="font-size: 14px; line-height: 140%;"><strong><span
                                                                    style="font-size: 18px; line-height: 25.2px;">Hej, {{$trip->booking->dropoffAddress->name}}</span></strong>
                                                        </p>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>

                                        <table id="u_content_text_1" style="font-family:'Montserrat',sans-serif;"
                                               role="presentation" cellpadding="0" cellspacing="0" width="100%"
                                               border="0">
                                            <tbody>
                                            <tr>
                                                <td class="v-container-padding-padding"
                                                    style="overflow-wrap:break-word;word-break:break-word;padding:10px 40px;font-family:'Montserrat',sans-serif;"
                                                    align="left">

                                                    <div class="v-text-align v-line-height"
                                                         style="line-height: 160%; text-align: left; word-wrap: break-word;">
                                                        <p style="font-size: 14px; line-height: 160%;">
                                                            Vi har leveret dine varer fra {{$trip->client->name}},
                                                            og vi kunne godt tænke os at høre hvad du tænker om
                                                            leveringsoplevelsen. <br/> <br/>

                                                            Klik her for at bedømme din oplevelse:
                                                            <a href="http://steptransport.youwe.dk/mobile/rate/{{$trip->booking->rate_link}}">LINK</a>
                                                            <br/> <br/>

                                                            Mvh <br/>
                                                            Step Transport
                                                        </p>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>

                                        <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
                                </div>
                            </div>
                            <!--[if (mso)|(IE)]></td><![endif]-->
                            <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->
                        </div>
                    </div>
                </div>


                <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
            </td>
        </tr>
        </tbody>
    </table>
    <!--[if mso]></div><![endif]-->
    <!--[if IE]></div><![endif]-->
    </body>
    </html>

@elseif ($trip->type===3)

@elseif ($trip->type===1)
    <!--pu confirmation-->
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <!--[if gte mso 9]>
        <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
        </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="x-apple-disable-message-reformatting">
        <!--[if !mso]><!-->
        <meta http-equiv="X-UA-Compatible" content="IE=edge"><!--<![endif]-->
        <title></title>

        <style type="text/css">
            @media only screen and (min-width: 620px) {
                .u-row {
                    width: 600px !important;
                }

                .u-row .u-col {
                    vertical-align: top;
                }

                .u-row .u-col-100 {
                    width: 600px !important;
                }

            }

            @media (max-width: 620px) {
                .u-row-container {
                    max-width: 100% !important;
                    padding-left: 0px !important;
                    padding-right: 0px !important;
                }

                .u-row .u-col {
                    min-width: 320px !important;
                    max-width: 100% !important;
                    display: block !important;
                }

                .u-row {
                    width: calc(100% - 40px) !important;
                }

                .u-col {
                    width: 100% !important;
                }

                .u-col > div {
                    margin: 0 auto;
                }
            }

            body {
                margin: 0;
                padding: 0;
            }

            table,
            tr,
            td {
                vertical-align: top;
                border-collapse: collapse;
            }

            p {
                margin: 0;
            }

            .ie-container table,
            .mso-container table {
                table-layout: fixed;
            }

            * {
                line-height: inherit;
            }

            a[x-apple-data-detectors='true'] {
                color: inherit !important;
                text-decoration: none !important;
            }

            @media (max-width: 480px) {
                .hide-mobile {
                    max-height: 0px;
                    overflow: hidden;
                    display: none !important;
                }
            }

            table, td {
                color: #000000;
            }

            #u_body a {
                color: #0000ee;
                text-decoration: underline;
            }

            @media (max-width: 480px) {
                #u_content_image_1 .v-src-width {
                    width: auto !important;
                }

                #u_content_image_1 .v-src-max-width {
                    max-width: 82% !important;
                }

                #u_content_heading_2 .v-container-padding-padding {
                    padding: 6px 10px 10px !important;
                }

                #u_content_heading_2 .v-line-height {
                    line-height: 110% !important;
                }

                #u_content_text_2 .v-container-padding-padding {
                    padding: 40px 20px 10px !important;
                }

                #u_content_text_1 .v-container-padding-padding {
                    padding: 10px 20px !important;
                }

                #u_content_text_1 .v-text-align {
                    text-align: justify !important;
                }

                #u_column_3 .v-col-padding {
                    padding: 50px 0px !important;
                }

                #u_content_text_3 .v-container-padding-padding {
                    padding: 10px 20px !important;
                }

                #u_content_text_3 .v-text-align {
                    text-align: center !important;
                }

                #u_content_text_3 .v-line-height {
                    line-height: 160% !important;
                }

                #u_content_divider_1 .v-container-padding-padding {
                    padding: 20px 10px !important;
                }

                #u_content_menu_1 .v-padding {
                    padding: 5px !important;
                }

                #u_content_text_5 .v-container-padding-padding {
                    padding: 10px 10px 0px !important;
                }

                #u_content_text_5 .v-text-align {
                    text-align: center !important;
                }

                #u_content_text_5 .v-line-height {
                    line-height: 160% !important;
                }
            }
        </style>
        <!--[if !mso]><!-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700&display=swap" rel="stylesheet"
              type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Raleway:400,700&display=swap" rel="stylesheet"
              type="text/css">
        <!--<![endif]-->
    </head>
    <body class="clean-body u_body"
          style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #e7e7e7;color: #000000">
    <!--[if IE]>
    <div class="ie-container"><![endif]-->
    <!--[if mso]>
    <div class="mso-container"><![endif]-->
    <table id="u_body"
           style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: #e7e7e7;width:100%"
           cellpadding="0" cellspacing="0">
        <tbody>
        <tr style="vertical-align: top">
            <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                <!--[if (mso)|(IE)]>
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td align="center" style="background-color: #e7e7e7;"><![endif]-->

                <div class="u-row-container" style="padding: 0px;background-color: transparent">
                    <div class="u-row"
                         style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;">
                        <div
                            style="border-collapse: collapse;display: table;width: 100%;height: 100%;background-color: transparent;">
                            <!--[if (mso)|(IE)]>
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td style="padding: 0px;background-color: transparent;" align="center">
                                    <table cellpadding="0" cellspacing="0" border="0" style="width:600px;">
                                        <tr style="background-color: transparent;"><![endif]-->

                            <!--[if (mso)|(IE)]>
                            <td align="center" width="600" class="v-col-padding"
                                style="background-color: #ffffff;width: 600px;padding: 0px 0px 50px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;"
                                valign="top"><![endif]-->
                            <div class="u-col u-col-100"
                                 style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                                <div style="background-color: #ffffff;height: 100%;width: 100% !important;">
                                    <!--[if (!mso)&(!IE)]><!-->
                                    <div class="v-col-padding"
                                         style="height: 100%; padding: 0px 0px 50px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                                        <!--<![endif]-->

                                        <table id="u_content_text_2" style="font-family:'Montserrat',sans-serif;"
                                               role="presentation" cellpadding="0" cellspacing="0" width="100%"
                                               border="0">
                                            <tbody>
                                            <tr>
                                                <td class="v-container-padding-padding"
                                                    style="overflow-wrap:break-word;word-break:break-word;padding:40px 10px 10px 40px;font-family:'Montserrat',sans-serif;"
                                                    align="left">
                                                    <div class="v-text-align v-line-height"
                                                         style="line-height: 140%; text-align: left; word-wrap: break-word;">
                                                        <p style="font-size: 14px; line-height: 140%;"><strong><span
                                                                    style="font-size: 18px; line-height: 25.2px;">Hej, {{$trip->booking->dropoffAddress->name}}</span></strong>
                                                        </p>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>

                                        <table id="u_content_text_1" style="font-family:'Montserrat',sans-serif;"
                                               role="presentation" cellpadding="0" cellspacing="0" width="100%"
                                               border="0">
                                            <tbody>
                                            <tr>
                                                <td class="v-container-padding-padding"
                                                    style="overflow-wrap:break-word;word-break:break-word;padding:10px 40px;font-family:'Montserrat',sans-serif;"
                                                    align="left">

                                                    <div class="v-text-align v-line-height"
                                                         style="line-height: 160%; text-align: left; word-wrap: break-word;">
                                                        <p style="font-size: 14px; line-height: 160%;">
                                                            Vi har modtaget besked om at afhente din pakke
                                                            til {{$trip->booking->dropoffAddress->name}} <br/>
                                                            Din afhentning vil ske d. {{$date}} mellem 08.00 og 16.00 på
                                                            {{$trip->address->street}}  {{$trip->address->zip}}
                                                            , {{$trip->address->city}}.<br/>
                                                            Du får kommunikeret et 4-timers interval senest 24 timer før
                                                            levering.<br/>

                                                            @if (count($trip->booking->bookingServices)>0)
                                                                <b> Afhentningsservice er: </b> <br/>
                                                                @foreach($trip->booking->bookingServices as $bookingService)
                                                                    {{$bookingService->service->name}} <br/>
                                                                @endforeach
                                                                <br/>
                                                            @endif


                                                            Hvis du ønsker at ændre leverancen, kan du klikke her:
                                                            <a href="http://steptransport.youwe.dk/mobile/check-delivery/{{$trip->unique_link}}">LINK</a>
                                                            <br/> <br/>

                                                            Mvh <br/>
                                                            Step Transport
                                                        </p>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>

                                        <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
                                </div>
                            </div>
                            <!--[if (mso)|(IE)]></td><![endif]-->
                            <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->
                        </div>
                    </div>
                </div>


                <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
            </td>
        </tr>
        </tbody>
    </table>
    <!--[if mso]></div><![endif]-->
    <!--[if IE]></div><![endif]-->
    </body>
    </html>
    <br/>

    <!--pu reconfirmation-->
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <!--[if gte mso 9]>
        <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
        </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="x-apple-disable-message-reformatting">
        <!--[if !mso]><!-->
        <meta http-equiv="X-UA-Compatible" content="IE=edge"><!--<![endif]-->
        <title></title>

        <style type="text/css">
            @media only screen and (min-width: 620px) {
                .u-row {
                    width: 600px !important;
                }

                .u-row .u-col {
                    vertical-align: top;
                }

                .u-row .u-col-100 {
                    width: 600px !important;
                }

            }

            @media (max-width: 620px) {
                .u-row-container {
                    max-width: 100% !important;
                    padding-left: 0px !important;
                    padding-right: 0px !important;
                }

                .u-row .u-col {
                    min-width: 320px !important;
                    max-width: 100% !important;
                    display: block !important;
                }

                .u-row {
                    width: calc(100% - 40px) !important;
                }

                .u-col {
                    width: 100% !important;
                }

                .u-col > div {
                    margin: 0 auto;
                }
            }

            body {
                margin: 0;
                padding: 0;
            }

            table,
            tr,
            td {
                vertical-align: top;
                border-collapse: collapse;
            }

            p {
                margin: 0;
            }

            .ie-container table,
            .mso-container table {
                table-layout: fixed;
            }

            * {
                line-height: inherit;
            }

            a[x-apple-data-detectors='true'] {
                color: inherit !important;
                text-decoration: none !important;
            }

            @media (max-width: 480px) {
                .hide-mobile {
                    max-height: 0px;
                    overflow: hidden;
                    display: none !important;
                }
            }

            table, td {
                color: #000000;
            }

            #u_body a {
                color: #0000ee;
                text-decoration: underline;
            }

            @media (max-width: 480px) {
                #u_content_image_1 .v-src-width {
                    width: auto !important;
                }

                #u_content_image_1 .v-src-max-width {
                    max-width: 82% !important;
                }

                #u_content_heading_2 .v-container-padding-padding {
                    padding: 6px 10px 10px !important;
                }

                #u_content_heading_2 .v-line-height {
                    line-height: 110% !important;
                }

                #u_content_text_2 .v-container-padding-padding {
                    padding: 40px 20px 10px !important;
                }

                #u_content_text_1 .v-container-padding-padding {
                    padding: 10px 20px !important;
                }

                #u_content_text_1 .v-text-align {
                    text-align: justify !important;
                }

                #u_column_3 .v-col-padding {
                    padding: 50px 0px !important;
                }

                #u_content_text_3 .v-container-padding-padding {
                    padding: 10px 20px !important;
                }

                #u_content_text_3 .v-text-align {
                    text-align: center !important;
                }

                #u_content_text_3 .v-line-height {
                    line-height: 160% !important;
                }

                #u_content_divider_1 .v-container-padding-padding {
                    padding: 20px 10px !important;
                }

                #u_content_menu_1 .v-padding {
                    padding: 5px !important;
                }

                #u_content_text_5 .v-container-padding-padding {
                    padding: 10px 10px 0px !important;
                }

                #u_content_text_5 .v-text-align {
                    text-align: center !important;
                }

                #u_content_text_5 .v-line-height {
                    line-height: 160% !important;
                }
            }
        </style>
        <!--[if !mso]><!-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700&display=swap" rel="stylesheet"
              type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Raleway:400,700&display=swap" rel="stylesheet"
              type="text/css">
        <!--<![endif]-->
    </head>
    <body class="clean-body u_body"
          style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #e7e7e7;color: #000000">
    <!--[if IE]>
    <div class="ie-container"><![endif]-->
    <!--[if mso]>
    <div class="mso-container"><![endif]-->
    <table id="u_body"
           style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: #e7e7e7;width:100%"
           cellpadding="0" cellspacing="0">
        <tbody>
        <tr style="vertical-align: top">
            <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                <!--[if (mso)|(IE)]>
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td align="center" style="background-color: #e7e7e7;"><![endif]-->

                <div class="u-row-container" style="padding: 0px;background-color: transparent">
                    <div class="u-row"
                         style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;">
                        <div
                            style="border-collapse: collapse;display: table;width: 100%;height: 100%;background-color: transparent;">
                            <!--[if (mso)|(IE)]>
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td style="padding: 0px;background-color: transparent;" align="center">
                                    <table cellpadding="0" cellspacing="0" border="0" style="width:600px;">
                                        <tr style="background-color: transparent;"><![endif]-->

                            <!--[if (mso)|(IE)]>
                            <td align="center" width="600" class="v-col-padding"
                                style="background-color: #ffffff;width: 600px;padding: 0px 0px 50px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;"
                                valign="top"><![endif]-->
                            <div class="u-col u-col-100"
                                 style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                                <div style="background-color: #ffffff;height: 100%;width: 100% !important;">
                                    <!--[if (!mso)&(!IE)]><!-->
                                    <div class="v-col-padding"
                                         style="height: 100%; padding: 0px 0px 50px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                                        <!--<![endif]-->

                                        <table id="u_content_text_2" style="font-family:'Montserrat',sans-serif;"
                                               role="presentation" cellpadding="0" cellspacing="0" width="100%"
                                               border="0">
                                            <tbody>
                                            <tr>
                                                <td class="v-container-padding-padding"
                                                    style="overflow-wrap:break-word;word-break:break-word;padding:40px 10px 10px 40px;font-family:'Montserrat',sans-serif;"
                                                    align="left">
                                                    <div class="v-text-align v-line-height"
                                                         style="line-height: 140%; text-align: left; word-wrap: break-word;">
                                                        <p style="font-size: 14px; line-height: 140%;"><strong><span
                                                                    style="font-size: 18px; line-height: 25.2px;">Hej, {{$trip->booking->dropoffAddress->name}}</span></strong>
                                                        </p>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>

                                        <table id="u_content_text_1" style="font-family:'Montserrat',sans-serif;"
                                               role="presentation" cellpadding="0" cellspacing="0" width="100%"
                                               border="0">
                                            <tbody>
                                            <tr>
                                                <td class="v-container-padding-padding"
                                                    style="overflow-wrap:break-word;word-break:break-word;padding:10px 40px;font-family:'Montserrat',sans-serif;"
                                                    align="left">

                                                    <div class="v-text-align v-line-height"
                                                         style="line-height: 160%; text-align: left; word-wrap: break-word;">
                                                        <p style="font-size: 14px; line-height: 160%;">
                                                            Hermed bekræftelse på dine ændrede afhentningsinformationer
                                                            på afhentning til {{$trip->booking->dropoffAddress->name}}
                                                            <br/>
                                                            Vi ankommer d. {{$date}} mellem 08.00 og 16.00 på
                                                            {{$trip->address->street}}  {{$trip->address->zip}}
                                                            , {{$trip->address->city}}.<br/>
                                                            Du får kommunikeret et 4-timers interval senest 24 timer før
                                                            levering.<br/>

                                                            @if (count($trip->booking->bookingServices)>0)
                                                                <b> Afhentningsservice er: </b> <br/>
                                                                @foreach($trip->booking->bookingServices as $bookingService)
                                                                    {{$bookingService->service->name}} <br/>
                                                                @endforeach
                                                                <br/>
                                                            @endif

                                                            Hvis du ønsker at ændre leverancen, kan du klikke her:
                                                            <a href="http://steptransport.youwe.dk/mobile/check-delivery/{{$trip->unique_link}}">LINK</a>
                                                            <br/> <br/>

                                                            Mvh <br/>
                                                            Step Transport
                                                        </p>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>

                                        <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
                                </div>
                            </div>
                            <!--[if (mso)|(IE)]></td><![endif]-->
                            <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->
                        </div>
                    </div>
                </div>


                <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
            </td>
        </tr>
        </tbody>
    </table>
    <!--[if mso]></div><![endif]-->
    <!--[if IE]></div><![endif]-->
    </body>
    </html>
    <br/>

    <!--pu tomorrow-->
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <!--[if gte mso 9]>
        <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
        </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="x-apple-disable-message-reformatting">
        <!--[if !mso]><!-->
        <meta http-equiv="X-UA-Compatible" content="IE=edge"><!--<![endif]-->
        <title></title>

        <style type="text/css">
            @media only screen and (min-width: 620px) {
                .u-row {
                    width: 600px !important;
                }

                .u-row .u-col {
                    vertical-align: top;
                }

                .u-row .u-col-100 {
                    width: 600px !important;
                }

            }

            @media (max-width: 620px) {
                .u-row-container {
                    max-width: 100% !important;
                    padding-left: 0px !important;
                    padding-right: 0px !important;
                }

                .u-row .u-col {
                    min-width: 320px !important;
                    max-width: 100% !important;
                    display: block !important;
                }

                .u-row {
                    width: calc(100% - 40px) !important;
                }

                .u-col {
                    width: 100% !important;
                }

                .u-col > div {
                    margin: 0 auto;
                }
            }

            body {
                margin: 0;
                padding: 0;
            }

            table,
            tr,
            td {
                vertical-align: top;
                border-collapse: collapse;
            }

            p {
                margin: 0;
            }

            .ie-container table,
            .mso-container table {
                table-layout: fixed;
            }

            * {
                line-height: inherit;
            }

            a[x-apple-data-detectors='true'] {
                color: inherit !important;
                text-decoration: none !important;
            }

            @media (max-width: 480px) {
                .hide-mobile {
                    max-height: 0px;
                    overflow: hidden;
                    display: none !important;
                }
            }

            table, td {
                color: #000000;
            }

            #u_body a {
                color: #0000ee;
                text-decoration: underline;
            }

            @media (max-width: 480px) {
                #u_content_image_1 .v-src-width {
                    width: auto !important;
                }

                #u_content_image_1 .v-src-max-width {
                    max-width: 82% !important;
                }

                #u_content_heading_2 .v-container-padding-padding {
                    padding: 6px 10px 10px !important;
                }

                #u_content_heading_2 .v-line-height {
                    line-height: 110% !important;
                }

                #u_content_text_2 .v-container-padding-padding {
                    padding: 40px 20px 10px !important;
                }

                #u_content_text_1 .v-container-padding-padding {
                    padding: 10px 20px !important;
                }

                #u_content_text_1 .v-text-align {
                    text-align: justify !important;
                }

                #u_column_3 .v-col-padding {
                    padding: 50px 0px !important;
                }

                #u_content_text_3 .v-container-padding-padding {
                    padding: 10px 20px !important;
                }

                #u_content_text_3 .v-text-align {
                    text-align: center !important;
                }

                #u_content_text_3 .v-line-height {
                    line-height: 160% !important;
                }

                #u_content_divider_1 .v-container-padding-padding {
                    padding: 20px 10px !important;
                }

                #u_content_menu_1 .v-padding {
                    padding: 5px !important;
                }

                #u_content_text_5 .v-container-padding-padding {
                    padding: 10px 10px 0px !important;
                }

                #u_content_text_5 .v-text-align {
                    text-align: center !important;
                }

                #u_content_text_5 .v-line-height {
                    line-height: 160% !important;
                }
            }
        </style>
        <!--[if !mso]><!-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700&display=swap" rel="stylesheet"
              type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Raleway:400,700&display=swap" rel="stylesheet"
              type="text/css">
        <!--<![endif]-->
    </head>
    <body class="clean-body u_body"
          style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #e7e7e7;color: #000000">
    <!--[if IE]>
    <div class="ie-container"><![endif]-->
    <!--[if mso]>
    <div class="mso-container"><![endif]-->
    <table id="u_body"
           style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: #e7e7e7;width:100%"
           cellpadding="0" cellspacing="0">
        <tbody>
        <tr style="vertical-align: top">
            <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                <!--[if (mso)|(IE)]>
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td align="center" style="background-color: #e7e7e7;"><![endif]-->

                <div class="u-row-container" style="padding: 0px;background-color: transparent">
                    <div class="u-row"
                         style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;">
                        <div
                            style="border-collapse: collapse;display: table;width: 100%;height: 100%;background-color: transparent;">
                            <!--[if (mso)|(IE)]>
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td style="padding: 0px;background-color: transparent;" align="center">
                                    <table cellpadding="0" cellspacing="0" border="0" style="width:600px;">
                                        <tr style="background-color: transparent;"><![endif]-->

                            <!--[if (mso)|(IE)]>
                            <td align="center" width="600" class="v-col-padding"
                                style="background-color: #ffffff;width: 600px;padding: 0px 0px 50px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;"
                                valign="top"><![endif]-->
                            <div class="u-col u-col-100"
                                 style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                                <div style="background-color: #ffffff;height: 100%;width: 100% !important;">
                                    <!--[if (!mso)&(!IE)]><!-->
                                    <div class="v-col-padding"
                                         style="height: 100%; padding: 0px 0px 50px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                                        <!--<![endif]-->

                                        <table id="u_content_text_2" style="font-family:'Montserrat',sans-serif;"
                                               role="presentation" cellpadding="0" cellspacing="0" width="100%"
                                               border="0">
                                            <tbody>
                                            <tr>
                                                <td class="v-container-padding-padding"
                                                    style="overflow-wrap:break-word;word-break:break-word;padding:40px 10px 10px 40px;font-family:'Montserrat',sans-serif;"
                                                    align="left">
                                                    <div class="v-text-align v-line-height"
                                                         style="line-height: 140%; text-align: left; word-wrap: break-word;">
                                                        <p style="font-size: 14px; line-height: 140%;"><strong><span
                                                                    style="font-size: 18px; line-height: 25.2px;">Hej, {{$trip->booking->dropoffAddress->name}}</span></strong>
                                                        </p>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>

                                        <table id="u_content_text_1" style="font-family:'Montserrat',sans-serif;"
                                               role="presentation" cellpadding="0" cellspacing="0" width="100%"
                                               border="0">
                                            <tbody>
                                            <tr>
                                                <td class="v-container-padding-padding"
                                                    style="overflow-wrap:break-word;word-break:break-word;padding:10px 40px;font-family:'Montserrat',sans-serif;"
                                                    align="left">

                                                    <div class="v-text-align v-line-height"
                                                         style="line-height: 160%; text-align: left; word-wrap: break-word;">
                                                        <p style="font-size: 14px; line-height: 160%;">
                                                            Husk din afhentning {{$date}} .
                                                            <br/>

                                                            Du kan ikke længere ændre leverancen.
                                                            <br/> <br/>

                                                            Mvh <br/>
                                                            Step Transport
                                                        </p>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>

                                        <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
                                </div>
                            </div>
                            <!--[if (mso)|(IE)]></td><![endif]-->
                            <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->
                        </div>
                    </div>
                </div>


                <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
            </td>
        </tr>
        </tbody>
    </table>
    <!--[if mso]></div><![endif]-->
    <!--[if IE]></div><![endif]-->
    </body>
    </html>
    <br/>

    <!--pu rate-->
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <!--[if gte mso 9]>
        <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
        </xml>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="x-apple-disable-message-reformatting">
        <!--[if !mso]><!-->
        <meta http-equiv="X-UA-Compatible" content="IE=edge"><!--<![endif]-->
        <title></title>

        <style type="text/css">
            @media only screen and (min-width: 620px) {
                .u-row {
                    width: 600px !important;
                }

                .u-row .u-col {
                    vertical-align: top;
                }

                .u-row .u-col-100 {
                    width: 600px !important;
                }

            }

            @media (max-width: 620px) {
                .u-row-container {
                    max-width: 100% !important;
                    padding-left: 0px !important;
                    padding-right: 0px !important;
                }

                .u-row .u-col {
                    min-width: 320px !important;
                    max-width: 100% !important;
                    display: block !important;
                }

                .u-row {
                    width: calc(100% - 40px) !important;
                }

                .u-col {
                    width: 100% !important;
                }

                .u-col > div {
                    margin: 0 auto;
                }
            }

            body {
                margin: 0;
                padding: 0;
            }

            table,
            tr,
            td {
                vertical-align: top;
                border-collapse: collapse;
            }

            p {
                margin: 0;
            }

            .ie-container table,
            .mso-container table {
                table-layout: fixed;
            }

            * {
                line-height: inherit;
            }

            a[x-apple-data-detectors='true'] {
                color: inherit !important;
                text-decoration: none !important;
            }

            @media (max-width: 480px) {
                .hide-mobile {
                    max-height: 0px;
                    overflow: hidden;
                    display: none !important;
                }
            }

            table, td {
                color: #000000;
            }

            #u_body a {
                color: #0000ee;
                text-decoration: underline;
            }

            @media (max-width: 480px) {
                #u_content_image_1 .v-src-width {
                    width: auto !important;
                }

                #u_content_image_1 .v-src-max-width {
                    max-width: 82% !important;
                }

                #u_content_heading_2 .v-container-padding-padding {
                    padding: 6px 10px 10px !important;
                }

                #u_content_heading_2 .v-line-height {
                    line-height: 110% !important;
                }

                #u_content_text_2 .v-container-padding-padding {
                    padding: 40px 20px 10px !important;
                }

                #u_content_text_1 .v-container-padding-padding {
                    padding: 10px 20px !important;
                }

                #u_content_text_1 .v-text-align {
                    text-align: justify !important;
                }

                #u_column_3 .v-col-padding {
                    padding: 50px 0px !important;
                }

                #u_content_text_3 .v-container-padding-padding {
                    padding: 10px 20px !important;
                }

                #u_content_text_3 .v-text-align {
                    text-align: center !important;
                }

                #u_content_text_3 .v-line-height {
                    line-height: 160% !important;
                }

                #u_content_divider_1 .v-container-padding-padding {
                    padding: 20px 10px !important;
                }

                #u_content_menu_1 .v-padding {
                    padding: 5px !important;
                }

                #u_content_text_5 .v-container-padding-padding {
                    padding: 10px 10px 0px !important;
                }

                #u_content_text_5 .v-text-align {
                    text-align: center !important;
                }

                #u_content_text_5 .v-line-height {
                    line-height: 160% !important;
                }
            }
        </style>
        <!--[if !mso]><!-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700&display=swap" rel="stylesheet"
              type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Raleway:400,700&display=swap" rel="stylesheet"
              type="text/css">
        <!--<![endif]-->
    </head>
    <body class="clean-body u_body"
          style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #e7e7e7;color: #000000">
    <!--[if IE]>
    <div class="ie-container"><![endif]-->
    <!--[if mso]>
    <div class="mso-container"><![endif]-->
    <table id="u_body"
           style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: #e7e7e7;width:100%"
           cellpadding="0" cellspacing="0">
        <tbody>
        <tr style="vertical-align: top">
            <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                <!--[if (mso)|(IE)]>
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td align="center" style="background-color: #e7e7e7;"><![endif]-->

                <div class="u-row-container" style="padding: 0px;background-color: transparent">
                    <div class="u-row"
                         style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;">
                        <div
                            style="border-collapse: collapse;display: table;width: 100%;height: 100%;background-color: transparent;">
                            <!--[if (mso)|(IE)]>
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td style="padding: 0px;background-color: transparent;" align="center">
                                    <table cellpadding="0" cellspacing="0" border="0" style="width:600px;">
                                        <tr style="background-color: transparent;"><![endif]-->

                            <!--[if (mso)|(IE)]>
                            <td align="center" width="600" class="v-col-padding"
                                style="background-color: #ffffff;width: 600px;padding: 0px 0px 50px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;"
                                valign="top"><![endif]-->
                            <div class="u-col u-col-100"
                                 style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                                <div style="background-color: #ffffff;height: 100%;width: 100% !important;">
                                    <!--[if (!mso)&(!IE)]><!-->
                                    <div class="v-col-padding"
                                         style="height: 100%; padding: 0px 0px 50px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                                        <!--<![endif]-->

                                        <table id="u_content_text_2" style="font-family:'Montserrat',sans-serif;"
                                               role="presentation" cellpadding="0" cellspacing="0" width="100%"
                                               border="0">
                                            <tbody>
                                            <tr>
                                                <td class="v-container-padding-padding"
                                                    style="overflow-wrap:break-word;word-break:break-word;padding:40px 10px 10px 40px;font-family:'Montserrat',sans-serif;"
                                                    align="left">
                                                    <div class="v-text-align v-line-height"
                                                         style="line-height: 140%; text-align: left; word-wrap: break-word;">
                                                        <p style="font-size: 14px; line-height: 140%;"><strong><span
                                                                    style="font-size: 18px; line-height: 25.2px;">Hej, {{$trip->booking->dropoffAddress->name}}</span></strong>
                                                        </p>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>

                                        <table id="u_content_text_1" style="font-family:'Montserrat',sans-serif;"
                                               role="presentation" cellpadding="0" cellspacing="0" width="100%"
                                               border="0">
                                            <tbody>
                                            <tr>
                                                <td class="v-container-padding-padding"
                                                    style="overflow-wrap:break-word;word-break:break-word;padding:10px 40px;font-family:'Montserrat',sans-serif;"
                                                    align="left">

                                                    <div class="v-text-align v-line-height"
                                                         style="line-height: 160%; text-align: left; word-wrap: break-word;">
                                                        <p style="font-size: 14px; line-height: 160%;">
                                                            Vi har afhentet dine varer
                                                            til {{$trip->booking->dropoffAddress->name}}, og vi kunne
                                                            godt tænke os at høre hvad du tænker om oplevelsen.<br/>
                                                            <br/>

                                                            Klik her for at bedømme din oplevelse:
                                                            <a href="http://steptransport.youwe.dk/mobile/rate/{{$trip->booking->rate_link}}">LINK</a>
                                                            <br/> <br/>

                                                            Mvh <br/>
                                                            Step Transport
                                                        </p>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>

                                        <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
                                </div>
                            </div>
                            <!--[if (mso)|(IE)]></td><![endif]-->
                            <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->
                        </div>
                    </div>
                </div>


                <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
            </td>
        </tr>
        </tbody>
    </table>
    <!--[if mso]></div><![endif]-->
    <!--[if IE]></div><![endif]-->
    </body>
    </html>
@endif




