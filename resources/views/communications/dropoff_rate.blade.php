@extends('layouts.communication')

@section('content')
    <table id="u_content_text_1" style="font-family:'Montserrat',sans-serif;"
           role="presentation" cellpadding="0" cellspacing="0" width="100%"
           border="0">
        <tbody>
        <tr>
            <td class="v-container-padding-padding"
                style="overflow-wrap:break-word;word-break:break-word;padding:10px 40px;font-family:'Montserrat',sans-serif;"
                align="left">

                <div class="v-text-align v-line-height"
                     style="line-height: 160%; text-align: left; word-wrap: break-word;">
                    <p style="font-size: 14px; line-height: 160%;">
                    <h2> Hej, {{$trip->booking->dropoffAddress->name}} </h2>

                    Vi har leveret dine varer fra {{$trip->client->name}}, og vi kunne godt tænke os at høre hvad du tænker om leveringsoplevelsen. <br/> <br/>

                    Klik her for at bedømme din oplevelse:
                    <a href="http://steptransport.youwe.dk/mobile/rate/{{$trip->booking->rate_link}}">LINK</a>
                    <br/> <br/>

                    Mvh <br/>
                    Step Transport
                    </p>
                </div>
            </td>
        </tr>
        </tbody>
    </table>


@endsection
