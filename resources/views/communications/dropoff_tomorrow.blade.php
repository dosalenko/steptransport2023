@extends('layouts.communication')

@section('content')
    <table id="u_content_text_1" style="font-family:'Montserrat',sans-serif;"
           role="presentation" cellpadding="0" cellspacing="0" width="100%"
           border="0">
        <tbody>
        <tr>
            <td class="v-container-padding-padding"
                style="overflow-wrap:break-word;word-break:break-word;padding:10px 40px;font-family:'Montserrat',sans-serif;"
                align="left">

                <div class="v-text-align v-line-height"
                     style="line-height: 160%; text-align: left; word-wrap: break-word;">
                    <div style="font-size: 14px; line-height: 160%;">
                    <h2> Hej, {{$trip->booking->dropoffAddress->name}} </h2>
                    Husk din leverance fra {{$trip->client->name}} <br/>
                    Vi ankommer {{$date}} .<br/>
                    Du kan ændre leverancen frem til kl. 12.00 i dag på følgende link: <br/>
                    {{route('site.checkDelivery',$trip->unique_link)}}
                    <br/> <br/>

                    Mvh <br/>
                    Step Transport
                    </div>
                </div>
            </td>
        </tr>
        </tbody>
    </table>

@endsection
