@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <user-create client="{{$clientData}}"></user-create>
        </div>
    </div>
@endsection
