@extends('layouts.mobile')

@section('content')
    <check-delivery trip-data="{{ json_encode($trip) }}"></check-delivery>
@endsection
