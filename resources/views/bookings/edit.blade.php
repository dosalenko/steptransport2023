@extends('layouts.app')

@section('content')
<div class="flex-center position-ref full-height">
    <div class="container-fluide mx-3 px-4 py-4 bg-light rounded-lg">
        <div class="row">
            <booking-edit url="{{ route('booking.getSingle',$booking->id) }}" role="{{ $userRole }}"></booking-edit>
        </div>
    </div>
</div>
@endsection
