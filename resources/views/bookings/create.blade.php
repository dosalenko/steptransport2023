@extends('layouts.app')

@section('content')
    <div class="flex-center position-ref full-height">
        <div class="container-fluid mx-3 px-4 py-4 bg-light rounded-lg">
            <div class="row">
                <booking-create env="{{app('env')}}" role="{{ $userRole }}">></booking-create>
            </div>
        </div>
    </div>
@endsection
