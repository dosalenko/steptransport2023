@extends('layouts.app')

@section('content')
    <div class="flex-center position-ref full-height">
        <div class="container-fluid mx-3 px-4 py-4 bg-light rounded-lg">
            <div class="row">
                <booking-recalculate-prices></booking-recalculate-prices>
            </div>
        </div>
    </div>
@endsection
