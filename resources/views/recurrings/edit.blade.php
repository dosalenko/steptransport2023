@extends('layouts.app')

@section('content')
    <div class="flex-center position-ref full-height">
        <div class="container-fluide mx-3 px-4 py-4 bg-light rounded-lg">
            <div class="row">
                <recurring-edit url="{{ route('recurring.getSingle',$recurring->id) }}"></recurring-edit>
            </div>
        </div>
    </div>
@endsection
