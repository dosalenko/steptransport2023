@php
    $generator = new Picqer\Barcode\BarcodeGeneratorHTML();
@endphp
    <!DOCTYPE html>
<html>
<head>
    <title> Report </title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
</head>
@foreach($collis as $colli)
    <body>
    <div class="">
        @if ($colli->booking!=null)
            <div class="booking-caption"><h1 style="font-size:48px;">B-{{$colli->booking_id}}</h1></div>
            <div class="reference-caption"><h2>
                    ({{strlen($colli->reference)>=50 ? substr($colli->reference,0,50).'...' : $colli->reference}})</h2>
            </div>
        @endif
        <div class="row">
            <table class="table table-borderless">
                <tbody>
                <tr>
                    <td>
                        <h3>From:</h3>
                        @if ($colli->booking===null || ($colli->booking!=null && $colli->booking->pickupAddress->name==='Step Transport') )
                            &nbsp; {{$colli->client->name}}<br/>
                            &nbsp; {{$colli->client->billing_street}}<br/>
                            &nbsp; {{$colli->client->billing_zip}} {{$colli->client->billing_city}}, Denmark<br/>
                            &nbsp; Denmark
                        @else
                            &nbsp; {{$colli->booking->pickupAddress->name}}<br/>
                            &nbsp; {{$colli->booking->pickupAddress->street}}<br/>
                            &nbsp; {{$colli->booking->pickupAddress->zip}} {{$colli->booking->pickupAddress->city}},
                            Denmark<br/>
                            &nbsp; Denmark
                        @endif
                    </td>
                    <td>
                        <span class="logo">
                          <img src="logo-min.png" width="320" alt="">
                        </span>
                    </td>
                </tr>
                </tbody>
            </table>
            <table class="table table-borderless">
                <tbody>
                <tr>
                    <td colspan="2">
                        <h3>To:</h3>
                        @if ($colli->booking===null)
                            UNKNOWN
                        @else
                            <h3>
                                &nbsp; {{$colli->booking->dropoffAddress->name}}<br/>
                                &nbsp; {{$colli->booking->dropoffAddress->street}}<br/>
                                &nbsp; {{$colli->booking->dropoffAddress->zip}} {{$colli->booking->dropoffAddress->city}}
                                , Denmark
                                <br/>
                                &nbsp; Denmark
                            </h3>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td style="width:50%">
                        <h3> Product Info</h3>
                        &nbsp; Id: {{$colli->id}}<br/>
                        &nbsp;
                        Reference: {{strlen($colli->reference)>=15 ? substr($colli->reference,0,15).'...' : $colli->reference}}
                        <br/>
                        &nbsp; Name: {{$colli->booking_id}}<br/>
                        &nbsp;
                        Weight: {{$colli->weight * \App\Services\Core\Helpers\Helper::getTotalColliBatches($colli->colli_batch)}}
                        <br/>
                        &nbsp;
                        m3: {{$colli->volume * \App\Services\Core\Helpers\Helper::getTotalColliBatches($colli->colli_batch)}}
                        <br/>
                        &nbsp;
                        Colli: {{\App\Services\Core\Helpers\Helper::checkColliStartingFromZero($colli,null,true)}}
                        /{{\App\Services\Core\Helpers\Helper::getTotalColliAmountByColliBatch($colli->colli_batch) }}
                        <br/>
                    </td>
                    <td>
                        <h3>Shipment info</h3>
                        Booking: {{$colli->booking_id!==null ? 'B-'.$colli->booking_id : 'UNKNOWN'}}<br/>
                        Reference: {{$colli->booking_id!==null ? $colli->booking->reference : ''}}<br/>
                        Pickup: {{$colli->pickup_start === null ? null : \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $colli->pickup_start)->format('Y-m-d')}}
                        <br/>
                        Delivery: {{$colli->dropoff_start === null ? null : \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $colli->dropoff_start)->format('Y-m-d')}}
                        <br/>
                    </td>
                </tr>

                <tr>
                    <td style="width:50%">
                        <h3>Product/Service(s)</h3>
                        <small>
                            @if ($colli->booking!==null)
                                {{$colli->booking->product->name}} {{$colli->booking->service_price!==null ? 'with': ''}}
                                @foreach(\App\Models\Service::getServicesNames($colli->booking->service_price) as $service)
                                    {{$service}} <br/>
                                @endforeach
                            @endif
                        </small>
                    </td>
                    <td style="width:50%">
                    </td>
                </tr>
                </tbody>
            </table>
            <span class="barcode">
                {!!$barcode[$colli->id]!!}
            </span>
        </div>
    </div>
    </body>
@endforeach
<style>
    body {
        font-size: 22px;
    }

    .barcode {
        position: absolute;
        bottom: 1%;
    }

    .logo {
        position: relative;
        left: 40px;
        top: 20px;
    }
</style>
</html>
