@php
    $generator = new Picqer\Barcode\BarcodeGeneratorHTML();
@endphp
    <!DOCTYPE html>
<html>
<head>
    <title> Report </title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
</head>
@foreach($collis as $colli)
    <body>
 HEAD
    <div>
        <table class="table table-borderless">
            <tbody>
            <tr>
                <td>
                    <div class="booking-caption">
                        @if(strlen($colli->reference)<=10)
                            <h1 style="font-size:105px;">{{$colli->reference}}</h1>

                        @else
                            <h1 style="font-size:75px;">{{$colli->reference}}</h1>
                        @endif
                    </div>
                    @if ($colli->booking!=null)
                        <div class="reference-caption">
                            @if(strlen($colli->reference)<=10)
                                <h2 style="font-size:105px;">B-{{$colli->booking_id}}</h2>
                            @else
                                <h2 style="font-size:75px;">B-{{$colli->booking_id}}</h2>
                            @endif
                        </div>
                    @endif
                </td>
            </tr>
            </tbody>
        </table>

        <table class="table table-borderless">
            <tbody>
            <tr>
                <td style="width:50%">
                    <h3> From </h3>
                    @if ($colli->booking===null || ($colli->booking!=null && $colli->booking->pickupAddress->name==='Step Transport') )
                        {{$colli->client->name}}<br/>
{{--                        {{$colli->client->billing_street}}<br/>--}}
{{--                        {{$colli->client->billing_zip}} {{$colli->client->billing_city}}, Denmark<br/>--}}
                    @else
                        {{$colli->booking->pickupAddress->name}}<br/>
{{--                        {{$colli->booking->pickupAddress->street}}<br/>--}}
{{--                        {{$colli->booking->pickupAddress->zip}} {{$colli->booking->pickupAddress->city}}, Denmark <br/>--}}
                    @endif
                </td>
                <td style="width:50%">
                    <h3> To </h3>
                    @if ($colli->booking===null)
                        UNKNOWN
                    @else
                        {{$colli->booking->dropoffAddress->name}}<br/>
                        {{$colli->booking->dropoffAddress->street}}<br/>
                        {{$colli->booking->dropoffAddress->zip}} {{$colli->booking->dropoffAddress->city}}, Denmark
                        <br/>
                    @endif
                    <br/>
                    <br/>
                    <br/>
                </td>
            </tr>
            </tbody>
        </table>
        <table class="table table-borderless">
            <tbody>
            <tr>
                <td style="width:50%">
                    <h3> Product Info</h3>
                    Id: {{$colli->id}}<br/>

                    Reference: {{$colli->reference}}
                    <br/>
                    Name: {{$colli->booking_id}}<br/>

                    Weight: {{$colli->weight * \App\Services\Core\Helpers\Helper::getTotalColliBatches($colli->colli_batch)}}
                    <br/>

                    m3: {{$colli->volume * \App\Services\Core\Helpers\Helper::getTotalColliBatches($colli->colli_batch)}}
                    <br/>

                    Colli: {{\App\Services\Core\Helpers\Helper::checkColliStartingFromZero($colli,null,true)}}
                    /{{\App\Services\Core\Helpers\Helper::getTotalColliAmountByColliBatch($colli->colli_batch) }}
                    <br/>
                </td>
                <td style="width:50%">
                        <span class="logo">
                            <img src="logo-min.png" width="320" alt="">
                       </span>
                </td>
            </tr>
            </tbody>
        </table>

        <div class="barcode">
            {!!$barcode[$colli->id]!!}
        </div>

    </div>
    </body>
@endforeach
<style>
    body {
        font-size: 22px;
    }

    .barcode {
        position: absolute;
        bottom: 1%;
    }

    .logo {
        position: relative;
        right: 20px;
        top: 20px;
    }

    table tr {
        border: 0.5px solid black !important;
    }

</style>
</html>
