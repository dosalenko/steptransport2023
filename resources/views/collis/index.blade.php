@extends('layouts.app')

@section('content')
    <div class="flex-center position-ref full-height">
        <div class="container-fluide mx-3 px-4 py-4 bg-light rounded-lg">
            <div class="row">
                <colli-data-table url="{{ route('collis.getAll') }}" role="{{ $userRole }}"></colli-data-table>
            </div>
        </div>
    </div>
@endsection
