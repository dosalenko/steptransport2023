@extends('layouts.app')

@section('title', 'Printers setting')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <form method="POST" action="{{ route('printer-update') }}">
                                    @csrf
                                    @method('PUT')

                                    <div class="form-group row">
                                        <label for="label_printer"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Label printer') }}</label>

                                        <div class="col-md-6">
                                            <select id="label_printer" class="form-control" name="label_printer">
                                                @foreach($printers as $id => $name)
                                                    <option
                                                        value="{{ $id }}" {{ $labelPrinter == $id ? 'selected="selected"' : '' }}>{{ $name }}</option>
                                                @endForeach
                                            </select>

                                            @error('label_printer')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="delivery_note_printer"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Tour list printer') }}</label>

                                        <div class="col-md-6">
                                            <select id="delivery_note_printer" class="form-control"
                                                    name="delivery_note_printer">
                                                @foreach($printers as $id => $name)
                                                    <option
                                                        value="{{ $id }}" {{ $tourListPrinter == $id ? 'selected="selected"' : '' }}>{{ $name }}</option>
                                                @endForeach
                                            </select>

                                            @error('delivery_note_printer')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="col-md-8 offset-md-4">
                                            <button type="submit" class="btn btn-primary">
                                                {{ __('Save') }}
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
