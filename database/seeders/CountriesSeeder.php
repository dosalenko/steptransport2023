<?php

namespace Database\Seeders;

use App\Models\Country;
use Illuminate\Database\Seeder;

class CountriesSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $countries = [
            [
                'name' => 'Denmark',
            ],
            [
                'name' => 'Sweden',
            ],
            [
                'name' => 'Germany',
            ],
        ];
        foreach ($countries as $v) {
            Country::create($v);
        }
    }
}
