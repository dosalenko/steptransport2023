<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexesNew extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->index('status');
            $table->index('pickup_start');
            $table->index('pickup_end');
            $table->index('dropoff_start');
            $table->index('dropoff_end');
            $table->index('created_at');
            $table->index('updated_at');
            $table->index('deleted_at');
            $table->index('created_by');
        });

        Schema::table('trips', function (Blueprint $table) {
            $table->index('tw_start');
            $table->index('tw_end');
            $table->index('created_at');
            $table->index('updated_at');
            $table->index('deleted_at');
            $table->index('is_prefixed_order');
        });

        Schema::table('colli', function (Blueprint $table) {
            $table->index('volume');
            $table->index('weight');
            $table->index('created_at');
            $table->index('updated_at');
            $table->index('received_at');
            $table->index('colli_packing_status');
            $table->index('created_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropIndex('status');
            $table->dropIndex('pickup_start');
            $table->dropIndex('pickup_end');
            $table->dropIndex('dropoff_start');
            $table->dropIndex('dropoff_end');
            $table->dropIndex('created_at');
            $table->dropIndex('updated_at');
            $table->dropIndex('deleted_at');
            $table->dropIndex('created_by');
        });

        Schema::table('trips', function (Blueprint $table) {
            $table->dropIndex('tw_start');
            $table->dropIndex('tw_end');
            $table->dropIndex('created_at');
            $table->dropIndex('updated_at');
            $table->dropIndex('deleted_at');
            $table->dropIndex('is_prefixed_order');
        });

        Schema::table('colli', function (Blueprint $table) {
            $table->dropIndex('volume');
            $table->dropIndex('weight');
            $table->dropIndex('created_at');
            $table->dropIndex('updated_at');
            $table->dropIndex('received_at');
            $table->dropIndex('colli_packing_status');
            $table->dropIndex('created_by');
        });
    }
}
