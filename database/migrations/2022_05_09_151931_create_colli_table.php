<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateColliTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('colli', function (Blueprint $table) {
            $table->id();
            $table->integer('booking_id')->nullable();
            $table->unsignedBigInteger('client_id');
            $table->unsignedBigInteger('agreement_id');
            $table->string('reference');
            $table->string('name');
            $table->integer('colli');
            $table->float('volume');
            $table->float('weight');
            $table->tinyInteger('status');
            $table->timestamps();
            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('agreement_id')->references('id')->on('agreements');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('colli');
    }
}
