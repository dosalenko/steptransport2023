<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingServicesTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $bookings = \App\Models\Booking::query()->get();
        foreach ($bookings as $booking) {
            if ($booking->service_price) {
                $services = json_decode($booking->service_price, true);
                if (!empty($services)) {
                    foreach ($services as $k => $service) {
                        $bookingServiceData = [
                            'booking_id' => $booking->id,
                            'service_id' => $k,
                            'price' => $service
                        ];
                        \App\Models\BookingService::create($bookingServiceData);
                    }
                }
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
