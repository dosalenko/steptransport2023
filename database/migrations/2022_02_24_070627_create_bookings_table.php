<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('status');
            $table->unsignedBigInteger('client_id');
            $table->unsignedBigInteger('agreement_id');
            $table->unsignedBigInteger('pickup_address');
            $table->dateTime('pickup_start');
            $table->dateTime('pickup_end');
            $table->string('pickup_note');
            $table->unsignedBigInteger('dropoff_address');
            $table->dateTime('dropoff_start');
            $table->dateTime('dropoff_end');
            $table->string('dropoff_note');
            $table->string('note');
            $table->string('reference')->nullable();
            $table->unsignedBigInteger('agreement_product_id');
            $table->float('product_price');
            $table->string('agreement_services_ids');
            $table->float('service_price');
            $table->integer('linehaul_type');
            $table->float('total_weight');
            $table->float('total_volume');
            $table->float('total_colli');
            $table->float('total_price');
            $table->timestamps();

            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('agreement_id')->references('id')->on('agreements');
            $table->foreign('pickup_address')->references('id')->on('addresses');
            $table->foreign('dropoff_address')->references('id')->on('addresses');
            $table->foreign('agreement_product_id')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
