<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgreementsProductsPrices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agreements_products_prices', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('agreement_interval_id');
            $table->unsignedBigInteger('agreement_product_id');
            $table->float('price')->default(0);
            $table->timestamps();

            $table->foreign('agreement_interval_id')->references('id')->on('agreements_intervals');
            $table->foreign('agreement_product_id')->references('id')->on('agreements_products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agreements_products_prices');
    }
}
