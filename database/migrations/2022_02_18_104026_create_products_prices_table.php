<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_prices', function (Blueprint $table) {
            $table->id();
            $table->float('standard_price')->default(0);
            $table->float('minimum_price')->default(0);
            $table->float('cost_price')->default(0);
            $table->unsignedBigInteger('interval_id');
            $table->unsignedBigInteger('product_id');
            $table->timestamps();

            $table->foreign('interval_id')->references('id')->on('intervals');
            $table->foreign('product_id')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_prices');
    }
}
