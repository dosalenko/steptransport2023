<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateStepAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('step_addresses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nickname');
            $table->string('name');
            $table->string('street');
            $table->string('zip');
            $table->string('city');
            $table->unsignedBigInteger('country_id');
            $table->timestamps();
        });

        DB::table('step_addresses')->insert([
            'nickname' => 'East',
            'name' => 'Step Transport',
            'street' => 'Priorparken 851',
            'zip' => '2605',
            'city' => 'Brøndby',
            'country_id' => 1,
        ]);

        DB::table('step_addresses')->insert([
            'nickname' => 'West',
            'name' => 'Step Tørring',
            'street' => 'Erhvervsparken 3',
            'zip' => '7160',
            'city' => 'Tørring',
            'country_id' => 1,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('step_addresses');
    }
}
