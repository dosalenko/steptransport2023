<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class NullableBookingDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->dateTime('pickup_start')->nullable()->change();
            $table->dateTime('pickup_end')->nullable()->change();
            $table->dateTime('dropoff_start')->nullable()->change();
            $table->dateTime('dropoff_end')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->dateTime('pickup_start')->newTrips->change();
            $table->dateTime('pickup_end')->newTrips->change();
            $table->dateTime('dropoff_start')->newTrips->change();
            $table->dateTime('dropoff_end')->newTrips->change();
        });
    }
}
