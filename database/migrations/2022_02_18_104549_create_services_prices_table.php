<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services_prices', function (Blueprint $table) {
            $table->id();
            $table->float('standard_addon_price')->default(0);
            $table->float('minimum_price')->default(0);
            $table->float('cost_price')->default(0);
            $table->unsignedBigInteger('interval_id');
            $table->unsignedBigInteger('service_id');
            $table->timestamps();

            $table->foreign('interval_id')->references('id')->on('intervals');
            $table->foreign('service_id')->references('id')->on('services');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services_prices');
    }
}
