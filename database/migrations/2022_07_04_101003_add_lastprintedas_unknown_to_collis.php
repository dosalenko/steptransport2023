<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLastprintedasUnknownToCollis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('colli', function (Blueprint $table) {
            $table->integer('lastprintedas_unknown')->integer()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('collis', function (Blueprint $table) {
            $table->dropColumn('lastprintedas_unknown');
        });
    }
}
