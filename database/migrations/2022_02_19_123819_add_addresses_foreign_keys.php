<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAddressesForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->foreign('billing_country_id')->references('id')->on('addresses_countries');
        });

        Schema::table('addresses', function (Blueprint $table) {
            $table->foreign('country_id')->references('id')->on('addresses_countries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->dropForeign(['billing_country_id']);
        });

        Schema::table('addresses', function (Blueprint $table) {
            $table->dropForeign(['country_id']);
        });
    }
}
