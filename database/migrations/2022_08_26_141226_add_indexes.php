<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('colli', function (Blueprint $table) {
            $table->index('booking_id');
            $table->index('location_id');
            $table->index('reference');
            $table->index('status');
            $table->index('colli');
            $table->index('colli_batch');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('colli', function (Blueprint $table) {
            $table->dropIndex('booking_id');
            $table->dropIndex('location_id');
            $table->dropIndex('reference');
            $table->dropIndex('status');
            $table->dropIndex('colli');
            $table->dropIndex('colli_batch');
        });
    }
}
