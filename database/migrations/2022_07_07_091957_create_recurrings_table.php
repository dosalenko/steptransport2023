<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecurringsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recurrings', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->unsignedBigInteger('client_id');
            $table->unsignedBigInteger('agreement_id');
            $table->unsignedBigInteger('driver_id');
            $table->text('recurring_dates');
            $table->float('total_price')->default(0);
            $table->unsignedBigInteger('agreement_product_id');
            $table->string('agreement_services_ids');
            $table->string('reference')->nullable();
            $table->string('internal_comment')->nullable();
            $table->text('stops')->nullable();
            $table->timestamps();

            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('agreement_id')->references('id')->on('agreements');
            $table->foreign('driver_id')->references('id')->on('drivers');
            $table->foreign('agreement_product_id')->references('id')->on('agreements_products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recurrings');
    }
}
