<?php

use App\Http\Controllers\AgreementController;
use App\Http\Controllers\Api\v1\ApiController;
use App\Http\Controllers\BookingController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\ColliController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\DeliveryController;
use App\Http\Controllers\DriverController;
use App\Http\Controllers\IntervalController;
use App\Http\Controllers\LocationController;
use App\Http\Controllers\OptimorouteController;
use App\Http\Controllers\PrinterController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\RecurringController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\SettingsController;
use App\Http\Controllers\SiteController;
use App\Http\Controllers\TestController;
use App\Http\Controllers\TripController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\WmsController;
use App\Models\UserRole;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::middleware('auth:api')->group(function () {
Route::group(['prefix' => '/v1', 'namespace' => 'Api\v1'], function () {
    Route::post('create_booking', [ApiController::class, 'store'])->name('api.booking_create');
    Route::get('get_booking', [ApiController::class, 'get'])->name('api.booking_get');
});
//});

Auth::routes(['register' => false]);

Route::get('/', function () {
    return view('welcome');
});


Route::middleware(['auth'])->group(function () {
    Route::get('/', [SiteController::class, 'index'])->name('site');
    Route::get('/home', [SiteController::class, 'index'])->name('site');

    Route::get('/settings/download-file/{file}', [SettingsController::class, 'downloadFile'])
        ->name('settings.downloadFile');
    /*
     * clients/contacts
     */
    Route::middleware(['role:' . UserRole::ROLE_ADMIN])->group(function () {
        Route::get('/recalculate-prices', [BookingController::class, 'recalculatePrices'])
            ->name('booking.recalculatePrices');
        Route::post('/get-recalculated-prices', [BookingController::class, 'getRecalculatedPrices'])
            ->name('booking.getRecalculatedPrices');
        Route::post('/store-recalculated-prices', [BookingController::class, 'storeRecalculatedPrices'])
            ->name('booking.storeRecalculatedPrices');


        Route::resource('clients', 'App\Http\Controllers\ClientController', ['except' => ['show']]);
        Route::post('/client/store-api-key', [ClientController::class, 'storeApiKey'])->name('client.storeApiKey');
        Route::patch('/client/update-api-key', [ClientController::class, 'updateApiKey'])->name('client.updateApiKey');
        Route::delete('/client/delete-api-key/{id}', [ClientController::class, 'deleteApiKey'])
            ->name('client.deleteApiKey');
    });
    Route::get('/client-list', [ClientController::class, 'getClients'])->name('clients.getAll');
    Route::get('/client-single/{id}', [ClientController::class, 'getSingle'])->name('client.getSingle');
    Route::get('/client-addresses/{id}', [ClientController::class, 'getClientAddresses'])
        ->name('client.getClientAddresses');
    Route::get('/client-agreements/{id}', [ClientController::class, 'getClientAgreements'])
        ->name('client.getClientAgreements');

    Route::middleware(['role:' . UserRole::ROLE_ADMIN])->group(function () {
        Route::resource('agreements', 'App\Http\Controllers\AgreementController', ['except' => ['create', 'show']]);
        Route::get('/agreement-list', [AgreementController::class, 'getAgreements'])->name('agreement.getAll');
        Route::get('/agreement-single/{id}', [AgreementController::class, 'getSingle'])->name('agreement.getSingle');
        Route::get('/agreements/create/{client_id}', [AgreementController::class, 'create'])->name('agreements.create');
        Route::get('/check-primary-agreement/{id}', [AgreementController::class, 'checkPrimaryAgreement'])
            ->name('agreement.checkPrimaryAgreement');
    });

    /*
     * users
     */
    Route::middleware(['role:' . UserRole::ROLE_ADMIN])->group(function () {
        Route::get('/users/create/{client_id}', [UserController::class, 'create'])->name('users.create');
        Route::post('/users', [UserController::class, 'store'])->name('users.store');
        Route::patch('/users', [UserController::class, 'update'])->name('users.update');
        Route::delete('/users/{id}', [UserController::class, 'destroy'])->name('users.destroy');
    });

    /*
      * contacts
    */
    Route::middleware(['role:' . UserRole::ROLE_ADMIN])->group(function () {
        Route::get('/contacts/create/{client_id}', [ContactController::class, 'create'])->name('contacts.create');
        Route::post('/contacts', [ContactController::class, 'store'])->name('contacts.store');
        Route::patch('/contacts', [ContactController::class, 'update'])->name('contacts.update');
    });

    /*
     * products/services
     */
    Route::middleware(['role:' . UserRole::ROLE_ADMIN])->group(function () {
        Route::resource('products', 'App\Http\Controllers\ProductController', ['except' => ['update']]);
        Route::patch('/products', [ProductController::class, 'update'])->name('products.update');
    });
    Route::get('/product-list/{agreementId}', [ProductController::class, 'getProducts'])->name('products.getAll');

    Route::middleware(['role:' . UserRole::ROLE_ADMIN])->group(function () {
        Route::resource('services', 'App\Http\Controllers\ServiceController', ['except' => ['update', 'index']]);
        Route::patch('/services', [ServiceController::class, 'update'])->name('services.update');
    });
    Route::get('/service-list/{agreementId}', [ServiceController::class, 'getServices'])->name('services.getAll');

    Route::get('/product-prices', [ProductController::class, 'getProductPrices'])->name('products.getProductPrices');
    Route::get(
        '/product-price-interval/{id}/{totalVolume}/{totalWeight}/{type}/{agreementId}',
        [ProductController::class, 'getProductPriceByInterval']
    )
        ->name('products.getProductPriceByInterval');
    Route::get('/service-prices', [ServiceController::class, 'getServicePrices'])->name('products.getServicePrices');
    Route::patch('/save-prices', [ProductController::class, 'savePrices'])->name('products.savePrices');

    Route::middleware(['auth', 'role:' . UserRole::ROLE_ADMIN])->group(function () {
        Route::patch('/save-agreement-products/{id}', [ProductController::class, 'saveAgreementProducts'])
            ->name('products.saveAgreementProducts');
        Route::patch('/save-agreement-services/{id}', [ServiceController::class, 'saveAgreementServices'])
            ->name('products.saveAgreementServices');

        Route::patch('/save-agreement-products-prices/{id}', [ProductController::class, 'saveAgreementProductsPrices'])
            ->name('products.saveAgreementProductsPrices');
        Route::patch('/save-agreement-services-prices/{id}', [ProductController::class, 'saveAgreementServicesPrices'])
            ->name('products.saveAgreementServicesPrices');
    });

    Route::get('/agreement-products/{id}', [ProductController::class, 'getAgreementProducts'])
        ->name('products.getAgreementProducts');
    Route::get('/agreement-services/{id}', [ServiceController::class, 'getAgreementServices'])
        ->name('products.getAgreementServices');

    Route::get('/intervals-list/{id}', [IntervalController::class, 'getIntervals'])->name('intervals.getAll');
    Route::post('/new-interval/{id}', [IntervalController::class, 'addNewInterval'])->name('intervals.addNewInterval');
    Route::delete('/interval/{id}/{agreement_id}', [IntervalController::class, 'removeInterval'])
        ->name('intervals.removeInterval');


    /*
    * bookings
    */
    Route::resource('bookings', 'App\Http\Controllers\BookingController');
    Route::get('/booking-list/{excludeDelivered?}', [BookingController::class, 'getBookings'])->name('bookings.getAll');
    Route::get('/booking-list-by-client/{client}/{excludeDelivered}', [BookingController::class, 'getBookingsClient'])
        ->name('bookings.getBookingsClient');
    Route::get('/booking-list-by-colli/{colli}', [BookingController::class, 'getBookingsListByColli'])
        ->name('bookings.getBookingsListByColli');
    Route::get('/booking-statuses-counter/{role}', [BookingController::class, 'statusesCounter'])
        ->name('bookings.statusesCounter');
    Route::get('/booking-null-hours-counter/{role}', [BookingController::class, 'nullHoursCounter'])
        ->name('bookings.nullHoursCounter');
    Route::post('/booking-confirm/{id}', [BookingController::class, 'confirm'])->name('booking.confirm');
    Route::post('/bookings/update-services/{id}', [BookingController::class, 'updateServices'])
        ->name('booking.updateServices');
    Route::get('/booking-error-notify/{message}/{client}', [BookingController::class, 'errorNotify'])
        ->name('booking.errorNotify');
    Route::get('/booking-single/{id}', [BookingController::class, 'getSingle'])->name('booking.getSingle');
    Route::post('/booking-check-exchange-service', [BookingController::class, 'checkExchangeService'])
        ->name('booking.checkExchangeService');
    Route::get('/linehaul-list', [BookingController::class, 'getLinehauls'])->name('booking.getLinehauls');
    Route::get('/get-city-by-zip/{zip}', [BookingController::class, 'getCityByZip'])->name('booking.getCityByZip');
    Route::get(
        '/colli/check-client-reference/{clientId}/{reference}',
        [BookingController::class, 'checkClientReference']
    )->name('booking.checkClientReference');

    /*
     * Routes
     */
    Route::resource('routes', 'App\Http\Controllers\RouteController', ['except' => ['edit', 'show', 'destroy']]);
    Route::resource('my-routes', 'App\Http\Controllers\RouteController', ['except' => ['edit', 'show', 'destroy']]);
    Route::get('/route-list', [App\Http\Controllers\RouteController::class, 'getRoutes'])->name('routes.getAll');
    Route::delete('/route/{id}', [App\Http\Controllers\RouteController::class, 'destroy'])->name('route.delete');
    /*
    * Colli
    */
    Route::resource('collis', 'App\Http\Controllers\ColliController');
    Route::post('/colli-list-by-reference', [ColliController::class, 'getCollisByReference'])
        ->name('collis.getCollisByReference');
    Route::post('/colli-list-by-colli-batch', [ColliController::class, 'getCollisByColliBatch'])
        ->name('collis.getCollisByColliBatch');
    Route::post('/delete-multiple-collis', [ColliController::class, 'deleteMultipleCollis'])
        ->name('colli.deleteMultipleCollis');
    Route::post('/update-multiple-collis', [ColliController::class, 'updateMultipleCollis'])
        ->name('colli.updateMultipleCollis');
    Route::get('/colli-list/{showAll?}', [ColliController::class, 'getCollis'])->name('collis.getAll');
    Route::post(
        '/colli/print-labels/{isForClients?}/{warehouseId?}/{isPrintByBooking?}',
        [ColliController::class, 'printLabels']
    )->name('colli.printLabels');
    Route::post('/colli/send-received-collis', [ColliController::class, 'sendReceivedCollis'])
        ->name('colli.sendReceivedCollis');
    Route::get('/colli/send-received-collis', [ColliController::class, 'sendReceivedCollis'])
        ->name('colli.sendReceivedCollis');
    Route::get('/colli/received-list', [ColliController::class, 'getReceivedCollis'])->name('colli.getReceivedCollis');
    Route::get('/colli/log/{batch}', [ColliController::class, 'getLog'])->name('colli.getLog');
    Route::post('/colli/store-image', [ColliController::class, 'storeImage'])->name('colli.storeImage');
    Route::get('/colli/images/{batch}', [ColliController::class, 'getImages'])
        ->name('collis.getImages');

    /*
    * location management
    */
    Route::get('/location-list', [LocationController::class, 'getLocations'])->name('locations.getAll');
    Route::get('/location-list-select', [LocationController::class, 'getLocationsSelect'])
        ->name('locations.getLocationsSelect');
    Route::get('/warehouse-list', [LocationController::class, 'getWarehouses'])->name('location.getWarehouses');

    Route::middleware(['role:' . UserRole::ROLE_ADMIN])->group(function () {

        /*
         * Colli
         */
        Route::post('/delete-multiple-collis', [ColliController::class, 'deleteMultipleCollis'])
            ->name('colli.deleteMultipleCollis');

        /*
         * Trips
         */
        Route::resource('trips', 'App\Http\Controllers\TripController');
        Route::get('/trip-list', [TripController::class, 'getTrips'])->name('trips.getAll');
        Route::get('/trip-single/{id}', [TripController::class, 'getSingle'])->name('trip.getSingle');
        Route::get('/trip-statuses-counter', [TripController::class, 'statusesCounter'])->name('trip.statusesCounter');
        Route::get('/trip-dimensions-sum', [TripController::class, 'getDimensionsSum'])->name('trip.getDimensionsSum');
        Route::get('/trip-drivers/{id}', [TripController::class, 'getDrivers'])->name('trip.getDrivers');
        Route::post('/set-trip-delivered/{id}/{type}/{date}', [TripController::class, 'setTripDelivered'])
            ->name('trip.setTripDelivered');
        Route::post('/update-dropoff-date/{id}/{date}', [TripController::class, 'updateDropoffDate'])
            ->name('trip.updateDropoffDate');

        Route::get('/booking-null-hours-counter/{role}', [BookingController::class, 'nullHoursCounter'])
            ->name('bookings.nullHoursCounter');
    });

    Route::middleware(['role:' . UserRole::ROLE_ADMIN])->group(function () {
        /*
         * settings
         */
        Route::get('/settings/reporting', [SettingsController::class, 'reporting'])->name('settings.index');
        Route::post('/settings/export-booking/{dateFrom}/{dateTo}', [SettingsController::class, 'exportBookings'])
            ->name('settings.exportBookings');
        Route::get('/settings/export-drivers', [SettingsController::class, 'exportDrivers'])
            ->name('settings.exportDrivers');
        Route::post('/settings/export-drivers', [SettingsController::class, 'exportDrivers'])
            ->name('settings.exportDrivers');
        Route::post('/settings/export-trips', [SettingsController::class, 'exportTrips'])->name('settings.exportTrips');

        Route::get('/settings/step-addresses', [SettingsController::class, 'stepAddresses'])
            ->name('settings.stepAddresses');
        Route::get('/settings/new-step-address', [SettingsController::class, 'newStepAddress'])
            ->name('settings.newStepAddress');
        Route::get('/settings/get-step-addresses', [SettingsController::class, 'getStepAddresses'])
            ->name('settings.getStepAddresses');
        Route::patch('/settings/update-step-addresses', [SettingsController::class, 'updateStepAddresses'])
            ->name('settings.updateStepAddresses');
        Route::post('/settings/store-step-address', [SettingsController::class, 'storeStepAddress'])
            ->name('settings.storeStepAddress');
        Route::get('/settings/locked-products', [SettingsController::class, 'showLockedProducts'])
            ->name('settings.showLockedProducts');
        Route::get('/settings/get-locked-products', [SettingsController::class, 'getLockedProducts'])
            ->name('settings.getLockedProducts');
        Route::post('/settings/locked-products', [SettingsController::class, 'updateLockedProducts'])
            ->name('settings.updateLockedProducts');

        Route::get('/settings/assets', [SettingsController::class, 'getPicklist'])->name('settings.getPicklist');
        Route::get('/settings/links', [SettingsController::class, 'getLinks'])->name('settings.getLinks');
        Route::get('/communication-log', [SettingsController::class, 'communicationLog'])->name('settings.communicationLog');
        Route::get('/communication-log/{bookingId}', [SettingsController::class, 'showCommunicationLog'])->name('settings.showCommunicationLog');

        /*
         * wms management
         */
        Route::get('/mobile/receive', [WmsController::class, 'receiveGoodsMobile'])->name('mobile.receiveGoods');

        Route::get('/settings/receive', [WmsController::class, 'receiveGoods'])->name('settings.receiveGoods');
        Route::get('/receive-colli', [WmsController::class, 'receiveColli'])->name('colli.receiveColli');
        Route::post('/receive-colli/{warehouse}', [WmsController::class, 'storeReceivedColli'])
            ->name('colli.storeReceivedColli');

        Route::get('/scan-barcode/{code}/{warehouse}', [WmsController::class, 'scanBarcode'])
            ->name('settings.scanBarcode');
        Route::get('/scan-tour/{tour}/{type}', [WmsController::class, 'scanTour'])->name('settings.scanTour');
        Route::get('/scan-colli/{tour}/{colli}/{type}', [WmsController::class, 'scanColli'])
            ->name('settings.scanColli');
        Route::post('/scan-location/{name}/{code}', [WmsController::class, 'scanLocation'])
            ->name('settings.scanLocation');
        Route::post('/move-location/{id}', [WmsController::class, 'moveLocation'])->name('settings.moveLocation');
        Route::get('/create-barcode/{warehouse}', [WmsController::class, 'createBarcode'])
            ->name('settings.createBarcode');
        Route::post('/create-barcode', [WmsController::class, 'storeBarcode'])->name('settings.storeBarcode');
        Route::post('/colli-register', [ColliController::class, 'colliRegister'])->name('collis.colliRegister');
        Route::post('/colli-dimensions-sum', [ColliController::class, 'getDimensionsSum'])
            ->name('colli.getDimensionsSum');


        /*
         * mobile views
         */
        Route::get('/settings/move', [WmsController::class, 'moveGoods'])->name('settings.moveGoods');
        Route::get('/settings/packing', [WmsController::class, 'packGoods'])->name('settings.packGoods');


        /*
         * location management
         */
        Route::resource('locations', 'App\Http\Controllers\LocationController');
        Route::get('/location-list', [LocationController::class, 'getLocations'])->name('locations.getAll');
        Route::get('/location-single/{id}', [LocationController::class, 'getSingle'])->name('location.getSingle');
        Route::post('/delete-multiple-locations', [LocationController::class, 'deleteMultipleLocations'])
            ->name('location.deleteMultipleLocations');
        Route::post('/locations/export', [LocationController::class, 'export'])->name('location.export');
        Route::post('/locations/import', [LocationController::class, 'import'])->name('location.import');

        /*
         * printer
         */
        Route::get('/printer', [PrinterController::class, 'index'])->name('printer');
        Route::put('/printer', [PrinterController::class, 'update'])->name('printer-update');

        /*
         * drivers
         */
        Route::resource('drivers', 'App\Http\Controllers\DriverController');
        Route::get('/driver-list', [DriverController::class, 'getDrivers'])->name('drivers.getAll');
        Route::get('/driver-list-select', [DriverController::class, 'getDriversSelect'])
            ->name('drivers.getDriversSelect');
        Route::get('/driver-single/{id}', [DriverController::class, 'getSingle'])->name('driver.getSingle');
        Route::post('/delete-multiple-drivers', [DriverController::class, 'deleteMultipleDrivers'])
            ->name('driver.deleteMultipleDrivers');

        /*
         * recurring booking module
         */
        Route::resource('recurrings', 'App\Http\Controllers\RecurringController');
        Route::get('/recurring-list', [RecurringController::class, 'getRecurrings'])->name('recurrings.getAll');
        Route::get('/recurring-list-select', [RecurringController::class, 'getRecurringsSelect'])
            ->name('recurrings.getRecurringsSelect');
        Route::get('/recurring-single/{id}', [RecurringController::class, 'getSingle'])->name('recurring.getSingle');
    });

    Route::get('/colli/images/{batch}', [ColliController::class, 'getImages'])
        ->name('collis.getImages');
    /*
     *optimoroute
     */
    Route::post('/optimoroute/create-order/{id}/{isMultiple}', [OptimorouteController::class, 'createOrder'])
        ->name('optimoroute.createOrder');
    Route::post('/optimoroute/delete-order/{id}/{isMultiple}', [OptimorouteController::class, 'deleteOrder'])
        ->name('optimoroute.deleteOrder');
});

/*
 * delivery
 */
Route::post('/deliveries', [DeliveryController::class, 'store'])->name('delivery.storeRate');

Route::get('/mobile/rate/{link}', [SiteController::class, 'rate'])->name('site.rate');
Route::post('/mobile/store-rate/{id}', [SiteController::class, 'storeRate'])->name('site.storeRate');
Route::get('/mobile/check-delivery/{link}', [SiteController::class, 'checkDelivery'])->name('site.checkDelivery');
Route::get('/mobile/check-delivery/get-available-dates/{zip}/{trip}', [SiteController::class, 'getAvailableDates'])->name('site.getAvailableDates');
Route::get('/mobile/check-delivery/communication-templates/{date}/{trip}', [SiteController::class, 'getCommunicationTemplates'])->name('site.getCommunicationTemplates');

Route::get('/optimoroute/get-orders/{id}', [OptimorouteController::class, 'getOrders'])->name('optimoroute.getOrders');
Route::get('/optimoroute/check-orders-statuses', [OptimorouteController::class, 'checkOrdersStatuses'])
    ->name('optimoroute.checkOrdersStatuses');
Route::get('/optimoroute/get-scheduling-info', [OptimorouteController::class, 'getSchedulingInfo'])
    ->name('optimoroute.getSchedulingInfo');
Route::post('/optimoroute/download-pod/{image}/{id}/{type}', [OptimorouteController::class, 'downloadPod'])
    ->name('optimoroute.downloadPod');

Route::get('/test/collis/', [TestController::class, 'collis'])->name('test.collis');
Route::get('/test/get-orders/{id}', [TestController::class, 'getOrders'])->name('test.getOrders');
Route::get('/test/delete-bookings', [TestController::class, 'deleteBookings'])->name('test.deleteBookings');
Route::get('/test/print', [TestController::class, 'printNode'])->name('test.printNode');
Route::get('/test/print-label/{isForClients?}', [TestController::class, 'printLabel'])->name('test.printLabel');
Route::get('/test/mail', [TestController::class, 'mail']);
Route::post('/test/upload', [TestController::class, 'image'])->name('image.upload.post');
