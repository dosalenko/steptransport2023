<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */
    'printnode' => [
        'APIKey' => env('PRINTNODE_API_KEY'),
        'source' => 'YouwePrint',
        'account' => [
            "firstname" => "",
            "lastname" => "",
            "email" => "",
            "password" => "",
            "creatorRef" => ""
        ],
        'tags' => [
            'for' => 'autoprint',
        ],
        'apiKeys' => [
            "development",
            "production"
        ],
        'accountID' => "",
        'options' => [
            //'color' => false,
            'duplex' => 'one-sided'
        ],
    ],

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        //'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.eu.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

];
