<?php

namespace App\Providers;

use App\Models\Colli;
use App\Models\Trip;
use App\Observers\ColliObserver;
use App\Observers\TripObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //Booking::observe(BookingObserver::class);
        Colli::observe(ColliObserver::class);
        Trip::observe(TripObserver::class);
    }
}
