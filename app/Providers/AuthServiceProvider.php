<?php

namespace App\Providers;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

    /*    Auth::viaRequest('api-token', function ($request) {
            if (!$key = $request->get('key') or $key !== env('APP_API_KEY', '12344321')) {
                throw new \Exception ('AUTH_KEY_UNKNOWN ');
            } else return $key;
        });*/
    }
}
