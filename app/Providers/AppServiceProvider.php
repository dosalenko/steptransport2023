<?php

namespace App\Providers;

use App\Models\UserRole;
use App\Repository\AgreementRepository;
use App\Repository\BookingRepository;
use App\Repository\ClientRepository;
use App\Repository\ColliRepository;
use App\Repository\ContactRepository;
use App\Repository\DeliveryRepository;
use App\Repository\DriverRepository;
use App\Repository\Interfaces\AgreementRepositoryInterface;
use App\Repository\Interfaces\BookingRepositoryInterface;
use App\Repository\Interfaces\ClientRepositoryInterface;
use App\Repository\Interfaces\ColliRepositoryInterface;
use App\Repository\Interfaces\ContactRepositoryInterface;
use App\Repository\Interfaces\DeliveryRepositoryInterface;
use App\Repository\Interfaces\DriverRepositoryInterface;
use App\Repository\Interfaces\LocationRepositoryInterface;
use App\Repository\Interfaces\ProductRepositoryInterface;
use App\Repository\Interfaces\RecurringRepositoryInterface;
use App\Repository\Interfaces\RouteRepositoryInterface;
use App\Repository\Interfaces\ServiceRepositoryInterface;
use App\Repository\Interfaces\TripRepositoryInterface;
use App\Repository\Interfaces\UserRepositoryInterface;
use App\Repository\LocationRepository;
use App\Repository\ProductRepository;
use App\Repository\RecurringRepository;
use App\Repository\RouteRepository;
use App\Repository\ServiceRepository;
use App\Repository\TripRepository;
use App\Repository\UserRepository;
use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->isLocal()) {
            $this->app->register(IdeHelperServiceProvider::class);
        }

        $this->app->bind(ClientRepositoryInterface::class, ClientRepository::class);
        $this->app->bind(ProductRepositoryInterface::class, ProductRepository::class);
        $this->app->bind(ServiceRepositoryInterface::class, ServiceRepository::class);
        $this->app->bind(BookingRepositoryInterface::class, BookingRepository::class);
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
        $this->app->bind(ContactRepositoryInterface::class, ContactRepository::class);
        $this->app->bind(AgreementRepositoryInterface::class, AgreementRepository::class);
        $this->app->bind(TripRepositoryInterface::class, TripRepository::class);
        $this->app->bind(LocationRepositoryInterface::class, LocationRepository::class);
        $this->app->bind(ColliRepositoryInterface::class, ColliRepository::class);
        $this->app->bind(DriverRepositoryInterface::class, DriverRepository::class);
        $this->app->bind(RecurringRepositoryInterface::class, RecurringRepository::class);
        $this->app->bind(RouteRepositoryInterface::class, RouteRepository::class);
        $this->app->bind(DeliveryRepositoryInterface::class, DeliveryRepository::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->bladeDirectives();
    }

    private function bladeDirectives()
    {
        Blade::directive('userCan', function ($expression) {
            return "<?php if (\\Auth::check()
             && (\\Auth::guard()->user()->hasRole({$expression}) || \\Auth::guard()->user()->hasRole('" . UserRole::ROLE_ADMIN . "'))) : ?>";
        });
        Blade::directive('endUserCan', function () {
            return "<?php endif; ?>";
        });
    }
}
