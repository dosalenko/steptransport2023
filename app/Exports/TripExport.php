<?php

namespace App\Exports;

use App\Models\Trip;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;

class TripExport implements FromArray, WithCustomCsvSettings, WithHeadings
{
    use Exportable;

    /*
     * array
     */
    public $columns;

    /*
     * array
     */
    public $ids;

    public function __construct($ids)
    {
        $this->columns = [
            'Id',
            'BookingId',
            'Trip',
            'Client',
            'Booking',
            'Status',
            'Booking date',
            'Type',
            'Address',
            'Time',
            'Volume',
            'Weight',
            'Locations',
            'Services',
            'Note',
            'Reference',
            'Colli',
            'Internal note',
            'Zip',
        ];

        $this->ids = $ids;
    }

    /**
     * @return array
     */
    public function array(): array
    {
        return Trip::getArrayForExportTripCsv($this->ids);
    }


    /**
     * @return array
     */
    public function getCsvSettings(): array
    {
        return [
            // 'enclosure' => '"',
            'input_encoding' => 'UTF-8'
        ];
    }

    public function headings(): array
    {
        return $this->columns;
    }
}
