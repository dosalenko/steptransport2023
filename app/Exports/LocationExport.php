<?php

namespace App\Exports;

use App\Models\Location;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;

class LocationExport implements FromArray, WithCustomCsvSettings, WithHeadings
{
    use Exportable;

    /*
     * array
     */
    public $columns;


    public function __construct(array $columns)
    {
        $this->columns = $columns;
    }

    /**
     * @return array
     */
    public function array(): array
    {
        return Location::getArrayForExportLocationCsv($this->columns);
    }


    /**
     * @return array
     */
    public function getCsvSettings(): array
    {
        return [
            'enclosure' => '"'
        ];
    }


    public function headings(): array
    {
        return $this->columns;
    }
}
