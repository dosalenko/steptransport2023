<?php

namespace App\Exports;

use App\Models\Booking;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;

class BookingExport implements FromArray, WithCustomCsvSettings, WithHeadings
{
    use Exportable;

    /*
     * array
     */
    public $columns;

    public $dateFrom;

    public $dateTo;


    public function __construct(array $columns, string $dateFrom, string $dateTo)
    {
        $this->columns = $columns;
        $this->dateFrom = $dateFrom;
        $this->dateTo = $dateTo;
    }

    /**
     * @return array
     */
    public function array(): array
    {
        return Booking::getArrayForExportBookingCsv($this->columns, $this->dateFrom, $this->dateTo);
    }


    /**
     * @return array
     */
    public function getCsvSettings(): array
    {
        return [
            'enclosure' => '"'
        ];
    }


    public function headings(): array
    {
        return $this->columns;
    }
}
