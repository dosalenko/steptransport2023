<?php

namespace App\Services\Printnode;

use App\Models\User;
use App\Services\Printnode\Exceptions\NotFoundPrinterException;
use App\Services\Printnode\Exceptions\PrintnodeException;

class Printer
{
    /**
     * @var array
     */
    protected $cachePrinters = [];

    /**
     * Printer constructor.
     */
    public function __construct()
    {
        if (app()->runningInConsole()) {
            $userId = User::ADMIN_ID;
            $this->cachePrinters = Printnode::getCachePrinters($userId);
        } else {
            $this->cachePrinters = Printnode::getCookiesPrinters();
        }
    }

    /**
     * @param $printerId
     * @param \App\Services\Printnode\Printnode|null $printnode
     * @return Printnode
     * @throws \Exception
     */
    public function getPrintnodePrinter($printerId, ?Printnode $printnode = null)
    {
        if ($printnode === null) {
            $printnode = new Printnode();
        }

        if (!$printnode->isAuth()) {
            throw new PrintnodeException("PrinterId {$printerId} authenticate error.");
        }

        if (!$printnode->isPrinter($printerId)) {
            throw new PrintnodeException("PrinterId {$printerId} not found.");
        }

        return $printnode;
    }

    /**
     * @param string $type
     * @return string
     */
    public function getId($type = 'delivery_note_printer')
    {
        if ($this->cachePrinters && isset($this->cachePrinters[$type])) {
            return (string)$this->cachePrinters[$type];
        }

        throw new NotFoundPrinterException("Printer {$type} id is missing");
    }

    /**
     * @return string|bool
     */
    public function getLabelId()
    {
        if ($this->cachePrinters && isset($this->cachePrinters['label_printer'])) {
            return (string)$this->cachePrinters['label_printer'];
        }

        return false;
    }

    /**
     * @return string
     */
    public function getTourId(): string
    {
        if ($this->cachePrinters && isset($this->cachePrinters['tour_printer'])) {
            return (string)$this->cachePrinters['tour_printer'];
        }

        throw new NotFoundPrinterException('Tour list printer id is missing');
    }


    /**
     * @return string
     */
    public function getCollectId(): string
    {
        if ($this->cachePrinters && isset($this->cachePrinters['collect_printer'])) {
            return (string)$this->cachePrinters['collect_printer'];
        }

        throw new NotFoundPrinterException('Clickncollect labels printer id is missing');
    }
}
