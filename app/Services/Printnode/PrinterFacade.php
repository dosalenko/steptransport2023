<?php

namespace App\Services\Printnode;

use Illuminate\Support\Facades\Facade;

class PrinterFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return Printer::class;
    }
}
