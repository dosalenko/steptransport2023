<?php

namespace App\Services\Printnode;

use App\Services\Printnode\Exceptions\PrintnodeException;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Log;
use PrintNode\Account;
use PrintNode\Response;
use Auth, Cache;


class Printnode
{
    private $credentials;
    private $request;
    private $auth = false;

    /**
     * Printnode constructor.
     * @param null|string $APIkey
     * @throws \Exception
     */
    public function __construct($APIkey = null)
    {
        $APIkey = $APIkey ?? config('services.printnode.APIKey');

        try {
            $this->credentials = new \PrintNode\Credentials();
            $this->credentials->setApiKey($APIkey);

            $this->request = new \PrintNode\Request($this->credentials);


            Log::channel('applog')->info('getComputers', ['action' => 'Printnode']);

            $computers = $this->getComputers();
            if (!empty($computers)) {
                $this->auth = true;
            }
        } catch (\Exception $e) {
            Log::channel('applog')->error($e->getMessage(), ['action' => 'Printnode']);
            throw new PrintnodeException($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param array $accountInfo
     * @param array $apiKeys
     * @param array $tags
     * @return array
     */
    public function createChildAccount(array $accountInfo, array $apiKeys = [], array $tags = [])
    {
        $result = ['success' => false, 'message' => ''];

        try {
            $account = $this->getAccount($accountInfo, $apiKeys, $tags);

            $response = $this->request->post($account);

            $result = $this->getResponse($response);
        } catch (\Exception $e) {
            $result['message'] = $e->getMessage();
            Log::channel('applog')->error($e->getMessage(), ['action' => 'Printnode createChildAccount']);
            throw new PrintnodeException($e->getMessage(), $e->getCode());
        }

        return $result;
    }

    /**
     * @return bool
     */
    public function isAuth()
    {
        return $this->auth;
    }

    /**
     * @param $id
     * @param array $accountInfo
     * @param array $apiKeys
     * @param array $tags
     * @return array
     */
    public function updateChildAccount($id, array $accountInfo, array $apiKeys = [], array $tags = [])
    {
        $result = ['success' => false, 'message' => ''];

        try {
            $account = $this->getAccount($accountInfo, $apiKeys, $tags);

            $this->request->setChildAccountById($id);
            $response = $this->request->patch($account);

            $result = $this->getResponse($response);
        } catch (\Exception $e) {
            $result['message'] = $e->getMessage();
            Log::channel('applog')->error($e->getMessage(), ['action' => 'Printnode updateChildAccount']);
            throw new PrintnodeException($e->getMessage(), $e->getCode());
        }

        return $result;
    }

    /**
     * @param $email
     * @return array
     */
    public function deleteChildAccount($email)
    {
        $result = ['success' => false, 'message' => ''];

        try {
            $account = new Account();

            $this->request->setChildAccountByEmail($email);
            $response = $this->request->delete($account);

            $result = $this->getResponse($response);
        } catch (\Exception $e) {
            $result['message'] = $e->getMessage();
            Log::channel('applog')->error($e->getMessage(), ['action' => 'Printnode deleteChildAccount']);
            throw new PrintnodeException($e->getMessage(), $e->getCode());
        }

        return $result;
    }

    /**
     * @param $email
     * @return array
     */
    public function getChildAccount($email)
    {
        $result = ['success' => false, 'message' => '', 'account' => []];

        try {
            $this->request->setChildAccountByEmail($email);
            $response = $this->request->__call('getWhoami', []);

            $result['account'] = $response;

            if (is_object($response)) {
                $result['success'] = true;
            }
        } catch (\Exception $e) {
            $result['message'] = $e->getMessage();
            Log::channel('applog')->error($e->getMessage(), ['action' => 'Printnode getChildAccount']);
            throw new PrintnodeException($e->getMessage(), $e->getCode());
        }

        return $result;
    }

    /**
     * @param array $accountInfo
     * @param array $apiKeys
     * @param array $tags
     * @return Account
     * @noinspection PhpSingleStatementWithBracesInspection
     */
    private function getAccount(array $accountInfo, array $apiKeys = [], array $tags = [])
    {
        $account = new Account();

        $account->Account = $accountInfo;

        if (is_array($apiKeys) && !empty($apiKeys)) {
            $account->ApiKeys = $apiKeys;
        }

        if (is_array($tags) && !empty($tags)) {
            $account->Tags = $tags;
        }

        return $account;
    }

    /**
     * @param $printerID
     * @param $file
     * @param $title
     * @param array $options
     * @param string $contentType
     * @return array
     */
    public function addPrintJob($printerID, $file, $title, $options = array(), $contentType = 'pdf_base64')
    {
        if (env('APP_ENV') === 'local') {
            return ['success' => true, 'message' => ''];
        }

        $result = ['success' => false, 'message' => ''];

        try {
            $printJob = new \PrintNode\PrintJob();

            $printJob->printer = $printerID;
            $printJob->contentType = $contentType;

            if (in_array($contentType, ['pdf_base64', 'raw_base64 '])) {
                $printJob->content = base64_encode(file_get_contents($file));
            } else {
                $printJob->content = $file;
            }

            $printJob->source = config('services.printnode.source');
            $printJob->title = $title;

            if (is_array($options) && !empty($options)) {
                $printJob->options = $options;
            }

            for ($i = 1; $i <= 4; $i++) {
                try {
                    if ($i > 1) {
                        sleep(1);
                    }

                    Log::channel('applog')->info('addPrintJob', ['action' => 'Printnode']);
                    $response = $this->request->post($printJob);

                    $statusCode = $response->getStatusCode();
                    $statusMessage = $response->getStatusMessage();
                    $content = $response->getDecodedContent();

                    if ($statusCode == 201 && $statusMessage == 'Created') {
                        $result['success'] = true;
                        $result['message'] = $content;
                        break;
                    } else {
                        Log::channel('applog')->error($response->getDecodedContent(), ['action' => 'Printnode addPrintJob']);
                    }
                } catch (\Exception | \RuntimeException $e) {
                    if ($i === 4) {
                        throw new PrintnodeException("Printjob failed. {$title}", $e->getCode());
                    }
                } catch (\Throwable $e) {
                    if ($i === 4) {
                        throw new PrintnodeException("Printjob failed. {$title}", $e->getCode());
                    }
                }
            }

        } catch (\Exception $e) {
            $result['message'] = $e->getMessage();
            Log::channel('applog')->error($e->getMessage(), [
                'response' => (isset($response) && !empty($response)) ? "{$response->getStatusCode()}|{$response->getStatusMessage()}|{$response->getDecodedContent()}" : '',
                'action' => 'Printnode addPrintJob',
            ]);
            throw new PrintnodeException($e->getMessage(), $e->getCode());
        }

        return $result;
    }

    /**
     * @param int $printerId
     * @param array $batchData
     * @param string $batchOrderNumber
     * @return array
     */
    public function addBatchPrintJob(int $printerId, array $batchData, string $batchOrderNumber): array
    {
        $printJobStatus = [];

        foreach ($batchData as $name => $data) {

            $printJobStatus[$name]['printJobStatus'] = $this->addPrintJob(
                $printerId,
                $data['file_path'],
                "$name (Batch order #$batchOrderNumber)",
                [
                    'duplex' => 'one-sided'
                ]
            );
        }

        return $printJobStatus;
    }

    /**
     * @param $printerID
     * @return bool
     */
    public function isPrinter($printerID)
    {
        $printer = $this->getPrinter($printerID);

        if (is_object($printer)) {
            return true;
        }

        return false;
    }

    /**
     * @param $printerID
     * @return bool|\PrintNode\Printer
     */
    public function getPrinter($printerID)
    {
        Log::channel('applog')->info('getPrinter', ['action' => 'Printnode']);
        $printers = $this->request->getPrinters($printerID);
        $printer = false;

        if (!empty($printers)) {
            $printer = $printers[0];
        }

        return $printer;
    }

    /**
     * @return \PrintNode\Printer[]
     */
    public function getPrinters()
    {
        Log::channel('applog')->info('getPrinters', ['action' => 'Printnode']);
        return $this->request->getPrinters();
    }

    /**
     * @param $printers
     * @param bool $user_id
     */
    public static function setCachePrinters($printers, $user_id = false)
    {
        $cachePrinters = [];

        if (!$user_id) {
            $user_id = Auth::user()->id;
        }

        if (Cache::has('printers')) {
            $cachePrinters = Cache::pull('printers');
        }

        $cachePrinters[$user_id] = $printers;
        Cache::put('printers', $cachePrinters);
    }

    /**
     * @param bool $user_id
     * @return bool|string
     */
    public static function getCachePrinters($user_id = false)
    {
        $cachePrinters = [];

        if (!$user_id) {
            $user_id = Auth::user()->id;
        }

        if (Cache::has('printers')) {
            $cachePrinters = Cache::get('printers');
        }

        if (isset($cachePrinters[$user_id])) {
            return $cachePrinters[$user_id];
        }

        return false;
    }

    /**
     * @param bool $all
     * @param bool $user_id
     */
    public static function clearCachePrinters($all = false, $user_id = false)
    {
        $cachePrinters = [];

        if (!$user_id) {
            $user_id = Auth::user()->id;
        }

        if (Cache::has('printers')) {
            $cachePrinters = Cache::pull('printers');
        }

        if (!$all) {
            if (isset($cachePrinters[$user_id])) {
                unset($cachePrinters[$user_id]);
                Cache::put('printers', $cachePrinters);
            }
        }
    }

    /**
     * @return array|bool|string|null
     */
    public static function getCookiesPrinterName()
    {
        $printerName = false;

        if (Cookie::has('printer_name')) {
            $printerName = Cookie::get('printer_name');
        }

        return $printerName;
    }

    public static function setCookiesPrinterName($printerName)
    {
        Cookie::queue(Cookie::make('printer_name', $printerName, 52560000));
    }

    public static function setCookiesPrinters($printers)
    {
        Cookie::queue(Cookie::make('printers', json_encode($printers), 52560000));
    }

    public static function getCookiesPrinters()
    {
        $result = false;

        if (Cookie::has('printers')) {
            $printersJson = Cookie::get('printers');
            $printers = json_decode($printersJson, true);

            if (is_array($printers)) {
                $result = $printers;
            }
        }

        return $result;
    }

    public static function clearCookiesPrinters()
    {
        Cookie::queue(Cookie::forget('printers'));
    }

    /**
     * @param Response $response
     * @return mixed
     */
    private function getResponse(Response $response)
    {
        $statusCode = $response->getStatusCode();
        $statusMessage = $response->getStatusMessage();
        $content = $response->getDecodedContent();

        $result['statusCode'] = $statusCode;
        $result['statusMessage'] = $statusMessage;
        $result['content'] = $content;

        if ($statusCode == 200 && $statusMessage == 'OK') {
            $result['success'] = true;
        }

        if (isset($content['message'])) {
            $result['message'] = $content['message'];
        }

        return $result;
    }

    private function getComputers()
    {
        return $this->request->getComputers();
    }

}
