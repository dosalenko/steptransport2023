<?php

namespace App\Services\Printnode\Exceptions;

class NotFoundPrinterException extends PrintnodeException
{
    //
}
