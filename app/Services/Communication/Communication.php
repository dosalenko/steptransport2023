<?php

namespace App\Services\Communication;

use App\Models\Communication as Model;
use App\Models\Delivery;
use App\Models\Trip;
use App\Services\Core\Helpers\Helper;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Twilio\Rest\Client;

/**
 * Class Communication
 * @package App\Services\Communication
 */
class Communication
{

    /*
     *
     */
    private $trip;

    /*
     *
     */
    private $type;

    public function __construct(Trip $trip, int $type)
    {
        $this->trip = $trip;
        $this->type = $type;
    }

    public function test(): bool
    {
        return true;
    }

    /**
     * @param
     * @return bool
     */
    public function start(): bool
    {
        try {
            $tripCommunications = Model::where('trip_id', '=', $this->trip->id)->get();
            $message = null;
            $viewNameEmail = null;
            $viewNameSms = null;
            $subject = '';
            $params = [];
            // if ($this->trip->tw_start && $this->trip->booking->pickup_email && $this->trip->type === Trip::TRIP_TYPE_PICKUP) {
            if ($this->trip->tw_start && $this->trip->address->phone && $this->trip->type === Trip::TRIP_TYPE_PICKUP) {
                //$email = $this->trip->booking->pickup_email;
                $date = Carbon::createFromFormat('Y-m-d', substr($this->trip->tw_start, 0, 10))->format('d-m-Y');
                $from = Carbon::createFromFormat('Y-m-d H:i', substr($this->trip->tw_start, 0, 16))->format('H:i');
                $to = Carbon::createFromFormat('Y-m-d H:i', substr($this->trip->tw_end, 0, 16))->format('H:i');
                $params = ['trip' => $this->trip, 'date' => $date, 'from' => $from ?? '08:00', 'to' => $to ?? '18:00'];
                if ($this->type === Model::MESSAGE_TYPE_PU_CONFIRMATION && count($tripCommunications) === 0) {
                    $message = 'confirmation';
                    //$viewNameEmail = 'communications.pickup_confirmation';
                    $viewNameSms = 'communications.sms.pickup_confirmation';
                    $subject = 'Din afhentning fra ' . $this->trip->client->name;

                    if ($from='00:00' || $to='00:00') {
                        $text = "Hej, " . $this->trip->booking->dropoffAddress->name . " <br/> <br/>

Vi har modtaget besked om at afhente din pakke til " . $this->trip->booking->dropoffAddress->name . " <br/>
Din afhentning vil ske d. " . $date . " på " . $this->trip->address->street . "  " . $this->trip->address->zip . ", " . $this->trip->address->city . ". <br/>
Du får kommunikeret et 4-timers interval senest 24 timer før levering. <br/> <br/>

Hvis du ønsker at ændre leverancen, kan du klikke her: <br/>
http://my.steptransport.dk/" . $this->trip->unique_link . " <br/>
Mvh <br/>
Step Transport";
                    } else {
                        $text = "Hej, " . $this->trip->booking->dropoffAddress->name . " <br/> <br/>

Vi har modtaget besked om at afhente din pakke til " . $this->trip->booking->dropoffAddress->name . " <br/>
Din afhentning vil ske d. " . $date . " mellem " . $from . " og " . $to . " på " . $this->trip->address->street . "  " . $this->trip->address->zip . ", " . $this->trip->address->city . ". <br/>
Du får kommunikeret et 4-timers interval senest 24 timer før levering. <br/> <br/>

Hvis du ønsker at ændre leverancen, kan du klikke her: <br/>
http://my.steptransport.dk/" . $this->trip->unique_link . " <br/>
Mvh <br/>
Step Transport";
                    }

                } elseif ($this->type === Model::MESSAGE_TYPE_PU_RECONFIRMATION) {
                    $delivery = Delivery::where('trip_id', '=', $this->trip->id)->latest('id')->first();
                    $message = 'reconfirmation';
                    $viewNameEmail = 'communications.pickup_reconfirmation';
                    $viewNameSms = 'communications.sms.pickup_reconfirmation';
                    $subject = 'Din afhentning fra ' . $this->trip->client->name;
                    $params['address'] = $delivery->address;
                    $text = '';
                } elseif ($this->type === Model::MESSAGE_TYPE_PU_TOMORROW) {
                    $message = 'tomorrow';
                    $viewNameEmail = 'communications.pickup_tomorrow';
                    $viewNameSms = 'communications.sms.pickup_tomorrow';
                    $subject = 'Husk din afhentning til ' . $this->trip->booking->dropoffAddress->name . ' i morgen';
                    $text = '';
                } elseif ($this->type === Model::MESSAGE_TYPE_PU_RATE) {
                    $message = null;
                    $viewNameEmail = 'communications.pickup_rate';
                    $viewNameSms = 'communications.sms.pickup_rate';
                    $subject = 'Bedøm din afhentning fra Step Transport';
                    $text = '';
                }
            }

            //if ($this->trip->tw_start && $this->trip->booking->dropoff_email && $this->trip->type === Trip::TRIP_TYPE_DROPOFF) {
            if ($this->trip->tw_start && $this->trip->address->phone && $this->trip->type === Trip::TRIP_TYPE_DROPOFF) {
                //$email = $this->trip->booking->dropoff_email;
                $date = Carbon::createFromFormat('Y-m-d', substr($this->trip->tw_start, 0, 10))->format('d-m-Y');
                $from = Carbon::createFromFormat('Y-m-d H:i', substr($this->trip->tw_start, 0, 16))->format('H:i');
                $to = Carbon::createFromFormat('Y-m-d H:i', substr($this->trip->tw_end, 0, 16))->format('H:i');
                $params = ['trip' => $this->trip, 'date' => $date, 'from' => $from ?? '08:00', 'to' => $to ?? '18:00'];
                if ($this->type === Model::MESSAGE_TYPE_DO_CONFIRMATION && count($tripCommunications) === 0) {
                    $message = 'confirmation';
                    //$viewNameEmail = 'communications.dropoff_confirmation';
                    $viewNameSms = 'communications.sms.dropoff_confirmation';
                    $subject = 'Din levering fra ' . $this->trip->client->name;

                    if ($from='00:00' || $to='00:00') {
                        $text = "Hej, " . $this->trip->booking->dropoffAddress->name . " <br/> <br/>

Vi har modtaget dine varer fra " . $this->trip->client->name . " <br/>
Din levering vil ske d. " . $date . " på " . $this->trip->address->street . " " . $this->trip->address->zip . ", " . $this->trip->address->city . ". <br/>
Du får kommunikeret et 4-timers interval senest 24 timer før levering.<br/> <br/>

Hvis du ønsker at ændre leverancen, kan du klikke her: <br/>
http://my.steptransport.dk/" . $this->trip->unique_link . " <br/>
Mvh <br/>
Step Transport
";
                    } else {
                    $text = "Hej, " . $this->trip->booking->dropoffAddress->name . " <br/> <br/>

Vi har modtaget dine varer fra " . $this->trip->client->name . " <br/>
Din levering vil ske d. " . $date . " mellem " . $from . " og " . $to . " på " . $this->trip->address->street . " " . $this->trip->address->zip . ", " . $this->trip->address->city . ". <br/>
Du får kommunikeret et 4-timers interval senest 24 timer før levering.<br/> <br/>

Hvis du ønsker at ændre leverancen, kan du klikke her: <br/>
http://my.steptransport.dk/" . $this->trip->unique_link . " <br/>
Mvh <br/>
Step Transport
";
                    }
                } elseif ($this->type === Model::MESSAGE_TYPE_DO_RECONFIRMATION) {
                    $delivery = Delivery::where('trip_id', '=', $this->trip->id)->latest('id')->first();
                    $message = 'reconfirmation';
                    $viewNameEmail = 'communications.dropoff_reconfirmation';
                    $viewNameSms = 'communications.sms.dropoff_reconfirmation';
                    $subject = 'Din levering fra ' . $this->trip->client->name;
                    $params['address'] = $delivery->address;
                    $text = '';
                } elseif ($this->type === Model::MESSAGE_TYPE_DO_TOMORROW) {
                    $message = 'tomorrow';
                    $viewNameEmail = 'communications.dropoff_tomorrow';
                    $viewNameSms = 'communications.sms.dropoff_tomorrow';
                    $subject = 'Husk din levering fra ' . $this->trip->client->name . ' i morgen';
                    $text = '';
                } elseif ($this->type === Model::MESSAGE_TYPE_DO_RATE) {
                    $message = null;
                    $viewNameEmail = 'communications.dropoff_rate';
                    $viewNameSms = 'communications.sms.dropoff_rate';
                    $subject = 'Bedøm din levering fra ' . $this->trip->client->name;
                    $text = '';
                }
            }
            if ($viewNameEmail) {
                //$this->sendMail($viewNameEmail, $subject, $email, $params);
                if ($message) {
                    $communicationData = [
                        'user_id' => Auth::user() ? Auth::user()->id : 1,
                        'communication_type' => Model::COMMUNICATION_TYPE_EMAIL,
                        'message_type' => $this->type,
                        'message' => $message,
                        'trip_id' => $this->trip->id,
                        'text' => $text,
                        'date_assigned' => $date . ' ' . $from . '-' . $to,
                    ];
                    Model::create($communicationData);
                }
            }
            if ($viewNameSms) {
                $this->sendSms(view($viewNameSms, $params)->render());
                if ($message) {
                    $communicationData = [
                        'user_id' => Auth::user() ? Auth::user()->id : 1,
                        'communication_type' => Model::COMMUNICATION_TYPE_SMS,
                        'message_type' => $this->type,
                        'message' => $message,
                        'trip_id' => $this->trip->id,
                        'text' => $text,
                        'date_assigned' => $date . ' ' . $from . '-' . $to,
                        'status' => 'success',
                    ];
                    Model::create($communicationData);
                }
            }
        } catch (\Throwable $exception) {
            \Log::error($exception . ' trip id: ' . $this->trip->id);
        }
        return true;
    }

    public function sendMail($viewName, $subject, $to, $params): void
    {
        Mail::send(
            $viewName,
            ['trip' => $this->trip, 'date' => $params['date'], 'from' => $params['from'], 'to' => $params['to'], 'address' => isset($params['address']) ? $params['address'] : null],
            function ($message) use ($to, $subject) {
                $message->to($to)->subject($subject);
            }
        );
    }

    /*
     *
     */
    public function sendSms(string $text): void
    {
        if ($this->trip->address->phone) {
            $text = strip_tags(preg_replace('/<[^>]*>/', '', str_replace(array("<br/><br/>"), "\n", html_entity_decode($text, ENT_QUOTES, 'UTF-8'))));
            $account_sid = env("TWILIO_SID");
            $auth_token = env("TWILIO_TOKEN");
            $twilio_from_name = env("TWILIO_FROM_NAME");

            $client = new Client($account_sid, $auth_token);
            $client->messages->create($this->trip->address->phone, [
                'from' => $twilio_from_name,
                'body' => $text
            ]);
        }
    }
}
