<?php

namespace App\Services\Core\Libraries\LaravelVueDatatable;

use JamesDordoy\LaravelVueDatatable\Classes\Filters\FilterBelongsToManyRelationships;
use JamesDordoy\LaravelVueDatatable\Classes\Filters\FilterHasManyRelationships;

class QueryBuilder extends \JamesDordoy\LaravelVueDatatable\Classes\QueryBuilder
{
    /**
     * @param $searchValue
     * @return $this|\JamesDordoy\LaravelVueDatatable\Classes\QueryBuilder
     * @throws \JamesDordoy\LaravelVueDatatable\Exceptions\RelationshipColumnsNotFoundException
     * @throws \JamesDordoy\LaravelVueDatatable\Exceptions\RelationshipModelNotSetException
     */
    public function filter($searchValue)
    {
        if (isset($searchValue) && ! empty($searchValue)) {

            $filterLocalData = new FilterLocalData;
            $this->query = $filterLocalData($this->query, $searchValue, $this->model, $this->localColumns);

            $filterBelongsTo = new FilterBelongsToRelationships;
            $this->query = $filterBelongsTo($this->query, $searchValue, $this->relationshipModelFactory, $this->model, $this->relationships);

            $filterHasMany = new FilterHasManyRelationships;
            $this->query = $filterHasMany($this->query, $searchValue, $this->relationshipModelFactory, $this->model, $this->relationships);

            $filterBelongsToMany = new FilterBelongsToManyRelationships;
            $this->query = $filterBelongsToMany($this->query, $searchValue, $this->relationshipModelFactory, $this->model, $this->relationships);

            return $this;
        }

        return $this;
    }
}
