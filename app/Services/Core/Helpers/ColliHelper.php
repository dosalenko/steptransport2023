<?php

namespace App\Services\Core\Helpers;

use App\Models\Booking;
use App\Models\Colli;
use App\Models\Log;
use App\Repository\Interfaces\BookingRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Picqer\Barcode\BarcodeGeneratorHTML;

class ColliHelper
{

    /*
     *  add new colli to batch group
     *
     * return bool
     */
    public static function addNewColliToBatch(Colli $colli, int $number, int $isFromBookingEdit = 0)
    {
        $maxColliNumber = Colli::where('colli_batch', '=', $colli->colli_batch)->max('colli');
        $oldTotalVolumeByColliBatch = Helper::getTotalColliMeasureByColliBatch($colli->colli_batch, 'volume');
        $oldTotalWeightByColliBatch = Helper::getTotalColliMeasureByColliBatch($colli->colli_batch, 'weight');
        $maxColliNumber += 1;

        if ($number > 0) {
            for ($i = 1; $i < $number; $i++) {
                $colliData[$i] = [
                    'reference' => $colli->reference,
                    'colli' => $maxColliNumber++,
                    'volume' => $colli->volume,
                    'weight' => $colli->weight,
                    'booking_id' => $colli->booking_id,
                    'client_id' => $colli->client_id,
                    'agreement_id' => Colli::DEFAULT_AGREEMENT,
                    'location_id' => $colli->location_id,
                    'status' => Colli::COLLI_STATUS_ON_DISPATCH_TERMINAL,
                    'name' => $colli->name,
                    'received_at' => $colli->received_at,
                    'colli_batch' => $colli->colli_batch,
                    'created_by' => Auth::user() ? Auth::user()->id : 0,
                ];
            }

            foreach ($colliData as $v) {
                Colli::create($v);
            }
        }
        if ($number <= 0) {
            Colli::where('colli_batch', '=', $colli->colli_batch)
                ->where('colli', '>', $maxColliNumber - abs($number) - 2)
                ->delete();
        }

        /*
         *  when changing the amount of colli per batch,
         * we should assume that the weight and volume
         * remains unchanged on the batch level.
         * This means that we need to recalculate
         * the volume and weight for individual collis
         * in order for the total weight and volume to
         * remain unchanged when we change the amount of colli.
         */

        $collis = Colli::where('colli_batch', '=', $colli->colli_batch)->get();
        $newColliVolumeUnit = $oldTotalVolumeByColliBatch / count($collis);
        $newColliWeightUnit = $oldTotalWeightByColliBatch / count($collis);

        foreach ($collis as $colli) {
            $colli->volume = round(Helper::format($newColliVolumeUnit), 2);
            $colli->weight = round(Helper::format($newColliWeightUnit), 2);
            $colli->save();
        }

        return true;
    }


    /*
    *  update batch group
    *
    * return bool
    */
    public
    static function updateColliBatch(array $data, $batchId)
    {
        $getCollisByColliBatch = Colli::where('colli_batch', '=', $batchId)->get();
        foreach ($getCollisByColliBatch as $batch) {
            \Illuminate\Support\Facades\Log::channel('applog')->info("Colli edited info before" . json_encode($batch));
            $batch->reference = $data['reference'];
            $batch->name = $data['name'];
            $batch->volume = round(Helper::format($data['totalVolume']) / count($getCollisByColliBatch), 2);
            $batch->weight = round(Helper::format($data['totalWeight']) / count($getCollisByColliBatch), 2);
            $batch->save();
            if ($batch->booking_id !== null) {
                $bookingRepo = App::make(BookingRepositoryInterface::class);
                $action = 'Colli [' . $batch->id . '] have been updated';
                $bookingRepo->saveHistoryLog($action, Auth::user()->email, $batch->booking);
            }
            \Illuminate\Support\Facades\Log::channel('applog')->info("Colli edited info after" . json_encode($batch));
        }
    }

    /*
     * @param Booking $booking
     * @return string|null
     */
    public
    static function generateBase64Label(Booking $booking)
    {
        $maxColliNumber = [];
        $collis = Colli::with(
            'booking',
            'booking.pickupAddress',
            'booking.product',
            'client',
            'location'
        )->where('booking_id', '=', $booking->id)
            ->orderBy('reference', 'ASC')
            ->get();
        foreach ($collis as $colli) {
            $maxColliNumber[$colli->reference][] = Helper::checkColliStartingFromZero($colli);
        }

        if (count($collis) > 0) {
            $barcode = [];
            $generator = new BarcodeGeneratorHTML();
            foreach ($collis as $colli) {
                $barcode[$colli->id] = $generator->getBarcode('00000000000000000000000000000' . $colli->id, $generator::TYPE_CODE_128, 3, 65);
            }
            $pdf = \PDF::loadView('collis.print_label', compact(['collis', 'maxColliNumber', 'barcode']));
            $pdf->setPaper('A4');
            $b64 = base64_encode($pdf->output());
            return $b64;
        }

        return null;
    }

    /*
     *
     *
     *  @return array
    */
    public
    static function getColliBatchLocationList($batchId)
    {
        $res = [];
        $getCollisByColliBatch = Colli::with('location')->where('colli_batch', '=', $batchId)->get();
        foreach ($getCollisByColliBatch as $colli) {
            if ($colli->location !== null) {
                $res[$colli->id] = '#' . $colli->location->name . ' (' . $colli->location->warehouse->nickname . ')';
            }
        }
        return array_unique($res);
    }

    public
    static function getGrouppedByLocations($batchId)
    {
        $res = [];
        $getCollisByColliBatch = Colli::with('location')->where('colli_batch', '=', $batchId)->get();
        foreach ($getCollisByColliBatch as $colli) {
            if ($colli->location !== null) {
                $res[] = '#' . $colli->location->name . ' (' . $colli->location->warehouse->nickname . ')';
            }
        }
        $counted = array_count_values($res);
        ksort($counted);
        return $counted;
    }

}
