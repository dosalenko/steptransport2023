<?php

namespace App\Services\Core\Helpers;

use App\Models\AgreementProductPrice;
use App\Models\AgreementService;
use App\Models\AgreementServicePrice;
use App\Models\Booking;
use App\Models\BookingService;
use App\Models\Route;
use App\Models\Trip;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class BookingHelper
{
    /*
     * get product price by interval
     *
     * @param int $productId
     * @param float $totalVolume
     * @param float $totalWeight
     *
     * return string
     */
    public static function getProductPriceByInterval($productId, $totalVolume, $totalWeight, $agreementId, $productIdFromStandartProductTable = 0)
    {
        if ($productIdFromStandartProductTable === 0) {
            $productPrice = AgreementProductPrice::whereHas('product', function ($q) use ($agreementId) {
                $q->where('agreement_id', '=', $agreementId);
            })->where('agreement_product_id', $productId)->get();
        } else {
            $productPrice = AgreementProductPrice::whereHas('product', function ($q) use ($agreementId, $productId) {
                $q->where('agreement_id', '=', $agreementId)
                    ->where('product_id', '=', $productId);
            })->get();
        }

        $productPrices = [];
        foreach ($productPrice as $item) {
            if ($totalVolume < $item->interval->max_volume && $totalWeight < $item->interval->max_weight) {
                if ($item->price > 0) {
                    $productPrices[] = $item->price;
                }
            }
        }
        return $productPrices ? $productPrices[0] : '';
    }

    /*
    * get service price by interval
    *
    * @param int $productId
    * @param float $totalVolume
    * @param float $totalWeight
    * @param string $type
    *
    *
    */
    public static function getServicePriceByInterval($serviceIds, $totalVolume, $totalWeight, $agreementId, $serviceIdFromStandartProductTable = 0)
    {
        $servicePriceArr = [];
        if ($serviceIds) {
            foreach (json_decode($serviceIds) as $k => $serviceId) {
                if ($serviceIdFromStandartProductTable === 0) {
                    $servicePrice = AgreementServicePrice::with('service')->
                    whereHas('service', function ($q) use ($agreementId) {
                        $q->where('agreement_id', '=', $agreementId);
                    })->where('agreement_service_id', $serviceId)->get();
                } else {
                    $servicePrice = AgreementServicePrice::with('service')
                        ->whereHas('service', function ($q) use ($agreementId, $k, $serviceId) {
                            $q->where('agreement_id', '=', $agreementId)
                                //->where('service_id', '=', (int)$k);
                                ->where('service_id', '=', (int)$serviceId);
                        })->get();
                }
                foreach ($servicePrice as $item) {
                    if ($totalVolume < $item->interval->max_volume && $totalWeight < $item->interval->max_weight) {
                        if ($item->price > 0) {
                            $servicePriceArr['priceService'][$serviceId] = $item->price;
                            $servicePriceArr['priceServiceDefaultAgreements'][$item->service->service_id] = $item->price;
                            break;
                        }
                    }
                }
            }
        }
        // check if service price is missing
        // then add service with null price
        foreach (json_decode($serviceIds) as $k => $serviceId) {
            if ($serviceId !== null) {
                if (!array_key_exists('priceService', $servicePriceArr) || !array_key_exists($serviceId, $servicePriceArr['priceService'])) {
                    if ($serviceIdFromStandartProductTable === 0) {
                        $defaultService = AgreementService::where('id', '=', $serviceId)->first();

                        $servicePriceArr['priceService'][$serviceId] = 0;
                        $servicePriceArr['priceServiceDefaultAgreements'][$defaultService->service_id] = 0;
                    } else {
                        $defaultService = AgreementService::where('service_id', '=', $serviceId)->where('agreement_id', '=', $agreementId)->first();

                        $servicePriceArr['priceService'][$defaultService->id] = 0;
                        $servicePriceArr['priceServiceDefaultAgreements'][$serviceId] = 0;
                    }
                }
            }
        }

        $servicePriceArr['priceServiceSum'] = count($servicePriceArr) > 0 ? array_sum($servicePriceArr['priceService']) : 0;
        return $servicePriceArr;
    }

    public static function getChildBooking($id)
    {
        $booking = Booking::where('depends_on', '=', $id)->first();
        return $booking ? $booking->id : null;
    }

    public static function getChildTrip($trip)
    {
        if ($trip->type === Trip::TRIP_TYPE_DROPOFF) {
            if (self::getChildBooking($trip->booking_id)) {
                $dependentPickupTrip = Trip::where('booking_id', '=', self::getChildBooking($trip->booking_id))
                    ->where('type', '=', Trip::TRIP_TYPE_PICKUP)->first();
                return $dependentPickupTrip ? $dependentPickupTrip->id : null;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public static function markAsNullHoursProduct($booking)
    {
        if ($booking->status == Booking::BOOKING_TYPE_DELIVERED && $booking->product->price_type !== 1 && $booking->product_time_units === 0) {
            return true;
        }
        return false;
    }

    public static function markAsNullHoursServices($booking)
    {
        if ($booking->status === Booking::BOOKING_TYPE_DELIVERED) {
            $bookingService = BookingService::with('service')
                ->whereHas('service', function ($q) {
                    $q->where('price_type', '<>', 1);
                })
                ->where('booking_id', '=', $booking->id)->where('time_unit', '=', 0)->count();
            if ($bookingService > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /*

    * @param string $role
    * return
    */
    public static function nullHoursCounter(string $role)
    {
        $list = [];

        $q = BookingService::with('service', 'booking')
            ->whereHas('service', function ($q) {
                $q->where('price_type', '<>', 1);
            })
            ->whereHas('booking', function ($q) {
                $q->where('status', '=', 10)
                    ->whereNull('deleted_at');
            })
            ->where('time_unit', '=', 0);

        if ($role === 'user') {
            $bookingService = $q->where('created_by', '', Auth::user()->id)->get();
        } else {
            $bookingService = $q->get();
        }

        foreach ($bookingService as $booking) {
            $list[$booking->booking_id] = $booking->booking_id;
        }

        $productsQuery = Booking::with('product')
            ->where(function ($q) {
                $q->whereHas('product', function ($q) {
                    $q->where('price_type', '<>', 1);
                })->where('product_time_units', '=', 0);
            })
            ->where('status', '=', Booking::BOOKING_TYPE_DELIVERED)
            ->whereNull('deleted_at');
        if ($role === 'user') {
            $products = $productsQuery->where('created_by', '', Auth::user()->id)->get();
        } else {
            $products = $productsQuery->get();
        }

        foreach ($products as $v) {
            $list[$v->id] = $v->id;
        }

        return $list;
    }

    /*
     *
     */
    public static function getAvailableDates($zip, $tripId)
    {
        $routes = Route::with('products', 'services')->whereNull('deleted_at')->latest()->get();

        $availableDates = [];
        $routeIdsForTrip = [];
        $tripParams = [];

        foreach ($routes as $route) {
            if ($route->is_active === 1) {
                $zip_codes = json_decode($route->zip_codes, true);
                foreach ($zip_codes as $zip_code) {
                    if ($zip >= $zip_code['from'] && $zip < $zip_code['to']) {
                        $trip = Trip::with('booking', 'booking.bookingServices')->findOrFail($tripId);
                        $routesServices = $route->services->pluck('service_id')->toArray();
                        $routesProducts = $route->products->pluck('product_id')->toArray();
                        $bookingServices = $trip->booking->bookingServices->pluck('service_id')->toArray();
                        $services_diff = array_diff($bookingServices, $routesServices);
                        $productsDiff = in_array($trip->booking->agreement_product_id, $routesProducts);
                        //check trip  date restrictions  (weight, volume, trip number)
                        if (count($services_diff) === 0 && $productsDiff) {
                            $routeIdsForTrip[] = $route;
                        }
                    }
                }
            }
        }
        if (count($routeIdsForTrip) > 0) {
            //check if day of week of current route is suitable for date
            foreach ($routeIdsForTrip as $route) {
                if ($route->days) {
                    $days = json_decode($route->days, true);
                    $intervals = json_decode($route->intervals, true);

                    foreach ($days as $day) {
                        $workingDates = self::getWorkingDates();
                        foreach ($workingDates as $v) {
                            $dayOfWeekNumber = Carbon::parse($v)->dayOfWeek;
                            if ($day === $dayOfWeekNumber) {
                                $availableDates[$v] = [
                                    'date' => $v,
                                    'route_id' => $route->id,
                                    'from' => $intervals[$day - 1]['from'],
                                    'to' => $intervals[$day - 1]['to'],
                                ];
                            }
                        }
                    }
                }
            }

            if (count($availableDates) > 0) {
                foreach ($availableDates as $day) {
                    foreach ($routeIdsForTrip as $route) {
                        $zip_codes = json_decode($route->zip_codes, true);
                        $volume_sum = 0;
                        $weight_sum = 0;
                        $stops = 0;
                        foreach ($zip_codes as $zip_code) {
                            $similarTripsSearch = Trip::with('address', 'booking')->
                            whereHas('address', function ($q) use ($zip_code) {
                                $q->where('zip', '>=', $zip_code['from'])
                                    ->where('zip', '<=', $zip_code['to']);
                            })->
                            whereDate('tw_start', Carbon::create($day['date'])->toDateString())->get();
                            // Calculate trip data.
                            foreach ($similarTripsSearch as $trip) {
                                $volume_sum += $trip->booking->total_volume;
                                $weight_sum += $trip->booking->total_weight;
                                $stops++;
                            }
                            $tripParams[$day['date']] = [
                                'stops' => $stops,
                                'total_volume' => $volume_sum,
                                'total_weight' => $weight_sum,
                            ];
                        }
                        foreach ($tripParams as $k => $tripParam) {
                            if ($tripParam['total_weight'] > $route->max_weight ||
                                $tripParam['total_volume'] > $route->max_volume ||
                                $tripParam['stops'] > $route->max_stops
                            ) {
                                unset($availableDates[$k]);
                            }
                        }
                    }
                }
            }
        }

        sort($availableDates);
        return $availableDates;
    }

    /*
     * @return array
     */
    public static function getWorkingDates(): array
    {
        $days = [];
        for ($i = 0; $i <= 30; $i++) {
            if (count($days) === 0) {
                $currentDate = Carbon::now();
                $noon = Carbon::createFromTime(12, 0, 0);
                if ($currentDate->gt($noon)) {
                    $currentDate->addDay();
                    if ($currentDate->isWeekend()) {
                        $currentDate->nextWeekday();
                        //$currentDate->addWeekdays(2);
                    }
                }
            } else {
                $lastDate = end($days);
                $currentDate = Carbon::createFromFormat('Y-m-d', $lastDate);
            }

            $currentDate->addDay();
            if ($currentDate->isWeekend()) {
                $currentDate->nextWeekday();
            }
            while (in_array($currentDate->toDateString(), Trip::HOLIDAYS)) {
                //$currentDate->addDay();
                $currentDate->addDays(2);
                if ($currentDate->isWeekend()) {
                    $currentDate->nextWeekday();
                }
            }

            $days[] = $currentDate->toDateString();
        }
        return $days;
    }
}
