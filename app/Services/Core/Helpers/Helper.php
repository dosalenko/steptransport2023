<?php

namespace App\Services\Core\Helpers;

use App\Models\Address;
use App\Models\AgreementInterval;
use App\Models\Booking;
use App\Models\Colli;
use App\Models\Recurring;
use App\Models\StepAddress;
use App\Models\Trip;
use App\Services\Optimoroute\Optimoroute;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class Helper
{
    /*
     * @param array data
     */
    public static function setAddresses(array $data)
    {
        $pickupAddressCountryId = isset($data['pickupAddressCountryId']) ? $data['pickupAddressCountryId'] : 1;
        $dropoffAddressCountryId = isset($data['dropoffAddressCountryId']) ? $data['dropoffAddressCountryId'] : 1;
        $pickupAddress = [
            'type' => Address::ADDRESS_TYPE_PICKUP,
            'name' => $data['pickupAddressName'],
            'country_id' => $pickupAddressCountryId,
            'zip' => $data['pickupAddressZip'],
            'city' => $data['pickupAddressCity'],
            'street' => $data['pickupAddressStreet'],
            'client_id' => $data['clientId']['id'],
            'phone' => isset($data['pickupAddressPhone']) ? $data['pickupAddressPhonePrefix'] . $data['pickupAddressPhone'] : null,
            'note' => $data['pickupNote'],
        ];

        $getFirstPickupUnique = Address::where('type', Address::ADDRESS_TYPE_PICKUP)->
        where('name', $data['pickupAddressName'])->
        where('country_id', $pickupAddressCountryId)->
        where('zip', $data['pickupAddressZip'])->
        where('street', $data['pickupAddressStreet'])->
        where('client_id', $data['clientId']['id'])->oldest()->first();

        if ($getFirstPickupUnique) {
            $getFirstPickupUnique->update($pickupAddress);
            $newPickupAddress = $getFirstPickupUnique;
        } else {
            $newPickupAddress = Address::create($pickupAddress);
        }

        $dropoffAddress = [
            'type' => Address::ADDRESS_TYPE_DROPOFF,
            'name' => $data['dropoffAddressName'],
            'country_id' => $dropoffAddressCountryId,
            'zip' => $data['dropoffAddressZip'],
            'city' => $data['dropoffAddressCity'],
            'street' => $data['dropoffAddressStreet'],
            'client_id' => $data['clientId']['id'],
            'phone' => isset($data['dropoffAddressPhone']) ? $data['dropoffAddressPhonePrefix'] . $data['dropoffAddressPhone'] : null,
            'note' => $data['dropoffNote'],
        ];

        $getFirstDropoffUnique = Address::where('type', Address::ADDRESS_TYPE_DROPOFF)->
        where('name', $data['dropoffAddressName'])->
        where('country_id', $dropoffAddressCountryId)->
        where('zip', $data['dropoffAddressZip'])->
        where('street', $data['dropoffAddressStreet'])->
        where('client_id', $data['clientId']['id'])->oldest()->first();

        if ($getFirstDropoffUnique) {
            $getFirstDropoffUnique->update($dropoffAddress);
            $newDropoffAddress = $getFirstDropoffUnique;
        } else {
            $newDropoffAddress = Address::create($dropoffAddress);
        }
        return [
            'pickup' => $newPickupAddress->id,
            'dropoff' => $newDropoffAddress->id
        ];
    }

    /*
     * check if dimensions are correct
     *
     * @param int $id
     * @param float $max_volume
     * @param float $max_weight
     *
     * return bool
     */
    public static function checkIntervals(int $id, float $max_volume, float $max_weight)
    {
        $currentIntervals = AgreementInterval::where('agreement_id', $id)->get();
        if (count($currentIntervals) > 2) {
            $result = false;
            $measurementsVolume = [];
            $measurementsWeight = [];

            foreach ($currentIntervals as $k => $v) {
                $measurementsVolume[] = $v->max_volume;
                $measurementsWeight[] = $v->max_weight;
            }

            $closestVolume = [];
            foreach ($measurementsVolume as $k => $item) {
                if (array_key_exists($k + 1, $measurementsVolume)) {
                    if ($max_volume >= $item && $max_volume <= $measurementsVolume[$k + 1]) {
                        $closestVolume = [$item, $measurementsVolume[$k + 1]];
                    }
                }
            }
            if ($max_volume >= max($measurementsVolume)) {
                $closestVolume = [max($measurementsVolume), 1000000];
            }
            if ($max_volume <= min($measurementsVolume)) {
                $closestVolume = [0, min($measurementsVolume)];
            }

            $relatedWeight = [];
            $keyVolume1 = array_search($closestVolume[0], $measurementsVolume);
            if ($keyVolume1 !== false) {
                $relatedWeight[0] = $measurementsWeight[$keyVolume1];
            }
            $keyVolume2 = array_search($closestVolume[1], $measurementsVolume);
            if ($keyVolume1 !== false) {
                $relatedWeight[1] = $measurementsWeight[$keyVolume2];
            }

            if (!empty($closestVolume) && !empty($relatedWeight)) {
                if (($max_volume > $closestVolume[0] && $max_volume < $closestVolume[1])
                    &&
                    ($max_weight > $relatedWeight[0] && $max_weight < $relatedWeight[1])
                ) {
                    $result = true;
                }
            }

            if ($max_volume < min($measurementsVolume) && $max_weight < min($measurementsWeight)) {
                $result = true;
            }
            if ($max_volume > max($measurementsVolume) && $max_weight > max($measurementsWeight)) {
                $result = true;
            }
            if (!$result) {
                $result = false;
            }
        } else {
            $result = true;
        }

        return $result;
    }

    /*
     * get possible linehaul combinations.
     * Nmber of linehaul will not exceed number of 3-5 elements
     * return array
     */
    public static function getLinehauls(): array
    {
        $linehauls = [];
        $combinations = [];
        $combinations['0-0'] = 'none';
        $stepAddresses = StepAddress::all();
        foreach ($stepAddresses as $address) {
            $linehauls[$address->id] = $address->nickname;
        }
        //get possible combinations
        foreach ($linehauls as $first) {
            foreach ($linehauls as $second) {
                $combination = [$first, $second];
                if ($first === $second || in_array($combination, $combinations)) {
                    continue;
                }
                $combinations[array_search($first, $linehauls) . '-' . array_search($second, $linehauls)] = $first . '-' . $second;
            }
        }
        return $combinations;
    }

    /*
     * @param string $role
     * return int
     */
    public static function bookingStatusesCounter(string $role): int
    {
        if ($role === 'user') {
            return Booking::where('status', '=', Booking::BOOKING_TYPE_NEW)->whereNull('deleted_at')->where('created_by', '', Auth::user()->id)->count();
        } else {
            return Booking::where('status', '=', Booking::BOOKING_TYPE_NEW)->whereNull('deleted_at')->count();
        }
    }

    /*
     *
     * return int
     */
    public static function getOptimorouteTripsToUpdateCount(): int
    {
        return Trip::where('type', '<>', Trip::TRIP_TYPE_LINEHAUL)->whereNotIn('status', [Trip::TRIP_STATUS_AWAITING_DATE, Trip::TRIP_STATUS_DELIVERED])->count();
    }

    /*
    * return int
    */
    public static function getClientReferencesNumber($clientId, $reference): int
    {
        return Colli::where('reference', '=', $reference)->where('client_id', '=', $clientId)->count();
    }

    /*
     * return bool
     */
    public static function markAsColliReceived($trip): bool
    {
        $receivedCollis = [];

        if ($trip->status == Trip::TRIP_STATUS_AWAITING_DATE && $trip->booking->collis) {
            foreach ($trip->booking->collis as $colli) {
                if ($colli->location_id !== null && $colli->booking_id !== null) {
                    $receivedCollis[$trip->id][] = $colli->id;
                }
            }
        }
        return count($receivedCollis) > 0;
    }

    /*
     * return bool
    */
    public static function markAsTransferred($trip): bool
    {
        if ($trip->status == Trip::TRIP_STATUS_TRANSFERRED) {
            if ($trip->tw_start <= Carbon::today()) {
                return true;
            }
        }
        return false;
    }

    /*
     * return bool
    */
    public static function markAsScheduled($trip): bool
    {
        if ($trip->status == Trip::TRIP_STATUS_SCHEDULED) {
            if ($trip->tw_start < Carbon::today()) {
                return true;
            }
        }
        return false;
    }


    /*
    * return bool
    */
    public static function markAsLinehauled($trip): bool
    {
        return false;
    }

    /*
    * return bool
    */
    public static function getLinehaulRowClass($trip): string
    {
        if (!$trip->tw_start) {
            return 'linehaul-yellow-row';
        } else {
            $now = Carbon::now();
            $match_date = Carbon::createFromDate($trip->tw_start);
            $isSameDay = $match_date->isSameDay($now);
            $isPast = $match_date->isPast();
            if ($isSameDay) {
                return 'linehaul-green-row';
            } elseif ($isPast) {
                return 'linehaul-red-row';
            }
        }
        return '';
    }


    /*
 * return array
 */
    public static function tripStatusesCounter(): array
    {
        $trips = Trip::with('booking')->get();

        $result = [
            'total' => count($trips),
            Trip::TRIP_STATUS_AWAITING_DATE => 0,
            Trip::TRIP_STATUS_NEW => 0,
            Trip::TRIP_STATUS_AWAITING_REPLY => 0,
            Trip::TRIP_STATUS_TRANSFERRED => 0,
            Trip::TRIP_STATUS_SCHEDULED => 0,
            Trip::TRIP_STATUS_FAILED => 0,
            Trip::TRIP_FILTER_TASKS => 0,
            'linehaul' => 0
        ];
        $receivedCollis = [];
        foreach ($trips as $trip) {
            if ($trip->booking->deleted_at === null) {
                if ($trip->type <> Trip::TRIP_TYPE_LINEHAUL) {
                    if ($trip->status == Trip::TRIP_STATUS_AWAITING_DATE) {
                        // $result[Trip::TRIP_STATUS_AWAITING_DATE] += 1;

                        if ($trip->booking->collis) {
                            foreach ($trip->booking->collis as $colli) {
                                if ($colli->location_id !== null && $colli->booking_id !== null) {
                                    $receivedCollis[$trip->id][] = $colli->id;
                                }
                            }
                        }
                    } elseif ($trip->status == Trip::TRIP_STATUS_NEW) {
                        $result[Trip::TRIP_STATUS_NEW] += 1;
                    } elseif ($trip->status == Trip::TRIP_STATUS_AWAITING_REPLY) {
                        $result[Trip::TRIP_STATUS_AWAITING_REPLY] += 1;
                    } elseif ($trip->status == Trip::TRIP_STATUS_TRANSFERRED) {
                        if ($trip->tw_start <= Carbon::today()) {
                            $result[Trip::TRIP_STATUS_TRANSFERRED] += 1;
                        }
                    } elseif ($trip->status == Trip::TRIP_STATUS_SCHEDULED) {
                        if ($trip->tw_start < Carbon::today()) {
                            $result[Trip::TRIP_STATUS_SCHEDULED] += 1;
                        }
                    } elseif ($trip->status == Trip::TRIP_STATUS_FAILED) {
                        $result[Trip::TRIP_STATUS_FAILED] += 1;
                    }

                    if ($trip->is_task > 0 && $trip->status !== Trip::TRIP_STATUS_DELIVERED) {
                        $result[Trip::TRIP_FILTER_TASKS] += 1;
                    }
                } else {
                    $result['linehaul'] += 1;
                }
            }
        }
        $result[Trip::TRIP_STATUS_AWAITING_DATE] = count($receivedCollis);
        return $result;
    }

    /*
     *
     */
    public
    static function format($val)
    {
        $val = str_replace(",", ".", $val);
        $val = preg_replace('/\.(?=.*\.)/', '', $val);
        return floatval($val);
    }

    /*
     * @param array $columns
     * return @array
     */
    public static function getBookingExportColumns($columns): array
    {
        $columns_array = [];
        if (in_array('pickup_address', $columns)) {
            $pickupKey = array_search('pickup_address', $columns);
            foreach ($columns as $k => $v) {
                if ($k > $pickupKey) {
                    $columns_array[$k + 100] = $v;
                } else {
                    $columns_array[$k] = $v;
                }
            }
            $columns_array[$pickupKey] = 'pickup_name';
            $columns_array[$pickupKey + 1] = 'pickup_street';
            $columns_array[$pickupKey + 2] = 'pickup_zip';
            $columns_array[$pickupKey + 3] = 'pickup_city';
            $columns_array[$pickupKey + 4] = 'pickup_country';
            $columns_array[$pickupKey + 5] = 'pickup_phone';

            if (in_array('dropoff_address', $columns)) {
                $dropoffKey = array_search('dropoff_address', $columns_array);
                foreach ($columns_array as $k => $v) {
                    if ($k > $dropoffKey) {
                        $columns_array[$k + 100] = $v;
                        unset($columns_array[$k]);
                    } else {
                        $columns_array[$k] = $v;
                    }
                }
                $columns_array[$dropoffKey] = 'dropoff_name';
                $columns_array[$dropoffKey + 1] = 'dropoff_street';
                $columns_array[$dropoffKey + 2] = 'dropoff_zip';
                $columns_array[$dropoffKey + 3] = 'dropoff_city';
                $columns_array[$dropoffKey + 4] = 'dropoff_country';
                $columns_array[$dropoffKey + 5] = 'dropoff_phone';
            }
        }

        if (in_array('dropoff_address', $columns) && !in_array('pickup_address', $columns)) {
            $dropoffKey = array_search('dropoff_address', $columns);
            foreach ($columns as $k => $v) {
                if ($k > $dropoffKey) {
                    $columns_array[$k + 100] = $v;
                } else {
                    $columns_array[$k] = $v;
                }
            }
            $columns_array[$dropoffKey] = 'dropoff_name';
            $columns_array[$dropoffKey + 1] = 'dropoff_street';
            $columns_array[$dropoffKey + 2] = 'dropoff_zip';
            $columns_array[$dropoffKey + 3] = 'dropoff_city';
            $columns_array[$dropoffKey + 4] = 'dropoff_country';
            $columns_array[$dropoffKey + 5] = 'dropoff_phone';
        }
        if (!in_array('dropoff_address', $columns) && !in_array('pickup_address', $columns)) {
            $columns_array = $columns;
        }
        ksort($columns_array);

        return $columns_array;
    }

    /*
     * @param string $date
     * return
    */
    public static function getDriversByDate($date)
    {
        //
    }

    /*
     * @param string $date
     * return Collection
    */
    public static function updateDriversByDate($date)
    {
        $routesStops = [];
        if ($date === 'all') {
            $drivers = Trip::select('driver')->whereNotNull('driver')->groupBy('driver')->get();
        } else {
            //get trip routes
            $optimoroute = new Optimoroute();
            $routes = $optimoroute->getRoutes($date);

            //get number of stops for each driver
            if ($routes['result'] = true) {
                foreach ($routes['routes'] as $k => $v) {
                    $routesStops[$v['driverName']] = $v['stops'];
                }
            }

            foreach ($routesStops as $driver => $stopNumber) {
                $trips = Trip::whereDate('tw_start', $date)->get();
                if ($trips) {
                    foreach ($trips as $k => $v) {
                        foreach ($stopNumber as $k2 => $v2) {
                            if (!empty($v2['orderNo'])) {
                                if ((int)$v2['orderNo'] === $v->id) {
                                    $v->route_number = $v2['stopNumber'];
                                    $v->driver = $driver;
                                    $v->save();
                                }
                            }
                        }
                    }
                }
            }
            $drivers = Trip::select('driver')->whereDate('tw_start', date($date))->whereNotNull('driver')->groupBy('driver')->get();
        }
        return ['drivers' => $drivers, 'routesStops' => $routesStops];
    }

    /*
     * get associated collis from trip picklist
     *
     * @param string pickListOrders
     * @param int $type
     *
     */
    public static function getAssociatedCollis(string $pickListOrders, int $type, int $excludeSpecifiedType = 0): array
    {
        $associatedCollis = [];
        $pickListArray = json_decode($pickListOrders, true);

        if ($type === Colli::COLLI_PACKING_STATUS_PREPARED && $excludeSpecifiedType === 1) {
            $excludeCollisWithStatus = [Colli::COLLI_PACKING_STATUS_PREPARED, Colli::COLLI_PACKING_STATUS_LOADED];
        } elseif ($type === Colli::COLLI_PACKING_STATUS_LOADED && $excludeSpecifiedType === 1) {
            $excludeCollisWithStatus = [Colli::COLLI_PACKING_STATUS_LOADED];
        } else {
            $excludeCollisWithStatus = [999];
        }

        foreach ($pickListArray as $order) {
            if ($order === 'depot') {
                continue;
            }
            $trip = Trip::where('id', '=', (int)$order)->first();
            if ($trip) {
                $colli = Colli::with('location', 'booking')
                    //->where('reference', '=', $trip->booking->reference)
                    ->whereHas('booking', function ($q) use ($trip) {
                        $q->whereHas('trips', function ($q) use ($trip) {
                            $q->where('id', '=', $trip->id);
                            $q->where('type', '<>', Trip::TRIP_TYPE_PICKUP);
                        });
                    })
                    ->whereNotIn('colli_packing_status', $excludeCollisWithStatus)
                    ->get();

                foreach ($colli as $k => $v) {
                    $associatedCollis[$v->id] = [
                        'id' => $v->id,
                        'tripId' => $trip->id,
                        'location' => $v->location_id ? $v->location->name : null,
                        'volume' => $v->volume,
                        'weight' => $v->weight,
                        'totalColliInPickList' => count($pickListArray),
                        'colliPackingStatus' => $v->colli_packing_status,
                    ];
                }
            }
        }
        return $associatedCollis;
    }


    /*
     * @param array $data
     *
     * return int
     */
    public static function checkIfTourAlreadyPrepared(array $data): string
    {
        $statuses = [
            'prepared' => 0,
            'loaded' => 0,
            'new' => 0,
            'total' => 0,
        ];

        foreach ($data as $v) {
            $statuses['total'] += 1;
            if ($v['colliPackingStatus'] === Colli::COLLI_PACKING_STATUS_PREPARED) {
                $statuses['prepared'] += 1;
            } elseif ($v['colliPackingStatus'] === Colli::COLLI_PACKING_STATUS_LOADED) {
                $statuses['loaded'] += 1;
            } else {
                $statuses['new'] += 1;
            }
        }

        if ($statuses['new'] > 0) {
            return 'progress';
        } elseif ($statuses['loaded'] == $statuses['total']) {
            return 'loaded';
        } elseif ($statuses['prepared'] > 0) {
            return 'prepared';
        }

        return 'progress';
    }


    /*
     * @param string $reference
     *
     * return int
     */
    public static function getTotalColliAmountByReference(string $reference): int
    {
        return Colli::where('reference', '=', $reference)->count();
    }

    /*
     * @param int $batch
     *
     * return int
     */
    public static function getTotalColliAmountByColliBatch(int $batch): int
    {
        return Colli::where('colli_batch', '=', $batch)->count();
    }

    /*
    * @param string $reference
    * @param string $type
     *
     * return float
    */
    public static function getTotalColliMeasureByReference(string $reference, string $type): float
    {
        $measure = 0;
        $collis = Colli::where('reference', '=', $reference)->get();
        foreach ($collis as $colli) {
            if ($type === 'volume') {
                $measure += $colli->volume;
            } else {
                $measure += $colli->weight;
            }
        }
        return $measure;
    }


    /*
    * @param int $colli_batch
    * @param string $type
     *
     * return float
    */
    public static function getTotalColliMeasureByColliBatch(int $colli_batch, string $type): float
    {
        $measure = 0;
        $collis = Colli::where('colli_batch', '=', $colli_batch)->get();
        foreach ($collis as $colli) {
            if ($type === 'volume') {
                $measure += $colli->volume;
            } else {
                $measure += $colli->weight;
            }
        }
        return $measure;
    }

    public static function getTotalBookingColliMeasureByReference($collis, string $type)
    {
        $measure = 0;
        foreach ($collis as $colli) {
            if ($type === 'volume') {
                $measure += $colli->volume;
            } else {
                $measure += $colli->weight;
            }
        }
        return $measure;
    }

    /*
     * get all jobs related to current jobs
     *
     *  @param int $driver
     *  @param int $currentJob
     *
     *
     */
    public static function getRecurringDriverJobs($driver, $currentJob): array
    {
        $result = [];

        $jobs = Recurring::with('client')->where('driver_id', '=', $driver)->where('id', '<>', $currentJob)->get();
        foreach ($jobs as $job) {
            $result[] = [
                'id' => $job->id,
                'name' => $job->name,
                'client' => $job->client->name,
            ];
        }
        return $result;
    }

    /*
     *
     */
    public static function checkColliStartingFromZero($data, $groupByBookingId = null, $searchByReference = false): int
    {
        $zeroColli = false;
        if ($groupByBookingId === null) {
            if ($searchByReference === true) {
                $checkGroupByRef = Colli::where('reference', '=', $data->reference)->get();
                foreach ($checkGroupByRef as $ref) {
                    if ($ref->colli === 0) {
                        $zeroColli = true;
                        break;
                    }
                }
                return $zeroColli ? $data->colli + 1 : $data->colli;
            } else {
                $checkGroupByColliBatch = Colli::where('colli_batch', '=', $data->colli_batch)->get();
                foreach ($checkGroupByColliBatch as $ref) {
                    if ($ref->colli === 0) {
                        $zeroColli = true;
                        break;
                    }
                }
                return $zeroColli ? $data->colli + 1 : $data->colli;
            }
        } else {
            $checkGroupByColliBatch = Colli::where('colli_batch', '=', $data->colli_batch)
                ->where('booking_id', '=', $groupByBookingId)
                ->get();
            return count($checkGroupByColliBatch);
        }
    }

    /*
     *
     */
    public static function getReceivingLocation(int $warehouseId)
    {
        $receivingLocation = StepAddress::whereHas('receivingLocation', function ($q) use ($warehouseId) {
            $q->where('warehouse_id', '=', $warehouseId);
        })->first();

        return $receivingLocation ? $receivingLocation->receiving_location_id : null;
    }

    /*
     *
     */
    public static function getTotalBookingCollis(int $bookingId)
    {
        return Colli::where('booking_id', '=', $bookingId)->count();
    }

    /*
     *
     */
    public static function getColliLocations(int $bookingId)
    {
        $locationsCounted = [];
        $locations = [];
        $collis = Colli::with('location')->where('booking_id', '=', $bookingId)->get();
        foreach ($collis as $v) {
            if ($v->location_id !== null) {
                $locations[] = $v->location->name;
            }
        }
        if ($locations) {
            foreach ($locations as $v) {
                if (array_key_exists($v, $locationsCounted)) {
                    $locationsCounted[$v] += 1;
                } else {
                    $locationsCounted[$v] = 1;
                }
            }
            return $locationsCounted;
        }
        return [];
    }

    /*
     *
    */
    public static function getTotalColliBatches($batch)
    {
        return Colli::where('colli_batch', '=', $batch)->count();
    }

    public static function getTripLocationList($booking)
    {
        $res = [];
        if ($booking->collis) {
            foreach ($booking->collis as $colli) {
                if ($colli->location !== null) {
                    $res[$colli->id] = '#' . $colli->location->name . ' (' . $colli->location->warehouse->nickname . ')';
                }
            }
        }

        return array_unique($res);
    }


    public static function getCommunicationText($trip)
    {
        if ($trip->type===Trip::TRIP_TYPE_PICKUP) {
//
        } else {
//
        }
    }
}
