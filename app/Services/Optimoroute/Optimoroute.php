<?php

namespace App\Services\Optimoroute;

use App\Models\Address;
use App\Models\AgreementProduct;
use App\Models\AgreementService;
use App\Models\Booking;
use App\Models\Trip;
use Carbon\Carbon;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Ramsey\Uuid\Type\Time;
use Throwable;

final class Optimoroute
{
    const ENDPOINT = 'https://api.optimoroute.com/v1/';

    /**
     * @var Client
     */
    private $client;

    public function __construct()
    {
        $this->client = new Client(['base_uri' => self::ENDPOINT]);
    }

    /**
     * @param array $data
     * @return int
     */
    public function calcDuration(array $data): int
    {
        //spent time
        $serviceDuration = 0;

        $agreementProductId = $data['booking']['agreement_product_id'];
        $serviceProductIds = json_decode($data['booking']['service_price'], true);

        $product = AgreementProduct::with('product')->where('id', '=', (int)$agreementProductId)->first();

        if (is_array($serviceProductIds) && count($serviceProductIds) > 0) {
            foreach ($serviceProductIds as $k => $v) {
                $service = AgreementService::with('service')->where('id', '=', (int)$k)->first();
                if ($service) {
                    $serviceDuration += $service->service->duration;
                }
            }
        } else {
            $serviceDuration = 0;
        }
        $productDuration = $product ? $product->product->duration : 0;

        return $data['type_id'] === Trip::TRIP_TYPE_PICKUP ? $productDuration : $productDuration + $serviceDuration;
    }

    /**
     * @param array $related
     * @param bool $isPrefixedOrder
     * @return string|null
     */
    public function getRelatedOrder(array $related, bool $isPrefixedOrder = false)
    {
        $relatedOrder = null;

        if (empty($related)) {
            return null;
        }

        foreach ($related as $v) {
            /*  if (!in_array($v['id'], TRIP::FORCE_REWRITE_TO_PREFIXED_ORDERS)) {
                  continue;
              }*/
            if ($v['type'] === Trip::tripTypes()[Trip::TRIP_TYPE_LINEHAUL]) {
                continue;
            }
            if ($v['type'] === Trip::tripTypes()[Trip::TRIP_TYPE_PICKUP]
                && $v['status'] === Trip::TRIP_STATUS_DELIVERED) {
                continue;
            }
            $relatedOrder = $isPrefixedOrder ? 'T-' . $v['id'] : (string)$v['id'];
            break;
        }

        return $relatedOrder;
    }

    /**
     * @param string $id
     * @return JsonResponse
     * @throws GuzzleException
     */
    public function deleteOrder(string $id)
    {
        $query = ['orderNo' => $id];
        $response = $this->client->post('delete_order?key=' . env('OPTIMOROUTE_KEY'), [
            'body' => json_encode($query)
        ]);
        return json_decode($response->getBody()->getContents(), true);
    }

    /**
     * @todo refuse from mass update orders because this method doesn't support geocoding
     * @param array $data
     * @param bool $isMultiple
     * @param bool $isPrefixedOrder
     * @return JsonResponse|array
     */
    public function createOrder(array $data, $isMultiple = false, $isPrefixedOrder = false)
    {
        if ((bool)$isMultiple === true) {
            try {
                $query = [];
                //check if child return booking exists
                foreach ($data as $k => $v) {
                    if ($v['status_id'] > 0) {
                        $checkOrderExists = $this->checkOrderExists((string)$v['id']);
                        $checkPrefixedOrderExists = $this->checkOrderExists('T-' . $v['id']);
                        $isPrefixedOrder = $v['id'] > Trip::START_PREFIXED_ORDERS_FROM_ID
                            || in_array($v['id'], TRIP::FORCE_REWRITE_TO_PREFIXED_ORDERS)
                            || $checkPrefixedOrderExists
                            || !$checkOrderExists;
                        $orderNo = $isPrefixedOrder ? 'T-' . $v['id'] : (string)$v['id'];

                        $operation = $checkOrderExists || $checkPrefixedOrderExists ? 'UPDATE' : 'CREATE';
                        if ($v['type'] === 'Pick-up') {
                            $note = $v['booking']['pickup_note'] ?? '';
                        }
                        if ($v['type'] === 'Drop-off') {
                            $note = $v['booking']['dropoff_note'] ?? '';
                        }

                        $query['orders'][$k] = [
                            'operation' => $operation,
                            'orderNo' => $orderNo,
                            'relatedOrderNo' => $this->getRelatedOrder($v['depending'], $isPrefixedOrder),
                            'type' => $v['type'] === 'Pick-up' || $v['type'] === 1 ? 'P' : 'D',
                            'date' => $v['time_day'],
                            'location' => [
                                'address' => $v['address']['street'] . ', ' . $v['address']['zip'] . ', '
                                    . $v['address']['city'] . ',  Denmark',
                                'locationName' => $v['address']['name'],
                                'locationNo' => (string)$v['address']['id'],
                                'acceptPartialMatch' => true,
                                'notes' => $note
                            ],
                            'timeWindows' => [
                                [
                                    'twFrom' => (!empty($v['time_start']) && $v['time_start'] !== '00:00') ?
                                        $v['time_start'] : null,
                                    'twTo' => (!empty($v['time_end']) && $v['time_end'] !== '00:00') ?
                                        $v['time_end'] : null,
                                ]
                            ],
                            'duration' => $this->calcDuration($v),
                            'load1' => ceil((float)array_sum(array_column($v['booking']['collis'], 'weight'))), //kg
                            'load2' => (float)array_sum(array_column($v['booking']['collis'], 'volume')) * 1000, //cm3
                            'vehicleFeatures' => [],
                            'skills' => $this->getSkills($v['services'], (float)$v['booking']['total_weight']),
                            'notes' => array_key_exists('note', $v) ? $v['note'] : '',
                            'phone' => $v['address']['phone'] ?? '',
                            'email' => '',
                            'customField1' => (string)$v['booking']['total_price'],
                            'customField2' => array_key_exists('collis', $v['booking']) ?
                                (string)count($v['booking']['collis']) : "0",
                            'customField3' => $v['client']['name'],
                            'customField4' => count($v['services']) > 0 ? implode(';', $v['services']) : 'Kantsten',
                            'notificationPreference' => 'sms',
                        ];
                    }
                    if (empty($v['time_start']) || $v['time_start'] == '00:00') {
                        unset($query['orders'][$k]['timeWindows'][0]['twFrom']);
                        unset($query['orders'][$k]['timeWindows'][0]['twTo']);
                    }

                    if (empty($query['orders'][$k]['timeWindows'][0])) {
                        unset($query['orders'][$k]['timeWindows'][0]);
                    }
                }
                $result = [];
                if (array_key_exists('orders', $query)) {
                    foreach ($query['orders'] as $order) {
                        $response = $this->client->post('create_order?key=' . env('OPTIMOROUTE_KEY'), [
                            'body' => json_encode($order)
                        ]);
                        $result[$order['orderNo']] = json_decode($response->getBody()->getContents(), true);
                    }
                }
                return $result;
            } catch (Throwable $e) {
                $message = 'Failed to create order optimo route: ' . $e->getMessage() .
                    '. Message: ' . $e->getMessage();
                Log::error($message);
                return response()->json($e->getMessage(), is_numeric($e->getCode() && $e->getCode() > 0) ?
                    $e->getCode() : 500);
            }
        } else {
            try {
                if ($data['status_id'] > 0) {
                    $orderNo = $isPrefixedOrder ? 'T-' . $data['id'] : (string)$data['id'];
                    $operation = $this->checkOrderExists($orderNo) === true ? 'UPDATE' : 'CREATE';
                    if ($data['type'] === 'Pick-up') {
                        $note = $data['booking']['pickup_note'] ?? '';
                    }
                    if ($data['type'] === 'Drop-off') {
                        $note = $data['booking']['dropoff_note'] ?? '';
                    }
                    $query = [
                        'operation' => $operation,
                        'orderNo' => $orderNo,
                        'relatedOrderNo' => $this->getRelatedOrder($data['depending'], $isPrefixedOrder),
                        'type' => $data['type'] === 'Pick-up' || $data['type'] === 1 ? 'P' : 'D',
                        'date' => $data['time_day'],
                        'location' => [
                            'address' => $data['address']['street'] . ', ' . $data['address']['zip'] . ', '
                                . $data['address']['city'] . ',  Denmark',
                            'locationName' => $data['address']['name'],
                            'locationNo' => (string)$data['address']['id'],
                            'acceptPartialMatch' => true,
                            'notes' => $note
                        ],
                        'timeWindows' => [
                            [
                                'twFrom' => (!empty($data['time_start']) && $data['time_start'] !== '00:00') ?
                                    $data['time_start'] : 'null',
                                'twTo' => (!empty($data['time_end']) && $data['time_end'] !== '00:00') ?
                                    $data['time_end'] : 'null',
                            ]
                        ],
                        'duration' => $this->calcDuration($data),
                        'load1' => ceil((float)array_sum(array_column($data['booking']['collis'], 'weight'))), //kg
                        'load2' => (float)array_sum(array_column($data['booking']['collis'], 'volume')) * 1000, //cm2
                        'vehicleFeatures' => [],
                        'skills' => $this->getSkills(
                            $data['services'],
                            (float)array_sum(array_column($data['booking']['collis'], 'weight'))
                        ),
                        'notes' => array_key_exists('note', $data) ? $data['note'] : '',
                        'phone' => $data['address']['phone'] ?? '',
                        'email' => '',
                        'customField1' => (string)$data['booking']['total_price'],
                        'customField2' => array_key_exists('collis', $data['booking']) ?
                            (string)count($data['booking']['collis']) : "0",
                        'customField3' => $data['client']['name'],
                        'customField4' => count($data['services']) > 0 ? implode(';', $data['services']) : 'Kantsten',
                        'notificationPreference' => 'sms',
                    ];

                    if (empty($data['time_start']) || $data['time_start'] == '00:00') {
                        unset($query['timeWindows'][0]['twFrom']);
                        unset($query['timeWindows'][0]['twTo']);
                    }

                    if (empty($query['timeWindows'][0])) {
                        unset($query['timeWindows'][0]);
                    }

                    $response = $this->client->post('create_order?key=' . env('OPTIMOROUTE_KEY'), [
                        'body' => json_encode($query)
                    ]);
                }
                return json_decode($response->getBody()->getContents(), true);
            } catch (Throwable $e) {
                $message = 'Failed to create order optimo route: ' . $e->getMessage() .
                    '. Message: ' . $e->getMessage();
                Log::error($message);
                return response()->json($e->getMessage(), is_numeric($e->getCode() && $e->getCode() > 0) ?
                    $e->getCode() : 500);
            }
        }
    }

    /**
     *
     * @param string $id
     * @return bool
     */
    public function checkOrderExists(string $id)
    {
        try {
            $response = $this->client->get('get_orders?key=' . env('OPTIMOROUTE_KEY') . '&orderNo=' . $id, [
            ]);
            $result = (json_decode($response->getBody()->getContents(), true))['success'];
            return $result === true;
        } catch (Throwable $e) {
            return false;
        }
    }


    /**
     * Skills: Must be manually mapped. The skill "2-mands" must be included if:
     * Service is "Bortskaffelse 1 til 1" and weight of booking is >= 40kg
     * Service is "Indbæring 2 mand"
     * Service is "Indbæring m. montering (inkl. emballage med retur)"
     * Service is "Indbæring m. Montering (inkl. emballage retur og bortskaffelse af gammel møbel)"
     *
     *
     * @param array $services
     * @param float $weight
     * @return array
     */
    public function getSkills(array $services, float $weight): array
    {
        $result = [];
        if ((in_array('Bortskaffelse 1 til 1', $services) && $weight >= 40) ||
            in_array('Indbæring 2 mand', $services) ||
            in_array('Indbæring m. montering (inkl. emballage med retur)', $services) ||
            in_array('Indbæring m. Montering (inkl. emballage retur og bortskaffelse af gammel møbel)', $services)
        ) {
            $result = ['2-mands'];
        }
        return $result;
    }

    /**
     * OptimoRoute should never be able to take a trip back in its state.
     * So f.x. if I change a trip manually (in our system) from "Transferred" to "Delivered",
     * and we update status from OptimoRoute and see that it's "Scheduled",
     * we shouldn't take the order back from "Delivered" to "Scheduled".
     * OptimoRoute should only be allowed to move orders forward in the flow - never back.
     *
     * @param int $currentStatus
     * @param int $externalStatus
     * @return boolean
     */
    public function isAllowedToChangeStatus(int $currentStatus, int $externalStatus): bool
    {
        if (($currentStatus === Trip::TRIP_STATUS_TRANSFERRED
                && $externalStatus == Trip::TRIP_STATUS_SCHEDULED)
            || ($currentStatus === Trip::TRIP_STATUS_FAILED)) {
            return true;
        }
        if ($currentStatus <= $externalStatus) {
            return true;
        }
        return false;
    }

    /**
     * @param int $id
     * check for external updates
     * @throws GuzzleException
     */
    public function getOrders(int $id)
    {
        try {
            $q = Trip::where('type', '<>', Trip::TRIP_TYPE_LINEHAUL)
                ->whereNotIn('status', [Trip::TRIP_STATUS_AWAITING_DATE, Trip::TRIP_STATUS_DELIVERED]);
            if ($id > 0) {
                $q->where('id', '=', $id);
            }
            $q->where('created_at', '>=', Carbon::today()->subDays(30));
            $q->whereNotIn('id', TRIP::EXCLUDED_TRIP);
            $trips = $q->get();
            foreach ($trips as $trip) {
                $orderNo = $trip->is_prefixed_order === 1 ? 'T-' . $trip->id : $trip->id;
                $response = $this->client->get('get_orders?key=' . env('OPTIMOROUTE_KEY') . '&orderNo=' . $orderNo, [
                ]);
                $result = (json_decode($response->getBody()->getContents(), true));

                if ($result['success'] === true) {
                    $orderInfo = $result['orders'][0]['data'] ?? null;
                    if ($orderInfo) {
                        $date = $orderInfo['date'];
                        $twFrom = $orderInfo['timeWindows'][0]['twFrom'] ?? '00:00';
                        $twTo = $orderInfo['timeWindows'][0]['twTo'] ?? '00:00';

                        $trip->tw_start = Carbon::createFromFormat('Y-m-d H:i', $date . ' ' . $twFrom)
                            ->format('Y-m-d H:i:s');
                        $trip->tw_end = Carbon::createFromFormat('Y-m-d H:i', $date . ' ' . $twTo)
                            ->format('Y-m-d H:i:s');
                        $trip->note = $orderInfo['notes'] ?? '';
                        $trip->type = $orderInfo['type'] === 'P' ? Trip::TRIP_TYPE_PICKUP : Trip::TRIP_TYPE_DROPOFF;
                        if ($trip->update()) {
                            //booking data
                            $bookingData = [
                                'total_weight' => (float)$orderInfo['load1'],
                                'total_volume' => (float)$orderInfo['load2'] / 1000,
                                'spent_minutes' => (float)$orderInfo['duration'] ?? 0
                            ];
                            if ($trip->type == Trip::TRIP_TYPE_PICKUP) {
                                $bookingData['pickup_start'] = $trip->tw_start;
                                $bookingData['pickup_end'] = $trip->tw_end;
                            } elseif ($trip->type == Trip::TRIP_TYPE_DROPOFF) {
                                $bookingData['dropoff_start'] = $trip->tw_start;
                                $bookingData['dropoff_end'] = $trip->tw_end;
                            }
                            $booking = Booking::findOrFail($trip->booking_id);
                            $booking->update($bookingData);

                            // address data
                            $addressData = [
                                'phone' => $orderInfo['phone'],
                            ];
                            $address = Address::findOrFail($trip->trip_address);
                            $address->update($addressData);
                        }
                    }
                }

                //
                $response = $this->client->get('get_completion_details?key=' . env('OPTIMOROUTE_KEY') .
                    '&orderNo=' . $orderNo, [
                ]);
                $result = (json_decode($response->getBody()->getContents(), true));

                $orderInfo = $result['orders'][0]['data'] ?? null;
                if ($orderInfo) {
                    $tw_start = $orderInfo['startTime']['utcTime'] ?? null;
                    $tw_end = $orderInfo['endTime']['utcTime'] ?? null;

                    if ($tw_start) {
                        $trip->tw_start = $tw_start;
                    }
                    if ($tw_end) {
                        $trip->tw_end = $tw_end;
                    }

                    //time diff
                    if ($tw_start && $tw_end) {
                        $trip->spent_minutes = (float)(Carbon::parse($tw_end)->diffInMinutes(Carbon::parse($tw_start)));
                    }

                    //map optimoroute statuses to  internal trip statuses
                    //status on_route===servicing
                    if (array_key_exists('status', $orderInfo)) {
                        if ($orderInfo['status'] === 'on_route') {
                            $orderInfo['status'] = 'servicing';
                        }

                        $externalStatus = Trip::mapOptimoRouteStatus($orderInfo['status']);

                        $isAllowedToChangeStatus = $this->isAllowedToChangeStatus($trip->status, $externalStatus);
                        if ($isAllowedToChangeStatus) {
                            $trip->status = $externalStatus;
                        }
                    }

                    $trip->tracking_url = $orderInfo['tracking_url'] ?? $trip->tracking_url;
                    $trip->delivery_comment = $orderInfo['form']['note'] ?? $trip->delivery_comment;
                    $trip->note = $orderInfo['form']['note'] ?? $trip->note;

                    //Proof of delivery
                    $image = $orderInfo['form']['images'][0]['url'] ?? null;

                    if ($image && $trip->image === null) {
                        Log::channel('applog')->info('pod image for trip id', ['id' => $trip->id, 'image' => $image]);

                        $contents = @file_get_contents($image);
                        if ($contents === false) {

                        } else {
                            $extension = $orderInfo['form']['images'][0]['type'] == 'image/jpeg' ? 'jpeg' : 'png';
                            Storage::disk('public')
                                ->put(
                                    'optimoroute/pods/image/' . time() . '_' . $trip->id . '.' . $extension,
                                    $contents
                                );
                            $trip->image = time() . '_' . $trip->id . '.' . $extension;
                        }
                    }

                    $signature = $orderInfo['form']['signature']['url'] ?? null;
                    if ($signature && $trip->signature === null) {
                        Log::channel('applog')->info('signature for trip id', ['id' => $trip->id, 'signature' => $signature]);
                        $contents = @file_get_contents($signature);
                        if ($contents === false) {

                        } else {
                            $extension = $orderInfo['form']['signature']['type'] == 'image/jpeg' ? 'jpeg' : 'png';
                            Storage::disk('public')
                                ->put(
                                    'optimoroute/pods/signature/' . time() . '_' . $trip->id . '.' . $extension,
                                    $contents
                                );
                            $trip->signature = time() . '_' . $trip->id . '.' . $extension;
                        }
                    }
                    $trip->update();
                }

                //
                $response = $this->client->get('get_scheduling_info?key=' . env('OPTIMOROUTE_KEY') .
                    '&orderNo=' . $orderNo, [
                ]);
                $result = (json_decode($response->getBody()->getContents(), true));
                $orderScheduled = $result['orderScheduled'] ?? null;
                if ($orderScheduled) {
                    $trip->driver = $result['scheduleInformation']['driverName'] ?? $trip->driver;
                    $trip->vehicle = $result['scheduleInformation']['vehicleRegistration'] ?? $trip->driver;
                    $trip->update();
                }
            }
        } catch (Exception $e) {
            $message = 'Failed to get scheduling info from optimo route. ' . $e->getCode() . '. Message: '
                . $e->getMessage();
            Log::error($message);
        }
    }


    /**
     * @param string $date
     * get current routes
     * https://optimoroute.com/api/#get-routes
     * @throws GuzzleException
     */
    public function getRoutes(string $date)
    {
        try {
            $response = $this->client->get('get_routes?key=' . env('OPTIMOROUTE_KEY') . '&date=' . $date, [
            ]);
            return json_decode($response->getBody()->getContents(), true);
        } catch (Exception $e) {
            $message = 'Failed to get routes info from optimo route. ' . $e->getCode() .
                '. Message: ' . $e->getMessage();
            Log::error($message);
        }
    }

    /**
     *
     */
    public function getPod($id, $type)
    {
        $trip = Trip::findOrFail($id);
        $orderNo = $trip->is_prefixed_order === 1 ? 'T-' . $trip->id : $trip->id;

        $response = $this->client->get('get_completion_details?key=' . env('OPTIMOROUTE_KEY') .
            '&orderNo=' . $orderNo, [
        ]);
        $result = (json_decode($response->getBody()->getContents(), true));

        $orderInfo = $result['orders'][0]['data'] ?? null;
        if ($orderInfo) {
            //Proof of delivery

            if ($type === Trip::FILE_TYPE_IMAGE) {
                $image = $orderInfo['form']['images'][0]['url'] ?? null;

                if ($image) {
                    Log::channel('applog')->info('pod image for trip id', ['id' => $trip->id, 'image' => $image]);
                    $contents = @file_get_contents($image);
                    if ($contents === false) {

                    } else {
                        $extension = $orderInfo['form']['images'][0]['type'] == 'image/jpeg' ? 'jpeg' : 'png';
                        Storage::disk('public')
                            ->put(
                                'optimoroute/pods/image/' . time() . '_' . $trip->id . '.' . $extension,
                                $contents
                            );
                        $trip->image = time() . '_' . $trip->id . '.' . $extension;
                    }
                }
            } else {
                $signature = $orderInfo['form']['signature']['url'] ?? null;
                if ($signature) {
                    Log::channel('applog')->info('signature for trip id', ['id' => $trip->id, 'signature' => $signature]);
                    $contents = file_get_contents($signature);
                    if ($contents === false) {

                    } else {
                        $extension = $orderInfo['form']['signature']['type'] == 'image/jpeg' ? 'jpeg' : 'png';
                        Storage::disk('public')
                            ->put(
                                'optimoroute/pods/signature/' . time() . '_' . $trip->id . '.' . $extension,
                                $contents
                            );
                        $trip->signature = time() . '_' . $trip->id . '.' . $extension;
                    }
                }
            }
            $trip->update();
            return time() . '_' . $trip->id . '.' . $extension;
        }
        $trip->update();
        return null;
    }
}
