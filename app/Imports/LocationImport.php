<?php

namespace App\Imports;

use App\Models\Location;
use App\Models\StepAddress;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class LocationImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new Location([
            'name' => $row[0],
            'warehouse_id' => StepAddress::where('nickname', '=', $row[1])->first()->id,
            'type' => ($row[2] === 'Transit' || $row[2] === 'transit') ? Location::LOCATION_TYPE_TRANSIT : Location::LOCATION_TYPE_WAREHOUSE,
        ]);
    }
}
