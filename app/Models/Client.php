<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use JamesDordoy\LaravelVueDatatable\Traits\LaravelVueDatatableTrait;

/**
 * App\Models\Client
 *
 * @property int $id
 * @property string $name
 * @property string $billing_street
 * @property string $billing_zip
 * @property string $billing_city
 * @property integer $billing_country_id
 * @property string $vat
 * @property string $billing_email
 * @property integer $credit_days
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Client eloquentQuery($orderBy = 'id', $orderByDir = 'asc', $searchValue = '', $relationships = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Client newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Client newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Client query()
 * @method static \Illuminate\Database\Eloquent\Builder|Client whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Client wherePrimaryContactId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Client whereBillingAddress($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Address[] $addresses
 * @property-read int|null $addresses_count
 * @property-read \App\Models\Agreement|null $agreement
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Agreement[] $agreements
 * @property-read int|null $agreements_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Booking[] $bookings
 * @property-read int|null $bookings_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Contact[] $contacts
 * @property-read int|null $contacts_count
 * @property-read mixed $billing_address
 * @property-read mixed $primary_contact_data
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ApiKey[] $keys
 * @property-read int|null $keys_count
 * @property-read \App\Models\Contact|null $primaryContact
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|Client searchString(string $search)
 * @method static \Illuminate\Database\Eloquent\Builder|Client whereBillingCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Client whereBillingCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Client whereBillingEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Client whereBillingStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Client whereBillingZip($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Client whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Client whereCreditDays($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Client whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Client whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Client whereVat($value)
 */
class Client extends Model
{
    use HasFactory;
    use LaravelVueDatatableTrait;
    use SoftDeletes;

    public const EXCLUDED_CLIENTS = [37];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'clients';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'billing_street', 'billing_zip', 'billing_city', 'billing_country_id', 'vat', 'billing_email', 'credit_days'];

    /**
     * Filtering data based on the following attributes
     * https://jamesdordoy.github.io/laravel-vue-datatable/laravel/trait
     *
     * @var array
     */
    protected $dataTableColumns = [
        'id' => [
            'searchable' => false,
            'orderable' => false,
        ],
        'name' => [
            'searchable' => true,
            'orderable' => true,
        ],
        'billing_street' => [
            'searchable' => true,
            'orderable' => true,
        ],
        'billing_zip' => [
            'searchable' => true,
            'orderable' => true,
        ],
        'billing_city' => [
            'searchable' => true,
            'orderable' => true,
        ],
        'billing_country_id' => [
            'searchable' => true,
            'orderable' => true,
        ],
        'vat' => [
            'searchable' => true,
            'orderable' => true,
        ],
        'billing_email' => [
            'searchable' => true,
            'orderable' => true,
        ],
        'credit_days' => [
            'searchable' => true,
            'orderable' => true,
        ],
        'created_at' => [
            'searchable' => false,
            'orderable' => false,
        ],
    ];

    /**
     * @return HasOne
     */
    public function primaryContact(): HasOne
    {
        return $this->hasOne(Contact::class, 'client_id', 'id')->where('primary_contact', 1)->limit(1);
    }

    /**
     * @return HasMany
     */
    public function contacts(): HasMany
    {
        return $this->hasMany(Contact::class, 'client_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function users(): HasMany
    {
        return $this->hasMany(User::class, 'client_id', 'id')->where('active', '=', 1)->withTrashed();
    }

    /**
     * @return HasOne
     */
    public function agreement(): HasOne
    {
        return $this->hasOne(Agreement::class)->where('primary_agreement', 1);
    }

    /**
     * @return HasMany
     */
    public function agreements(): HasMany
    {
        return $this->hasMany(Agreement::class, 'client_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function bookings(): HasMany
    {
        return $this->hasMany(Booking::class, 'client_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function addresses(): HasMany
    {
        return $this->hasMany(Address::class, 'client_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function keys(): HasMany
    {
        return $this->hasMany(ApiKey::class, 'client_id', 'id');
    }

    public function getBillingAddressAttribute()
    {
        return $this->billing_street . ', ' . PHP_EOL . $this->billing_zip . ' ' . $this->billing_city . ', Denmark';
    }

    public function getPrimaryContactDataAttribute()
    {
        return $this->primaryContact->name . ', ' . PHP_EOL . $this->primaryContact->phone . ', ' . PHP_EOL . $this->primaryContact->email;
    }

    /*
     * @param int $clientId
     */
    public static function getLastMonthBookingsCount(int $clientId): int
    {
        return Booking::where('client_id', '=', $clientId)->whereNull('deleted_at')->where('created_at', '>=', Carbon::now()->subDays(30)->toDateTimeString())->count();
    }

    /*
     * @param int $clientId
     */
    public static function getLastBookingDate(int $clientId): string
    {
        $booking = Booking::where('client_id', '=', $clientId)->whereNull('deleted_at')->orderBy('created_at', 'desc')->first();
        return $booking ? Carbon::createFromFormat('Y-m-d H:i:s', $booking->created_at)->format('Y-m-d') : 'N/A';
    }

    /**
     * Scope search client/dropoff address
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $search
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearchString($query, string $search)
    {
        return $query->where('name', 'LIKE', "%$search%");
//            ->orWhereHas('addresses', function ($q) use ($search) {
//                $q->where('name', 'LIKE', "%$search%")
//                    ->orWhere('street', 'LIKE', "%$search%")
//                    ->orWhere('zip', 'LIKE', "%$search%")
//                    ->orWhere('city', 'LIKE', "%$search%")
//                    ->orWhere('phone', 'LIKE', "%$search%");
//            });
    }
}
