<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\Client
 *
 * @property int $id
 * @property int $agreement_id
 * @property double $max_weight
 * @property double $max_volume
 * @method static \Illuminate\Database\Eloquent\Builder|Client eloquentQuery($orderBy = 'id', $orderByDir = 'asc', $searchValue = '', $relationships = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Client newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Client newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Client query()
 * @mixin \Eloquent
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\AgreementProductPrice[] $agreementProductPrices
 * @property-read int|null $agreement_product_prices_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\AgreementServicePrice[] $agreementServicePrices
 * @property-read int|null $agreement_service_prices_count
 * @method static \Illuminate\Database\Eloquent\Builder|AgreementInterval whereAgreementId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AgreementInterval whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AgreementInterval whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AgreementInterval whereMaxVolume($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AgreementInterval whereMaxWeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AgreementInterval whereUpdatedAt($value)
 */
class AgreementInterval extends Model
{
    use HasFactory;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'agreements_intervals';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'agreement_id', 'max_weight', 'max_volume'];

    /**
     * @return HasMany
     */
    public function agreementProductPrices(): HasMany
    {
        return $this->hasMany(AgreementProductPrice::class, 'agreement_interval_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function agreementServicePrices(): HasMany
    {
        return $this->hasMany(AgreementServicePrice::class, 'agreement_interval_id', 'id');
    }

}
