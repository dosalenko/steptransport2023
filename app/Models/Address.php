<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Client
 *
 * @property int $id
 * @property string $name
 * @property int $counry_id
 * @property string $street
 * @property string $zip
 * @property string $city
 * @property string $att
 * @property int $client_id
 * @property int $note
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Client eloquentQuery($orderBy = 'id', $orderByDir = 'asc', $searchValue = '', $relationships = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Client newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Client newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Client query()
 * @mixin \Eloquent
 * @property int $type
 * @property int $country_id
 * @property string|null $phone
 * @property string|null $deleted_at
 * @property-read \App\Models\Client $client
 * @method static \Illuminate\Database\Eloquent\Builder|Address whereAtt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Address whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Address whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Address whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Address whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Address whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Address whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Address whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Address whereNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Address wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Address whereStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Address whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Address whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Address whereZip($value)
 */
class Address extends Model
{
    use HasFactory;

    public const ADDRESS_TYPE_PICKUP = 1;
    public const ADDRESS_TYPE_DROPOFF = 2;

    public const DEPOT_ADDRESS = 'Priorparken 851, 2605, Brøndby, Denmark';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'addresses';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'type', 'country_id', 'zip', 'city', 'street', 'att', 'client_id', 'phone', 'note'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class)->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * check if filled addresses is own Step Transport warehouses address
     *
     * @param array $data
     * @return array
     * @todo move to settings
     *
     */
    public static function isOwnAddress($data)
    {
        $isOwnAddresses = [
            'pickup' => false,
            'dropoff' => false,
        ];

        $stepAddresses = StepAddress::get();
        foreach ($stepAddresses as $address) {
            if (
                //$address->name === $data['pickupAddressName'] &&
                $address->street === $data['pickupAddressStreet'] &&
                ((int)$address->zip === (int)$data['pickupAddressZip'] || (int)$address->zip === (int)StepAddress::DEFAULT_ZIP) &&
                $address->city === $data['pickupAddressCity']
            ) {
                $isOwnAddresses['pickup'] = true;
            }

            if (
                //$address->name === $data['dropoffAddressName'] &&
                $address->street === $data['dropoffAddressStreet'] &&
                ((int)$address->zip === (int)$data['dropoffAddressCity'] || (int)$address->zip === (int)StepAddress::DEFAULT_ZIP) &&
                $address->city === $data['dropoffAddressCity']
            ) {
                $isOwnAddresses['dropoff'] = true;
            }
        }
        return $isOwnAddresses;
    }
}
