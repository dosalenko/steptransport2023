<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\AgreementService
 *
 * @property int $id
 * @property int $agreement_id
 * @property int $service_id
 * @property int $active
 * @property int $price_type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|AgreementService eloquentQuery($orderBy = 'id', $orderByDir = 'asc', $searchValue = '', $relationships = [])
 * @method static \Illuminate\Database\Eloquent\Builder|AgreementService newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AgreementService newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AgreementService query()
 * @method static \Illuminate\Database\Eloquent\Builder|AgreementService whereBillingAddress($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Agreement $agreement
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\AgreementServicePrice[] $prices
 * @property-read int|null $prices_count
 * @property-read \App\Models\Service $service
 * @method static \Illuminate\Database\Eloquent\Builder|AgreementService whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AgreementService whereAgreementId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AgreementService whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AgreementService whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AgreementService wherePriceType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AgreementService whereServiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AgreementService whereUpdatedAt($value)
 */
class AgreementService extends Model
{
    use HasFactory;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'agreements_services';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'agreement_id', 'service_id', 'price_type', 'active'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function agreement()
    {
        return $this->belongsTo(Agreement::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function service()
    {
        //return $this->belongsTo(Service::class)->withTrashed();
        return $this->belongsTo(Service::class);
    }

    /**
     * @return HasMany
     */
    public function prices(): HasMany
    {
        return $this->hasMany(AgreementServicePrice::class, 'agreement_service_id', 'id');
    }
}
