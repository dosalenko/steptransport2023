<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use JamesDordoy\LaravelVueDatatable\Traits\LaravelVueDatatableTrait;

/**
 * App\Models\Service
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $type
 * @property integer $price_type
 * @property integer $duration
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Service eloquentQuery($orderBy = 'id', $orderByDir = 'asc', $searchValue = '', $relationships = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Service newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Service newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Service query()
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\AgreementService[] $agreementServices
 * @property-read int|null $agreement_services_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ServicePrice[] $prices
 * @property-read int|null $prices_count
 * @method static \Illuminate\Database\Eloquent\Builder|Service whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Service whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Service whereDuration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Service whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Service whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Service wherePriceType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Service whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Service whereUpdatedAt($value)
 */
class Service extends Model
{
    use HasFactory;
    use LaravelVueDatatableTrait;
    //use SoftDeletes;

    public const SERVICE_TYPE_PRODUCT = 1;
    public const SERVICE_TYPE_HOURLY = 2;
    public const SERVICE_TYPE_WAREHOUSE_PRODUCT = 3;

    public const SERVICE_PRICE_TYPE_ONE_TIME = 1;
    public const SERVICE_PRICE_TYPE_HOURLY = 2;
    public const SERVICE_PRICE_TYPE_DAILY = 3;
    public const SERVICE_PRICE_TYPE_WEEKLY = 4;
    public const SERVICE_PRICE_TYPE_MONTHLY = 5;
    public const SERVICE_PRICE_TYPE_QUARTERLY = 6;
    public const SERVICE_PRICE_TYPE_YEARLY = 7;

    public const EXCHANGE_SERVICE_NAME = 'Ombytning';
    public const RETURN_SERVICE_NAME = 'Retur';

    public static function serviceTypes()
    {
        return [
            self::SERVICE_TYPE_PRODUCT => 'Service',
            self::SERVICE_TYPE_HOURLY => 'Hourly service',
            self::SERVICE_TYPE_WAREHOUSE_PRODUCT => 'Warehouse service',
        ];
    }

    public static function servicePriceTypes()
    {
        return [
            self::SERVICE_PRICE_TYPE_ONE_TIME => 1,
            self::SERVICE_PRICE_TYPE_HOURLY => 2,
            self::SERVICE_PRICE_TYPE_DAILY => 3,
            self::SERVICE_PRICE_TYPE_WEEKLY => 4,
            self::SERVICE_PRICE_TYPE_MONTHLY => 5,
            self::SERVICE_PRICE_TYPE_QUARTERLY => 6,
            self::SERVICE_PRICE_TYPE_YEARLY => 7,
        ];
    }

    public static function servicePriceTypesReadable()
    {
        return [
            self::SERVICE_PRICE_TYPE_ONE_TIME => 'One time',
            self::SERVICE_PRICE_TYPE_HOURLY => 'Hourly product',
            self::SERVICE_PRICE_TYPE_DAILY => 'Daily product',
            self::SERVICE_PRICE_TYPE_WEEKLY => 'Weekly product',
            self::SERVICE_PRICE_TYPE_MONTHLY => 'Monthly product',
            self::SERVICE_PRICE_TYPE_QUARTERLY => 'Quarterly product',
            self::SERVICE_PRICE_TYPE_YEARLY => 'Yearly product',
        ];
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'services';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'type', 'price_type', 'duration'];

    /**
     * Filtering data based on the following attributes
     * https://jamesdordoy.github.io/laravel-vue-datatable/laravel/trait
     *
     * @var array
     */
    protected $dataTableColumns = [
        'id' => [
            'searchable' => false,
            'orderable' => false,
        ],
        'name' => [
            'searchable' => true,
            'orderable' => true,
        ],
        'description' => [
            'searchable' => false,
            'orderable' => false,
        ],
        'type' => [
            'searchable' => false,
            'orderable' => false,
        ],
        'price_type' => [
            'searchable' => false,
            'orderable' => false,
        ],
    ];

    /**
     * @return HasMany
     */
    public function prices(): HasMany
    {
        return $this->hasMany(ServicePrice::class, 'service_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function agreementServices(): HasMany
    {
        return $this->hasMany(AgreementService::class, 'service_id', 'id');
    }

    /*
     * @param array $data
     * return array
     */
    public static function getServicesNames($data)
    {
        $names = [];
        $arr = json_decode($data, true);
        if (!empty($arr)) {
            foreach ($arr as $k => $v) {
                $names[] = self::findorFail($k)->name;
            }
            return $names;
        }
        return [];
    }

    /*
    * @param array $data
    * @param bool $forRecurring
     *
    * return array
    */
    public static function getServicesData($data, $forRecurring = false)
    {
        $result = [];
        $arr = json_decode($data->service_price, true);
        if (is_array($arr)) {
            foreach ($arr as $k => $v) {
                $serviceInfo = $forRecurring === true ? AgreementService::with('service')->findorFail($v) : self::findorFail($k);

                if ($forRecurring === false) {
                    $bookingService = BookingService::where('booking_id', '=', $data->id)->where('service_id', '=', $serviceInfo->id)->first();
                    $result[$serviceInfo->id]['time_unit'] = $bookingService ? $bookingService->time_unit : 0;
                } else {
                    $result[$serviceInfo->id]['time_unit'] = 0;
                }

                $result[$serviceInfo->id]['id'] = $serviceInfo->id;
                $result[$serviceInfo->id]['name'] = $forRecurring === true ? $serviceInfo->service->name : $serviceInfo->name;
                $result[$serviceInfo->id]['price_type'] = $serviceInfo->price_type;
                $result[$serviceInfo->id]['price'] = $v;
            }
        }
        return $result;
    }

    public static function getTotalServicesPrice($data)
    {
        $sum = 0;

        $arr = json_decode($data, true);
        foreach ($arr as $k => $v) {
            $sum += $v;
        }

        return $sum;
    }
}
