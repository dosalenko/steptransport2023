<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\Client
 *
 * @property int $id
 * @property int $agreement_interval_id
 * @property int $agreement_product_id
 * @property double $price
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Client eloquentQuery($orderBy = 'id', $orderByDir = 'asc', $searchValue = '', $relationships = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Client newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Client newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Client query()
 * @mixin \Eloquent
 * @property-read \App\Models\AgreementInterval $interval
 * @property-read \App\Models\AgreementProduct $product
 * @method static \Illuminate\Database\Eloquent\Builder|AgreementProductPrice whereAgreementIntervalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AgreementProductPrice whereAgreementProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AgreementProductPrice whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AgreementProductPrice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AgreementProductPrice wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AgreementProductPrice whereUpdatedAt($value)
 */
class AgreementProductPrice extends Model
{
    use HasFactory;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'agreements_products_prices';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['agreement_interval_id', 'agreement_product_id', 'price'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function interval()
    {
        return $this->belongsTo(AgreementInterval::class, 'agreement_interval_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(AgreementProduct::class, 'agreement_product_id', 'id');
    }

    /**
     * todo remove
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function price()
    {
        return $this->belongsTo(AgreementProduct::class, 'agreement_product_id', 'id');
    }
}
