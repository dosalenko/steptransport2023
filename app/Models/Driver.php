<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use JamesDordoy\LaravelVueDatatable\Traits\LaravelVueDatatableTrait;

/**
 * App\Models\Agreement
 *
 * @property int $id
 * @property string $name
 * @property integer $type
 * @property integer $optimoroute_driver_id
 * @property string $phone
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @mixin \Eloquent
 * @property-read \App\Models\StepAddress|null $warehouse
 * @method static \Illuminate\Database\Eloquent\Builder|Driver eloquentQuery($orderBy = 'id', $orderByDir = 'asc', $searchValue = '', $relationships = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Driver newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Driver newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Driver query()
 * @method static \Illuminate\Database\Eloquent\Builder|Driver searchString(string $search)
 * @method static \Illuminate\Database\Eloquent\Builder|Driver whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Driver whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Driver whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Driver whereOptimorouteDriverId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Driver wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Driver whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Driver whereUpdatedAt($value)
 */
class Driver extends Model
{
    use HasFactory;
    use LaravelVueDatatableTrait;

    public const DRIVER_TYPE_EMPLOYEE = 1;
    public const DRIVER_TYPE_EXTERNAL = 2;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'drivers';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'type', 'optimoroute_driver_id', 'phone'];

    /**
     * Filtering data based on the following attributes
     * https://jamesdordoy.github.io/laravel-vue-datatable/laravel/trait
     *
     * @var array
     */
    protected $dataTableColumns = [
        'name' => [
            'searchable' => true,
            'orderable' => true,
        ],
        'type' => [
            'searchable' => true,
            'orderable' => true,
        ],
        'optimoroute_driver_id' => [
            'searchable' => true,
            'orderable' => true,
        ],
        'phone' => [
            'searchable' => true,
            'orderable' => true,
        ],
    ];

    /*
     * @param array $columns
     * @return array
     */
    public static function getArrayForExportLocationCsv($columns): array
    {
        $groups = Location::with('warehouse');
        $export = [];
        $groups->latest()->chunk(100, function ($groups) use (&$export, $columns) {
            foreach ($groups as $k => $group) {
                $arrayGroups = [
                    //'id' => $group->id,
                    'name' => $group->name,
                    'warehouse' => $group->warehouse->nickname,
                    'type' => $group->type === 10 ? 'Transit' : 'Warehouse'
                ];
                $export[] = $arrayGroups;
            }
        });
        return $export;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function warehouse()
    {
        return $this->belongsTo(StepAddress::class, 'warehouse_id', 'id');
    }

    /**
     * Scope search
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $search
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearchString($query, string $search)
    {
        return $query->where('name', 'LIKE', "%$search%")
            ->orWhere('optimoroute_driver_id', 'LIKE', "%$search%")
            ->orWhere('phone', 'LIKE', "%$search%");
    }
}
