<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\DB;
use JamesDordoy\LaravelVueDatatable\Traits\LaravelVueDatatableTrait;

/**
 * App\Models\Agreement
 *
 * @property int $id
 * @property int $booking_id
 * @property int $client_id
 * @property int $agreement_id
 * @property int $location_id
 * @property int $trip_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @mixin \Eloquent
 * @property string $reference
 * @property string|null $name
 * @property int $colli
 * @property float $volume
 * @property float $weight
 * @property int $status
 * @property string|null $received_at
 * @property int $lastprintedas_unknown
 * @property int $colli_packing_status
 * @property int $colli_batch
 * @property int $created_by
 * @property-read \App\Models\Booking|null $booking
 * @property-read \App\Models\Client $client
 * @property-read \App\Models\Location|null $location
 * @property-read \App\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|Colli eloquentQuery($orderBy = 'id', $orderByDir = 'asc', $searchValue = '', $relationships = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Colli newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Colli newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Colli query()
 * @method static \Illuminate\Database\Eloquent\Builder|Colli searchByBooking(int $id)
 * @method static \Illuminate\Database\Eloquent\Builder|Colli searchByClient(int $id)
 * @method static \Illuminate\Database\Eloquent\Builder|Colli searchByNoDelivered()
 * @method static \Illuminate\Database\Eloquent\Builder|Colli searchByReference(string $search)
 * @method static \Illuminate\Database\Eloquent\Builder|Colli searchByWarehouse(int $id)
 * @method static \Illuminate\Database\Eloquent\Builder|Colli searchString(string $search)
 * @method static \Illuminate\Database\Eloquent\Builder|Colli whereAgreementId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Colli whereBookingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Colli whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Colli whereColli($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Colli whereColliBatch($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Colli whereColliPackingStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Colli whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Colli whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Colli whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Colli whereLastprintedasUnknown($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Colli whereLocationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Colli whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Colli whereReceivedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Colli whereReference($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Colli whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Colli whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Colli whereVolume($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Colli whereWeight($value)
 */
class Colli extends Model
{
    use HasFactory;
    use LaravelVueDatatableTrait;

    public const COLLI_STATUS_NEW = 1;
    public const COLLI_STATUS_PICKUP_ONGOING = 2;
    public const COLLI_STATUS_PICKED_UP = 3;
    public const COLLI_STATUS_ON_DISPATCH_TERMINAL = 4;
    public const COLLI_STATUS_LINEHAUL_TRANSIT = 5;
    public const COLLI_STATUS_ON_RECEIVER_TERMINAL = 6;
    public const COLLI_STATUS_ON_THE_WAY_FOR_DELIVERY = 7;
    public const COLLI_STATUS_DELIVERED = 8;

    public const COLLI_PACKING_STATUS_NEW = 1;
    public const COLLI_PACKING_STATUS_PREPARED = 2;
    public const COLLI_PACKING_STATUS_LOADED = 3;

    public const DEFAULT_AGREEMENT = 38;

    public const RECEIVED_GOODS_OWNER_EMAIL = 'booking@steptransport.dk';
    public const RECEIVED_GOODS_OWNER_NAME = 'Step Booking';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'colli';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'booking_id',
        'client_id',
        'agreement_id',
        'location_id',
        'trip_id',
        'reference',
        'name',
        'colli',
        'volume',
        'weight',
        'status',
        'received_at',
        'lastprintedas_unknown',
        'colli_batch',
        'created_by',
    ];

    /**
     * Filtering data based on the following attributes
     * https://jamesdordoy.github.io/laravel-vue-datatable/laravel/trait
     *
     * @var array
     */
    protected $dataTableColumns = [
        'id' => [
            'searchable' => true,
            'orderable' => true,
        ],
        'status' => [
            'searchable' => true,
            'orderable' => true,
        ],
        'booking_id' => [
            'searchable' => true,
            'orderable' => true,
        ],
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id', 'id')->withTrashed();
    }

    /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function booking()
    {
        return $this->belongsTo(Booking::class, 'booking_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function location()
    {
        return $this->belongsTo(Location::class, 'location_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function images(): HasMany
    {
        return $this->hasMany(Image::class, 'source_id', 'id')->where('source', '=', 'colli');
    }

    /**
     *
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $search
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearchString($query, string $search)
    {
        return $query
            ->where('id', 'LIKE', "%$search%")
            ->orWhere('booking_id', 'LIKE', "%$search%")
            ->orWhere('name', 'LIKE', "%$search%")
            ->orWhere('reference', 'LIKE', "%$search%")
            ->orWhereHas('booking', function ($q) use ($search) {
                $q->where(DB::raw('concat("B-",id)'), 'LIKE', "%$search%")
                    ->orWhere('reference', 'LIKE', "%$search%");
            });
    }

    /**
     *
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $search
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearchByReference($query, string $search)
    {
        return $query
            ->where('reference', 'like', "%$search%");
    }

    /**
     *
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearchByNoDelivered($query)
    {
        return $query
            //->whereNull('booking_id')
            ->where('status', '<>', Colli::COLLI_STATUS_DELIVERED);
    }

    /**
     *
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param int $id
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearchByClient($query, int $id)
    {
        return $query
            ->where('client_id', '=', $id);
    }

    /**
     *
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param mixed $ids
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearchByBooking($query, $ids)
    {
        return is_array($ids) ? $query->whereIn('booking_id', $ids) : $query->where('booking_id', '=', $ids);
    }

    /**
     *
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param int $id
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearchByWarehouse($query, int $id)
    {
        return $query
            ->whereHas('location', function ($q) use ($id) {
                $q->where('warehouse_id', '=', $id);
            });
    }


    /**
     * Scope search
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $dateFrom
     * @param string $dateTo
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearchByReceivedDate($query, string $dateFrom, string $dateTo)
    {
        $q = $query;
        if ($dateFrom) {
            $q->whereDate('received_at', '>=', $dateFrom);
        }
        if ($dateTo) {
            $q->whereDate('received_at', '<=', $dateTo);
        }
        return $q;
    }
}
