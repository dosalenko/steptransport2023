<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Client
 *
 * @property int $id
 * @property double $standard_price
 * @property double $minimum_price
 * @property double $cost_price
 * @property int $interval_id
 * @property int $product_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Client eloquentQuery($orderBy = 'id', $orderByDir = 'asc', $searchValue = '', $relationships = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Client newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Client newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Client query()
 * @mixin \Eloquent
 * @property-read \App\Models\Interval $interval
 * @property-read \App\Models\Product $product
 * @method static \Illuminate\Database\Eloquent\Builder|ProductPrice whereCostPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductPrice whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductPrice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductPrice whereIntervalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductPrice whereMinimumPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductPrice whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductPrice whereStandardPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductPrice whereUpdatedAt($value)
 */
class ProductPrice extends Model
{
    use HasFactory;

    public const PRODUCT_PRICE_TYPE_ONE_TIME = 1;
    public const PRODUCT_PRICE_TYPE_HOURLY = 2;
    public const PRODUCT_PRICE_TYPE_DAILY = 3;
    public const PRODUCT_PRICE_TYPE_WEEKLY = 4;
    public const PRODUCT_PRICE_TYPE_MONTHLY = 5;
    public const PRODUCT_PRICE_TYPE_QUARTERLY = 6;
    public const PRODUCT_PRICE_TYPE_YEARLY = 7;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products_prices';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['standard_price', 'minimum_price', 'cost_price', 'interval_id', 'telegram_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function interval()
    {
        return $this->belongsTo(Interval::class);
    }
}
