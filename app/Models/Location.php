<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use JamesDordoy\LaravelVueDatatable\Traits\LaravelVueDatatableTrait;

/**
 * App\Models\Agreement
 *
 * @property int $id
 * @property int $warehouse_id
 * @property string $name
 * @property integer $type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @mixin \Eloquent
 * @property-read \App\Models\StepAddress $warehouse
 * @method static \Illuminate\Database\Eloquent\Builder|Location eloquentQuery($orderBy = 'id', $orderByDir = 'asc', $searchValue = '', $relationships = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Location newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Location newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Location query()
 * @method static \Illuminate\Database\Eloquent\Builder|Location searchByType(int $type)
 * @method static \Illuminate\Database\Eloquent\Builder|Location searchByWarehouse(string $search)
 * @method static \Illuminate\Database\Eloquent\Builder|Location searchString(string $search)
 * @method static \Illuminate\Database\Eloquent\Builder|Location whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Location whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Location whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Location whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Location whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Location whereWarehouseId($value)
 */
class Location extends Model
{
    use HasFactory;
    use LaravelVueDatatableTrait;

    public const LOCATION_TYPE_TRANSIT = 10;
    public const LOCATION_TYPE_WAREHOUSE = 20;

    public const DEFAULT_LOCATION_NAME = 'Varemodtagelse';
    public const DEFAULT_PREPARED_LOCATION_ID = 83;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'locations';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['warehouse_id', 'name', 'type'];


    /**
     * Filtering data based on the following attributes
     * https://jamesdordoy.github.io/laravel-vue-datatable/laravel/trait
     *
     * @var array
     */
    protected $dataTableColumns = [
        'name' => [
            'searchable' => true,
            'orderable' => true,
        ],
        'warehouse_id' => [
            'searchable' => true,
            'orderable' => true,
        ],
        'type' => [
            'searchable' => true,
            'orderable' => true,
        ],
    ];

    /*
     * @param array $columns
     * @return array
     */
    public static function getArrayForExportLocationCsv($columns): array
    {
        $groups = Location::with('warehouse');
        $export = [];
        $groups->latest()->chunk(100, function ($groups) use (&$export, $columns) {
            foreach ($groups as $k => $group) {
                $arrayGroups = [
                    //'id' => $group->id,
                    'name' => $group->name,
                    'warehouse' => $group->warehouse->nickname,
                    'type' => $group->type === 10 ? 'Transit' : 'Warehouse'
                ];
                $export[] = $arrayGroups;
            }
        });
        return $export;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function warehouse()
    {
        return $this->belongsTo(StepAddress::class, 'warehouse_id', 'id');
    }

    /**
     * Scope search
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $search
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearchString($query, string $search)
    {
        return $query->where('name', 'LIKE', "%$search%");
    }

    /**
     * Scope search
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param int $warehouse
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearchByWarehouse($query, string $search)
    {
        return $query->where('name', 'LIKE', "%$search%");
    }

    /**
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param int $type
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearchByType($query, int $type)
    {
        return $query->where('type', '=', $type);
    }
}
