<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\BookingService
 *
 * @property int $id
 * @property int $booking_id
 * @property int $service_id
 * @property float $price
 * @property int $time_unit
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|BookingService newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BookingService newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BookingService query()
 * @method static \Illuminate\Database\Eloquent\Builder|BookingService whereBookingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BookingService whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BookingService whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BookingService wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BookingService whereServiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BookingService whereTimeUnit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BookingService whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class BookingService extends Model
{
    use HasFactory;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function booking()
    {
        return $this->belongsTo(Booking::class, 'booking_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function service()
    {
        //return $this->belongsTo(Service::class, 'service_id', 'id')->withTrashed();
        return $this->belongsTo(Service::class, 'service_id', 'id');
    }

}
