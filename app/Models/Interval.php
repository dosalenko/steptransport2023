<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\Client
 *
 * @property int $id
 * @property double $max_weight
 * @property double $max_volume
 * @method static \Illuminate\Database\Eloquent\Builder|Client eloquentQuery($orderBy = 'id', $orderByDir = 'asc', $searchValue = '', $relationships = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Client newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Client newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Client query()
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProductPrice[] $productPrices
 * @property-read int|null $product_prices_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ServicePrice[] $servicePrices
 * @property-read int|null $service_prices_count
 * @method static \Illuminate\Database\Eloquent\Builder|Interval whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Interval whereMaxVolume($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Interval whereMaxWeight($value)
 */
class Interval extends Model
{
    use HasFactory;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'intervals';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['max_weight', 'max_volume'];

    public $timestamps = false;

    /**
     * @return HasMany
     */
    public function productPrices(): HasMany
    {
        return $this->hasMany(ProductPrice::class, 'interval_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function servicePrices(): HasMany
    {
        return $this->hasMany(ServicePrice::class, 'interval_id', 'id');
    }
}
