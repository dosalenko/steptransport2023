<?php

namespace App\Models;

use App\Services\Core\Helpers\Helper;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\DB;
use JamesDordoy\LaravelVueDatatable\Traits\LaravelVueDatatableTrait;

/**
 * App\Models\Trip
 *
 * @property int $id
 * @property int $status
 * @property int $type
 * @property string $optimoroute_result
 * @property integer $is_task
 * @property integer $route_number
 * @property integer $is_prefixed_order
 * @property string $barcode
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Trip eloquentQuery($orderBy = 'id', $orderByDir = 'asc', $searchValue = '', $relationships = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Trip newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Trip newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Trip query()
 * @method static \Illuminate\Database\Eloquent\Builder|Trip whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Trip whereStatus($value)
 * @mixin \Eloquent
 * @property string $trip_address
 * @property string|null $tw_start
 * @property string|null $tw_end
 * @property string|null $note
 * @property int $booking_id
 * @property int $client_id
 * @property float $spent_minutes
 * @property string|null $driver
 * @property string|null $vehicle
 * @property string|null $delivery_comment
 * @property string|null $tracking_url
 * @property string|null $image
 * @property string|null $signature
 * @property string|null $deleted_at
 * @property-read \App\Models\Address|null $address
 * @property-read \App\Models\Booking $booking
 * @property-read \App\Models\Client $client
 * @property-read string $address_data
 * @property-read string $date
 * @property-read array $depending
 * @property-read string $dropoff_address_data
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TourStop[] $tourStops
 * @property-read int|null $tour_stops_count
 * @method static \Illuminate\Database\Eloquent\Builder|Trip searchByColliReceived()
 * @method static \Illuminate\Database\Eloquent\Builder|Trip searchByDate(string $dateFrom, string $dateTo)
 * @method static \Illuminate\Database\Eloquent\Builder|Trip searchByDriver(string $driver)
 * @method static \Illuminate\Database\Eloquent\Builder|Trip searchByLinehaulType()
 * @method static \Illuminate\Database\Eloquent\Builder|Trip searchByNullStatus()
 * @method static \Illuminate\Database\Eloquent\Builder|Trip searchByStatus(int $status)
 * @method static \Illuminate\Database\Eloquent\Builder|Trip searchByTasks()
 * @method static \Illuminate\Database\Eloquent\Builder|Trip searchByZip(int $zip)
 * @method static \Illuminate\Database\Eloquent\Builder|Trip searchString(string $search)
 * @method static \Illuminate\Database\Eloquent\Builder|Trip whereBookingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Trip whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Trip whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Trip whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Trip whereDeliveryComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Trip whereDriver($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Trip whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Trip whereIsTask($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Trip whereNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Trip whereOptimorouteResult($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Trip whereRouteNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Trip whereSignature($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Trip whereSpentMinutes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Trip whereTrackingUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Trip whereTripAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Trip whereTwEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Trip whereTwStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Trip whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Trip whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Trip whereVehicle($value)
 */
class Trip extends Model
{
    use HasFactory;
    use LaravelVueDatatableTrait;

    public const TRIP_TYPE_PICKUP = 1;
    public const TRIP_TYPE_LINEHAUL = 2;
    public const TRIP_TYPE_DROPOFF = 3;

    public const TRIP_STATUS_AWAITING_DATE = 0;
    public const TRIP_STATUS_NEW = 1;
    public const TRIP_STATUS_TRANSFERRED = 2;
    public const TRIP_STATUS_SCHEDULED = 3; //Allocated is another name
    public const TRIP_STATUS_SERVICING = 4;
    public const TRIP_STATUS_FAILED = 5;
    public const TRIP_STATUS_REJECTED = 6;
    public const TRIP_STATUS_DELIVERED = 7;
    public const TRIP_STATUS_AWAITING_REPLY = 8; //according to last updates - should be between new and transferred

    public const TRIP_FILTER_LINEHAUL = 99;
    public const TRIP_FILTER_TASKS = 88;

    public const OPTIMOROUTE_STATUS_UNSCHEDULED = 2;
    public const OPTIMOROUTE_STATUS_SCHEDULED = 3;
    public const OPTIMOROUTE_STATUS_SERVICING = 4;
    public const OPTIMOROUTE_STATUS_FAILED = 5;
    public const OPTIMOROUTE_STATUS_REJECTED = 6;
    public const OPTIMOROUTE_STATUS_SUCCESS = 7;

    public const DRIVERS_ALL = 0;
    public const DRIVERS_UNASSIGNED = 1;

    public const EXCLUDED_TRIP = [3096, 5888];
    public const START_PREFIXED_ORDERS_FROM_ID = 12401;
    public const FORCE_REWRITE_TO_PREFIXED_ORDERS = [12389, 12181, 12175, 12171, 12161];

    public const FILE_TYPE_IMAGE = 1;
    public const FILE_TYPE_SIGNATURE = 2;

    public const HOLIDAYS = [
        '2023-01-01',
        '2023-04-06',
        '2023-04-07',
        '2023-04-09',
        '2023-04-10',
        '2023-05-05',
        '2023-05-18',
        '2023-05-28',
        '2023-05-30',
        '2023-12-25',
        '2023-12-26',

        '2024-01-01',
        '2024-03-28',
        '2024-03-29',
        '2024-03-31',
        '2024-04-01',
        '2024-04-26',
        '2024-05-09',
        '2024-05-19',
        '2024-05-20',
        '2024-12-25',
        '2024-12-26',
    ];

    /*
    * @return array
    *
    */
    public static function tripStatuses()
    {
        return [
            self::TRIP_STATUS_AWAITING_DATE => 'Awaiting date',
            self::TRIP_STATUS_NEW => 'new',
            self::TRIP_STATUS_AWAITING_REPLY => 'Awaiting reply',
            self::TRIP_STATUS_TRANSFERRED => 'Transferred',
            self::TRIP_STATUS_SCHEDULED => 'Scheduled',
            self::TRIP_STATUS_SERVICING => 'Servicing',
            self::TRIP_STATUS_FAILED => 'Failed',
            self::TRIP_STATUS_REJECTED => 'Rejected',
            self::TRIP_STATUS_DELIVERED => 'Delivered',
        ];
    }

    /*
    * @return array
    *
    */
    public static function optimoRouteStatuses()
    {
        return [
            self::OPTIMOROUTE_STATUS_UNSCHEDULED => 'unscheduled',
            self::OPTIMOROUTE_STATUS_SCHEDULED => 'scheduled',
            self::OPTIMOROUTE_STATUS_SERVICING => 'servicing',
            self::OPTIMOROUTE_STATUS_FAILED => 'failed',
            self::OPTIMOROUTE_STATUS_REJECTED => 'rejected',
            self::OPTIMOROUTE_STATUS_SUCCESS => 'success',
        ];
    }

    /*
    * @param string $status
    * @return int
    */
    public static function mapOptimoRouteStatus(string $status): int
    {
        return array_search($status, self::optimoRouteStatuses(), false) ?? self::OPTIMOROUTE_STATUS_UNSCHEDULED;
    }

    /*
    * @return array
    *
    */
    public static function tripTypes()
    {
        return [
            self::TRIP_TYPE_PICKUP => 'Pick-up',
            self::TRIP_TYPE_LINEHAUL => 'Linehaul',
            self::TRIP_TYPE_DROPOFF => 'Drop-off',
        ];
    }

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id',
    ];

    /**
     * Filtering data based on the following attributes
     * https://jamesdordoy.github.io/laravel-vue-datatable/laravel/trait
     *
     * @var array
     */
    protected $dataTableColumns = [
        'id' => [
            'searchable' => true,
            'orderable' => true,
        ],
        'status' => [
            'searchable' => true,
            'orderable' => true,
        ],
        'type' => [
            'searchable' => true,
            'orderable' => true,
        ],
        'trip_address' => [
            'searchable' => true,
            'orderable' => true,
        ],
        'tw_start' => [
            'searchable' => true,
            'orderable' => true,
        ],
        'tw_end' => [
            'searchable' => true,
            'orderable' => true,
        ],
        'internal_comment' => [
            'searchable' => true,
            'orderable' => true,
        ],
    ];

    /*
     * @param array $ids
     * @return array
    */
    public static function getArrayForExportTripCsv($ids): array
    {
        $groups = Trip::with(
            'booking',
            'booking.collis',
            'booking.dropoffAddress',
            'address',
            'client'
        )->whereIn('id', array_keys($ids));

        $export = [];
        $groups->latest()->chunk(100, function ($groups) use (&$export, $ids) {
            foreach ($groups as $group) {
                $arrayGroups = [
                    'id' => $group->id
                ];
                $arrayGroups['BookingId'] = $group->booking->id;
                $arrayGroups['Trip'] = $group->type === Trip::TRIP_TYPE_LINEHAUL ?
                    $group->booking->dropoffAddress->name : $group->address->name;
                $arrayGroups['Client'] = $group->client->name;
                $arrayGroups['Booking'] = Booking::bookingStatuses()[$group->booking->status];
                $arrayGroups['Status'] = Trip::tripStatuses()[$group->status];
                $arrayGroups['BookingDate'] = Carbon::createFromFormat('Y-m-d H:i:s', $group->booking->created_at)
                    ->format('Y-m-d');
                $arrayGroups['Type'] = Trip::tripTypes()[$group->type];
                $arrayGroups['Address'] = $group->booking->dropoffAddress->name . ',
' . $group->booking->dropoffAddress->street . ' ' . $group->booking->dropoffAddress->zip . ',
' . $group->booking->dropoffAddress->city;
                $arrayGroups['Time'] = $group->tw_start;
                $arrayGroups['Volume'] = str_replace('.', ', ', $group->booking->total_volume);
                $arrayGroups['Weight'] = str_replace('.', ', ', $group->booking->total_weight);
                $arrayGroups['Locations'] = implode(', ', Helper::getTripLocationList($group->booking));
                $arrayGroups['Services'] = implode(', ', Service::getServicesNames($group->booking->service_price));
                $arrayGroups['Note'] = $group->booking->note;
                $arrayGroups['Reference'] = $group->booking->reference;
                $arrayGroups['Colli'] = $group->booking->collis->count();
                $arrayGroups['InternalNote'] = $group->booking->internal_comment;
                $arrayGroups['Zip'] = $group->booking->dropoffAddress->zip;

                $export[] = $arrayGroups;
            }
        });
        return $export;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id', 'id')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function booking()
    {
        return $this->belongsTo(Booking::class, 'booking_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function address()
    {
        return $this->belongsTo(Address::class, 'trip_address', 'id');
    }

    /**
     * @return HasMany
     */
    public function tourStops(): HasMany
    {
        return $this->hasMany(TourStop::class, 'trip_id', 'id');
    }

    /**
     * @param int $tripType
     * @return string
     */
    public static function getTripAdress($tripType)
    {
        if ($tripType === self::TRIP_TYPE_LINEHAUL) {
            return '';
        }
    }

    /**
     * @return string
     */
    public function getAddressDataAttribute()
    {
        return $this->address->name . ', '
            . PHP_EOL . $this->address->street . ', '
            . PHP_EOL . $this->address->zip . ', '
            . PHP_EOL . $this->address->city;
    }

    /**
     * @return string
     */
    public function getDropoffAddressDataAttribute()
    {
        if ($this->booking->dropoffAddress) {
            return $this->booking->dropoffAddress->name . ', '
                . PHP_EOL . $this->booking->dropoffAddress->street . ', '
                . PHP_EOL . $this->booking->dropoffAddress->zip . ', '
                . PHP_EOL . $this->booking->dropoffAddress->city;
        } else {
            return null;
        }
    }

    /**
     * @return string
     */
    public function getDateAttribute()
    {
        $day = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->tw_start)->format('Y-m-d');
        $timeStart = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->tw_start)->format('H:i');
        $timeEnd = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->tw_end)->format('H:i');

        if ($timeStart === '00:00') {
            $timeStart = '';
        } else {
            $timeStart .= ' - ';
        }

        if ($timeEnd === '00:00') {
            $timeEnd = '';
        }

        return $day . ' '
            . PHP_EOL . $timeStart
            . PHP_EOL . $timeEnd;
    }

    /**
     * This indicates what other trip this trip is depending on. Logic is like this:
     * If trip type is 3:
     * If booking has another trip with type 2, it is dependent on that trip.
     * Else if booking has another trip with type 1, it is dependent on that trip.
     * Else it is not dependent on any trip.
     * If trip type is 2:
     * If booking has another trip with type 1, it is dependent on that trip.
     * Else it is not dependent on any trip.
     *
     * @return array
     */
    public function getDependingAttribute()
    {
        $list = [];
        foreach ($this->booking->trips as $trip) {
            if ($this->type === self::TRIP_TYPE_DROPOFF) {
                if ($trip->type === self::TRIP_TYPE_PICKUP) {
                    $list[$trip->id]['type'] = 'Pick-up';
                    $list[$trip->id]['id'] = $trip->id;
                    $list[$trip->id]['status'] = $trip->status;
                }
                if ($trip->type === self::TRIP_TYPE_LINEHAUL) {
                    $list[$trip->id]['type'] = 'Linehaul';
                    $list[$trip->id]['id'] = $trip->id;
                    $list[$trip->id]['status'] = $trip->status;
                }
            }
            if ($this->type === self::TRIP_TYPE_LINEHAUL) {
                if ($trip->type === self::TRIP_TYPE_PICKUP) {
                    $list[$trip->id]['type'] = 'Pick-up';
                    $list[$trip->id]['id'] = $trip->id;
                    $list[$trip->id]['status'] = $trip->status;
                }
            }
        }
        return $list;
    }

    /**
     * Scope search client/dropoff address
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $search
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearchString($query, string $search)
    {
        return $query->whereHas('client', function ($q) use ($search) {
            $q->where('name', 'LIKE', "%$search%");
        })->orWhereHas('booking.dropoffAddress', function ($q) use ($search) {
            $q->where('name', 'LIKE', "%$search%")
                ->orWhere('street', 'LIKE', "%$search%")
                ->orWhere('zip', 'LIKE', "%$search%")
                ->orWhere('city', 'LIKE', "%$search%")
                ->orWhere('phone', 'LIKE', "%$search%");
        })->orWhereHas('booking.pickupAddress', function ($q) use ($search) {
            $q->where('name', 'LIKE', "%$search%")
                ->orWhere('street', 'LIKE', "%$search%")
                ->orWhere('zip', 'LIKE', "%$search%")
                ->orWhere('city', 'LIKE', "%$search%")
                ->orWhere('phone', 'LIKE', "%$search%");
        })->orWhereHas('booking.collis', function ($q) use ($search) {
            $q->where('reference', 'LIKE', "%$search%");
        })->orWhere('id', 'LIKE', "%$search%")
            ->orWhere(DB::raw('concat("T-", id)'), 'LIKE', "%$search%")
            ->orWhere('note', 'LIKE', "%$search%");
    }

    /**
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearchByColliReceived($query)
    {
        return $query->where('status', '=', self::TRIP_STATUS_AWAITING_DATE)
            ->where('type', '<>', Trip::TRIP_TYPE_LINEHAUL);
    }

    /**
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param int $zip
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearchByZip($query, int $zip)
    {
        return $query->whereHas('booking.dropoffAddress', function ($q) use ($zip) {
            if ($zip === 1) {
                $q->where('zip', '<', 5000);
            } else {
                $q->where('zip', '>=', 5000);
            }
        });
    }

    /**
     *
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearchByTasks($query)
    {
        return $query->where('is_task', '=', 1)->where('status', '<>', Trip::TRIP_STATUS_DELIVERED);
    }

    /**
     *
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearchByLinehaulType($query)
    {
        return $query->where('type', '=', Trip::TRIP_TYPE_LINEHAUL)->where('status', '<>', Trip::TRIP_STATUS_DELIVERED);
    }

    /**
     *
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param int $status
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearchByStatus($query, int $status)
    {
        return $query->where('status', '=', $status)->where('type', '<>', Trip::TRIP_TYPE_LINEHAUL);
    }

    /**
     *
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearchByNullStatus($query)
    {
        return $query->where('type', '<>', Trip::TRIP_TYPE_LINEHAUL);
    }

    /**
     *
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $dateFrom
     * @param string $dateTo
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearchByDate($query, string $dateFrom, string $dateTo)
    {
        if ($dateFrom === $dateTo) {
            return $query->whereDate('tw_start', $dateFrom);
        } else {
            return $query->whereBetween('tw_start', [$dateFrom, $dateTo]);
        }
    }

    /**
     *
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $driver
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearchByDriver($query, string $driver)
    {
        return $query->where('driver', $driver);
    }
}
