<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use JamesDordoy\LaravelVueDatatable\Traits\LaravelVueDatatableTrait;

/**
 * App\Models\Agreement
 *
 * @property int $id
 * @property string $name
 * @property int $primary_agreement
 * @property int $default_pickup_address_id
 * @property int $default_dropoff_address_id
 * @property int $client_id
 * @property int $client_contacts_id
 * @property int $is_prices_copied
 * @property int $is_prices_modified
 * @property int $is_locked_products
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Client eloquentQuery($orderBy = 'id', $orderByDir = 'asc', $searchValue = '', $relationships = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Client newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Client newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Client query()
 * @mixin \Eloquent
 * @property int $is_deleted
 * @property string|null $deleted_at
 * @property-read \App\Models\Client $client
 * @property-read \App\Models\Address|null $dropoffAddress
 * @property-read \App\Models\Address|null $pickupAddress
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\AgreementProduct[] $products
 * @property-read int|null $products_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\AgreementService[] $services
 * @property-read int|null $services_count
 * @method static \Illuminate\Database\Eloquent\Builder|Agreement whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Agreement whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Agreement whereDefaultDropoffAddressId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Agreement whereDefaultPickupAddressId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Agreement whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Agreement whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Agreement whereIsDeleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Agreement whereIsPricesCopied($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Agreement whereIsPricesModified($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Agreement whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Agreement wherePrimaryAgreement($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Agreement whereUpdatedAt($value)
 */
class Agreement extends Model
{
    use HasFactory;
    use LaravelVueDatatableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'agreements';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'primary_agreement',
        'default_pickup_address_id', 'default_dropoff_address_id',
        'client_id', 'client_contacts_id', 'is_prices_copied', 'is_prices_modified','is_locked_products'];

    /**
     * Filtering data based on the following attributes
     * https://jamesdordoy.github.io/laravel-vue-datatable/laravel/trait
     *
     * @var array
     */
    protected $dataTableColumns = [
        'id' => [
            'searchable' => false,
            'orderable' => false,
        ],
        'name' => [
            'searchable' => true,
            'orderable' => true,
        ],
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class)->withTrashed();
    }

    public function pickupAddress()
    {
        return $this->hasOne(Address::class, 'id', 'default_pickup_address_id')
            ->where('type', 1);
            //->whereNull('deleted_at');
    }

    public function dropoffAddress()
    {
        return $this->hasOne(Address::class, 'id', 'default_dropoff_address_id')
            ->where('type', 2);
            //->whereNull('deleted_at');
    }

    /**
     * @return HasMany
     */
    public function products(): HasMany
    {
        return $this->hasMany(AgreementProduct::class, 'agreement_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function services(): HasMany
    {
        return $this->hasMany(AgreementService::class, 'agreement_id', 'id');
    }
}
