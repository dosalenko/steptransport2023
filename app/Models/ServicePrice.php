<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\Client
 *
 * @property int $id
 * @property double $standard_addon_price
 * @property double $minimum_price
 * @property double $cost_price
 * @property int $interval_id
 * @property int $service_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Client eloquentQuery($orderBy = 'id', $orderByDir = 'asc', $searchValue = '', $relationships = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Client newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Client newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Client query()
 * @mixin \Eloquent
 * @property-read \App\Models\Interval $interval
 * @property-read \App\Models\Service $service
 * @method static \Illuminate\Database\Eloquent\Builder|ServicePrice whereCostPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServicePrice whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServicePrice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServicePrice whereIntervalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServicePrice whereMinimumPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServicePrice whereServiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServicePrice whereStandardAddonPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServicePrice whereUpdatedAt($value)
 */
class ServicePrice extends Model
{
    use HasFactory;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'services_prices';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['standard_addon_price', 'minimum_price', 'cost_price', 'interval_id', 'service_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function service()
    {
        return $this->belongsTo(Service::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function interval()
    {
        return $this->belongsTo(Interval::class);
    }
}
