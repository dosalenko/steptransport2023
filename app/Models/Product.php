<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use JamesDordoy\LaravelVueDatatable\Traits\LaravelVueDatatableTrait;

/**
 * App\Models\Client
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $type
 * @property integer $price_type
 * @property integer $duration
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Product eloquentQuery($orderBy = 'id', $orderByDir = 'asc', $searchValue = '', $relationships = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Product newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Product newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Product query()
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\AgreementProduct[] $agreementProducts
 * @property-read int|null $agreement_products_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProductPrice[] $prices
 * @property-read int|null $prices_count
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereDuration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product wherePriceType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereUpdatedAt($value)
 */
class Product extends Model
{
    use HasFactory;
    use LaravelVueDatatableTrait;
    use SoftDeletes;

    public const PRODUCT_TYPE_PRODUCT = 1;
    public const PRODUCT_TYPE_HOURLY = 2;
    public const PRODUCT_TYPE_WAREHOUSE_PRODUCT = 3;

    public const PRODUCT_PRICE_TYPE_ONE_TIME = 1;
    public const PRODUCT_PRICE_TYPE_HOURLY = 2;
    public const PRODUCT_PRICE_TYPE_DAILY = 3;
    public const PRODUCT_PRICE_TYPE_WEEKLY = 4;
    public const PRODUCT_PRICE_TYPE_MONTHLY = 5;
    public const PRODUCT_PRICE_TYPE_QUARTERLY = 6;
    public const PRODUCT_PRICE_TYPE_YEARLY = 7;

    public static function productTypes()
    {
        return [
            self::PRODUCT_TYPE_PRODUCT => 'Product',
            self::PRODUCT_TYPE_HOURLY => 'Hourly product',
            self::PRODUCT_TYPE_WAREHOUSE_PRODUCT => 'Warehouse product',
        ];
    }

    public static function productPriceTypes()
    {
        return [
            self::PRODUCT_PRICE_TYPE_ONE_TIME => 1,
            self::PRODUCT_PRICE_TYPE_HOURLY => 2,
            self::PRODUCT_PRICE_TYPE_DAILY => 3,
            self::PRODUCT_PRICE_TYPE_WEEKLY => 4,
            self::PRODUCT_PRICE_TYPE_MONTHLY => 5,
            self::PRODUCT_PRICE_TYPE_QUARTERLY => 6,
            self::PRODUCT_PRICE_TYPE_YEARLY => 7,
        ];
    }

    public static function productPriceTypesReadable()
    {
        return [
            self::PRODUCT_PRICE_TYPE_ONE_TIME => 'One time',
            self::PRODUCT_PRICE_TYPE_HOURLY => 'Hourly product',
            self::PRODUCT_PRICE_TYPE_DAILY => 'Daily product',
            self::PRODUCT_PRICE_TYPE_WEEKLY => 'Weekly product',
            self::PRODUCT_PRICE_TYPE_MONTHLY => 'Monthly product',
            self::PRODUCT_PRICE_TYPE_QUARTERLY => 'Quarterly product',
            self::PRODUCT_PRICE_TYPE_YEARLY => 'Yearly product',
        ];
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'type', 'price_type', 'duration'];

    /**
     * Filtering data based on the following attributes
     * https://jamesdordoy.github.io/laravel-vue-datatable/laravel/trait
     *
     * @var array
     */
    protected $dataTableColumns = [
        'id' => [
            'searchable' => false,
            'orderable' => false,
        ],
        'name' => [
            'searchable' => false,
            'orderable' => false,
        ],
        'description' => [
            'searchable' => false,
            'orderable' => false,
        ],
        'type' => [
            'searchable' => false,
            'orderable' => false,
        ],
        'price_type' => [
            'searchable' => false,
            'orderable' => false,
        ],
    ];

    /**
     * @return HasMany
     */
    public function prices(): HasMany
    {
        return $this->hasMany(ProductPrice::class, 'product_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function agreementProducts(): HasMany
    {
        return $this->hasMany(AgreementProduct::class, 'product_id', 'id');
    }
}
