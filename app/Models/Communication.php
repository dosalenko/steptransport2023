<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Communication
 *
 * @property int $id
 * @property int|null $user_id
 * @property int $communication_type
 * @property int $message_type
 * @property string $message
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Communication newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Communication newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Communication query()
 * @method static \Illuminate\Database\Eloquent\Builder|Communication whereCommunicationType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Communication whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Communication whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Communication whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Communication whereMessageType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Communication whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Communication whereUserId($value)
 * @mixin \Eloquent
 */
class Communication extends Model
{
    use HasFactory;

    public const COMMUNICATION_TYPE_SMS = 1;
    public const COMMUNICATION_TYPE_EMAIL = 2;
    public const COMMUNICATION_TYPE_UNASSIGNED = 3;

    public const MESSAGE_TYPE_DATE_ASSIGNED = 0;
    public const MESSAGE_TYPE_PU_CONFIRMATION = 1;
    public const MESSAGE_TYPE_PU_RECONFIRMATION = 2;
    public const MESSAGE_TYPE_PU_TOMORROW = 3;
    public const MESSAGE_TYPE_PU_RATE = 4;
    public const MESSAGE_TYPE_DO_CONFIRMATION = 5;
    public const MESSAGE_TYPE_DO_RECONFIRMATION = 6;
    public const MESSAGE_TYPE_DO_TOMORROW = 7;
    public const MESSAGE_TYPE_DO_RATE = 8;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function trip()
    {
        return $this->belongsTo(Trip::class, 'trip_id', 'id');
    }
}
