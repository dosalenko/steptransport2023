<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use JamesDordoy\LaravelVueDatatable\Traits\LaravelVueDatatableTrait;

/**
 * App\Models\Booking
 *
 * @property int $id
 * @property int $status
 * @property int client_id
 * @property int agreement_id
 * @property int pickup_address
 * @property int dropoff_address
 * @property Carbon|null pickup_start
 * @property Carbon|null pickup_end
 * @property Carbon|null dropoff_start
 * @property Carbon|null dropoff_end
 * @property int agreement_product_id
 * @property string pickup_note
 * @property string dropoff_note
 * @property string note
 * @property string reference
 * @property string linehaul_type
 * @property float total_volume
 * @property float total_weight
 * @property float total_colli
 * @property float product_price
 * @property float service_price
 * @property float total_price
 * @property string internal_comment
 * @property int created_by
 * @property int is_seen
 * @property int depends_on
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 * @method static Builder|Booking eloquentQuery($orderBy = 'id', $orderByDir = 'asc', $searchValue = '', $relationships = [])
 * @method static Builder|Booking newModelQuery()
 * @method static Builder|Booking newQuery()
 * @method static Builder|Booking query()
 * @method static Builder|Booking whereStatus($value)
 * @mixin \Eloquent
 * @property int $isRecurring
 * @property-read \App\Models\Agreement $agreement
 * @property-read \App\Models\Client $client
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Colli[] $collis
 * @property-read int|null $collis_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Colli[] $collisByColliBatch
 * @property-read int|null $collis_by_colli_batch_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Colli[] $collisByReference
 * @property-read int|null $collis_by_reference_count
 * @property-read \App\Models\Address $dropoffAddress
 * @property-read \App\Models\Trip|null $dropoffTrip
 * @property-read \App\Models\Trip|null $linehaulTrip
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Log[] $logs
 * @property-read int|null $logs_count
 * @property-read \App\Models\Address $pickupAddress
 * @property-read \App\Models\Trip|null $pickupTrip
 * @property-read \App\Models\Product $product
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Trip[] $trips
 * @property-read int|null $trips_count
 * @method static Builder|Booking searchApiOrders(array $search)
 * @method static Builder|Booking searchByStatus(int $status)
 * @method static Builder|Booking searchString(string $search)
 * @method static Builder|Booking whereAgreementId($value)
 * @method static Builder|Booking whereAgreementProductId($value)
 * @method static Builder|Booking whereClientId($value)
 * @method static Builder|Booking whereCreatedAt($value)
 * @method static Builder|Booking whereCreatedBy($value)
 * @method static Builder|Booking whereDeletedAt($value)
 * @method static Builder|Booking whereDropoffAddress($value)
 * @method static Builder|Booking whereDropoffEnd($value)
 * @method static Builder|Booking whereDropoffNote($value)
 * @method static Builder|Booking whereDropoffStart($value)
 * @method static Builder|Booking whereId($value)
 * @method static Builder|Booking whereInternalComment($value)
 * @method static Builder|Booking whereIsRecurring($value)
 * @method static Builder|Booking whereIsSeen($value)
 * @method static Builder|Booking whereLinehaulType($value)
 * @method static Builder|Booking whereNote($value)
 * @method static Builder|Booking wherePickupAddress($value)
 * @method static Builder|Booking wherePickupEnd($value)
 * @method static Builder|Booking wherePickupNote($value)
 * @method static Builder|Booking wherePickupStart($value)
 * @method static Builder|Booking whereProductPrice($value)
 * @method static Builder|Booking whereReference($value)
 * @method static Builder|Booking whereServicePrice($value)
 * @method static Builder|Booking whereTotalColli($value)
 * @method static Builder|Booking whereTotalPrice($value)
 * @method static Builder|Booking whereTotalVolume($value)
 * @method static Builder|Booking whereTotalWeight($value)
 * @method static Builder|Booking whereUpdatedAt($value)
 * @property int $is_api
 * @property int $product_time_units
 * @property int|null $pickup_address_type
 * @property int|null $dropoff_address_type
 * @property string|null $service_time_units
 * @property string|null $rate_link
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\BookingService[] $bookingServices
 * @property-read int|null $booking_services_count
 * @method static Builder|Booking searchByDate(int $dateType, string $dateFrom, string $dateTo)
 * @method static Builder|Booking whereDependsOn($value)
 * @method static Builder|Booking whereDropoffAddressType($value)
 * @method static Builder|Booking whereIsApi($value)
 * @method static Builder|Booking wherePickupAddressType($value)
 * @method static Builder|Booking whereProductTimeUnits($value)
 * @method static Builder|Booking whereRateLink($value)
 * @method static Builder|Booking whereServiceTimeUnits($value)
 */
class Booking extends Model
{
    use HasFactory;
    use LaravelVueDatatableTrait;

    public const BOOKING_TYPE_NEW = 1;
    public const BOOKING_TYPE_CONFIRMED = 2;
    public const BOOKING_TYPE_AWAITING_PICKUP = 3;
    public const BOOKING_TYPE_AWAITING_RECIPIENT_CONFIRMATION = 4;
    public const BOOKING_TYPE_AWAITING_TRANSIT = 5;
    public const BOOKING_TYPE_AWAITING_LINEHAUL = 6;
    public const BOOKING_TYPE_READY_FOR_DELIVERY = 7;
    public const BOOKING_TYPE_PACKING_STARTED = 8;
    public const BOOKING_TYPE_BEING_DELIVERED = 9;
    public const BOOKING_TYPE_DELIVERED = 10;

    public const BOOKING_IS_SEEN = 1;

    public const ADDRESS_TYPE_CLIENT = 1;
    public const ADDRESS_TYPE_STEP = 2;
    public const ADDRESS_TYPE_CUSTOMER = 3;

    public static function addressTypes()
    {
        return [
            self::ADDRESS_TYPE_CLIENT => 'client',
            self::ADDRESS_TYPE_STEP => 'step',
            self::ADDRESS_TYPE_CUSTOMER => 'customer',
        ];
    }

    public static function bookingStatuses()
    {
        return [
            self::BOOKING_TYPE_NEW => 'New',
            self::BOOKING_TYPE_CONFIRMED => 'Confirmed',
            self::BOOKING_TYPE_AWAITING_PICKUP => 'Awaiting pickup',
            self::BOOKING_TYPE_AWAITING_RECIPIENT_CONFIRMATION => 'Awaiting receipt confirmation', //not available tmp
            self::BOOKING_TYPE_AWAITING_TRANSIT => 'On pickup',
            self::BOOKING_TYPE_AWAITING_LINEHAUL => 'Awaiting linehaul',
            self::BOOKING_TYPE_READY_FOR_DELIVERY => 'Ready for delivery',
            self::BOOKING_TYPE_PACKING_STARTED => 'Packing started', //not available tmp
            self::BOOKING_TYPE_BEING_DELIVERED => 'On dropoff',
            self::BOOKING_TYPE_DELIVERED => 'Delivered',
        ];
    }

    /*
     * @param array $linehaulId
     * @return string
     *
    */
    public static function getLinehaulNames($linehaulIds)
    {
        if ($linehaulIds !== '0-0') {
            $linehaulIdsArr = explode('-', $linehaulIds);

            $linehaulFirst = StepAddress::findOrFail((int)$linehaulIdsArr[0]);
            $linehaulSecond = StepAddress::findOrFail((int)$linehaulIdsArr[1]);
            return $linehaulFirst->nickname . '-' . $linehaulSecond->nickname;
        } else {
            return 'none';
        }
    }


    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */

    protected $guarded = [
        'id'
    ];

    /**
     * Filtering data based on the following attributes
     * https://jamesdordoy.github.io/laravel-vue-datatable/laravel/trait
     *
     * @var array
     */
    protected $dataTableColumns = [
        'id' => [
            'searchable' => true,
            'orderable' => true,
        ],
        'status' => [
            'searchable' => true,
            'orderable' => true,
        ],
        'client_id' => [
            'searchable' => true,
            'orderable' => true,
        ],
        'pickup_start' => [
            'searchable' => true,
            'orderable' => true,
        ],
        'total_volume' => [
            'searchable' => true,
            'orderable' => true,
        ],
        'total_weight' => [
            'searchable' => true,
            'orderable' => true,
        ],
        'created_at' => [
            'searchable' => true,
            'orderable' => true,
        ],
        'deleted_at' => [
            'searchable' => true,
            'orderable' => true,
        ],
    ];

    /**
     * @return BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id', 'id')->withTrashed();
    }

    /**
     * @return BelongsTo
     */
    public function agreement()
    {
        return $this->belongsTo(Agreement::class, 'agreement_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function pickupAddress()
    {
        return $this->belongsTo(Address::class, 'pickup_address', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function dropoffAddress()
    {
        return $this->belongsTo(Address::class, 'dropoff_address', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class, 'agreement_product_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function trips(): HasMany
    {
        return $this->hasMany(Trip::class, 'booking_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function collis(): HasMany
    {
        return $this->hasMany(Colli::class, 'booking_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function collisByReference(): HasMany
    {
        return $this->hasMany(Colli::class, 'booking_id', 'id')->groupBy('reference');
    }

    /**
     * @return HasMany
     */
    public function collisByColliBatch(): HasMany
    {
        return $this->hasMany(Colli::class, 'booking_id', 'id')->groupBy('colli_batch');
    }


    /**
     * @return HasMany
     */
    public function bookingServices(): HasMany
    {
        return $this->hasMany(BookingService::class, 'booking_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function logs(): HasMany
    {
        return $this->hasMany(Log::class, 'booking_id', 'id');
    }

    /**
     * @return HasOne
     */
    public function pickupTrip(): HasOne
    {
        return $this->hasOne(Trip::class, 'booking_id', 'id')->where('type', Trip::TRIP_TYPE_PICKUP);
    }

    /**
     * @return HasOne
     */
    public function dropoffTrip(): HasOne
    {
        return $this->hasOne(Trip::class, 'booking_id', 'id')->where('type', Trip::TRIP_TYPE_DROPOFF);
    }

    /**
     * @return HasOne
     */
    public function linehaulTrip(): HasOne
    {
        return $this->hasOne(Trip::class, 'booking_id', 'id')->where('type', Trip::TRIP_TYPE_LINEHAUL);
    }

    /*
     * get number of seen bookings by someone
     * @return int
    */
    public static function getSeenNumber(): int
    {
        $notSeenBookings = Booking::where('status', '=', Booking::BOOKING_TYPE_NEW)->where('is_seen', '=', 0)->count();
        return $notSeenBookings;
    }

    /*
     * @param int $agreementProductId
     * @return int
     */
    public static function getStandardProductByAgreementProduct($agreementProductId): int
    {
        $agreementProduct = AgreementProduct::findOrFail($agreementProductId);
        return $agreementProduct->product_id;
    }

    /*
     * @param arr $agreementServiceIds
     * @return string
     */
    public static function getStandardAgreementByAgreementService($agreementServiceIds)
    {
        $arr = [];
        foreach ($agreementServiceIds as $agreement) {
            $agreementService = AgreementService::findOrFail($agreement)->first();
            $arr[] = $agreementService->service_id;
        }
        return json_encode($arr);
    }

    /*
     * @param int $agreementProductId
     * @return string
     */
    public static function getStandardAgreementPriceByAgreementService($agreementServiceIds)
    {
        $arr = [];
        foreach ($agreementServiceIds as $k => $price) {
            if ($price !== null) {
                $agreementService = AgreementService::findOrFail($k);
                $arr[$agreementService->service_id] = $price;
            }
        }
        return json_encode($arr);
    }

    /*
     * @param array $columns
     * @return array
     */
    public static function getArrayForExportBookingCsv($columns, $dateFrom, $dateTo): array
    {
        $groups = Booking::with(
            'client',
            'agreement',
            'pickupAddress',
            'pickupAddress',
            'product',
        )->whereDate('dropoff_start', '>=', $dateFrom)
            ->whereDate('dropoff_start', '<=', $dateTo)->orderBy('dropoff_start')->whereNull('deleted_at');

        $export = [];
        $groups->latest()->chunk(100, function ($groups) use (&$export, $columns) {
            foreach ($groups as $group) {
                $arrayGroups = [
                    'id' => $group->id
                ];
                if (in_array('client', $columns)) {
                    $arrayGroups['client'] = $group->client->name;
                }
                if (in_array('agreement', $columns)) {
                    $arrayGroups['agreement'] = $group->agreement->name;
                }

                if (in_array('pickup_name', $columns)) {
                    $arrayGroups['pickup_name'] = $group->pickupAddress->name;
                }
                if (in_array('pickup_street', $columns)) {
                    $arrayGroups['pickup_street'] = $group->pickupAddress->street;
                }
                if (in_array('pickup_zip', $columns)) {
                    $arrayGroups['pickup_zip'] = $group->pickupAddress->zip;
                }
                if (in_array('pickup_city', $columns)) {
                    $arrayGroups['pickup_city'] = $group->pickupAddress->city;
                }
                if (in_array('pickup_country', $columns)) {
                    $arrayGroups['pickup_country'] = 'Denmark';
                }
                if (in_array('pickup_phone', $columns)) {
                    $arrayGroups['pickup_phone'] = $group->pickupAddress->phone;
                }

                if (in_array('pickup_date', $columns)) {
                    $arrayGroups['pickup_date'] = $group->pickup_start . '-' . $group->pickup_end;
                }

                if (in_array('dropoff_name', $columns)) {
                    $arrayGroups['dropoff_name'] = $group->dropoffAddress->name;
                }
                if (in_array('dropoff_street', $columns)) {
                    $arrayGroups['dropoff_street'] = $group->dropoffAddress->street;
                }
                if (in_array('dropoff_zip', $columns)) {
                    $arrayGroups['dropoff_zip'] = $group->dropoffAddress->zip;
                }
                if (in_array('dropoff_city', $columns)) {
                    $arrayGroups['dropoff_city'] = $group->dropoffAddress->city;
                }
                if (in_array('dropoff_country', $columns)) {
                    $arrayGroups['dropoff_country'] = 'Denmark';
                }
                if (in_array('dropoff_phone', $columns)) {
                    $arrayGroups['dropoff_phone'] = $group->dropoffAddress->phone;
                }

                if (in_array('dropoff_date', $columns)) {
                    $arrayGroups['dropoff_date'] = $group->dropoff_start . '-' . $group->dropoff_end;
                }
                if (in_array('product', $columns)) {
                    $arrayGroups['product'] = $group->product->name;
                }
                if (in_array('services', $columns)) {
                    $arrayGroups['services'] = implode(',', Service::getServicesNames($group->service_price));
                }
                if (in_array('total_weight', $columns)) {
                    $arrayGroups['total_weight'] = $group->total_weight;
                }
                if (in_array('total_volume', $columns)) {
                    $arrayGroups['total_volume'] = $group->total_volume;
                }
                if (in_array('total_colli', $columns)) {
                    $arrayGroups['total_colli'] = $group->total_colli;
                }
                if (in_array('total_price', $columns)) {
                    $arrayGroups['total_price'] = $group->total_price;
                }
                if (in_array('note', $columns)) {
                    $arrayGroups['note'] = $group->note;
                }
                if (in_array('reference', $columns)) {
                    $arrayGroups['reference'] = $group->reference;
                }
                if (in_array('driver', $columns)) {
                    $drivers = [];
                    foreach ($group->trips as $v) {
                        $drivers[$v->driver] = $v->driver;
                    }
                    $arrayGroups['driver'] = implode(',', array_filter($drivers));
                }
                if (in_array('internal_comment', $columns)) {
                    $arrayGroups['internal_comment'] = $group->internal_comment;
                }
                $export[] = $arrayGroups;
            }
        });

        return $export;
    }

    /**
     * Scope search
     *
     * @param Builder $query
     * @param string $search
     *
     * @return Builder
     */
    public function scopeSearchString($query, string $search)
    {
        return $query->where('id', 'LIKE', "%$search%")
            ->orWhere(DB::raw('concat("B-",id)'), 'LIKE', "%$search%")
            ->orWhere('reference', 'LIKE', "%$search%")
            ->orWhereHas('client', function ($q) use ($search) {
                $q->where('name', 'LIKE', "%$search%");
            })->orWhereHas('pickupAddress', function ($q) use ($search) {
                $q->where('name', 'LIKE', "%$search%")
                    ->orWhere('street', 'LIKE', "%$search%")
                    ->orWhere('zip', 'LIKE', "%$search%")
                    ->orWhere('city', 'LIKE', "%$search%");
            })->orWhereHas('dropoffAddress', function ($q) use ($search) {
                $q->where('name', 'LIKE', "%$search%")
                    ->orWhere('street', 'LIKE', "%$search%")
                    ->orWhere('zip', 'LIKE', "%$search%")
                    ->orWhere('city', 'LIKE', "%$search%");
            })->orWhereHas('trips.address', function ($q) use ($search) {
                $q->where('phone', 'LIKE', "%$search%");
            })->orWhereHas('trips', function ($q) use ($search) {
                $q->where('id', 'LIKE', "%$search%")
                    ->orWhere(DB::raw('concat("T-",id)'), 'LIKE', "%$search%");
            })->orWhereHas('collis', function ($q) use ($search) {
                $q->where('reference', 'LIKE', "%$search%");
            });
    }

    /**
     * Scope search
     *
     * @param Builder $query
     * @param int $dateType
     * @param string $dateFrom
     * @param string $dateTo
     *
     * @return Builder
     */
    public function scopeSearchByDate($query, int $dateType, string $dateFrom, string $dateTo)
    {
        if ($dateType === 1) {
            $column = 'created_at';
        } elseif ($dateType === 2) {
            $column = 'pickup_start';
        } else {
            $column = 'dropoff_start';
        }

        $q = $query;
        if ($dateFrom) {
            $q->whereDate($column, '>=', $dateFrom);
        }
        if ($dateTo) {
            $q->whereDate($column, '<=', $dateTo);
        }
        return $q;
    }

    /**
     * Scope search by api orders
     *
     * @param Builder $query
     * @param array $search
     *
     * @return Builder
     */
    public function scopeSearchApiOrders($query, array $search)
    {
        $q = $query->where('client_id', '=', $search['client_id']);
        if (array_key_exists('ids', $search)) {
            $q->whereIn('id', $search['ids']);
        }
        if (array_key_exists('start', $search)) {
            $q->whereDate('created_at', '>=', $search['start']);
        }
        if (array_key_exists('end', $search)) {
            $q->whereDate('created_at', '<=', $search['end']);
        }

        return $q;
    }

    /**
     * scope search
     *
     * @param Builder $query
     * @param int $status
     *
     * @return Builder
     */
    public function scopeSearchByStatus($query, int $status)
    {
        return $query->where('status', '=', $status);
    }
}
