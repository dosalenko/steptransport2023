<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\TourStop
 *
 * @mixin \Eloquent
 * @property int $id
 * @property int $trip_id
 * @property string $driver
 * @property int $stop_number
 * @property string $date
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Trip $trip
 * @method static \Illuminate\Database\Eloquent\Builder|TourStop newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TourStop newQuery()
 * @method static \Illuminate\Database\Query\Builder|TourStop onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|TourStop query()
 * @method static \Illuminate\Database\Eloquent\Builder|TourStop whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TourStop whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TourStop whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TourStop whereDriver($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TourStop whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TourStop whereStopNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TourStop whereTripId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TourStop whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|TourStop withTrashed()
 * @method static \Illuminate\Database\Query\Builder|TourStop withoutTrashed()
 */
class TourStop extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tour_stops';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'trip_id',
        'driver',
        'stop_number',
        'date',
        'deleted_at',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function trip()
    {
        return $this->belongsTo(Trip::class, 'trip_id', 'id');
    }

}
