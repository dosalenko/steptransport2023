<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\DB;
use JamesDordoy\LaravelVueDatatable\Traits\LaravelVueDatatableTrait;

/**
 * App\Models\Recurring
 *
 * @property int $id
 * @property string $name
 * @property int client_id
 * @property int agreement_id
 * @property int driver_id
 * @property string recurring_dates
 * @property string reference
 * @property string internal_comment
 * @property int agreement_product_id
 * @property string agreement_services_ids
 * @property float total_price
 * @property int created_by
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $stops
 * @property-read \App\Models\Agreement $agreement
 * @property-read \App\Models\Client $client
 * @property-read \App\Models\Driver $driver
 * @property-read \App\Models\AgreementProduct $product
 * @method static \Illuminate\Database\Eloquent\Builder|Recurring eloquentQuery($orderBy = 'id', $orderByDir = 'asc', $searchValue = '', $relationships = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Recurring newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Recurring newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Recurring query()
 * @method static \Illuminate\Database\Eloquent\Builder|Recurring searchString(string $search)
 * @method static \Illuminate\Database\Eloquent\Builder|Recurring whereAgreementId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Recurring whereAgreementProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Recurring whereAgreementServicesIds($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Recurring whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Recurring whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Recurring whereDriverId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Recurring whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Recurring whereInternalComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Recurring whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Recurring whereRecurringDates($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Recurring whereReference($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Recurring whereStops($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Recurring whereTotalPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Recurring whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Recurring extends Model
{
    use HasFactory;
    use LaravelVueDatatableTrait;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'client_id',
        'agreement_id',
        'driver_id',
        'recurring_dates',
        'reference',
        'internal_comment',
        'agreement_product_id',
        'agreement_services_ids',
        'total_price',
        'stops',
    ];

    /**
     * Filtering data based on the following attributes
     * https://jamesdordoy.github.io/laravel-vue-datatable/laravel/trait
     *
     * @var array
     */
    protected $dataTableColumns = [
        'id' => [
            'searchable' => true,
            'orderable' => true,
        ],
        'status' => [
            'searchable' => true,
            'orderable' => true,
        ],
        'client_id' => [
            'searchable' => true,
            'orderable' => true,
        ],
        'pickup_start' => [
            'searchable' => true,
            'orderable' => true,
        ],
        'total_volume' => [
            'searchable' => true,
            'orderable' => true,
        ],
        'total_weight' => [
            'searchable' => true,
            'orderable' => true,
        ],
        'created_at' => [
            'searchable' => true,
            'orderable' => true,
        ],
        'deleted_at' => [
            'searchable' => true,
            'orderable' => true,
        ],
    ];

    /**
     * @return BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id', 'id')->withTrashed();
    }

    /**
     * @return BelongsTo
     */
    public function agreement()
    {
        return $this->belongsTo(Agreement::class, 'agreement_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function driver()
    {
        return $this->belongsTo(Driver::class, 'driver_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(AgreementProduct::class, 'agreement_product_id', 'id');
    }

    /**
     * Scope search
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $search
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearchString($query, string $search)
    {
        return $query->where('name', 'LIKE', "%$search%")->
        orWhereHas('client', function ($q) use ($search) {
            $q->where('name', 'LIKE', "%$search%");
        })->orWhereHas('driver', function ($q) use ($search) {
            $q->where('name', 'LIKE', "%$search%");
        });
    }
}
