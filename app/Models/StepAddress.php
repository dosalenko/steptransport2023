<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * App\Models\Client
 *
 * @property int $id
 * @property string $nickname
 * @property string $name
 * @property int $counry_id
 * @property string $street
 * @property string $zip
 * @property string $city
 * @property string $att
 * @property int $client_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Client eloquentQuery($orderBy = 'id', $orderByDir = 'asc', $searchValue = '', $relationships = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Client newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Client newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Client query()
 * @mixin \Eloquent
 * @property int $country_id
 * @property int|null $receiving_location_id
 * @property-read \App\Models\Location|null $receivingLocation
 * @method static \Illuminate\Database\Eloquent\Builder|StepAddress whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StepAddress whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StepAddress whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StepAddress whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StepAddress whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StepAddress whereNickname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StepAddress whereReceivingLocationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StepAddress whereStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StepAddress whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StepAddress whereZip($value)
 */
class StepAddress extends Model
{
    use HasFactory;

    public const DEFAULT_STREET = 'Priorparken 851';
    public const DEFAULT_ZIP = '2650';
    public const DEFAULT_CITY = 'Brøndby';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'step_addresses';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nickname', 'name', 'country_id', 'zip', 'city', 'street', 'receiving_location_id'];

    /**
     * @return HasOne
     */
    public function receivingLocation(): HasOne
    {
        return $this->hasOne(Location::class, 'id', 'receiving_location_id');
    }
}
