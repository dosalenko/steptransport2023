<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\Client
 *
 * @property int $id
 * @property int $agreement_id
 * @property int $product_id
 * @property int $active
 * @property int $price_type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|AgreementProduct eloquentQuery($orderBy = 'id', $orderByDir = 'asc', $searchValue = '', $relationships = [])
 * @method static \Illuminate\Database\Eloquent\Builder|AgreementProduct newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AgreementProduct newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AgreementProduct query()
 * @method static \Illuminate\Database\Eloquent\Builder|AgreementProduct whereBillingAddress($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Agreement $agreement
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\AgreementProductPrice[] $prices
 * @property-read int|null $prices_count
 * @property-read \App\Models\Product $product
 * @method static \Illuminate\Database\Eloquent\Builder|AgreementProduct whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AgreementProduct whereAgreementId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AgreementProduct whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AgreementProduct whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AgreementProduct wherePriceType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AgreementProduct whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AgreementProduct whereUpdatedAt($value)
 */
class AgreementProduct extends Model
{
    use HasFactory;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'agreements_products';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'agreement_id', 'product_id', 'price_type', 'active'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function agreement()
    {
        return $this->belongsTo(Agreement::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class)->withTrashed();
    }

    /**
     * @return HasMany
     */
    public function prices(): HasMany
    {
        return $this->hasMany(AgreementProductPrice::class, 'agreement_product_id', 'id');
    }
}
