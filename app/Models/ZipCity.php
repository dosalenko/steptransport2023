<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ZipCity
 *
 * @property int $id
 * @property string $zip
 * @property string $city
 * @method static \Illuminate\Database\Eloquent\Builder|ZipCity newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ZipCity newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ZipCity query()
 * @method static \Illuminate\Database\Eloquent\Builder|ZipCity whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ZipCity whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ZipCity whereZip($value)
 * @mixin \Eloquent
 */
class ZipCity extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'zip_cities';

    public $timestamps = false;
}
