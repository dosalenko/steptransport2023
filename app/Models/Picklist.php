<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Picklist
 *
 * @property int $id
 * @property string $driver
 * @property string $date
 * @property string $orders
 * @property string $file
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|Picklist newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Picklist newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Picklist query()
 * @method static \Illuminate\Database\Eloquent\Builder|Picklist whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Picklist whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Picklist whereDriver($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Picklist whereFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Picklist whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Picklist whereOrders($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Picklist whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Picklist whereUpdatedAt($value)
 */
class Picklist extends Model
{
    use HasFactory;

    public const PICKLIST_STATUS_NEW = 0;
    public const PICKLIST_STATUS_PRINTED = 1;
    public const PICKLIST_PICKED_SCANNED = 2;

    public const PREPORT_TYPE_UNASSIGNED = 0;
    public const PREPORT_TYPE_ALL = 1;
    public const PREPORT_TYPE_SINGLE_DRIVER = 2;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'picklist';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'driver',
        'date',
        'orders',
        'file',
        'status',
    ];
}
