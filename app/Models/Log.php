<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Booking
 *
 * @property int $id
 * @property int $user_id
 * @property int $booking_id
 * @property string $action
 * @property string $initiated_by
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $action_time
 * @method static \Illuminate\Database\Eloquent\Builder|Booking eloquentQuery($orderBy = 'id', $orderByDir = 'asc', $searchValue = '', $relationships = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Booking newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Booking newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Booking query()
 * @mixin \Eloquent
 * @property string|null $description
 * @property-read \App\Models\Booking $booking
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Log whereAction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Log whereActionTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Log whereBookingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Log whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Log whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Log whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Log whereInitiatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Log whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Log whereUserId($value)
 */
class Log extends Model
{
    use HasFactory;

    public const LOG_ACTION_CREATED = 1;
    public const LOG_ACTION_DELETED = 2;
    public const LOG_ACTION_UPDATED = 3;
    public const LOG_ACTION_CONFIRMED = 4;
    public const LOG_ACTION_STATUS_CHANGED = 5;
    public const LOG_ACTION_LINEHAULS_CHANGED = 6;

    public const LOG_ACTION_COLLI_PREPARED = 7;
    public const LOG_ACTION_COLLI_LOADED = 8;
    public const LOG_ACTION_COLLI_CREATED = 9;
    public const LOG_ACTION_COLLI_EDITED = 10;
    public const LOG_ACTION_COLLI_DELETED = 11;
    public const LOG_ACTION_COLLI_RECEIVED = 12;
    public const LOG_ACTION_COLLI_DELIVERED = 13;
    public const LOG_ACTION_COLLI_LOCATION_CHANGED = 16;

    public const LOG_ACTION_PRODUCT_PRICE_RECALCULATED = 14;
    public const LOG_ACTION_SERVICE_PRICE_RECALCULATED = 15;

    public const DEFAULT_LOG_EMAIL = 'admin@gmail.com';

    public static function logActions()
    {
        return [
            self::LOG_ACTION_CREATED => 'Booking created',
            self::LOG_ACTION_DELETED => 'Booking deleted',
            self::LOG_ACTION_UPDATED => 'Booking data updated',
            self::LOG_ACTION_CONFIRMED => 'Booking status confirmed',
            self::LOG_ACTION_STATUS_CHANGED => 'Booking status changed',
            self::LOG_ACTION_LINEHAULS_CHANGED => 'Booking linehauls changed',
            self::LOG_ACTION_PRODUCT_PRICE_RECALCULATED => 'Price product was recalculated',
            self::LOG_ACTION_SERVICE_PRICE_RECALCULATED => 'Price services were recalculated',

            self::LOG_ACTION_COLLI_PREPARED => 'Colli prepared',
            self::LOG_ACTION_COLLI_LOADED => 'Colli loaded',
            self::LOG_ACTION_COLLI_CREATED => 'Colli created',
            self::LOG_ACTION_COLLI_EDITED => 'Colli edited',
            self::LOG_ACTION_COLLI_DELETED => 'Colli deleted',
            self::LOG_ACTION_COLLI_RECEIVED => 'Colli received',
            self::LOG_ACTION_COLLI_DELIVERED => 'Colli delivered',
            self::LOG_ACTION_COLLI_LOCATION_CHANGED => 'Colli location changed',
        ];
    }

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'booking_id',
        'colli_id',
        'action',
        'action_time',
        'initiated_by',
        'description',
    ];

    /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function booking()
    {
        return $this->belongsTo(Booking::class, 'booking_id', 'id');
    }
}
