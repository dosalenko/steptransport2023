<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\Client
 *
 * @property int $id
 * @property int $agreement_interval_id
 * @property int $agreement_service_id
 * @property double $price
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Client eloquentQuery($orderBy = 'id', $orderByDir = 'asc', $searchValue = '', $relationships = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Client newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Client newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Client query()
 * @mixin \Eloquent
 * @property-read \App\Models\AgreementInterval $interval
 * @property-read \App\Models\AgreementService $service
 * @method static \Illuminate\Database\Eloquent\Builder|AgreementServicePrice whereAgreementIntervalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AgreementServicePrice whereAgreementServiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AgreementServicePrice whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AgreementServicePrice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AgreementServicePrice wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AgreementServicePrice whereUpdatedAt($value)
 */
class AgreementServicePrice extends Model
{
    use HasFactory;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'agreements_services_prices';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['agreement_interval_id', 'agreement_service_id', 'price'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function interval()
    {
        return $this->belongsTo(AgreementInterval::class, 'agreement_interval_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function service()
    {
        return $this->belongsTo(AgreementService::class, 'agreement_service_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * todo remove
     */
    public function price()
    {
        return $this->belongsTo(AgreementService::class, 'agreement_service_id', 'id');
    }
}
