<?php

namespace App\Repository;

use App\Models\Booking;
use App\Models\Colli;
use App\Models\Log;
use App\Models\Trip;
use App\Repository\Interfaces\BookingRepositoryInterface;
use App\Repository\Interfaces\ColliRepositoryInterface;
use App\Services\Communication\Communication;
use App\Services\Core\Helpers\ColliHelper;
use App\Services\Core\Helpers\Helper;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class ColliRepository implements ColliRepositoryInterface
{
    /**
     * @var Colli
     */
    private $model;

    /**
     * @param Colli $colli
     */
    public function __construct(Colli $colli)
    {
        $this->model = $colli;
    }

    /**
     * @param int $id
     * @return Colli
     */
    public function findOneById(int $id): Colli
    {
        return $this->model->find($id);
    }

    /**
     * @param array $data
     */
    public function create(array $data): Colli
    {
        $findColliBatch = Colli::orderBy('colli_batch', 'desc')->first();
        $maxColliBatchNumber = $findColliBatch['colli_batch'];

        //split colli
        $colliData = [];
        $maxColliNumber = Colli::where('reference', '=', $data['reference'])->max('colli');
        if (!$maxColliNumber) {
            $maxColliNumber = 0;
        } else {
            $maxColliNumber += 1;
        }

        for ($i = 1; $i <= (int)$data['colli']; $i++) {
            $colliData[$i] = [
                'reference' => $data['reference'],
                'colli' => $maxColliNumber++,
                'volume' => round(Helper::format($data['volume']) / (int)$data['colli'], 2),
                'weight' => round(Helper::format($data['weight']) / (int)$data['colli'], 2),
                'booking_id' => array_key_exists('booking', $data) && !empty($data['booking']['id']) ? (int)$data['booking']['id'] : null,
                'client_id' => (int)$data['client']['id'],
                'agreement_id' => Colli::DEFAULT_AGREEMENT,
                'status' => Colli::COLLI_STATUS_ON_DISPATCH_TERMINAL,
                'name' => array_key_exists('name', $data) ? $data['name'] : null,
                'received_at' => $data['received_at'] ?? null,
                'location_id' => array_key_exists('location', $data) && !empty($data['location']['id']) ? (int)$data['location']['id'] : Helper::getReceivingLocation((int)$data['warehouse']),
                'colli_packing_status' => Colli::COLLI_PACKING_STATUS_NEW,
                'lastprintedas_unknown' => !array_key_exists('booking', $data) || empty($data['booking']['id']) ? 1 : 0,
                'colli_batch' => $maxColliBatchNumber + 1,
                'created_by' => Auth::user() ? Auth::user()->id : 0,
            ];
        }

        foreach ($colliData as $v) {
            $colli = Colli::create($v);
            if (array_key_exists('delivery', $data)) {
                $booking = Booking::with('pickupTrip')->findOrFail($colli->booking_id);
                $booking->dropoff_start = $data['delivery'];
                $booking->save();

                $trips = Trip::where('booking_id', $colli->booking_id)->get();
                foreach ($trips as $trip) {
                    $trip->tw_start = $data['delivery'];
                    $trip->save();
                }
            }
            $bookingRepo = App::make(BookingRepositoryInterface::class);
            if ($colli->booking_id !== null) {
                $action = 'Colli [' . $colli->id . '] have been created';
                $findPreparedColli = Log::where('booking_id', '=', $colli->booking_id)
                    ->where('action', '=', $action)
                    ->first();
                if (!$findPreparedColli) {
                    $bookingRepo->saveHistoryLog($action, Auth::user()->email, $colli->booking);
                }
            }
        }
        return $colli;
    }

    /**
     * @param array $data
     * @param Colli $colli
     * @return Colli
     * @throws \Exception
     */
    public function update(array $data, Colli $colli): Colli
    {
        if (array_key_exists('amount', $data)) {
            ColliHelper::addNewColliToBatch(
                $colli,
                (int)$data['amount'] - Helper::getTotalColliAmountByColliBatch($colli->colli_batch) + 1
            );
        }
        $colliExisting = Colli::where('id', '=', $colli->id)->first();
        if ($colliExisting) {
            $getCollisByColliBatch = Colli::where('colli_batch', '=', $colliExisting->colli_batch)->get();
            foreach ($getCollisByColliBatch as $batch) {
                if (!empty($data['name'])) {
                    $batch->name = $data['name'];
                }
                if (!empty($data['colli_reference'])) {
                    $batch->reference = $data['colli_reference'];
                }
                if (!empty($data['received_at'])) {
                    $batch->received_at = $data['received_at'];
                }
                if (!empty($data['delivery']) && !empty($batch->booking_id)) {
                    $booking = Booking::with('pickupTrip')->findOrFail($batch->booking_id);
                    $booking->dropoff_start = $data['delivery'];
                    $booking->save();

                    $trips = Trip::where('booking_id', $colli->booking_id)->get();
                    foreach ($trips as $trip) {
                        $trip->tw_start = $data['delivery'];
                        $trip->save();
                    }
                }
                if (array_key_exists('location_id', $data)) {
                    $batch->location_id = $data['location_id'];
                }
                if (array_key_exists('client_id', $data)) {
                    $batch->client_id = $data['client_id'];
                }
                if (array_key_exists('booking_id', $data)) {
                    $batch->booking_id = $data['booking_id'];
                }
                if (!empty($data['volume'])) {
                    // $batch->volume = round(Helper::format($data['volume']) / count($getCollisByColliBatch), 2);
                }
                if (!empty($data['weight'])) {
                    // $batch->weight = round(Helper::format($data['weight']) / count($getCollisByColliBatch), 2);
                }
                //colli log
                $this->saveHistoryLog(
                    Log::logActions()[LOG::LOG_ACTION_COLLI_LOCATION_CHANGED],
                    $batch->id,
                    '#' . $batch->location->name . ' (' . $batch->location->warehouse->nickname . ')',
                );
                $batch->save();
            }
        }
        if (!empty($data['booking_reference']) && $colli->booking_id !== null) {
            $booking = Booking::findOrFail($colli->booking_id);
            $booking->reference = $data['booking_reference'];
            $booking->save();
        }

        $bookingRepo = App::make(BookingRepositoryInterface::class);
        if ($colli->booking_id !== null) {
            $action = 'Colli [' . $colli->id . '] have been updated';
            $findPreparedColli = Log::where('booking_id', '=', $colli->booking_id)->where('action', '=', $action)->first();
            if (!$findPreparedColli) {
                $bookingRepo->saveHistoryLog($action, Auth::user()->email, $colli->booking);
            }
        }
        return $colli;
    }

    /**
     * @param array $data
     * @return bool
     * @throws \Exception
     */
    public function massUpdate(array $data): bool
    {
        $tmp = [];
        foreach ($data['existingColli'] as $k => $v) {
            $tmp[$v['id']][$v['field']] = $v['value'];
        }

        foreach ($tmp as $k => $data) {
            $colliExisting = Colli::where('id', '=', $k)->first();
            if ($colliExisting) {
                if (array_key_exists('amount', $data)) {
                    ColliHelper::addNewColliToBatch(
                        $colliExisting,
                        (int)$data['amount'] - Helper::getTotalColliAmountByColliBatch($colliExisting->colli_batch) + 1
                    );
                }
                $getCollisByColliBatch = Colli::where('colli_batch', '=', $colliExisting->colli_batch)->get();
                foreach ($getCollisByColliBatch as $batch) {
                    if (!empty($data['name'])) {
                        $batch->name = $data['name'];
                    }
                    if (!empty($data['colli_reference'])) {
                        $batch->reference = $data['colli_reference'];
                    }
                    if (array_key_exists('location_id', $data)) {
                        $batch->location_id = $data['location_id'];
                    }
                    if (array_key_exists('booking_id', $data)) {
                        $batch->booking_id = $data['booking_id'];
                    }
                    if (!empty($data['volume'])) {
                        $batch->volume = round(Helper::format($data['volume']) / count($getCollisByColliBatch), 2);
                    }
                    if (!empty($data['weight'])) {
                        $batch->weight = round(Helper::format($data['weight']) / count($getCollisByColliBatch), 2);
                    }

                    $batch->save();
                    if (array_key_exists('location_id', $data)) {
                        //colli log
                        $this->saveHistoryLog(
                            Log::logActions()[LOG::LOG_ACTION_COLLI_LOCATION_CHANGED],
                            $batch->id,
                            '#' . $batch->location->name . ' (' . $batch->location->warehouse->nickname . ')',
                        );
                    }
                }
            }
        }
        if (isset($data['newColli'])) {
            foreach ($data['newColli'] as $k => $v) {
                $this->create($v);
            }
        }
        return true;
    }

    /**
     * @param array $data
     * @param Colli $colli
     * @return Colli
     * @throws \Exception
     */
    public function updateGoods(array $data, Colli $colli): Colli
    {
        $data['booking_id'] = $data['booking']['id'];
        $data['status'] = Colli::COLLI_STATUS_ON_DISPATCH_TERMINAL;
        $colli->update($data);
        return $colli;
    }

    /**
     * @param array $ids
     * @return bool
     */
    public function delete(array $ids): bool
    {
        foreach ($ids as $k => $v) {
            $colli = Colli::findOrFail((int)$k);
            $getCollisByColliBatch = Colli::where('colli_batch', '=', $colli->colli_batch)->get();
            foreach ($getCollisByColliBatch as $batch) {
                $batch->delete();
            }
            $bookingRepo = App::make(BookingRepositoryInterface::class);
            if ($colli->booking_id !== null) {
                $action = 'Colli [' . $k . '] have been deleted';
                $bookingRepo->saveHistoryLog($action, Auth::user()->email, $colli->booking);
            }
        }
        return true;
    }

    /**
     * @param array|null $data
     * @return LengthAwarePaginator
     */
    public function get(array $data): LengthAwarePaginator
    {
        $length = ($data && !empty($data['length'])) ? (int)$data['length'] : 1000;
        $search = ($data && !empty($data['search'])) ? $data['search'] : '';
        $column = $data['column'] ?? 'id';
        $dir = $data['dir'] ?? 'desc';

        if (array_key_exists('warehouseId', $data) && $data['warehouseId'] !== null) {
            if (is_array($data['warehouseId'])) {
                $warehouseId = $data['warehouseId']['id'];
            } else {
                $warehouseId = json_decode($data['warehouseId'], true)['id'];
            }
        } else {
            $warehouseId = null;
        }

        if ($data && array_key_exists('bookingId', $data) && $data['bookingId'] !== null) {
            if (!empty($data['isForColliReceive'])) {
                $bookingId = [];
                foreach ($data['bookingId'] as $v) {
                    $bookingId[] = json_decode($v)->id;
                }
            } else {
                $bookingId = (int)$data['bookingId'];
            }
        } else {
            $bookingId = null;
        }


        if ($data && array_key_exists('clientIdSingle', $data)) {
            $clientId = (int)$data['clientIdSingle'];
        } elseif (($data && array_key_exists('clientId', $data) && !empty($data['clientId']))) {
            if (is_array($data['clientId'])) {
                $clientId = $data['clientId']['id'];
            } else {
                $clientId = json_decode($data['clientId'])->id;
            }
        } else {
            $clientId = null;
        }

        $q = Colli::with('booking', 'client', 'location', 'location.warehouse');
        if (!empty($data['receivedFrom']) || !empty($data['receivedTo'])) {
            $q->with('client.contacts');
        }
        if (empty($data['isForBookingEdit'])) {
            if (array_key_exists('showDelivered', $data) && $data['showDelivered'] === 'true') {
            } else {
                $q->where(function ($q) {
                    $q->whereNull('booking_id')->orWhereHas('booking', function ($q) {
                        $q->where('status', '<>', Booking::BOOKING_TYPE_DELIVERED);
                    });
                });
            }
        }

        if (!empty($warehouseId) && empty($data['searchAllWarehouses'])) {
            $q->searchByWarehouse($warehouseId);
        }

        if (Auth::user() && in_array('user', Auth::user()->roles)) {
            $q->where('client_id', '=', Auth::user()->client_id);
        }

        if (empty($data['searchAllWarehouses']) && empty($data['isForBookingEdit'])) {
            $q->whereNotIn(
                'colli_packing_status',
                [Colli::COLLI_PACKING_STATUS_PREPARED, Colli::COLLI_PACKING_STATUS_LOADED]
            );
        }

        if (!empty($data['groupByColliBatch']) && (bool)$data['groupByColliBatch'] === true) {
            $q->groupBy('colli_batch');
        }

        if (!empty($data['excludeAssigned']) && (bool)$data['excludeAssigned'] === true) {
            $q->whereNull('booking_id');
        }

        if (array_key_exists('showDelivered', $data) && $data['showDelivered'] === 'false') {
            $q->searchByNoDelivered();
        }

        if (!empty($data['excludeDelivered']) && (bool)$data['excludeDelivered'] === true) {
            $q->searchByNoDelivered();
        }

        if (!empty($search)) {
            $q->searchString($search);
        }

        if (!empty($data['searchByReference'])) {
            $q->searchByReference($data['searchByReference']);
        }

        if (!empty($clientId)) {
            $q->searchByClient($clientId);
        }

        if (!empty($bookingId)) {
            $q->searchByBooking($bookingId);
        }

        if (!empty($warehouseId) && empty($data['searchAllWarehouses'])) {
            $q->searchByWarehouse($warehouseId);
        }

        if (!empty($data['isForColliReceive'])) {
            $q->orderBy('booking_id', 'desc');
        } elseif (!empty($data['receivedFrom']) || !empty($data['receivedTo'])) {
            $q->searchByReceivedDate($data['receivedFrom'] ?? '', $data['receivedTo'] ?? '');
            if (!array_key_exists('clientIdSingle', $data)) {
                $q->groupBy('client_id');
                $q->orderBy('client_id', 'desc');
            }
        } else {
            if ($column === 'client') {
                $q->orderByRaw('(SELECT name FROM clients WHERE clients.id = colli.client_id) ' . $dir . ' ');
            } elseif ($column === 'booking') {
                $q->orderBy('booking_id', $dir);
            } elseif ($column === 'booking_reference') {
                $q->orderByRaw('(SELECT reference FROM bookings WHERE bookings.id = colli.booking_id) ' . $dir . ' ');
            } elseif ($column === 'colli_reference') {
                $q->orderBy('reference', $dir);
            } elseif ($column === 'delivery') {
                $q->orderByRaw('(SELECT dropoff_start FROM bookings WHERE bookings.id = colli.booking_id) ' . $dir . ' ');
            } elseif ($column === 'weight') {
                $q->orderByRaw('(SELECT total_weight FROM bookings WHERE bookings.id = colli.booking_id) ' . $dir . ' ');
            } elseif ($column === 'volume') {
                $q->orderByRaw('(SELECT total_volume FROM bookings WHERE bookings.id = colli.booking_id) ' . $dir . ' ');
            } elseif ($column === 'colli_number') {
                $q->orderByRaw('(SELECT total_colli FROM bookings WHERE bookings.id = colli.booking_id) ' . $dir . ' ');
            } elseif ($column === 'location') {
                $q->orderBy('location_id', $dir);
            } elseif ($column === 'created_by') {
                $q->orderBy('created_by', $dir);
            } else {
                $q->orderBy($column, $dir);
            }
        }


        return $q->paginate($length);
    }

    /**
     * @param array $batch
     * @return LengthAwarePaginator
     */
    public function getByColliBatch(array $batch): LengthAwarePaginator
    {
        return Colli::with('booking', 'client', 'location', 'location.warehouse')
            ->where('colli_batch', '=', (int)$batch['data'])
            ->orderBy('id', 'desc')
            ->paginate(100);
    }

    /**
     * @param string $reference
     * @return LengthAwarePaginator
     */
    public function getByReference(string $reference): LengthAwarePaginator
    {
        return Colli::with('booking', 'client', 'location')
            ->where('reference', '=', $reference)
            ->orderBy('id', 'desc')
            ->paginate(1000);
    }

    /**
     * @param string $action
     * @param string $data
     * @param int $id
     * @return bool
     */
    public function saveHistoryLog(string $action, int $id, string $data)
    {
        $actionTime = Carbon::now();

        $logData = [
            'colli_id' => $id,
            'user_id' => Auth::user() ? Auth::user()->id : 1,
            'initiated_by' => Auth::user() ? Auth::user()->email : Log::DEFAULT_LOG_EMAIL,
            'action' => $action,
            'action_time' => $actionTime,
            'description' => $data,
        ];
        $log = new Log();
        return $log->create($logData);
    }

    /**
     * @param int $id
     * @return LengthAwarePaginator
     */
    public function getLog(int $id): LengthAwarePaginator
    {
        return Log::where('colli_id', '=', $id)
            ->where('action', '=', Log::logActions()[Log::LOG_ACTION_COLLI_LOCATION_CHANGED])
            ->paginate(100);
    }

    /**
     *
     * @param int $bookingId
     * @return bool
     */
    public function checkIfAllBookingCollisReceived(int $bookingId)
    {
        $booking = Booking::findOrFail($bookingId);

        if ($booking->collis->filter(function ($colli) {
            return !$colli->received_at;
        })->isEmpty()) {

            $isBookingPickupExists = (bool)$booking->pickupTrip;
            $isBookingLinehaulExists = (bool)$booking->linehaulTrip;
            $isBookingDropoffExists = (bool)$booking->dropoffTrip;

            if ($isBookingPickupExists && $isBookingDropoffExists && !$isBookingLinehaulExists) {
                $booking->pickupTrip->update(['status' => Trip::TRIP_STATUS_DELIVERED]);
            } elseif ($isBookingPickupExists && $isBookingLinehaulExists) {
                if ($booking->collis[0]->location) {

                    $linehaulDirection = null;
                    if ($booking->linehaul_type === '2-1') {
                        $linehaulDirection = 'WEST-EAST';
                    } elseif ($booking->linehaul_type === '1-2') {
                        $linehaulDirection = 'EAST-WEST';
                    }

                    if ($linehaulDirection) {
                        if ($booking->collis[0]->location->warehouse->nickname === 'Brøndby') {
                            if ($linehaulDirection === 'WEST-EAST') {
                                $booking->pickupTrip->update(['status' => Trip::TRIP_STATUS_DELIVERED]);
                                $booking->linehaulTrip->update(['status' => Trip::TRIP_STATUS_DELIVERED]);
                            } else {
                                $booking->pickupTrip->update(['status' => Trip::TRIP_STATUS_DELIVERED]);
                            }
                        } elseif ($booking->collis[0]->location->warehouse->nickname === 'Tørring') {
                            if ($linehaulDirection === 'EAST-WEST') {
                                $booking->pickupTrip->update(['status' => Trip::TRIP_STATUS_DELIVERED]);
                                $booking->linehaulTrip->update(['status' => Trip::TRIP_STATUS_DELIVERED]);
                            } else {
                                $booking->pickupTrip->update(['status' => Trip::TRIP_STATUS_DELIVERED]);
                            }
                        }
                    }
                }
            }
            //start communication flow
            // $communication = new Communication($booking->dropoffTrip, \App\Models\Communication::MESSAGE_TYPE_DO_CONFIRMATION);
            //$communication->start();
            return true;
        }
        return false;
    }
}
