<?php

namespace App\Repository;

use App\Models\Contact;
use App\Repository\Interfaces\ContactRepositoryInterface;
use Illuminate\Support\Collection;

class ContactRepository implements ContactRepositoryInterface
{
    /**
     * @var Contact
     */
    private $model;

    /**
     * @param Contact $contact
     */
    public function __construct(Contact $contact)
    {
        $this->model = $contact;
    }

    /**
     * @inheritDoc
     */
    public function create(array $data): Contact
    {
        $data['client_id'] = $data['clientId'];
        $data['primary_contact'] = 0;

        $contact = $this->model->create($data);
        return $contact;
    }

    /**
     * @param int $id
     * @return Contact
     */
    public function findOneById(int $id): Contact
    {
        return $this->model->find($id);
    }

    /**
     * @param array $data
     * @param Contact $contact
     * @return Contact
     * @throws \Exception
     */
    public function update(array $data, Contact $contact): Contact
    {
        $contact->name = $data['name'];

        if (!$contact->save()) {
            throw new \Exception("Can't update contact model");
        }

        return $contact;
    }

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool
    {
        return $this->model->destroy($id);
    }

    /**
     * @return Collection
     */
    public function getAll(): Collection
    {
        return $this->model->all();
    }
}
