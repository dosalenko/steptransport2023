<?php

namespace App\Repository;

use App\Models\Location;
use App\Repository\Interfaces\LocationRepositoryInterface;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class LocationRepository implements LocationRepositoryInterface
{
    /**
     * @var Location
     */
    private $model;

    /**
     * @param Location $location
     */
    public function __construct(Location $location)
    {
        $this->model = $location;
    }

    /**
     * @inheritDoc
     */
    public function create(array $data): Location
    {
        $location = $this->model->create($data);

        return $location;
    }

    /**
     * @param int $id
     * @return Location
     */
    public function findOneById(int $id): Location
    {
        return $this->model->find($id);
    }

    /**
     * @param array $data
     * @param Location $location
     * @return Location
     * @throws \Exception
     */
    public function update(array $data, Location $location): Location
    {
        if (!$location->save()) {
            throw new \Exception("Can't update model");
        }

        return $location;
    }

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool
    {
        return $this->model->destroy($id);
    }

    /**
     * @return Collection
     */
    public function getAll(): Collection
    {
        return $this->model->all();
    }

    /**
     * @param array $data
     * @return LengthAwarePaginator
     */
    public function get(array $data): LengthAwarePaginator
    {
        $length = ($data && isset($data['length'])) ? (int)$data['length'] : 100;
        $column = $data['column'] ?? 'name';
        $dir = $data['dir'] ?? 'desc';
        $search = ($data && isset($data['search'])) ? $data['search'] : '';

        $q = Location::query();

        if (!empty($search)) {
            $q->searchString($search);
        }

        if (!empty($data['warehouse'])) {
            $q->searchByWarehouse((int)$data['warehouse']);
        }

        if (!empty($data['type'])) {
            $q->searchByType((int)$data['type']);
        }

        return $q->paginate($length);
    }
}
