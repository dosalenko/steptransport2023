<?php

namespace App\Repository;

use App\Models\User;
use App\Repository\Interfaces\UserRepositoryInterface;
use Illuminate\Support\Collection;

class UserRepository implements UserRepositoryInterface
{
    /**
     * @var User
     */
    private $model;

    /**
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->model = $user;
    }

    /**
     * @inheritDoc
     */
    public function create(array $data): User
    {
        $roles = ["user"];
        $dataUser = [
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'client_id' => $data['clientId'],
            'roles' => $roles,
            //'agreement_id' => $data['agreementId'],
            'agreements' => json_encode($data['agreements']),
        ];
        $user = $this->model->create($dataUser);
        return $user;
    }

    /**
     * @param int $id
     * @return User
     */
    public function findOneById(int $id): User
    {
        return $this->model->find($id);
    }

    /**
     * @param array $data
     * @param User $user
     * @return User
     * @throws \Exception
     */
    public function update(array $data, User $user): User
    {
        $user->name = $data['name'];

        if (!$user->save()) {
            throw new \Exception("Can't update user model");
        }

        return $user;
    }

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool
    {
        return $this->model->destroy($id);
    }

    /**
     * @return Collection
     */
    public function getAll(): Collection
    {
        return $this->model->all();
    }
}
