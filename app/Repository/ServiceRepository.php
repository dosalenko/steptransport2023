<?php

namespace App\Repository;

use App\Models\AgreementService;
use App\Models\AgreementServicePrice;
use App\Models\Service;
use App\Repository\Interfaces\ServiceRepositoryInterface;
use Illuminate\Support\Collection;

class ServiceRepository implements ServiceRepositoryInterface
{
    /**
     * @var Service
     */
    private $model;

    /**
     * @param Service $service
     */
    public function __construct(Service $service)
    {
        $this->model = $service;
    }

    /**
     * @inheritDoc
     */
    public function create(array $data): Service
    {
        $service = $this->model->create($data);
        return $service;
    }

    /**
     * @param int $id
     * @return Service
     */
    public function findOneById(int $id): Service
    {
        return $this->model->find($id);
    }

    /**
     * @param array $data
     * @param Service $service
     * @return Service
     * @throws \Exception
     */
    public function update(array $data, Service $service): Service
    {
        $service->name = $data['name'];

        if (!$service->save()) {
            throw new \Exception("Can't update client model");
        }

        return $service;
    }

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool
    {
        $service = Service::findOrFail($id);
        AgreementServicePrice::whereHas('product', function ($q) use ($id) {
            $q->where('service_id', '=', $id);
        })->delete();
        AgreementService::where('service_id', $id)->delete();
        $service->prices()->delete();
        $service->delete();

        return true;
    }

    /**
     * @return Collection
     */
    public function getAll(): Collection
    {
        return $this->model->all();
    }
}
