<?php

namespace App\Repository;

use App\Models\Address;
use App\Models\Recurring;
use App\Models\Contact;
use App\Models\Log;
use App\Models\Trip;
use App\Models\UserRole;
use App\Repository\Interfaces\RecurringRepositoryInterface;
use App\Services\Core\Helpers\Helper;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class RecurringRepository implements RecurringRepositoryInterface
{
    /**
     * @var Recurring
     */
    private $model;

    /**
     * @param Recurring $booking
     */
    public function __construct(Recurring $booking)
    {
        $this->model = $booking;
    }

    /**
     * @inheritDoc
     */
    public function create(array $data): Recurring
    {
        $data['driver_id'] = $data['driver']['id'];
        $data['client_id'] = $data['client']['id'];
        $data['agreement_id'] = $data['agreement']['id'];
        $data['stops'] = json_encode($data['stops']);
        $data['agreement_services_ids'] = json_encode($data['agreement_services_ids']);
        $data['recurring_dates'] = json_encode($data['recurring_dates']);
        $data['internal_comment'] = $data['note'] ?? null;

        $recurring = Recurring::create($data);
        return $recurring;
    }

    /**
     * @param int $id
     * @return Recurring
     */
    public function findOneById(int $id): Recurring
    {
        return $this->model->find($id);
    }

    /**
     * @param array $data
     * @param Recurring $booking
     * @return Recurring
     * @throws \Exception
     */
    public function update(array $data, Recurring $recurring): Recurring
    {
        $recurringUpdateData = [
            'name' => $data['name'],
            'driver_id' => $data['driver']['id'],
            'client_id' => $data['client']['id'],
            'agreement_id' => $data['agreement']['id'],
            'stops' => json_encode($data['stops']) ?? $recurring['stops'],
            'agreement_product_id' => (int)$data['agreement_product_id'],
            'agreement_services_ids' => json_encode($data['agreement_services_ids']) ?? [],
            'recurring_dates' => json_encode($data['recurring_dates']) ?? $recurring['recurring_dates'],
            'total_price' => (float)($data['total_price']),
            'internal_comment' => $data['note'] ?? $recurring['note'],
            'reference' => $data['reference'] ?? $recurring['reference'],
        ];
        $recurring->update($recurringUpdateData);
        return $recurring;
    }

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool
    {
        return $this->model->destroy($id);
    }

    /**
     * @return Collection
     */
    public function getAll(): Collection
    {
        return $this->model->all();
    }

    /**
     * @param array $data
     * @return LengthAwarePaginator
     */
    public function get(array $data): LengthAwarePaginator
    {
        $length = $data['length'] ?? 100;
        $column = $data['column'] ?? 'id';
        $dir = $data['dir'] ?? 'desc';
        $search = $data['search'] ?? '';

        $q = Recurring::with('client', 'driver', 'agreement', 'product');

        if (!empty($search)) {
            $q->searchString($search);
        }

        return $q->paginate($length);
    }

}
