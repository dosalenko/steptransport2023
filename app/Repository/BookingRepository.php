<?php

namespace App\Repository;

use App\Http\Resources\TripResource;
use App\Models\Address;
use App\Models\AgreementService;
use App\Models\Booking;
use App\Models\BookingService;
use App\Models\Colli;
use App\Models\Communication as Model;
use App\Models\Contact;
use App\Models\Log;
use App\Models\Service;
use App\Models\Trip;
use App\Models\UserRole;
use App\Repository\Interfaces\BookingRepositoryInterface;
use App\Services\Communication\Communication;
use App\Services\Core\Helpers\BookingHelper;
use App\Services\Core\Helpers\ColliHelper;
use App\Services\Core\Helpers\Helper;
use App\Services\Optimoroute\Optimoroute;
use Carbon\Carbon;
use Exception;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use InvalidArgumentException;

class BookingRepository implements BookingRepositoryInterface
{
    /**
     * @var Booking
     */
    private $model;

    /**
     * @param Booking $booking
     */
    public function __construct(Booking $booking)
    {
        $this->model = $booking;
    }

    /**
     * @inheritDoc
     */
    public function create(array $data): Booking
    {
        $data['totalWeight'] = Helper::format($data['totalWeight']);
        $data['totalVolume'] = Helper::format($data['totalVolume']);
        $data['priceProduct'] = Helper::format($data['priceProduct']);
        $data['priceService'] = !empty($data['priceService']) ? Helper::format($data['priceService']) : 0;

        $addresses = Helper::setAddresses($data);

        $pickupDate = $data['pickupDate'];
        $pickupTimeStart = $data['pickupTimeStart'] ?
            str_replace(['HH', 'mm'], '00', $data['pickupTimeStart']) : '00:00';
        $pickupTimeEnd = $data['pickupTimeEnd'] ?
            str_replace(['HH', 'mm'], '00', $data['pickupTimeEnd']) : '00:00';

        $dropoffDate = $data['dropoffDate'];
        $dropoffTimeStart = $data['dropoffTimeStart'] ?
            str_replace(['HH', 'mm'], '00', $data['dropoffTimeStart']) : '00:00';
        $dropoffTimeEnd = $data['dropoffTimeEnd'] ?
            str_replace(['HH', 'mm'], '00', $data['dropoffTimeEnd']) : '00:00';

        $dataBooking = [
            'depends_on' => array_key_exists('createDependingBooking', $data) ? (int)$data['createDependingBooking'] : null,
            'client_id' => $data['clientId']['id'],
            'agreement_id' => $data['clientAgreement'],

            'pickup_address' => (int)$addresses['pickup'],
            'pickup_start' => !$pickupDate ?
                null : Carbon::createFromFormat(
                    'Y-m-d H:i',
                    $pickupDate . ' ' . $pickupTimeStart
                )->format('Y-m-d H:i:s'),
            'pickup_end' => !$pickupDate ? null : Carbon::createFromFormat(
                'Y-m-d H:i',
                $pickupDate . ' ' . $pickupTimeEnd
            )
                ->format('Y-m-d H:i:s'),
            'pickup_note' => $data['pickupNote'] ?? '',

            'dropoff_address' => (int)$addresses['dropoff'],
            'dropoff_start' => !$dropoffDate ? null : Carbon::createFromFormat('Y-m-d H:i', $dropoffDate . ' ' . $dropoffTimeStart)->format('Y-m-d H:i:s'),
            'dropoff_end' => !$dropoffDate ? null : Carbon::createFromFormat('Y-m-d H:i', $dropoffDate . ' ' . $dropoffTimeEnd)->format('Y-m-d H:i:s'),
            'dropoff_note' => $data['dropoffNote'] ?? '',
            'linehaul_type' => $data['linehaulType'],

            'note' => $data['note'] ?? '',
            'reference' => $data['reference'] ?? '',

            'agreement_product_id' => Booking::getStandardProductByAgreementProduct($data['productId']),

            /*
             * deprecated
             * @todo remove
             */
            'service_price' => Booking::getStandardAgreementPriceByAgreementService($data['newIndividualPriceService']),

            'total_volume' => $data['totalVolume'],
            'total_weight' => $data['totalWeight'],
            'total_colli' => $data['totalColli'] ?? 1,
            'product_price' => $data['priceProduct'] > 0 ? (float)($data['newTotalPriceProductByPiece']) : 0,
            'total_price' => (float)($data['priceProduct'] + $data['priceServiceSum']),
            'status' => (!empty($data['client'])) ? Booking::BOOKING_TYPE_NEW : (Auth::user()->hasRole(UserRole::ROLE_USER) ? Booking::BOOKING_TYPE_NEW : Booking::BOOKING_TYPE_CONFIRMED),
            'created_by' => empty($data['client']) ? Auth::user()->id : 1,
            'is_api' => !empty($data['isApiOrder']) ? 1 : 0,
            'pickup_address_type' => array_key_exists('pickup_address_type', $data) ? (int)$data['pickup_address_type'] : null,
            'dropoff_address_type' => array_key_exists('dropoff_address_type', $data) ? (int)$data['dropoff_address_type'] : null,
            'pickup_email' => array_key_exists('pickup_email', $data) ? $data['pickup_email'] : null,
            'dropoff_email' => array_key_exists('dropoff_email', $data) ? $data['dropoff_email'] : null,
            'rate_link' => md5(time() . rand()),
        ];
        $booking = $this->model->create($dataBooking);
        if ($booking) {
            $this->saveHistoryLog(Log::logActions()[Log::LOG_ACTION_CREATED], Auth::user() ? Auth::user()->email : Log::DEFAULT_LOG_EMAIL, $booking);

            foreach ($data['newIndividualPriceService'] as $k => $price) {
                if ($price !== null) {
                    $agreementService = AgreementService::findOrFail($k);
                    $bookingServiceData = [
                        'booking_id' => $booking->id,
                        'service_id' => $agreementService->service_id,
                        'price' => $price
                    ];
                    BookingService::create($bookingServiceData);
                }
            }

            //check and create trips
            $this->manageTrips($data, $booking, $dropoffDate);

            //manage collis
            if (!empty($data['colli'])) {
                $this->saveCollis($data['colli'], $booking, !empty($data['isApiOrder']) ? 1 : 0);
            }

            if ($booking->status === Booking::BOOKING_TYPE_CONFIRMED) {
                $this->assignDate($booking, Trip::TRIP_TYPE_PICKUP);
                if ($booking->collis->filter(function ($colli) {
                    return !$colli->received_at;
                })->isEmpty()) {
                    if (!(bool)$booking->pickupTrip && !(bool)$booking->linehaulTrip && (bool)$booking->dropoffTrip) {
                        $this->assignDate($booking, Trip::TRIP_TYPE_DROPOFF);
                    }
                }
            }

            return $booking;
        } else {
            throw new Exception("Can't create new booking");
        }
    }

    /*
     * @param array $data
     * @param $booking
     * @param bool $isApiOrder
     * @return bool
     */
    public function saveCollis($data, $booking, $isApiOrder = 0): bool
    {
        $findColliBatch = Colli::orderBy('colli_batch', 'desc')->first();
        $maxColliBatchNumber = $findColliBatch['colli_batch'];

        $colliDataNoExisting = [];
        foreach ($data as $k => $colli) {
            if ($isApiOrder === 1 || array_key_exists('isExisting', $colli) && $colli['isExisting'] === 0) {
                for ($i = 1; $i <= (int)$colli['colli']; $i++) {
                    $colliDataNoExisting[$k . '_' . $i] = [
                        'reference' => $colli['reference'],
                        'name' => $colli['name'],
                        'colli' => $i,
                        'volume' => round(Helper::format($colli['totalVolume'] ?? $colli['volume']) / (int)$colli['colli'], 2),
                        'weight' => round(Helper::format($colli['totalWeight'] ?? $colli['weight']) / (int)$colli['colli'], 2),
                        'booking_id' => $booking->id,
                        'client_id' => $booking->client_id,
                        'agreement_id' => $booking->agreement_id,
                        'status' => Colli::COLLI_STATUS_ON_DISPATCH_TERMINAL,
                        'colli_batch' => $maxColliBatchNumber + 1,
                        'created_by' => Auth::user() ? Auth::user()->id : 0,
                    ];
                }
                $maxColliBatchNumber += 1;
                $maxColliBatchNumber += 1;
            } elseif (!empty($colli['isExisting']) && $colli['isExisting'] === 1) {
                $colliExisting = Colli::findOrFail((int)$colli['id']);
                $getCollisByColliBatch = Colli::where('colli_batch', '=', $colliExisting->colli_batch)->get();
                foreach ($getCollisByColliBatch as $batch) {
                    $batch->booking_id = $booking->id;
                    $batch->save();
                }
            }
        }

        foreach ($colliDataNoExisting as $v) {
            $colli = Colli::create($v);
            if ($colli->booking_id !== null) {
                $action = 'Colli [' . $colli->id . '] have been created';
                $findPreparedColli = Log::where('booking_id', '=', $colli->booking_id)->where('action', '=', $action)->first();
                if (!$findPreparedColli) {
                    $this->saveHistoryLog($action, Auth::user() ? Auth::user()->email : Log::DEFAULT_LOG_EMAIL, $colli->booking);
                }
            }
        }

        return true;
    }

    /**
     * @param int $id
     * @return Booking
     */
    public function findOneById(int $id): Booking
    {
        return $this->model->find($id);
    }

    /**
     * @param array $data
     * @param Booking $booking
     * @return Booking
     * @throws Exception
     */
    public function update(array $data, Booking $booking): Booking
    {
        $oldData = clone $booking;

        $contact = Contact::where('client_id', $data['client_data']['id'])->firstOrFail();
        $contact->name = $data['client_data']['primary_contact']['name'];
        $contact->email = $data['client_data']['primary_contact']['email'];
        $contact->phone = $data['client_data']['primary_contact']['phone'];
        $contact->save();

        $updateOptimoRouteIds = [];

        /*update OR if changed:
        Adresse / Address
        Colli antal / Colli number
        Kubik / cubic
        Vægt / weight
        Telefonnummer / phone
        Navneændring / name
        Services / services
        Noter / notes*/
        if ($booking->pickupTrip) {
            if ((array_key_exists('phone', $data['pickup_address']) && $oldData->pickupTrip->address->phone !== $data['pickup_address']['phone']) ||
                (array_key_exists('pickup_note', $data) && $oldData->pickup_note !== $data['pickup_note'])
            ) {
                $updateOptimoRouteIds[$booking->pickupTrip->id] = $booking->pickupTrip->id;
            }
        }

        if ($booking->dropoffTrip) {
            if ((array_key_exists('phone', $data['dropoff_address']) && $oldData->dropoffTrip->address->phone !== $data['dropoff_address']['phone']) ||
                (array_key_exists('dropoff_note', $data) && $oldData->dropoff_note !== $data['dropoff_note'])
            ) {
                $updateOptimoRouteIds[$booking->dropoffTrip->id] = $booking->dropoffTrip->id;
            }
        }


        if ($data['pickup_address']['name'] !== $booking->pickupAddress->name ||
            $data['pickup_address']['street'] !== $booking->pickupAddress->street ||
            $data['pickup_address']['zip'] !== $booking->pickupAddress->zip ||
            $data['pickup_address']['city'] !== $booking->pickupAddress->city) {
            $pickupAddress = [
                'type' => Address::ADDRESS_TYPE_PICKUP,
                'name' => $data['pickup_address']['name'],
                'country_id' => $data['pickup_address']['country_id'],
                'zip' => $data['pickup_address']['zip'],
                'city' => $data['pickup_address']['city'],
                'street' => $data['pickup_address']['street'],
                'client_id' => $data['pickup_address']['client_id'],
                'phone' => $data['pickup_address']['phone'],
                'note' => $data['pickup_address']['note'],
            ];
            $pickupAddress = Address::create($pickupAddress);
            if ($booking->pickupTrip) {
                $updateOptimoRouteIds[$booking->pickupTrip->id] = $booking->pickupTrip->id;
            }
        } else {
            $pickupAddress = Address::findOrFail($data['pickup_address']['id']);
            $pickupAddress->update($data['pickup_address']);
        }

        if ($data['dropoff_address']['name'] !== $booking->dropoffAddress->name ||
            $data['dropoff_address']['street'] !== $booking->dropoffAddress->street ||
            $data['dropoff_address']['zip'] !== $booking->dropoffAddress->zip ||
            $data['dropoff_address']['city'] !== $booking->dropoffAddress->city) {
            $dropoffAddress = [
                'type' => Address::ADDRESS_TYPE_DROPOFF,
                'name' => $data['dropoff_address']['name'],
                'country_id' => $data['dropoff_address']['country_id'],
                'zip' => $data['dropoff_address']['zip'],
                'city' => $data['dropoff_address']['city'],
                'street' => $data['dropoff_address']['street'],
                'client_id' => $data['dropoff_address']['client_id'],
                'phone' => $data['dropoff_address']['phone'],
                'note' => $data['dropoff_address']['note'],
            ];
            $dropoffAddress = Address::create($dropoffAddress);
            if ($booking->dropoffTrip) {
                $updateOptimoRouteIds[$booking->dropoffTrip->id] = $booking->dropoffTrip->id;
            }
        } else {
            $dropoffAddress = Address::findOrFail($data['dropoff_address']['id']);
            $dropoffAddress->update($data['dropoff_address']);
        }

        //services management
        $servicesData = [];
        $servicesSumPrice = 0;
        foreach ($data['services_data'] as $service) {
            if (in_array($service['id'], $data['new_services'])) {
                $servicesData[$service['id']] = (float)$service['price'];
                $servicesSumPrice += (float)$service['price'];
            } else {
                unset($servicesData[$service['id']]);
            }
        }

        $newServicesPrices = BookingHelper::getServicePriceByInterval(json_encode($data['new_services']), $booking->total_volume, $booking->total_weight, $booking->agreement_id, 1);
        //check if new services were added

        foreach ($data['new_services'] as $service) {
            if (!array_key_exists($service, $servicesData)) {
                if (array_key_exists('priceServiceDefaultAgreements', $newServicesPrices)) {
                    if (array_key_exists($service, $newServicesPrices['priceServiceDefaultAgreements'])) {
                        $servicesData[$service] = $newServicesPrices['priceServiceDefaultAgreements'][$service];
                        $servicesSumPrice += $newServicesPrices['priceServiceDefaultAgreements'][$service];
                    } else {
                        $servicesData[$service] = 0;
                    }
                } else {
                    $servicesData[$service] = 0;
                }
            }
        }

        if ($oldData['service_price'] !== json_encode(array_flip($data['new_services']))) {
            if ($booking->pickupTrip) {
                $updateOptimoRouteIds[$booking->pickupTrip->id] = $booking->pickupTrip->id;
            }
            if ($booking->dropoffTrip) {
                $updateOptimoRouteIds[$booking->dropoffTrip->id] = $booking->dropoffTrip->id;
            }
        }

        $bookingUpdateData = [
            'status' => $data['status_id'],
            'pickup_address' => $pickupAddress->id,
            'dropoff_address' => $dropoffAddress->id,
            'pickup_start' => $data['pickup_date'] ? $data['pickup_date'] . ' ' . $data['pickup_start'] : null,
            'pickup_end' => $data['pickup_date'] ? $data['pickup_date'] . ' ' . $data['pickup_end'] : null,
            'dropoff_start' => $data['dropoff_date'] ? $data['dropoff_date'] . ' ' . $data['dropoff_start'] : null,
            'dropoff_end' => $data['dropoff_date'] ? $data['dropoff_date'] . ' ' . $data['dropoff_end'] : null,
            'pickup_note' => $data['pickup_note'] ?? '',
            'dropoff_note' => $data['dropoff_note'] ?? '',
            'note' => $data['note'] ?? '',
            'reference' => $data['reference'] ?? '',
            'total_volume' => $data['volume'],
            'total_weight' => $data['weight'],
            'total_colli' => $data['colli'] ?? 1,
            'product_price' => (float)$data['product_price'],
            'service_price' => json_encode($servicesData),
            'total_price' => (float)$data['product_price'] + $servicesSumPrice,
            'linehaul_type' => $data['linehaul_type'],
            'internal_comment' => $data['internal_comment'],
            //'service_time_units' => $data['service_time_units'],
            'product_time_units' => (float)$data['product_time_units'],
        ];

        //booking services
        foreach ($servicesData as $k => $bookingService) {
            $checkNewBookingService = BookingService::where('booking_id', '=', $booking->id)->where('service_id', '=', $k)->first();
            if (!$checkNewBookingService) {
                $timeUnitKey = array_search($k, array_column($data['services_data'], 'id'));
                $bookingServiceData = [
                    'booking_id' => $booking->id,
                    'service_id' => $k,
                    'price' => $bookingService,
                    'time_unit' => $timeUnitKey !== false && $timeUnitKey >= 0 ? (float)$data['services_data'][$timeUnitKey]['time_unit'] : 0,
                ];
                BookingService::create($bookingServiceData);
            }
        }

        $bookingServices = BookingService::where('booking_id', '=', $booking->id)->get();
        foreach ($bookingServices as $bookingService) {
            if (!array_key_exists($bookingService->service_id, $servicesData)) {
                $bookingService->delete();
            } else {
                $timeUnitKey = array_search($bookingService->service_id, array_column($data['services_data'], 'id'));

                $bookingService->price = $servicesData[$bookingService->service_id];
                $bookingService->time_unit = $timeUnitKey !== false && $timeUnitKey >= 0 ? (float)$data['services_data'][$timeUnitKey]['time_unit'] : 0;
                $bookingService->save();
            }
        }


        //add new linehaul trips
        if ($booking->linehaul_type === '0-0' && $data['linehaul_type'] !== '0-0') {
            $tripData = [
                'status' => Trip::TRIP_STATUS_NEW,
                'type' => Trip::TRIP_TYPE_LINEHAUL,
                'trip_address' => $booking->pickup_address,
                'booking_id' => $booking->id,
                'client_id' => $booking->client_id,
                'spent_minutes' => 0,
            ];
            Trip::create($tripData);
        }

        if ($booking->linehaul_type !== '0-0' && $data['linehaul_type'] === '0-0') {
            $trip = Trip::where('booking_id', '=', $booking->id)->where('type', '=', Trip::TRIP_TYPE_LINEHAUL)->first();
            $trip->delete();
        }

        if ($booking->linehaul_type !== '0-0' && $data['linehaul_type'] !== $booking->linehaul_type) {
            $pickupTrip = Trip::where('booking_id', '=', $booking->id)->where('type', '=', Trip::TRIP_TYPE_PICKUP)->first();
            $dropoffTrip = Trip::where('booking_id', '=', $booking->id)->where('type', '=', Trip::TRIP_TYPE_DROPOFF)->first();

            if ($pickupTrip) {
                $currentPickupAddress = $pickupTrip->trip_address;
                $pickupTrip->trip_address = $currentPickupAddress;
                $pickupTrip->save();
            }

            if ($dropoffTrip) {
                $currentDropoffAddress = $dropoffTrip->trip_address;
                $dropoffTrip->trip_address = $currentDropoffAddress;
                $dropoffTrip->save();
            }
        }

        //synchronize time and addresses between trips and bookings
        foreach ($booking->trips as $bookingTrip) {
            $bookingTrip = Trip::findOrFail($bookingTrip->id);
            if ($bookingTrip->type == Trip::TRIP_TYPE_PICKUP) {

                $bookingTrip->trip_address = $bookingUpdateData['pickup_address'];
                $bookingTrip->note = $bookingUpdateData['pickup_note'];

                if ($data['delivered_date']['pickupDate'] !== null) {
                    $formattedDate = Carbon::createFromFormat('Y-m-d', $data['delivered_date']['pickupDate'])->format('Y-m-d');
                    if (!$bookingTrip->tw_start) {
                        $bookingTrip->tw_start = $formattedDate . ' 00:00:00';
                        $bookingTrip->tw_end = $formattedDate . ' 00:00:00';
                        $bookingTrip->status = Trip::TRIP_STATUS_NEW;
                        $bookingTrip->save();
                        $bookingUpdateData['pickup_start'] = $formattedDate . ' 00:00:00';
                        $bookingUpdateData['pickup_end'] = $formattedDate . ' 00:00:00';

                        $tripDataJson = [];
                        $tripDatasJon[] = json_decode(TripResource::make($bookingTrip)->toJson(), true);

                        //create new external optimo route order based on current trip
                        $optimoroute = new Optimoroute();
                        if (!empty($tripDataJson)) {
                            $optimoroute->createOrder($tripDataJson, true);
                        }
                    }
                } else {
                    $bookingTrip->tw_start = $bookingUpdateData['pickup_start'];
                    $bookingTrip->tw_end = $bookingUpdateData['pickup_end'];

                }
                $bookingTrip->save();
            } elseif ($bookingTrip->type == Trip::TRIP_TYPE_DROPOFF) {
                $bookingTrip->trip_address = $bookingUpdateData['dropoff_address'];
                $bookingTrip->note = $bookingUpdateData['dropoff_note'];
                if ($data['delivered_date']['dropoffDate'] !== null) {
                    $formattedDate = Carbon::createFromFormat('Y-m-d', $data['delivered_date']['dropoffDate'])->format('Y-m-d');
                    if (!$bookingTrip->tw_start) {
                        $bookingTrip->tw_start = $formattedDate . ' 00:00:00';
                        $bookingTrip->tw_end = $formattedDate . ' 00:00:00';
                        $bookingTrip->status = Trip::TRIP_STATUS_NEW;
                        $bookingTrip->save();
                        $bookingUpdateData['dropoff_start'] = $formattedDate . ' 00:00:00';
                        $bookingUpdateData['dropoff_end'] = $formattedDate . ' 00:00:00';

                        $tripDataJson = [];
                        $tripDataJson[] = json_decode(TripResource::make($bookingTrip)->toJson(), true);

                        //create new external optimo route order based on current trip
                        $optimoroute = new Optimoroute();
                        if (!empty($tripDataJson)) {
                            $optimoroute->createOrder($tripDataJson, true);
                        }
                    }
                } else {
                    $bookingTrip->tw_start = $bookingUpdateData['dropoff_start'];
                    $bookingTrip->tw_end = $bookingUpdateData['dropoff_end'];
                }
                $bookingTrip->save();
            } elseif ($bookingTrip->type == Trip::TRIP_TYPE_LINEHAUL && $bookingTrip->status !== Trip::TRIP_STATUS_DELIVERED) {
                if ($oldData->dropoff_start) {
                    $dropoffData = Carbon::parse($oldData->dropoff_start)->toDateString();
                    if ($dropoffData !== $data['dropoff_date']) {
                        $linehaulDate = Carbon::parse($data['dropoff_date'])->subWeekdays(1)->toDateString();
                        $bookingTrip->tw_start = $linehaulDate . ' 00:00:00';
                        $bookingTrip->tw_end = $linehaulDate . ' 00:00:00';
                        $bookingTrip->save();
                    }
                } else {
                    if (!empty($data['dropoff_date'])) {
                        $linehaulDate = Carbon::parse($data['dropoff_date'])->subWeekdays(1)->toDateString();
                        $bookingTrip->tw_start = $linehaulDate . ' 00:00:00';
                        $bookingTrip->tw_end = $linehaulDate . ' 00:00:00';
                        $bookingTrip->save();
                    }
                }
            }
        }


        /*
        * colli management
        */

        //delete existing collis
        if (!empty($data['colli_deleted'])) {
            foreach ($data['colli_deleted'] as $colli) {
                $colliExisting = Colli::where('id', (int)$colli['id'])->first();
                if ($colliExisting) {
                    if ($colliExisting->location_id == null) {
                        $getCollisByColliBatch = Colli::where('colli_batch', '=', $colliExisting->colli_batch)->get();
                        foreach ($getCollisByColliBatch as $batch) {
                            if ($batch->booking_id !== null) {
                                $action = 'Colli [' . $batch->id . '] have been deleted';
                                $findPreparedColli = Log::where('booking_id', '=', $batch->booking_id)->where('action', '=', $action)->first();
                                if (!$findPreparedColli) {
                                    $this->saveHistoryLog($action, Auth::user()->email, $batch->booking);
                                }
                            }
                        }
                        Colli::where('colli_batch', '=', $colliExisting->colli_batch)->delete();
                    } else {
                        $getCollisByColliBatch = Colli::where('colli_batch', '=', $colliExisting->colli_batch)->get();
                        foreach ($getCollisByColliBatch as $batch) {
                            $batch->booking_id = null;
                            $batch->save();
                        }
                    }
                }
            }
        }

        //add new collis
        if (!empty($data['colli_data'])) {
            //split colli
            $this->saveCollis($data['colli_data'], $booking);
        }

        //recalculate prices and dimensions if collis changed
        if (!empty($data['colliDataChanged'])) {
            //edit colli data
            foreach ($data['colli_data'] as $v) {
                if (!empty($v['id'])) {
                    $colliExisting = Colli::where('id', '=', (int)$v['id'])->first();
                    if ($colliExisting) {
                        if (!empty($data['colliBatchNumberChanged'])) {
                            $colliNumber = Colli::where('colli_batch', '=', $colliExisting->colli_batch)->count('id');
                            if ($v['totalColliAmountByColliBatch'] !== $colliNumber) {
                                ColliHelper::addNewColliToBatch($colliExisting, $v['totalColliAmountByColliBatch'] - $colliNumber + 1, 1);
                            }
                        }
                        ColliHelper::updateColliBatch($v, $colliExisting->colli_batch);
                    }
                }
            }

            //recalculate prices
            $newTotalVolume = 0;
            $newTotalWeight = 0;
            foreach ($booking->collis as $v) {
                $newTotalVolume += $v->volume;
                $newTotalWeight += $v->weight;
            }

            $newTotalProductPrice = BookingHelper::getProductPriceByInterval($booking->agreement_product_id, $newTotalVolume, $newTotalWeight, $booking->agreement_id, 1);
            $newTotalServicePrice = BookingHelper::getServicePriceByInterval(json_encode($data['new_services']), $newTotalVolume, $newTotalWeight, $booking->agreement_id, 1);

            $booking->total_volume = $newTotalVolume;
            $booking->total_weight = $newTotalWeight;

            $bookingUpdateData['product_price'] = (float)$newTotalProductPrice;
            if (array_key_exists('priceServiceDefaultAgreements', $newTotalServicePrice)) {
                $bookingUpdateData['service_price'] = json_encode($newTotalServicePrice['priceServiceDefaultAgreements']);
            }
            $bookingUpdateData['total_price'] = (float)$newTotalProductPrice + (float)$newTotalServicePrice['priceServiceSum'];

            if ($booking->pickupTrip) {
                $updateOptimoRouteIds[$booking->pickupTrip->id] = $booking->pickupTrip->id;
            }
            if ($booking->dropoffTrip) {
                $updateOptimoRouteIds[$booking->dropoffTrip->id] = $booking->dropoffTrip->id;
            }
        }

        //check statuses
        $bookingStatusOld = $booking->status;
        $booking->update($bookingUpdateData);
        $bookingStatusNew = $booking->status;
        if ($bookingStatusOld != $bookingStatusNew) {
            $action = Log::logActions()[Log::LOG_ACTION_STATUS_CHANGED] . ' (' . Booking::bookingStatuses()[$bookingStatusOld] . ' -> ' . Booking::bookingStatuses()[$bookingStatusNew] . ')';
        } else {
            $action = Log::logActions()[Log::LOG_ACTION_UPDATED];
        }

        if (!empty($updateOptimoRouteIds)) {
            $tripDataJson = [];
            foreach ($updateOptimoRouteIds as $k => $v) {
                $trip = Trip::with('booking', 'booking.dropoffAddress', 'booking.collis', 'client', 'client.primaryContact')->findOrFail($k);
                if ($trip->status >= Trip::TRIP_STATUS_TRANSFERRED && $trip->status <= Trip::TRIP_STATUS_DELIVERED && $trip->booking->status > Booking::BOOKING_TYPE_NEW) {
                    $tripDataJson[] = json_decode(TripResource::make($trip)->toJson(), true);
                }
            }

            //create new external optimo route order based on current trip
            $optimoroute = new Optimoroute();
            if (!empty($tripDataJson)) {
                $optimoroute->createOrder($tripDataJson, true);
            }
        }

        $this->saveHistoryLog($action, Auth::user()->email, $booking, $oldData);
        $booking->refresh();
        //start communication flow
        if ($booking->dropoffTrip) {
            $communication = new Communication($booking->dropoffTrip, \App\Models\Communication::MESSAGE_TYPE_DO_CONFIRMATION);
            $communication->start();
        }

        if ($booking->pickupTrip) {
            $communication = new Communication($booking->pickupTrip, \App\Models\Communication::MESSAGE_TYPE_PU_CONFIRMATION);
            $communication->start();
        }

        return $booking;
    }

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool
    {
        $booking = Booking::findOrFail($id);
        $userRole = in_array('user', Auth::user()->roles) ? UserRole::ROLE_USER : UserRole::ROLE_ADMIN;
        if ($userRole === UserRole::ROLE_USER
            && $booking->created_by !== Auth::user()->id
            && $booking->status !== Booking::BOOKING_TYPE_NEW) {
            return false;
        }
        $booking->deleted_at = Carbon::now();
        if ($booking->save()) {
            foreach ($booking->collis as $v) {
                $colli = Colli::where('booking_id', '=', $v->booking_id)->first();
                if (!$colli->location_id) {
                    $colli->delete();
                } else {
                    $colli->booking_id = null;
                    $colli->save();
                }
            }
            foreach ($booking->trips as $v) {
                $trip = Trip::where('booking_id', '=', $v->booking_id)->first();
                $trip->deleted_at = Carbon::now();
                $trip->save();
            }
            $this->saveHistoryLog(
                Log::logActions()[Log::LOG_ACTION_DELETED],
                Auth::user()->email,
                $booking
            );
            return true;
        }
        return false;
    }

    /**
     * @return Collection
     */
    public function getAll(): Collection
    {
        return $this->model->all();
    }

    /**
     * @param array $data
     * @return LengthAwarePaginator
     */
    public function get(array $data): LengthAwarePaginator
    {
        $length = $data['length'] ?? 100;
        $column = $data['column'] ?? 'id';
        $dir = $data['dir'] ?? 'desc';
        $search = $data['search'] ?? '';
        $apiOrders = $data['apiOrders'] ?? '';

        $status = array_key_exists('activeStatus', $data) ? (int)$data['activeStatus'] : 0;

        $q = Booking::with('client', 'trips', 'trips.address', 'trips.address.country', 'pickupAddress', 'pickupAddress.country', 'dropoffAddress', 'dropoffAddress.country', 'logs', 'collis')
            ->whereNull('deleted_at');

        if (Auth::user() && in_array('user', Auth::user()->roles)) {
            $q->where('client_id', '=', Auth::user()->client_id);
        }

        if (array_key_exists('excludeDelivered', $data) && (bool)$data['excludeDelivered'] === true) {
            $q->where('status', '<>', Booking::BOOKING_TYPE_DELIVERED);
        }

        if (!empty($search)) {
            $q->searchString($search);
        }

        if (!empty($data['dateFrom']) || !empty($data['dateTo'])) {
            $q->searchByDate((int)$data['dateType'], $data['dateFrom'] ?? '', $data['dateTo'] ?? '');
        }

        if ($status > 0) {
            $q->searchByStatus($status);
        }

        if (!empty($apiOrders)) {
            $q->searchApiOrders($apiOrders);
        }

        if ($column === 'address') {
            $q->orderByRaw('(SELECT name FROM addresses WHERE addresses.id = bookings.dropoff_address) ' . $dir . ' ');
        } elseif ($column === 'client') {
            $q->orderByRaw('(SELECT name FROM clients WHERE clients.id = bookings.client_id) ' . $dir . ' ');
        } else {
            $q->orderBy($column, $dir);
        }

        return $q->paginate($length);
    }

    /**
     * @param string $action
     * @param string $initiated_by
     * @param Booking $booking
     *
     * return bool
     */
    public function saveHistoryLog(string $action, string $initiated_by, Booking $booking, $oldData = null)
    {
        if ($action == Log::logActions()[Log::LOG_ACTION_DELETED]) {
            $booking->deleted_at = Carbon::now();
            if ($booking->save()) {
                $actionTime = $booking->deleted_at;
            } else {
                $actionTime = Carbon::now();
            }
        } else {
            $actionTime = Carbon::now();
        }

        $changes = [];
        if ($oldData !== null) {
            if ((int)$oldData->status !== (int)$booking->status) {
                $changes['status'] = [
                    Booking::bookingStatuses()[(int)$oldData->status],
                    Booking::bookingStatuses()[(int)$booking->status],
                ];
            }
            if ((int)$oldData->pickup_address !== (int)$booking->pickup_address) {
                $changes['pickup_address'] = [
                    Address::where('id', '=', (int)$oldData->pickup_address)->first()->name,
                    Address::where('id', '=', (int)$booking->pickup_address)->first()->name,
                ];
            }
            if (substr($oldData->pickup_start, 0, 10) !== substr($booking->pickup_start, 0, 10)) {
                $changes['pickup_start'] = [
                    $oldData->pickup_start,
                    $booking->pickup_start,
                ];
            }
            if (substr($oldData->pickup_end, 0, 10) !== substr($booking->pickup_end, 0, 10)) {
                $changes['pickup_end'] = [
                    $oldData->pickup_end,
                    $booking->pickup_end,
                ];
            }
            if ($oldData->pickup_note !== $booking->pickup_note) {
                $changes['pickup_note'] = [
                    $oldData->pickup_note,
                    $booking->pickup_note,
                ];
            }

            if ((int)$oldData->dropoff_address !== (int)$booking->dropoff_address) {
                $changes['dropoff_address'] = [
                    Address::where('id', '=', (int)$oldData->dropoff_address)->first()->name,
                    Address::where('id', '=', (int)$booking->dropoff_address)->first()->name,
                ];
            }
            if (substr($oldData->dropoff_start, 0, 10) !== substr($booking->dropoff_start, 0, 10)) {
                $changes['dropoff_start'] = [
                    $oldData->dropoff_start,
                    $booking->dropoff_start,
                ];
            }
            if (substr($oldData->dropoff_end, 0, 10) !== substr($booking->dropoff_end, 0, 10)) {
                $changes['dropoff_end'] = [
                    $oldData->dropoff_end,
                    $booking->dropoff_end,
                ];
            }
            if ($oldData->dropoff_note !== $booking->dropoff_note) {
                $changes['dropoff_note'] = [
                    $oldData->dropoff_note,
                    $booking->dropoff_note,
                ];
            }
            if ($oldData->note !== $booking->note) {
                $changes['note'] = [
                    $oldData->note,
                    $booking->note,
                ];
            }
            if ($oldData->reference !== $booking->reference) {
                $changes['reference'] = [
                    $oldData->reference,
                    $booking->reference,
                ];
            }
            if ($oldData->internal_comment !== $booking->internal_comment) {
                $changes['internal_comment'] = [
                    $oldData->internal_comment,
                    $booking->internal_comment,
                ];
            }
            if ($oldData->linehaul_type !== $booking->linehaul_type) {
                $changes['linehaul_type'] = [
                    $oldData->linehaul_type,
                    $booking->linehaul_type,
                ];
            }
            if ($oldData->product_price !== $booking->product_price) {
                $changes['product_price'] = [
                    $oldData->product_price,
                    $booking->product_price,
                ];
            }
            if ($oldData->service_price !== $booking->service_price) {
                $changes['services'] = [
                    implode(',', Service::getServicesNames($oldData->service_price)),
                    implode(',', Service::getServicesNames($booking->service_price)),
                ];
            }
            if ($oldData->total_price !== $booking->total_price) {
                $changes['total_price'] = [
                    $oldData->total_price,
                    $booking->total_price,
                ];
            }
        }

        $logData = [
            'booking_id' => $booking->id,
            'user_id' => $booking->created_by === 0 ? 1 : $booking->created_by,
            'initiated_by' => $initiated_by,
            'action' => $action,
            'action_time' => $actionTime,
            'description' => count($changes) > 0 ? json_encode($changes) : null,
        ];
        $log = new Log();
        \Illuminate\Support\Facades\Log::channel('applog')->info("Booking info previous" . json_encode($booking));
        \Illuminate\Support\Facades\Log::channel('applog')->info("Booking info changes" . json_encode($changes));

        return $log->create($logData);
    }

    /*
    * From client: 'System can't take a booking from state 8 to 4.
    * Only the user can.
    * However, system can take booking from 8 to 9'
    * need to recheck statuses many times in a row
    * @param int $tripBooking
    * @todo add exceptions
    *
    * @param Booking $tripBooking
    * return bool
    */
    public function checkStatusUpdates($tripBooking): bool
    {
        $booking = Booking::with('pickupTrip', 'dropoffTrip', 'linehaulTrip')->where('id', '=', $tripBooking)->first();
        if ($booking) {
            $bookingStatusNew = 0;
            $bookingStatusOld = $booking->status;

            $isBookingPickupExists = (bool)$booking->pickupTrip;
            $isBookingLinehaulExists = (bool)$booking->linehaulTrip;
            $isBookingDropoffExists = (bool)$booking->dropoffTrip;

            $pickupStatus = $isBookingPickupExists ? $booking->pickupTrip->status : 0;
            $linehaulStatus = $isBookingLinehaulExists ? $booking->linehaulTrip->status : 0;
            $dropoffStatus = $isBookingDropoffExists ? $booking->dropoffTrip->status : 0;

            $address = $isBookingPickupExists && $booking->pickupTrip->address && $booking->pickupTrip->address->name && ($booking->pickupTrip->address->name === 'Step Transport' || $booking->pickupTrip->address->name === 'Step Tørring') ? 'own' : null;
            if ($booking->status !== Booking::BOOKING_TYPE_NEW) {
                if ($booking->status === Booking::BOOKING_TYPE_CONFIRMED) {
                    $bookingStatusNew = Booking::BOOKING_TYPE_AWAITING_PICKUP;
                } elseif
                ($isBookingPickupExists && $pickupStatus === TRIP::TRIP_STATUS_DELIVERED && $isBookingLinehaulExists && $linehaulStatus === TRIP::TRIP_STATUS_DELIVERED && $isBookingDropoffExists && $dropoffStatus === TRIP::TRIP_STATUS_DELIVERED) {
                    $bookingStatusNew = Booking::BOOKING_TYPE_DELIVERED;
                } elseif
                (!$isBookingPickupExists && !$isBookingLinehaulExists && $isBookingDropoffExists && $dropoffStatus === TRIP::TRIP_STATUS_DELIVERED) {
                    $bookingStatusNew = Booking::BOOKING_TYPE_DELIVERED;
                } elseif
                (!$isBookingDropoffExists && !$isBookingLinehaulExists && $isBookingPickupExists && $dropoffStatus === TRIP::TRIP_STATUS_DELIVERED) {
                    $bookingStatusNew = Booking::BOOKING_TYPE_DELIVERED;
                } elseif
                ($isBookingPickupExists && $pickupStatus === TRIP::TRIP_STATUS_DELIVERED && !$isBookingLinehaulExists && $isBookingDropoffExists && $dropoffStatus < TRIP::TRIP_STATUS_DELIVERED) {
                    $bookingStatusNew = Booking::BOOKING_TYPE_READY_FOR_DELIVERY;
                } elseif
                ($isBookingPickupExists && $pickupStatus === TRIP::TRIP_STATUS_DELIVERED && !$isBookingLinehaulExists && $isBookingDropoffExists && $dropoffStatus === TRIP::TRIP_STATUS_DELIVERED) {
                    $bookingStatusNew = Booking::BOOKING_TYPE_DELIVERED;
                } elseif
                (!$isBookingPickupExists && $isBookingLinehaulExists && $linehaulStatus !== TRIP::TRIP_STATUS_DELIVERED) {
                    $bookingStatusNew = Booking::BOOKING_TYPE_AWAITING_LINEHAUL;
                } elseif
                ($isBookingPickupExists && $isBookingLinehaulExists && $address && $linehaulStatus !== TRIP::TRIP_STATUS_DELIVERED) {
                    $bookingStatusNew = Booking::BOOKING_TYPE_AWAITING_LINEHAUL;
                } elseif
                ($isBookingPickupExists && $pickupStatus === TRIP::TRIP_STATUS_SERVICING && $booking->status <= Booking::BOOKING_TYPE_AWAITING_TRANSIT) {
                    $bookingStatusNew = Booking::BOOKING_TYPE_AWAITING_TRANSIT;
                } elseif
                ($isBookingPickupExists && $isBookingLinehaulExists && $pickupStatus === TRIP::TRIP_STATUS_DELIVERED && $booking->status <= Booking::BOOKING_TYPE_AWAITING_LINEHAUL && $pickupStatus >= TRIP::TRIP_STATUS_SERVICING && $linehaulStatus !== TRIP::TRIP_STATUS_DELIVERED) {
                    $bookingStatusNew = Booking::BOOKING_TYPE_AWAITING_LINEHAUL;
                } elseif
                (!$isBookingLinehaulExists && $pickupStatus >= TRIP::TRIP_STATUS_SERVICING && $pickupStatus != TRIP::TRIP_STATUS_DELIVERED) {
                    //} elseif (!$isBookingLinehaulExists && $pickupStatus >= TRIP::TRIP_STATUS_SERVICING) {
                    $bookingStatusNew = Booking::BOOKING_TYPE_READY_FOR_DELIVERY;
                } elseif
                ($isBookingLinehaulExists && $linehaulStatus === TRIP::TRIP_STATUS_DELIVERED && $booking->status <= Booking::BOOKING_TYPE_READY_FOR_DELIVERY && $pickupStatus >= TRIP::TRIP_STATUS_SERVICING) {
                    $bookingStatusNew = Booking::BOOKING_TYPE_READY_FOR_DELIVERY;
                } elseif
                (!$isBookingDropoffExists && $pickupStatus >= TRIP::TRIP_STATUS_SERVICING) {
                    $bookingStatusNew = Booking::BOOKING_TYPE_DELIVERED;
                } elseif
                ($isBookingDropoffExists && !$isBookingPickupExists && $isBookingLinehaulExists && $linehaulStatus === TRIP::TRIP_STATUS_DELIVERED && $dropoffStatus !== TRIP::TRIP_STATUS_DELIVERED) {
                    $bookingStatusNew = Booking::BOOKING_TYPE_READY_FOR_DELIVERY;
                } elseif
                ($isBookingDropoffExists && !$isBookingPickupExists && !$isBookingLinehaulExists && $dropoffStatus !== TRIP::TRIP_STATUS_DELIVERED) {
                    $bookingStatusNew = Booking::BOOKING_TYPE_READY_FOR_DELIVERY;
                } elseif
                ($isBookingDropoffExists && $dropoffStatus === TRIP::TRIP_STATUS_SERVICING && $booking->status <= Booking::BOOKING_TYPE_BEING_DELIVERED && $pickupStatus >= TRIP::TRIP_STATUS_SERVICING) {
                    $bookingStatusNew = Booking::BOOKING_TYPE_BEING_DELIVERED;
                } elseif
                ($isBookingDropoffExists && $dropoffStatus === TRIP::TRIP_STATUS_DELIVERED && $booking->status <= Booking::BOOKING_TYPE_DELIVERED) {
                    $bookingStatusNew = Booking::BOOKING_TYPE_DELIVERED;
                }
            }

            if ($bookingStatusNew > 0) {
                if ($bookingStatusNew === Booking::BOOKING_TYPE_DELIVERED) {
                    $this->setColliDelivered($booking->id);
                }
                $booking->status = $bookingStatusNew;
                if ($booking->save()) {
                    if ($bookingStatusOld !== $bookingStatusNew) {
                        $this->saveHistoryLog(Log::logActions()[Log::LOG_ACTION_STATUS_CHANGED] . ' (' . Booking::bookingStatuses()[$bookingStatusOld] . ' -> ' . Booking::bookingStatuses()[$bookingStatusNew] . ')', 'Step Core', $booking);
                        $this->checkStatusUpdates($tripBooking);
                    }
                }
            }
        }
        return true;
    }

    /*
     * @param int $bookingId
     * @return bool
     */
    public function setColliDelivered($bookingId): bool
    {
        $colli = Colli::where('booking_id', '=', $bookingId)->first();
        if ($colli) {
            $colli->status = Colli::COLLI_STATUS_DELIVERED;
            $colli->colli_packing_status = Colli::COLLI_PACKING_STATUS_LOADED;
            $colli->location_id = null;
            $colli->save();
            if ($colli->booking_id !== null) {
                $action = 'Colli [' . $colli->id . '] have been delivered';
                $findPreparedColli = Log::where('booking_id', '=', $colli->booking_id)->where('action', '=', $action)->first();
                if (!$findPreparedColli) {
                    $this->saveHistoryLog($action, Auth::user() ? Auth::user()->email : Log::DEFAULT_LOG_EMAIL, $colli->booking);
                }
            }
        }
        return true;
    }

    /**
     * @param Booking $booking
     * @param int $pickupType
     * @return bool
     * @throws Exception
     */
    public function assignDate(Booking $booking, int $pickupType): bool
    {
        if ($pickupType === Trip::TRIP_TYPE_PICKUP) {
            $trip = $booking->pickupTrip;
            $addressType = Booking::ADDRESS_TYPE_CUSTOMER;
            $startDateField = 'pickup_start';
            $endDateField = 'pickup_end';
            $assignDate = false;
            if ($trip && $booking->pickup_address_type === $addressType && !$trip->tw_start) {
                $assignDate = true;
            };
        } elseif ($pickupType === Trip::TRIP_TYPE_DROPOFF) {
            $trip = $booking->dropoffTrip;
            $addressType = Booking::ADDRESS_TYPE_CUSTOMER;
            $startDateField = 'dropoff_start';
            $endDateField = 'dropoff_end';
            $assignDate = false;
            if ($trip && $booking->dropoff_address_type === $addressType && !$trip->tw_start) {
                $assignDate = true;
            }
        } else {
            throw new InvalidArgumentException('Invalid trip type.');
        }
        if ($assignDate) {
            $availableDates = BookingHelper::getAvailableDates((int)$trip->address->zip, $trip->id);
            if (count($availableDates) > 0) {
                $trip->update([
                    'tw_start' => $availableDates[0]['date'] . ' ' . $availableDates[0]['from'],
                    'tw_end' => $availableDates[0]['date'] . ' ' . $availableDates[0]['to'],
                    'status' => $trip->status === Trip::TRIP_STATUS_AWAITING_DATE ? Trip::TRIP_STATUS_NEW : $trip->status,
                ]);

                $bookingUpdateData = [
                    $startDateField => $availableDates[0]['date'] . ' ' . $availableDates[0]['from'],
                    $endDateField => $availableDates[0]['date'] . ' ' . $availableDates[0]['to'],
                ];

                if (!$booking->update($bookingUpdateData)) {
                    throw new Exception("Can't assign a new date.");
                }
                $communicationData = [
                    'user_id' => Auth::user() ? Auth::user()->id : 1,
                    'communication_type' => Model::COMMUNICATION_TYPE_UNASSIGNED,
                    'message_type' => Model::MESSAGE_TYPE_DATE_ASSIGNED,
                    'message' => 'Date assigned',
                    'trip_id' => $trip->id,
                    'date_assigned' => $availableDates[0]['date'] . ' ' . $availableDates[0]['from'] . '-' . $availableDates[0]['to'],
                ];
                Model::create($communicationData);

                $bookingTrip = Trip::findOrFail($trip->id);
                $tripDataJson = [];
                $tripDataJson[] = json_decode(TripResource::make($bookingTrip)->toJson(), true);

                //create new external optimo route order based on current trip
                $optimoroute = new Optimoroute();
                if (!empty($tripDataJson)) {
                    $optimoroute->createOrder($tripDataJson, true);
                }
            }

            //start communication flow
            $communication = new Communication($trip, \App\Models\Communication::MESSAGE_TYPE_PU_CONFIRMATION);
            $communication->start();
            //start communication flow
            $communication = new Communication($trip, \App\Models\Communication::MESSAGE_TYPE_DO_CONFIRMATION);
            $communication->start();
        }

        return true;
    }

    public function manageTrips($data, $booking, $dropoffDate = null): bool
    {
        $isOwnAddress = Address::isOwnAddress($data);
        if (!$isOwnAddress['pickup']) {
            $tripData = [
                'status' => !$booking->pickup_start ? Trip::TRIP_STATUS_AWAITING_DATE : Trip::TRIP_STATUS_NEW,
                'type' => Trip::TRIP_TYPE_PICKUP,
                'trip_address' => $booking->pickup_address,
                'tw_start' => $booking->pickup_start,
                'tw_end' => $booking->pickup_end,
                'note' => $booking->pickup_note,
                'booking_id' => $booking->id,
                'client_id' => $booking->client_id,
                'spent_minutes' => 0,
                'unique_link' => md5(time() . rand())
            ];
            if (Trip::create($tripData)) {
                \Illuminate\Support\Facades\Log::channel('applog')->info("pickup trip created" . json_encode($tripData));
            }
        }

        if ($data['linehaulType'] !== '0-0') {
            if ($dropoffDate) {
                $linehaulDate = Carbon::parse($dropoffDate)->subWeekdays(1)->toDateString();
            }
            $tripData = [
                'status' => Trip::TRIP_STATUS_NEW,
                'type' => Trip::TRIP_TYPE_LINEHAUL,
                'trip_address' => $booking->pickup_address,
                'booking_id' => $booking->id,
                'client_id' => $booking->client_id,
                'tw_start' => $linehaulDate ?? null,
                'tw_end' => $linehaulDate ?? null,
                'spent_minutes' => 0,
                'unique_link' => md5(time() . rand())
            ];
            if (Trip::create($tripData)) {
                \Illuminate\Support\Facades\Log::channel('applog')->info("linehaul trip created" . json_encode($tripData));
            }
        }

        if (!$isOwnAddress['dropoff']) {
            $tripData = [
                'status' => !$booking->dropoff_start ? Trip::TRIP_STATUS_AWAITING_DATE : Trip::TRIP_STATUS_NEW,
                'type' => Trip::TRIP_TYPE_DROPOFF,
                'trip_address' => $booking->dropoff_address,
                'tw_start' => $booking->dropoff_start,
                'tw_end' => $booking->dropoff_end,
                'note' => $booking->dropoff_note,
                'booking_id' => $booking->id,
                'client_id' => $booking->client_id,
                'spent_minutes' => 0,
                'unique_link' => md5(time() . rand())
            ];
            if (Trip::create($tripData)) {
                \Illuminate\Support\Facades\Log::channel('applog')->info("dropoff trip created" . json_encode($tripData));
            }
        }

        return true;
    }

    public function manageCollis($data, $booking): bool
    {
        return true;
    }

}
