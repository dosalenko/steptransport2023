<?php

namespace App\Repository;

use App\Models\ApiKey;
use App\Models\Client;
use App\Models\Contact;
use App\Models\Trip;
use App\Models\UserRole;
use App\Repository\Interfaces\ClientRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class ClientRepository implements ClientRepositoryInterface
{
    /**
     * @var Client
     */
    private $model;

    /**
     * @var Contact
     */
    private $modelContact;


    /**
     * @var ApiKey
     */
    private $modelApiKey;

    /**
     * @param Client $client
     * @param Contact $contact
     * @param ApiKey $apiKey
     */
    public function __construct(Client $client, Contact $contact, ApiKey $apiKey)
    {
        $this->model = $client;
        $this->modelContact = $contact;
        $this->modelApiKey = $apiKey;
    }

    /**
     * @inheritDoc
     */
    public function create(array $data): Client
    {
        $client = $this->model->create($data);

        $dataContact = [
            'name' => $data['contact_name'],
            'email' => $data['contact_email'],
            'phone' => $data['contact_phone'],
            'client_id' => $client->id,
        ];

        $contact = $this->modelContact->create($dataContact);

        return $client;
    }

    /**
     * @inheritDoc
     */
    public function createApiKey(array $data): ApiKey
    {
        $dataApi = [
            'name' => $data['name'],
            'key' => $data['key'],
            'client_id' => $data['clientId'],
        ];

        $apiKey = $this->modelApiKey->create($dataApi);
        return $apiKey;
    }

    /**
     * @inheritDoc
     */
    public function updateApiKey(array $data, ApiKey $api): ApiKey
    {
        if (!$api->save()) {
            throw new \Exception("Can't update api keys");
        }
        return $api;
    }

    /**
     * @param int $id
     * @return Client
     */
    public function findOneById(int $id): Client
    {
        return $this->model->find($id);
    }

    /**
     * @param array $data
     * @param Client $client
     * @return Client
     * @throws \Exception
     */
    public function update(array $data, Client $client): Client
    {
        $client->name = $data['name'];

        if (!$client->save()) {
            throw new \Exception("Can't update client model");
        }

        return $client;
    }

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool
    {
        return $this->model->destroy($id);
    }

    /**
     * @return Collection
     */
    public function getAll(): Collection
    {
        return $this->model->all();
    }

    /**
     * @param array $data
     * @return LengthAwarePaginator
     */
    public function get(array $data): LengthAwarePaginator
    {
        $length = ($data && isset($data['length'])) ? (int)$data['length'] : 1000;
        $column = $data['column'] ?? 'name';
        $dir = $data['dir'] ?? 'desc';
        $search = ($data && isset($data['search'])) ? $data['search'] : '';

        if (isset($data['role']) && $data['role'] === UserRole::ROLE_USER) {
            $q = Client::with('addresses', 'contacts')->where('id', '=', Auth::user()->client_id);
        } else {
            $q = Client::with('addresses', 'contacts');
        }
        $q->whereNotIn('id', Client::EXCLUDED_CLIENTS);
        $q->whereNull('deleted_at');

        if (!empty($search)) {
            $q->searchString($search);
        }

        if ($column === 'last_30_days_bookings') {
            $q->orderByRaw('(SELECT count(id) FROM bookings WHERE bookings.client_id=clients.id ) ' . $dir . ' ');
        } elseif ($column === 'last_booking_date') {
            $q->orderBy('created_at', $dir);
        } elseif ($column === 'billing_address') {
            $q->orderBy('billing_street', $dir);
        } elseif ($column === 'primary_contact') {
            $q->orderBy('billing_email', $dir);
        } else {
            $q->orderBy($column, $dir);
        }

        return $q->paginate($length);
    }
}
