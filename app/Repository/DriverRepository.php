<?php

namespace App\Repository;


use App\Models\Driver;
use App\Repository\Interfaces\DriverRepositoryInterface;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class DriverRepository implements DriverRepositoryInterface
{
    /**
     * @var Driver
     */
    private $model;

    /**
     * @param Driver $driver
     */
    public function __construct(Driver $driver)
    {
        $this->model = $driver;
    }

    /**
     * @inheritDoc
     */
    public function create(array $data): Driver
    {
        $driver = $this->model->create($data);

        return $driver;
    }

    /**
     * @param int $id
     * @return Driver
     */
    public function findOneById(int $id): Driver
    {
        return $this->model->find($id);
    }

    /**
     * @param array $data
     * @param Driver $driver
     * @return Driver
     * @throws \Exception
     */
    public function update(array $data, Driver $driver): Driver
    {
        if (!$driver->save()) {
            throw new \Exception("Can't update model");
        }

        return $driver;
    }

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool
    {
        return $this->model->destroy($id);
    }

    /**
     * @return Collection
     */
    public function getAll(): Collection
    {
        return $this->model->all();
    }

    /**
     * @param array $data
     * @return LengthAwarePaginator
     */
    public function get(array $data): LengthAwarePaginator
    {
        $length = ($data && isset($data['length'])) ? (int)$data['length'] : 100;
        $column = $data['column'] ?? 'name';
        $dir = $data['dir'] ?? 'desc';
        $search = ($data && isset($data['search'])) ? $data['search'] : '';

        $q = Driver::query();

        if (!empty($search)) {
            $q->searchString($search);
        }

        return $q->paginate($length);
    }
}
