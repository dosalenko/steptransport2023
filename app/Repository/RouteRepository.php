<?php

namespace App\Repository;

use App\Models\Route;
use App\Models\RouteProduct;
use App\Models\RouteService;
use App\Repository\Interfaces\RouteRepositoryInterface;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class RouteRepository implements RouteRepositoryInterface
{
    /**
     * @var Route
     */
    private $model;

    /**
     * @param Route $route
     */
    public function __construct(Route $route)
    {
        $this->model = $route;
    }

    /**
     * @param array $data
     * @return bool
     */
    public function create(array $data): bool
    {
        foreach ($data as $v) {
            if ($v['is_existing'] === 0) {
                $route = new Route();
                $route->name = $v['name'];
                $route->days = json_encode($v['days']);
                $route->intervals = json_encode($v['intervals']);
                $route->max_volume = (float)($v['max_volume']);
                $route->max_weight = (float)($v['max_weight']);
                $route->max_stops = (int)($v['max_stops']);
                $route->zip_codes = json_encode($v['zip_codes']);
                $route->is_active =(int)($v['is_active']);
                $route->save();

                foreach ($v['services'] as $service) {
                    $routeService = new RouteService();
                    $routeService->route_id = $route->id;
                    $routeService->service_id = (int)$service;
                    $routeService->save();
                }

                foreach ($v['products'] as $product) {
                    $routeProduct = new RouteProduct();
                    $routeProduct->route_id = $route->id;
                    $routeProduct->product_id = (int)$product;
                    $routeProduct->save();
                }
            } else {
                $this->update($v);
            }
        }

        return true;
    }

    /**
     * @param int $id
     * @return Route
     */
    public function findOneById(int $id): Route
    {
        return $this->model->find($id);
    }

    /**
     * @param array $data
     * @return bool
     */
    public function update(array $el): bool
    {
        $route = Route::findOrFail($el['id']);
        $route->name = $el['name'];
        $route->days = json_encode($el['days']);
        $route->intervals = json_encode($el['intervals']);
        $route->max_volume = (float)($el['max_volume']);
        $route->max_weight = (float)($el['max_weight']);
        $route->max_stops = (int)($el['max_stops']);
        $route->zip_codes = json_encode($el['zip_codes']);
        $route->is_active =(int)($el['is_active']);

        //update route services
        $routesServices = RouteService::where('route_id', $el['id']);
        foreach ($routesServices->get() as $v) {
            if (!in_array($v->id, $el['services'])) {
                $v->delete();
            }
        }
        foreach ($el['services'] as $v) {
            if (!in_array($v, $routesServices->pluck('service_id')->toArray())) {
                $routeService = new RouteService();
                $routeService->route_id = $route->id;
                $routeService->service_id = (int)$v;
                $routeService->save();
            }
        }

        //update route products
        $routesProducts = RouteProduct::where('route_id', $el['id']);
        foreach ($routesProducts->get() as $v) {
            if (!in_array($v->id, $el['products'])) {
                $v->delete();
            }
        }
        foreach ($el['products'] as $v) {
            if (!in_array($v, $routesProducts->pluck('product_id')->toArray())) {
                $routeProduct = new RouteProduct();
                $routeProduct->route_id = $route->id;
                $routeProduct->product_id = (int)$v;
                $routeProduct->save();
            }
        }


        if (!$route->save()) {
            throw new \ErrorException('failed to update');
        }
        return true;
    }

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool
    {
        return $this->model->destroy($id);
    }

    /**
     * @return Collection
     */
    public function getAll(): Collection
    {
        return $this->model->all();
    }

    /**
     * @param array $data
     * @return LengthAwarePaginator
     */
    public function get(array $data): LengthAwarePaginator
    {
        $length = $data['length'] ?? 100;

        $q = Route::with('products', 'services')->whereNull('deleted_at')->latest();

        return $q->paginate($length);
    }
}
