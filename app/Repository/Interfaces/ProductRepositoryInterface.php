<?php

namespace App\Repository\Interfaces;

use App\Models\Product;

interface ProductRepositoryInterface
{
    /**
     * @param array $data
     *
     */
    public function create(array $data);

    /**
     * @param int $id
     * @return Product
     */
    public function findOneById(int $id): Product;

    /**
     * @param array $data
     * @param Product $product
     * @return Product
     */
    public function update(array $data, Product $product): Product;

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool;
}

