<?php

namespace App\Repository\Interfaces;

use App\Models\Recurring;
use Illuminate\Pagination\LengthAwarePaginator;

interface RecurringRepositoryInterface
{
    /**
     * @param array $data
     *
     */
    public function create(array $data);

    /**
     * @param int $id
     * @return Recurring
     */
    public function findOneById(int $id): Recurring;

    /**
     * @param array $data
     * @param Recurring $booking
     * @return Recurring
     */
    public function update(array $data, Recurring $booking): Recurring;

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool;

    /**
     * @param array $data
     * @return LengthAwarePaginator
     */
    public function get(array $data): LengthAwarePaginator;
}

