<?php

namespace App\Repository\Interfaces;

use App\Models\Service;

interface ServiceRepositoryInterface
{
    /**
     * @param array $data
     *
     */
    public function create(array $data);

    /**
     * @param int $id
     * @return Service
     */
    public function findOneById(int $id): Service;

    /**
     * @param array $data
     * @param Service $service
     * @return Service
     */
    public function update(array $data, Service $service): Service;

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool;
}

