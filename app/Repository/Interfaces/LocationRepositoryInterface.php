<?php

namespace App\Repository\Interfaces;

use App\Models\Location;
use Illuminate\Pagination\LengthAwarePaginator;

interface LocationRepositoryInterface
{
    /**
     * @param array $data
     *
     */
    public function create(array $data);

    /**
     * @param int $id
     * @return Location
     */
    public function findOneById(int $id): Location;

    /**
     * @param array $data
     * @param Location $location
     * @return Location
     */
    public function update(array $data, Location $location): Location;

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool;

    /**
     * @param array $data
     * @return LengthAwarePaginator
     */
    public function get(array $data): LengthAwarePaginator;
}

