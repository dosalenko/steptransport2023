<?php

namespace App\Repository\Interfaces;

use App\Models\Trip;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

interface TripRepositoryInterface
{
    /**
     * @param int $id
     * @return Trip
     */
    public function findOneById(int $id): Trip;

    /**
     * @param array $data
     *
     */
    public function create(array $data);

    /**
     * @param array $data
     * @param Trip $trip
     * @return Trip
     */
    public function update(array $data, Trip $trip): Trip;

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool;

    /**
     * @param array $data
     * @return LengthAwarePaginator
     */
    public function get(array $data): LengthAwarePaginator;
}

