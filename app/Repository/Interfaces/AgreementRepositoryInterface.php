<?php

namespace App\Repository\Interfaces;

use App\Models\Agreement;
use App\Models\Client;

interface AgreementRepositoryInterface
{
    /**
     * @param array $data
     *
     */
    public function create(array $data);

    /**
     * @param int $id
     * @return Agreement
     */
    public function findOneById(int $id): Agreement;

    /**
     * @param array $data
     * @param Agreement $client
     * @return Agreement
     */
    public function update(array $data, Agreement $client): Agreement;

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool;

    /**
     * @param Agreement $agreement
     * @return Agreement
     */
    public function getSingle(Agreement $agreement): Agreement;
}

