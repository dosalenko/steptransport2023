<?php

namespace App\Repository\Interfaces;

use App\Models\Booking;
use Exception;
use Illuminate\Pagination\LengthAwarePaginator;

interface BookingRepositoryInterface
{
    /**
     * @param array $data
     *
     */
    public function create(array $data);

    /**
     * @param int $id
     * @return Booking
     */
    public function findOneById(int $id): Booking;

    /**
     * @param array $data
     * @param Booking $booking
     * @return Booking
     */
    public function update(array $data, Booking $booking): Booking;

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool;

    /**
     * @param array $data
     * @return LengthAwarePaginator
     */
    public function get(array $data): LengthAwarePaginator;

    /**
     * @param string $action
     * @param string $initiated_by
     * @param Booking $booking
     *
     * return bool
     */
    public function saveHistoryLog(string $action, string $initiated_by, Booking $booking);

    /**
     * @param Booking $booking
     * @param int $pickupType
     * return bool
     *
     * @throws Exception
     */
    public function assignDate(Booking $booking, int $pickupType): bool;
}

