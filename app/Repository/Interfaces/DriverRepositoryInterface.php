<?php

namespace App\Repository\Interfaces;

use App\Models\Driver;
use Illuminate\Pagination\LengthAwarePaginator;

interface DriverRepositoryInterface
{
    /**
     * @param array $data
     *
     */
    public function create(array $data);

    /**
     * @param int $id
     * @return Driver
     */
    public function findOneById(int $id): Driver;

    /**
     * @param array $data
     * @param Driver $driver
     * @return Driver
     */
    public function update(array $data, Driver $driver): Driver;

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool;

    /**
     * @param array $data
     * @return LengthAwarePaginator
     */
    public function get(array $data): LengthAwarePaginator;
}

