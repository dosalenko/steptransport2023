<?php

namespace App\Repository\Interfaces;

use App\Models\User;

interface UserRepositoryInterface
{
    /**
     * @param array $data
     *
     */
    public function create(array $data);

    /**
     * @param int $id
     * @return User
     */
    public function findOneById(int $id): User;

    /**
     * @param array $data
     * @param User $user
     * @return User
     */
    public function update(array $data, User $user): User;

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool;
}

