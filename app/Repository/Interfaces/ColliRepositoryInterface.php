<?php

namespace App\Repository\Interfaces;

use App\Models\Colli;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

interface ColliRepositoryInterface
{
    /**
     * @param int $id
     * @return Colli
     */
    public function findOneById(int $id): Colli;

    /**
     * @param array $data
     *
     */
    public function create(array $data);

    /**
     * @param array $data
     * @param Colli $colli
     * @return Colli
     */
    public function update(array $data, Colli $colli): Colli;

    /**
     * @param array $ids
     * @return bool
     */
    public function delete(array $ids): bool;

    /**
     * @param array $data
     * @return LengthAwarePaginator
     */
    public function get(array $data): LengthAwarePaginator;

    /**
     * @param string $action
     * @param string $data
     * @param int $id
     *
     * return bool
     */
    public function saveHistoryLog(string $action, int $id, string $data);

    /**
     * @param int $batch
     * @return LengthAwarePaginator
     */
    public function getLog(int $batch);
}

