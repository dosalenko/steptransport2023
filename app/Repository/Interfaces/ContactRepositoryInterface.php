<?php

namespace App\Repository\Interfaces;

use App\Models\Contact;

interface ContactRepositoryInterface
{
    /**
     * @param array $data
     *
     */
    public function create(array $data);

    /**
     * @param int $id
     * @return Contact
     */
    public function findOneById(int $id): Contact;

    /**
     * @param array $data
     * @param Contact $contact
     * @return Contact
     */
    public function update(array $data, Contact $contact): Contact;

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool;
}

