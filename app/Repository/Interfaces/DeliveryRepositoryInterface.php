<?php

namespace App\Repository\Interfaces;

use App\Models\Delivery;
use Illuminate\Pagination\LengthAwarePaginator;

interface DeliveryRepositoryInterface
{
    /**
     * @param array $data
     * @return Delivery
     *
     */
    public function create(array $data): Delivery;

    /**
     * @param int $id
     * @return Delivery
     */
    public function findOneById(int $id): Delivery;

    /**
     * @param array $data
     * @return bool
     */
    public function update(array $data): bool;

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool;

    /**
     * @param array $data
     * @return LengthAwarePaginator
     */
    public function get(array $data): LengthAwarePaginator;
}

