<?php

namespace App\Repository\Interfaces;

use App\Models\Route;
use Illuminate\Pagination\LengthAwarePaginator;

interface RouteRepositoryInterface
{
    /**
     * @param array $data
     * @return bool
     *
     */
    public function create(array $data);

    /**
     * @param int $id
     * @return Route
     */
    public function findOneById(int $id): Route;

    /**
     * @param array $data
     * @return bool
     */
    public function update(array $data): bool;

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool;

    /**
     * @param array $data
     * @return LengthAwarePaginator
     */
    public function get(array $data): LengthAwarePaginator;
}

