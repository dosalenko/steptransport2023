<?php

namespace App\Repository;

use App\Models\Address;
use App\Models\Agreement;
use App\Models\AgreementInterval;
use App\Models\AgreementProduct;
use App\Models\AgreementProductPrice;
use App\Models\AgreementService;
use App\Models\AgreementServicePrice;
use App\Models\Client;
use App\Models\Interval;
use App\Models\Product;
use App\Models\ProductPrice;
use App\Models\Service;
use App\Models\ServicePrice;
use App\Repository\Interfaces\AgreementRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class AgreementRepository implements AgreementRepositoryInterface
{
    /**
     * @var Agreement
     */
    private $model;

    /**
     * @param Agreement $agreement
     */
    public function __construct(Agreement $agreement)
    {
        $this->model = $agreement;

    }

    /**
     * @inheritDoc
     */
    public function create(array $data): Agreement
    {
        $data['client_id'] = $data['clientId'];
        $checkClientPrimaryAgreements = $this->getClientPrimaryAgreements($data['clientId']);
        if (count($checkClientPrimaryAgreements) === 0) {
            $data['primary_agreement'] = 1;
        }

        if (empty($data['existingPickupAddresses'])) {
            $requestPickupAddressData = [
                'client_id' => $data['clientId'],
                'name' => $data['pickupAddressName'],
                'street' => $data['pickupAddressStreet'],
                'zip' => $data['pickupAddressZip'],
                'city' => $data['pickupAddressCity'],
                'country_id' => $data['pickupAddressCountryId'],
                'type' => Address::ADDRESS_TYPE_PICKUP,
            ];
            $pickupAddress = Address::updateOrCreate($requestPickupAddressData);
        }

        if (empty($data['existingDropoffAddresses'])) {
            $requestDropoffAddressData = [
                'client_id' => $data['clientId'],
                'name' => $data['dropoffAddressName'],
                'street' => $data['dropoffAddressStreet'],
                'zip' => $data['dropoffAddressZip'],
                'city' => $data['dropoffAddressCity'],
                'country_id' => $data['dropoffAddressCountryId'],
                'type' => Address::ADDRESS_TYPE_DROPOFF,
            ];
            $dropoffAddress = Address::updateOrCreate($requestDropoffAddressData);
        }

        $data['default_pickup_address_id'] = !empty($data['existingPickupAddresses']) ? $data['existingPickupAddresses'] : $pickupAddress->id;
        $data['default_dropoff_address_id'] = !empty($data['existingDropoffAddresses']) ? $data['existingDropoffAddresses'] : $dropoffAddress->id;

        $agreement = Agreement::create($data);
        return $agreement;
    }

    /**
     * @param int $id
     * @return Agreement
     */
    public function findOneById(int $id): Agreement
    {
        return $this->model->find($id);
    }

    /**
     * @param array $data
     * @param Agreement $agreement
     * @return Agreement
     * @throws \Exception
     */
    public function update(array $data, Agreement $agreement): Agreement
    {

        $data['country_id'] = 1;
        if (!empty($data['existingPickupAddresses'])) {
            $data['default_pickup_address_id'] = $data['existingPickupAddresses'];
        }

        if (!empty($data['existingDropoffAddresses'])) {
            $data['default_dropoff_address_id'] = $data['existingDropoffAddresses'];
        }

        $checkClientPrimaryAgreements = $this->getClientPrimaryAgreements($data['client_id']);
        if (count($checkClientPrimaryAgreements) === 0) {
            $data['primary_agreement'] = 1;
        }
        //make other agreements not primary except current if current agreement became primary
        if ($data['primary_agreement'] === 1 || $data['primary_agreement'] === true) {
            Agreement::where('client_id', '=', $data['client_id'])->where('id', '<>', $data['id'])->update(['primary_agreement' => 0]);
        }

        $agreement->update($data);

        $pickupAddress = Address::findOrFail($data['pickup_address']['id']);
        $dropoffAddress = Address::findOrFail($data['dropoff_address']['id']);
        $pickupAddress->update($data['pickup_address']);
        $dropoffAddress->update($data['dropoff_address']);

        return $agreement;
    }

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool
    {
        $agreement = Agreement::findOrFail($id);
        $agreement->primary_agreement = 0;
        $agreement->deleted_at = Carbon::now();
        return $agreement->save();
    }

    /**
     * @return Collection
     */
    public function getAll(): Collection
    {
        return $this->model->all();
    }

    /**
     * @param $clientId
     * @return Collection
     */
    public function getClientPrimaryAgreements($clientId): Collection
    {
        return $this->model->where('client_id', '=', $clientId)->where('primary_agreement', 1)->get();
    }

    /**
     * @param Agreement $agreement
     * @return Agreement
     */
    public function getSingle(Agreement $agreement): Agreement
    {
        //copy existing products/services/intervals to concrete agreement entity
        //if ($agreement->is_prices_copied === 0) {
        $productPrices = ProductPrice::get();
        foreach ($productPrices as $productPrice) {
            $agreementProduct = AgreementProduct::where('product_id', '=', $productPrice->product_id)->where('agreement_id', '=', $agreement->id)->first();
            if (!$agreementProduct) {
                $agreementProduct = new AgreementProduct();
                $agreementProduct->agreement_id = $agreement->id;
                $agreementProduct->product_id = $productPrice->product_id;
                $agreementProduct->active = 0;
                $agreementProduct->save();
            }

            $interval = Interval::findOrFail($productPrice->interval_id);
            $agreementInterval = AgreementInterval::where('agreement_id', '=', $agreement->id)
                ->where('max_weight', '=', $interval->max_weight)
                ->where('max_volume', '=', $interval->max_volume)->first();
            if (!$agreementInterval && $agreement->is_prices_copied === 0) {
                $agreementInterval = new AgreementInterval();
                $agreementInterval->agreement_id = $agreement->id;
                $agreementInterval->max_volume = $interval->max_volume;
                $agreementInterval->max_weight = $interval->max_weight;
                $agreementInterval->save();
            }

            if ($agreement->is_prices_copied === 0) {
                $agreementProductPrice = AgreementProductPrice::where('agreement_product_id', '=', $agreementProduct->id)->where('agreement_interval_id', '=', $agreementInterval->id)->first();
                if (!$agreementProductPrice) {
                    $agreementProductPrice = new AgreementProductPrice();
                    $agreementProductPrice->agreement_product_id = $agreementProduct->id;
                    $agreementProductPrice->agreement_interval_id = $agreementInterval->id;
                    $agreementProductPrice->price = $productPrice->standard_price;
                    $agreementProductPrice->save();
                }
            }
        }

        $servicePrices = ServicePrice::get();
        foreach ($servicePrices as $servicePrice) {
            $agreementService = AgreementService::where('service_id', '=', $servicePrice->service_id)->where('agreement_id', '=', $agreement->id)->first();
            if (!$agreementService) {
                $agreementService = new AgreementService();
                $agreementService->agreement_id = $agreement->id;
                $agreementService->service_id = $servicePrice->service_id;
                $agreementService->active = 0;
                $agreementService->save();
            }

            $interval = Interval::findOrFail($servicePrice->interval_id);
            $agreementInterval = AgreementInterval::where('agreement_id', '=', $agreement->id)
                ->where('max_weight', '=', $interval->max_weight)
                ->where('max_volume', '=', $interval->max_volume)->first();
            if (!$agreementInterval && $agreement->is_prices_copied === 0) {
                $agreementInterval = new AgreementInterval();
                $agreementInterval->agreement_id = $agreement->id;
                $agreementInterval->max_volume = $interval->max_volume;
                $agreementInterval->max_weight = $interval->max_weight;
                $agreementInterval->save();
            }

            if ($agreement->is_prices_copied === 0) {
                $agreementServicePrice = AgreementServicePrice::where('agreement_service_id', '=', $agreementService->id)->where('agreement_interval_id', '=', $agreementInterval->id)->first();
                if (!$agreementServicePrice) {
                    $agreementServicePrice = new AgreementServicePrice();
                    $agreementServicePrice->agreement_service_id = $agreementService->id;
                    $agreementServicePrice->agreement_interval_id = $agreementInterval->id;
                    $agreementServicePrice->price = $servicePrice->standard_addon_price;
                    $agreementServicePrice->save();
                }
            }
        }

        $agreement->is_prices_copied = 1;
        $agreement->save();
        //}

        //missing intervals if they added in agreements
        $agreementIntervals = AgreementInterval::where('agreement_id', '=', $agreement->id)->get();
        foreach ($agreementIntervals as $agreementInterval) {
            $agreementProducts = AgreementProduct::where('agreement_id', '=', $agreement->id)->get();
            foreach ($agreementProducts as $agreementProduct) {
                $agreementProductPrice = AgreementProductPrice::where('agreement_product_id', '=', $agreementProduct->id)->where('agreement_interval_id', '=', $agreementInterval->id)->first();
                if (!$agreementProductPrice) {
                    $agreementProductPrice = new AgreementProductPrice();
                    $agreementProductPrice->agreement_product_id = $agreementProduct->id;
                    $agreementProductPrice->agreement_interval_id = $agreementInterval->id;
                    $agreementProductPrice->price = 0;
                    $agreementProductPrice->save();
                }
            }
            $agreementServices = AgreementService::where('agreement_id', '=', $agreement->id)->get();
            foreach ($agreementServices as $agreementService) {
                $agreementServicePrice = AgreementServicePrice::where('agreement_service_id', '=', $agreementService->id)->where('agreement_interval_id', '=', $agreementInterval->id)->first();
                if (!$agreementServicePrice) {
                    $agreementServicePrice = new AgreementServicePrice();
                    $agreementServicePrice->agreement_service_id = $agreementService->id;
                    $agreementServicePrice->agreement_interval_id = $agreementInterval->id;
                    $agreementServicePrice->price = 0;
                    $agreementServicePrice->save();
                }
            }
        }
        return $agreement;
    }
}
