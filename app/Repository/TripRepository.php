<?php

namespace App\Repository;

use App\Models\Address;
use App\Models\Booking;
use App\Models\Trip;
use App\Repository\Interfaces\TripRepositoryInterface;
use App\Services\Communication\Communication;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;

class TripRepository implements TripRepositoryInterface
{
    /**
     * @var Trip
     */
    private $model;

    /**
     * @param Trip $trip
     */
    public function __construct(Trip $trip)
    {
        $this->model = $trip;
    }

    /**
     * @param int $id
     * @return Trip
     */
    public function findOneById(int $id): Trip
    {
        return $this->model->find($id);
    }

    /**
     * @param array $data
     */
    public function create(array $data): Trip
    {
        //
    }

    /**
     * @param array $data
     * @param Trip $trip
     * @return Trip
     * @throws \Exception
     */
    public function update(array $data, Trip $trip): Trip
    {
        $oldData = clone $trip;

        $address = Address::findOrFail($data['address']['id']);
        $addressData = [
            'name' => $data['address']['name'],
            'zip' => $data['address']['zip'],
            'city' => $data['address']['city'],
            'country_id' => $data['address']['country_id'],
            'street' => $data['address']['street'],
            'phone' => $data['address']['phone'],
        ];
        $address->update($addressData);

        $updateTripData = [
            'status' => $data['status_id'],
            'note' => $data['note'] ?? $trip->note,
            'tw_start' => $data['time_day'] ? $data['time_day'] . ' ' . $data['time_start'] : null,
            'tw_end' => $data['time_day'] ? $data['time_day'] . ' ' . $data['time_end'] : null,
            'is_task' => !empty($data['is_task']) ? (int)$data['is_task'] : 0
        ];

        if ($data['status_id'] === Trip::TRIP_STATUS_AWAITING_DATE && $data['time_day'] === null) {
            $updateTripData['status'] = Trip::TRIP_STATUS_NEW;
        }

        if ($trip->update($updateTripData)) {
            //on change trip - change status of related booking (dependent on trip type)
            $booking = Booking::findOrFail($trip->booking->id);

            //synchronize time between trips and bookings
            if ($trip->type == Trip::TRIP_TYPE_PICKUP) {
                $booking->pickup_start = $trip->tw_start;
                $booking->pickup_end = $trip->tw_end;
                $booking->save();
            } elseif ($trip->type == Trip::TRIP_TYPE_DROPOFF) {
                $booking->dropoff_start = $trip->tw_start;
                $booking->dropoff_end = $trip->tw_end;
                $booking->save();
            }

            if ($booking->linehaulTrip && $booking->linehaulTrip->status !== Trip::TRIP_STATUS_DELIVERED) {
                $linehaulTrip = Trip::findOrFail($booking->linehaulTrip->id);
                if ($oldData->tw_start) {
                    $dropoffData = Carbon::parse($oldData->tw_start)->toDateString();
                    if ($dropoffData !== $data['time_data']) {
                        $linehaulDate = Carbon::parse($data['time_day'])->subWeekdays(1)->toDateString();
                        $linehaulTrip->tw_start = $linehaulDate . ' 00:00:00';
                        $linehaulTrip->tw_end = $linehaulDate . ' 00:00:00';
                        $linehaulTrip->save();
                    }
                } else {
                    if (!empty($data['time_day'])) {
                        $linehaulDate = Carbon::parse($data['time_day'])->subWeekdays(1)->toDateString();
                        $linehaulTrip->tw_start = $linehaulDate . ' 00:00:00';
                        $linehaulTrip->tw_end = $linehaulDate . ' 00:00:00';
                        $linehaulTrip->save();
                    }
                }
            }

            //start communication flow
            $communication = new Communication($trip, \App\Models\Communication::MESSAGE_TYPE_PU_CONFIRMATION);
            $communication->start();

            //check for booking updates
            $bookingRepo = new BookingRepository($booking);
            $bookingRepo->checkStatusUpdates($trip->booking->id);
        }

        return $trip;
    }

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool
    {
        return $this->model->destroy($id);
    }

    /**
     * @param array|null $data
     * @return LengthAwarePaginator
     */
    public function get(array $data): LengthAwarePaginator
    {
        $length = ($data && $data['length']) ? (int)$data['length'] : 100;
        $column = $data['column'] ?? 'tw_start';
        $dir = $data['dir'] ?? 'desc';
        $search = ($data && $data['search']) ? $data['search'] : '';
        $status = ($data && $data['status']) ? (int)$data['status'] : 10;
        $zip = ($data && $data['zip']) ? (int)$data['zip'] : 0;

        $q = Trip::with('booking', 'booking.linehaulTrip', 'booking.dropoffAddress', 'booking.dropoffAddress.country', 'booking.pickupAddress', 'booking.pickupAddress.country', 'booking.collis', 'client')
            ->whereNull('deleted_at')
            ->whereHas('booking', function ($q) {
                $q->whereNull('deleted_at');
            });

        if (!empty($search)) {
            $q->searchString($search);
        }

        if ($status === 9) {
            $q->searchByColliReceived();
        }

        if ($status === 10) {
            $q->searchByNullStatus();
        }

        if ($status === Trip::TRIP_FILTER_LINEHAUL) {
            $q->searchByLinehaulType();
        }

        if ($status === Trip::TRIP_FILTER_TASKS) {
            $q->searchByTasks();
        }

        if ($zip > 0) {
            $q->searchByZip($zip);
        }

        if (!empty($data['dateFrom']) || !empty($data['dateTo'])) {
            $q->searchByDate($data['dateFrom'] ?? date('Y-m-d 00:00:00', strtotime('-12 month')), $data['dateTo'] ?? date('Y-m-d 00:00:00', strtotime('+12 month')));
        }

        /*   if (in_array($status, [1, 2, 3, 4, 5, 7, 8, 9])) {
               if ($status === 9) {
                   $status = Trip::TRIP_STATUS_AWAITING_DATE;
               }
               $q->searchByStatus($status);
           }*/

        if (in_array($status, [1, 2, 3, 4, 5, 7, 8])) {
            $q->searchByStatus($status);
        }

        if ($data['driver']) {
            $q->searchByDriver($data['driver']);
        }

        if ($column === 'volume') {
            $q->orderByRaw('(SELECT total_volume FROM bookings WHERE trips.booking_id = bookings.id) ' . $dir . ' ');
        } elseif ($column === 'weight') {
            $q->orderByRaw('(SELECT total_weight FROM bookings WHERE trips.booking_id = bookings.id) ' . $dir . ' ');
        } elseif ($column === 'address') {
            $q->orderByRaw('(SELECT name FROM addresses WHERE trips.trip_address = addresses.id) ' . $dir . ' ');
        } elseif ($column === 'booking_id') {
            $q->orderByRaw('(SELECT status FROM bookings WHERE trips.booking_id = bookings.id) ' . $dir . ' ');
        } elseif ($column === 'client') {
            $q->orderByRaw('(SELECT client_id FROM bookings WHERE trips.booking_id = bookings.id) ' . $dir . ' ');
        } elseif ($column === 'internal_comment') {
            $q->orderByRaw('(SELECT internal_comment FROM bookings WHERE trips.booking_id = bookings.id) ' . $dir . ' ');
        } else {
            $q->orderBy($column, $dir);
        }

        return $q->paginate($length);
    }
}
