<?php

namespace App\Repository;

use App\Models\Delivery;
use App\Repository\Interfaces\DeliveryRepositoryInterface;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class DeliveryRepository implements DeliveryRepositoryInterface
{
    /**
     * @var Delivery
     */
    private $model;

    /**
     * @param Delivery $delivery
     */
    public function __construct(Delivery $delivery)
    {
        $this->model = $delivery;
    }

    /**
     * @param array $data
     * @return Delivery
     */
    public function create(array $data): Delivery
    {
        $data = [
            'trip_id' => (int)$data['trip']['id'],
            'address' => $data['address'],
            'tw_start' => $data['date']['date'] . ' ' . $data['date']['from'],
            'tw_end' => $data['date']['date'] . ' ' . $data['date']['to'],
        ];

        $delivery = Delivery::create($data);
        return $delivery;
    }

    /**
     * @param int $id
     * @return Delivery
     */
    public function findOneById(int $id): Delivery
    {
        return $this->model->find($id);
    }

    /**
     * @param array $data
     * @return bool
     */
    public function update(array $el): bool
    {
        return true;
    }

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool
    {
        return $this->model->destroy($id);
    }

    /**
     * @return Collection
     */
    public function getAll(): Collection
    {
        return $this->model->all();
    }

    /**
     * @param array $data
     * @return LengthAwarePaginator
     */
    public function get(array $data): LengthAwarePaginator
    {
        //
    }
}
