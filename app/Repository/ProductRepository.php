<?php

namespace App\Repository;

use App\Models\Product;
use App\Repository\Interfaces\ProductRepositoryInterface;
use Illuminate\Support\Collection;

class ProductRepository implements ProductRepositoryInterface
{
    /**
     * @var Product
     */
    private $model;

    /**
     * @param Product $product
     */
    public function __construct(Product $product)
    {
        $this->model = $product;

    }

    /**
     * @inheritDoc
     */
    public function create(array $data): Product
    {
        $product = $this->model->create($data);

        return $product;
    }

    /**
     * @param int $id
     * @return Product
     */
    public function findOneById(int $id): Product
    {
        return $this->model->find($id);
    }

    /**
     * @param array $data
     * @param Product $product
     * @return Product
     * @throws \Exception
     */
    public function update(array $data, Product $product): Product
    {
        $product->name = $data['name'];

        if (!$product->save()) {
            throw new \Exception("Can't update client model");
        }

        return $product;
    }

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool
    {
        return $this->model->destroy($id);
    }

    /**
     * @return Collection
     */
    public function getAll(): Collection
    {
        return $this->model->all();
    }
}
