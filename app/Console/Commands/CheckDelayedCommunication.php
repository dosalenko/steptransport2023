<?php

namespace App\Console\Commands;

use App\Jobs\DelayedCommunicationJob;
use App\Models\Trip;
use App\Services\Communication\Communication;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CheckDelayedCommunication extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:communication';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'check delayed communication messages';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        dispatch(new DelayedCommunicationJob());
    }
}
