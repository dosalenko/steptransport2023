<?php

namespace App\Console\Commands;

use App\Jobs\CheckBookingStatusesJob;
use App\Models\Booking;
use Illuminate\Console\Command;

class CheckBookingStatusesOld extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:bookingstatusesold';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'check old booking statuses';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        //dispatch(new CheckBookingStatusesJob(1000, Booking::BOOKING_TYPE_DELIVERED));
        dispatch(new CheckBookingStatusesJob(1000));
    }
}
