<?php

namespace App\Console\Commands;

use App\Models\Recurring;
use App\Repository\Interfaces\BookingRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;

class CreateRecurringBookings extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:recurring';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'create recurring bookings';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $recurrings = Recurring::with('client')->get();
        foreach ($recurrings as $recurring) {
            $days = json_decode($recurring->recurring_dates);
            $stops = json_decode($recurring->stops, true);

            if (count($stops) > 0) {
                foreach ($days as $day) {
                    $data = [
                        'totalWeight' => 0,
                        'totalVolume' => 0,
                        'priceProduct' => 0,
                        'priceService' => 0,
                        'pickupAddressName' => $recurring->client->name,
                        'pickupAddressStreet' => $recurring->client->billing_street,
                        'pickupAddressZip' => $recurring->client->billing_zip,
                        'pickupAddressCity' => $recurring->client->billing_city,
                        'pickupAddressPhone' => null,
                        'pickupNote' => null,

                        'dropoffAddressName' => $stops[count($stops) - 1]['addressName'],
                        'dropoffAddressStreet' => $stops[count($stops) - 1]['addressStreet'] ?? 'null',
                        'dropoffAddressZip' => $stops[count($stops) - 1]['addressZip'] ?? 'null',
                        'dropoffAddressCity' => $stops[count($stops) - 1]['addressCity'] ?? 'null',
                        'dropoffAddressPhone' => null,
                        'dropoffNote' => null,

                        'clientId' => ['id' => $recurring->client_id],
                        'note' => $recurring->internal_comment,
                        'reference' => $recurring->note,
                        'pickupDate' => Carbon::now()->addDays((int)$day + 2)->toDateString(),
                        'pickupTimeStart' => '00:00',
                        'pickupTimeEnd' => '00:00',
                        'dropoffDate' => Carbon::now()->addDays((int)$day + 2)->toDateString(),
                        'dropoffTimeStart' => '00:00',
                        'dropoffTimeEnd' => '00:00',

                        'clientAgreement' => $recurring->agreement_id,
                        'linehaulType' => '0-0',
                        'productId' => $recurring->agreement_product_id,
                        'serviceIds' => json_decode($recurring->agreement_services_ids, true),
                        'priceServiceSum' => $recurring->total_price,
                        'client' => $recurring->client(),
                    ];

                    $bookingRepo = App::make(BookingRepositoryInterface::class);
                    $bookingRepo->create($data);
                }
            }
        }
        //dispatch(new CreateRecurringBookings());
    }
}
