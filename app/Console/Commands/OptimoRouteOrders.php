<?php

namespace App\Console\Commands;

use App\Jobs\OptimoRouteGetOrdersJob;
use Illuminate\Console\Command;

class OptimoRouteOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:optimorouteorders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'get optimoroute orders';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        dispatch(new OptimoRouteGetOrdersJob(0));
    }
}
