<?php

namespace App\Console\Commands;

use App\Models\Booking;
use App\Services\Communication\Communication;
use Illuminate\Console\Command;

class TestCommunication extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:communication';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'communication';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $id = 123;
        $booking = Booking::findOrFail($id);
        $communication = new Communication($booking->dropoffTrip, \App\Models\Communication::MESSAGE_TYPE_DO_CONFIRMATION);
        $communication->test();
    }
}
