<?php

namespace App\Console\Commands;

use App\Jobs\CheckBookingEmptyTrips;
use Illuminate\Console\Command;

class CheckIfBookingHasNoTrips extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:emptytrips';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'check empty trips';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        dispatch(new CheckBookingEmptyTrips());
    }
}
