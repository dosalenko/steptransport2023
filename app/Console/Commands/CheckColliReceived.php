<?php

namespace App\Console\Commands;

use App\Models\Booking;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CheckColliReceived extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:colli-received';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'check if colli received';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $bookings = Booking::with('collis')->whereNull('deleted_at')
            ->where('status', '<>', Booking::BOOKING_TYPE_DELIVERED)
            ->where('created_at', '>=', Carbon::now()->subDays(30))
            ->get();
        //dispatch(new CheckColliReceivedJob());
    }
}
