<?php

namespace App\Console\Commands;

use App\Jobs\CheckBookingStatusesJob;
use Illuminate\Console\Command;

class CheckBookingStatuses extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:bookingstatuses';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'check booking statuses';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        dispatch(new CheckBookingStatusesJob(30));
    }
}
