<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\OptimoRouteOrders::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('get:optimorouteorders')->everyThirtyMinutes();
        $schedule->command('check:bookingstatuses')->hourly();
        $schedule->command('check:bookingstatusesold')->daily();
        $schedule->command('check:emptytrips')->cron('0 4 * * *');
        $schedule->command('create:recurring')->cron('0 8 * * FRI');
        $schedule->command('check:communication')->dailyAt('08:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        /** @noinspection PhpIncludeInspection */
        require base_path('routes/console.php');
    }
}
