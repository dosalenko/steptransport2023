<?php

namespace App\Jobs;

use App\Exports\BookingExport;
use App\Models\Booking;
use http\Encoding\Stream;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Maatwebsite\Excel\Facades\Excel;

class BookingReportJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     */
    public function handle()
    {
        try {
            Excel::download(new BookingExport(['id'], 0, 0), 'booking_' . time() . '.csv');
        } catch (\Throwable $exception) {
            \Log::error($exception);
        }
    }
}
