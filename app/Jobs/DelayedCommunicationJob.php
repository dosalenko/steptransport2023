<?php

namespace App\Jobs;

use App\Exports\BookingExport;
use App\Models\Booking;
use App\Models\Trip;
use App\Services\Communication\Communication;
use Carbon\Carbon;
use http\Encoding\Stream;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Maatwebsite\Excel\Facades\Excel;

class DelayedCommunicationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     */
    public function handle()
    {
        try {
            //tomorrow
            $communications = \App\Models\Communication::with('trip')->whereDate('created_at', date("Y-m-d", strtotime('-1 days')))->where('message_type', '=', \App\Models\Communication::MESSAGE_TYPE_PU_CONFIRMATION)->get();
            foreach ($communications as $communication) {
                if ($communication->trip->type === Trip::TRIP_TYPE_PICKUP) {
                    $reconfirmation = \App\Models\Communication::where('trip_id', '=', $communication->trip_id)->where('message_type', '=', \App\Models\Communication::MESSAGE_TYPE_PU_TOMORROW)->count();
                    if ($reconfirmation === 0) {
                        //start communication flow
                        $communication = new Communication($communication->trip, \App\Models\Communication::MESSAGE_TYPE_PU_TOMORROW);
                        $communication->start();
                    }
                }
            }

            $communicationsDropoff = \App\Models\Communication::with('trip')->whereDate('created_at', date("Y-m-d", strtotime('-1 days')))->where('message_type', '=', \App\Models\Communication::MESSAGE_TYPE_DO_CONFIRMATION)->get();
            foreach ($communicationsDropoff as $communication) {
                if ($communication->trip->type === Trip::TRIP_TYPE_DROPOFF) {
                    $reconfirmation = \App\Models\Communication::where('trip_id', '=', $communication->trip_id)->where('message_type', '=', \App\Models\Communication::MESSAGE_TYPE_DO_TOMORROW)->count();
                    if ($reconfirmation === 0) {
                        //start communication flow
                        $communication = new Communication($communication->trip, \App\Models\Communication::MESSAGE_TYPE_DO_TOMORROW);
                        $communication->start();
                    }
                }
            }


            //rate
            $today = Carbon::today();

            $communications = \App\Models\Communication::with('trip')
                ->where('message_type', '=', \App\Models\Communication::MESSAGE_TYPE_PU_RATE)
                ->whereDate('send_at', $today)
                ->get();

            $communicationsDropoff = \App\Models\Communication::with('trip')
                ->where('message_type', '=', \App\Models\Communication::MESSAGE_TYPE_DO_RATE)
                ->whereDate('send_at', $today)
                ->get();

            foreach ($communications as $communication) {
                if ($communication->trip->type === Trip::TRIP_TYPE_PICKUP) {
                    $newCommunication = new Communication($communication->trip, \App\Models\Communication::MESSAGE_TYPE_PU_RATE);
                    $newCommunication->start();
                }
            }

            foreach ($communicationsDropoff as $communication) {
                if ($communication->trip->type === Trip::TRIP_TYPE_DROPOFF) {
                    $newCommunication = new Communication($communication->trip, \App\Models\Communication::MESSAGE_TYPE_DO_RATE);
                    $newCommunication->start();
                }
            }
        } catch (\Throwable $exception) {
            \Log::error($exception);
        }
    }
}
