<?php

namespace App\Jobs;

use App\Models\Booking;
use App\Models\Trip;
use App\Repository\BookingRepository;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CheckBookingStatusesJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    /*
     * period of checking
     */
    public $days = 30;

    /*
     * booking status
    */
    public $status;

    public function __construct($days = 30, $status = null)
    {
        $this->days = $days;
        $this->status = $status;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $bookings = Booking::where('status', '<>', Booking::BOOKING_TYPE_DELIVERED)
            ->where('created_at', '>=', Carbon::today()->subDays($this->days))
            ->whereNull('deleted_at')
            ->get();

        foreach ($bookings as $booking) {
            $bookingRepo = new BookingRepository($booking);
            $bookingRepo->checkStatusUpdates($booking->id);
        }
    }
}
