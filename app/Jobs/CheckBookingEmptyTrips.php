<?php

namespace App\Jobs;

use App\Exports\BookingExport;
use App\Models\Address;
use App\Models\Booking;
use App\Models\Trip;
use App\Repository\BookingRepository;
use http\Encoding\Stream;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

class CheckBookingEmptyTrips implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     */
    public function handle()
    {
        $bookings = Booking::with('pickupTrip', 'linehaulTrip', 'dropoffTrip')->whereNull('deleted_at')->get();
        foreach ($bookings as $booking) {
            if (!$booking->pickupTrip && !$booking->linehaulTrip && !$booking->dropoffTrip) {
                $addressData = [
                    'pickupAddressStreet' => $booking->pickupAddress->street,
                    'pickupAddressZip' => $booking->pickupAddress->zip,
                    'pickupAddressCity' => $booking->pickupAddress->city,
                    'dropoffAddressStreet' => $booking->dropoffAddress->street,
                    'dropoffAddressZip' => $booking->dropoffAddress->zip,
                    'dropoffAddressCity' => $booking->dropoffAddress->city,
                ];

                $isOwnAddress = Address::isOwnAddress($addressData);
                if (!$isOwnAddress['pickup']) {
                    $tripData = [
                        'status' => !$booking->pickup_start ? Trip::TRIP_STATUS_AWAITING_DATE : Trip::TRIP_STATUS_NEW,
                        'type' => Trip::TRIP_TYPE_PICKUP,
                        'trip_address' => $booking->pickup_address,
                        'tw_start' => $booking->pickup_start,
                        'tw_end' => $booking->pickup_end,
                        'note' => $booking->pickup_note,
                        'booking_id' => $booking->id,
                        'client_id' => $booking->client_id,
                        'spent_minutes' => 0,
                    ];
                    Trip::create($tripData);
                }

                if ($booking->linehaul_type !== '0-0') {
                    $tripData = [
                        'status' => Trip::TRIP_STATUS_NEW,
                        'type' => Trip::TRIP_TYPE_LINEHAUL,
                        'trip_address' => $booking->pickup_address,
                        'booking_id' => $booking->id,
                        'client_id' => $booking->client_id,
                        'spent_minutes' => 0,
                    ];
                    Trip::create($tripData);
                }

                if (!$isOwnAddress['dropoff']) {
                    $tripData = [
                        'status' => !$booking->dropoff_start ? Trip::TRIP_STATUS_AWAITING_DATE : Trip::TRIP_STATUS_NEW,
                        'type' => Trip::TRIP_TYPE_DROPOFF,
                        'trip_address' => $booking->dropoff_address,
                        'tw_start' => $booking->dropoff_start,
                        'tw_end' => $booking->dropoff_end,
                        'note' => $booking->dropoff_note,
                        'booking_id' => $booking->id,
                        'client_id' => $booking->client_id,
                        'spent_minutes' => 0,
                    ];
                    Trip::create($tripData);
                }
                Log::channel('emptytripslog')->info("Booking trips regenerated for booking " . $booking->id);
            } else {
                Log::channel('emptytripslog')->info("no empty trips");
            }
        }
        return true;
    }
}
