<?php

namespace App\Observers;

use App\Models\Colli;
use App\Models\Trip;
use App\Repository\Interfaces\BookingRepositoryInterface;
use App\Repository\Interfaces\ColliRepositoryInterface;

class ColliObserver
{

    /**
     * @var ColliRepositoryInterface
     */
    protected $colliRepo;

    /**
     * @var ColliRepositoryInterface
     */
    protected $bookingRepo;

    /**
     * @param ColliRepositoryInterface $colliRepo
     */
    public function __construct(ColliRepositoryInterface $colliRepo, BookingRepositoryInterface $bookingRepo)
    {
        $this->colliRepo = $colliRepo;
        $this->bookingRepo = $bookingRepo;
    }

    /**
     * Handle the Colli "created" event.
     *
     * @param \App\Models\Colli $colli
     * @return void
     */
    public function created(Colli $colli)
    {
        //
    }

    /**
     * Handle the Colli "updated" event.
     *
     * @param \App\Models\Colli $colli
     * @return void
     */
    public function updated(Colli $colli)
    {
        if ($colli->wasChanged('received_at')) {
            if ($colli->booking_id) {
                $checkBooking = $this->colliRepo->checkIfAllBookingCollisReceived($colli->booking_id);
                if ($checkBooking) {
                    $this->bookingRepo->checkStatusUpdates($colli->booking);
                    $this->bookingRepo->assignDate($colli->booking, Trip::TRIP_TYPE_PICKUP);
                    $this->bookingRepo->assignDate($colli->booking, Trip::TRIP_TYPE_DROPOFF);
                }
            }
        }
    }

    /**
     * Handle the Colli "deleted" event.
     *
     * @param \App\Models\Colli $colli
     * @return void
     */
    public function deleted(Colli $colli)
    {
        //
    }

    /**
     * Handle the Colli "restored" event.
     *
     * @param \App\Models\Colli $colli
     * @return void
     */
    public function restored(Colli $colli)
    {
        //
    }

    /**
     * Handle the Colli "force deleted" event.
     *
     * @param \App\Models\Colli $colli
     * @return void
     */
    public function forceDeleted(Colli $colli)
    {
        //
    }
}
