<?php

namespace App\Observers;

use App\Models\Communication as Model;
use App\Models\Trip;
use App\Repository\Interfaces\TripRepositoryInterface;
use App\Services\Communication\Communication;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class TripObserver
{

    /**
     * @var TripRepositoryInterface
     */
    protected $tripRepo;

    /**
     * @param TripRepositoryInterface $tripRepo
     */
    public function __construct(TripRepositoryInterface $tripRepo)
    {
        $this->tripRepo = $tripRepo;
    }

    /**
     * Handle the Trip "created" event.
     *
     * @param \App\Models\Trip $trip
     * @return void
     */
    public function created(Trip $trip)
    {
        //
    }

    /**
     * Handle the Trip "updated" event.
     *
     * @param \App\Models\Trip $trip
     * @return void
     */
    public function updated(Trip $trip)
    {
        if ($trip->wasChanged('status')) {
            if ($trip->status == Trip::TRIP_STATUS_DELIVERED) {
                $date = Carbon::now()->addDays(1);
                while ($date->isWeekend()) {
                    $date->addDay();
                }
                $communications = \App\Models\Communication::where('trip_id', '=', $trip->id)->first();
                if ($communications) {
                    $communicationData = [
                        'user_id' => Auth::user() ? Auth::user()->id : 1,
                        'communication_type' => Model::COMMUNICATION_TYPE_EMAIL,
                        'message' => 'rate',
                        'trip_id' => $trip->id,
                        'send_at' => $date
                    ];
                    if ($trip->type == Trip::TRIP_TYPE_PICKUP) {
                        $communicationData['message_type'] = Model::MESSAGE_TYPE_PU_RATE;
                        Model::create($communicationData);
                    }
                    if ($trip->type == Trip::TRIP_TYPE_DROPOFF) {
                        $communicationData['message_type'] = Model::COMMUNICATION_TYPE_EMAIL;
                        Model::create($communicationData);
                    }
                }
            }
        }
    }

    /**
     * Handle the Trip "deleted" event.
     *
     * @param \App\Models\Trip $trip
     * @return void
     */
    public function deleted(Trip $trip)
    {
        //
    }

    /**
     * Handle the Trip "restored" event.
     *
     * @param \App\Models\Trip $trip
     * @return void
     */
    public function restored(Trip $trip)
    {
        //
    }

    /**
     * Handle the Trip "force deleted" event.
     *
     * @param \App\Models\Trip $trip
     * @return void
     */
    public function forceDeleted(Trip $trip)
    {
        //
    }
}
