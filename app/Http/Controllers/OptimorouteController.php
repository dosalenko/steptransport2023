<?php

namespace App\Http\Controllers;

use App\Http\Resources\TripResource;
use App\Jobs\OptimoRouteGetOrdersJob;
use App\Models\Booking;
use App\Models\Trip;
use App\Repository\Interfaces\BookingRepositoryInterface;
use App\Services\Core\Helpers\BookingHelper;
use App\Services\Core\Helpers\Helper;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Services\Optimoroute\Optimoroute;
use Illuminate\Support\Facades\Storage;

class OptimorouteController extends Controller
{
    /**
     * @var BookingRepositoryInterface
     */
    protected $bookingRepo;

    /**
     * @param BookingRepositoryInterface $bookingRepo
     */
    public function __construct(BookingRepositoryInterface $bookingRepo)
    {
        $this->bookingRepo = $bookingRepo;
    }

    /**
     *  create new external order based on current trip
     *
     * @param Request $request
     * @param int $id
     * @param bool $isMultiple
     *
     * @return JsonResponse
     */
    public function createOrder(Request $request, int $id, bool $isMultiple): JsonResponse
    {
        try {
            if ($isMultiple === true) {
                $ids = $request->all();
                $tripDataJson = [];
                $response = [];
                foreach ($ids as $k => $v) {
                    if ($v) {
                        $id = (int)$k;
                        $trip = Trip::with(
                            'booking',
                            'booking.dropoffAddress',
                            'booking.collis',
                            'client',
                            'client.primaryContact'
                        )->findOrFail($id);
                        $dependentTrip = BookingHelper::getChildTrip($trip);
                        if ($dependentTrip) {
                            $ids[$dependentTrip] = true;
                        }
                        if ($trip->booking->status < Booking::BOOKING_TYPE_CONFIRMED) {
                            $response[$id]['success'] = false;
                            $response[$id]['message'] = 'Booking not yet confirmed';
                            continue;
                        }
//                        elseif (($trip->type === Trip::TRIP_TYPE_DROPOFF
//                            && $trip->booking->linehaulTrip
//                            && $trip->booking->linehaulTrip->status != Trip::TRIP_STATUS_DELIVERED)
//                        ) {
//                            $response[$id]['success'] = false;
//                            $response[$id]['message'] = 'Linehaul trip not yet delivered';
//                            continue;
//                        }
                        elseif ($trip->type === Trip::TRIP_TYPE_LINEHAUL
                            || $trip->status === Trip::TRIP_STATUS_AWAITING_DATE
                            || $trip->booking->status < Booking::BOOKING_TYPE_CONFIRMED
                        ) {
                            continue;
                        }
                        $tripDataJson[] = json_decode(TripResource::make($trip)->toJson(), true);
                    }
                }
                if (!empty($tripDataJson)) {
                    $optimoroute = new Optimoroute();
                    $result = $optimoroute->createOrder($tripDataJson, true);
                    foreach ($result as $k => $v) {
                        $isPrefixedOrder = strpos($k, 'T') !== false;
                        $k = $isPrefixedOrder ? (int)substr($k, 2) : (int)($k);

                        $trip = Trip::with(
                            'booking',
                            'booking.dropoffAddress',
                            'booking.collis',
                            'client',
                            'client.primaryContact'
                        )->findOrFail($k);

                        if ($trip) {
                            if ($v['success'] === true) {
                                //change trip status on successful creating external order
                                $updateTripData = [
                                    'status' => $trip->status === Trip::TRIP_STATUS_NEW ?
                                        Trip::TRIP_STATUS_TRANSFERRED : $trip->status,
                                    'optimoroute_result' => 'Transferred: [' . date('Y-m-d H:i:s') . ']',
                                    'is_prefixed_order' => (int)$isPrefixedOrder,
                                ];
                                $response[$k]['success'] = true;
                                $response[$k]['message'] = '';
                            } else {
                                $updateTripData = [
                                    'status' => Trip::TRIP_STATUS_FAILED,
                                    'optimoroute_result' => 'Failed: [' . $v['message'] . ']',
                                    'is_prefixed_order' => (int)$isPrefixedOrder,
                                ];
                                $response[$k]['success'] = false;
                                $response[$k]['message'] = $v['message'];
                            }
                            $trip->update($updateTripData);
                        }
                    }
                }
            } else {
                $trip = Trip::with(
                    'booking',
                    'booking.dropoffAddress',
                    'booking.collis',
                    'client',
                    'client.primaryContact'
                )->findOrFail($id);
                if ($trip->type !== Trip::TRIP_TYPE_LINEHAUL) {
                    if ($trip->status === Trip::TRIP_STATUS_AWAITING_DATE) {
                        $updateTripData = [
                            'status' => Trip::TRIP_STATUS_FAILED,
                            'optimoroute_result' => 'Failed: [Trip can\'t be transferred because of missing date]',
                        ];
                        $response = $updateTripData['optimoroute_result'];
                    }
//                    } elseif ($trip->type === Trip::TRIP_TYPE_DROPOFF
//                        && $trip->booking->linehaulTrip
//                        && $trip->booking->linehaulTrip->status != Trip::TRIP_STATUS_DELIVERED) {
//                        $updateTripData = [
//                            'status' => Trip::TRIP_STATUS_FAILED,
//                            'optimoroute_result' => 'Failed: [Linehaul not delivered.
//                             Please set linehaul trip to delivered before transferring again]',
//                        ];
//                        $response = $updateTripData['optimoroute_result'];
//                    }
                    else {
                            $tripData = TripResource::make($trip);
                            //create new external optimo route order based on current trip
                            $optimoroute = new Optimoroute();
                            $checkOrderExists = $optimoroute->checkOrderExists((string)$trip->id);
                            $checkPrefixedOrderExists = $optimoroute->checkOrderExists('T-' . $trip->id);
                            $isPrefixedOrder = $trip->id > TRIP::START_PREFIXED_ORDERS_FROM_ID
                                || in_array($trip->id, TRIP::FORCE_REWRITE_TO_PREFIXED_ORDERS)
                                || $checkPrefixedOrderExists
                                || !$checkOrderExists;

                            $response[$id] = $optimoroute->createOrder(
                                json_decode($tripData->toJson(), true),
                                false,
                                $isPrefixedOrder
                            );

                            if ($response[$id]['success'] === true) {
                                //change trip status on successful creating external order
                                $updateTripData = [
                                    'status' => $trip->status === Trip::TRIP_STATUS_NEW ?
                                        Trip::TRIP_STATUS_TRANSFERRED : $trip->status,
                                    'optimoroute_result' => 'Transferred: [' . date('Y-m-d H:i:s') . ']',
                                    'is_prefixed_order' => (int)$isPrefixedOrder,
                                ];
                            } else {
                                $updateTripData = [
                                    'status' => Trip::TRIP_STATUS_FAILED,
                                    'optimoroute_result' => 'Failed: [' . $response[$id]['message'] . ']',
                                    'is_prefixed_order' => (int)$isPrefixedOrder,
                                ];
                            }
                        }
                        $trip->update($updateTripData);
                    }
                }
                return response()->json(['response' => $response]);
            }
        catch
            (Exception $e) {
                return response()->json(['response' => $e->getMessage()], 400);
            }
    }

    /**
     *  create new external order based on current trip
     *
     * @param int $id
     *
     * @return JsonResponse
     */
    public function deleteOrder(int $id): JsonResponse
    {
        try {
            $trip = Trip::findOrFail($id);

            //create new external optimo route order based on current trip
            $optimoroute = new Optimoroute();
            $response = $optimoroute->deleteOrder($trip->is_prefixed_order ? 'T-' . $id : $id);

            if ($response['success'] === true) {
                //change trip status on successful creating external order
                $updateTripData = [
                    'status' => Trip::TRIP_STATUS_NEW,
                    'optimoroute_result' => '',
                ];
            } else {
                $updateTripData = [
                    'optimoroute_result' => 'Failed to delete order: [' . $response['message'] . ']',
                ];
            }
            $trip->update($updateTripData);
            return response()->json(['response' => $response]);
        } catch (Exception $e) {
            return response()->json(['response' => $e->getMessage()], 400);
        }
    }

    /**
     *  fetch external order updates
     * @param int $id
     * @return JsonResponse
     */
    public function getOrders(int $id): JsonResponse
    {
        dispatch(new OptimoRouteGetOrdersJob($id));
        return response()->json(['tripsToUpdateCount' => Helper::getOptimorouteTripsToUpdateCount()]);
    }

    /**
     *  Get Order Completion
     *
     */
    public function checkOrdersStatuses()
    {
        //
    }

    /**
     *  Get Order details
     *
     */
    public function getSchedulingInfo()
    {
        //
    }

    /*
     * get proof of delivery
     *
     * @param string image
     * @param int $id
     * @param int $type
     * @return Storage
     */
    public function downloadPod($image, $id, $type)
    {
        $podType = (int)$type === Trip::FILE_TYPE_IMAGE ? 'image' : 'signature';
        $fileExists = Storage::disk('public')->has('/optimoroute/pods/' . $podType . '/' . $image);
        if ($fileExists) {
            return Storage::disk('public')->download('/optimoroute/pods/' . $podType . '/' . $image);
        } else {
            //re-download file from external api
            $optimoroute = new Optimoroute();
            $image = $optimoroute->getPod((int)$id, $type);
            return Storage::disk('public')->download('/optimoroute/pods/' . $podType . '/' . $image);
        }
    }
}
