<?php

namespace App\Http\Controllers;

use App\Repository\Interfaces\ContactRepositoryInterface;
use App\Models\Contact;
use App\Models\Client;
use Exception;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\StoreContactRequest;
use App\Http\Requests\UpdateContactRequest;
use Illuminate\View\View;
use Throwable;

class ContactController extends Controller
{

    /**
     * @var ContactRepositoryInterface
     */
    protected $contactRepo;

    /**
     * @param ContactRepositoryInterface $contactRepo
     */
    public function __construct(ContactRepositoryInterface $contactRepo)
    {
        $this->contactRepo = $contactRepo;
    }

    /**
     * @param int $contactId
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(int $contactId): View
    {
        $clientData = Client::with('addresses')->findOrFail($contactId);

        return view('contacts.create', compact('clientData'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreContactRequest $request
     * @return JsonResponse
     */
    public function store(StoreContactRequest $request): JsonResponse
    {
        try {
            $contact = $this->contactRepo->create($request->all());
            return response()->json(['message' => 'success', 'contactId' => $contact->id]);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     * @param UpdateContactRequest $request
     *
     * @return JsonResponse
     */
    public function update(UpdateContactRequest $request): JsonResponse
    {
        try {
            $requestData = $request->all();

            foreach ($requestData as $v) {
                $contact = Contact::findOrFail($v['id']);
                $contact->name = $v['name'];
                $contact->email = $v['email'];
                $contact->phone = $v['phone'];
                $contact->comment = $v['comment'];
                $contact->save();
            }

            return response()->json(['result' => 'success']);
        } catch (Throwable $e) {
            return response()->json($e->getMessage(), 400);
        }
    }
}
