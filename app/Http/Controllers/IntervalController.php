<?php

namespace App\Http\Controllers;

use App\Models\AgreementInterval;
use App\Models\AgreementProduct;
use App\Models\AgreementProductPrice;
use App\Models\AgreementService;
use App\Models\AgreementServicePrice;
use App\Models\Product;
use App\Models\ProductPrice;
use App\Models\Service;
use App\Models\ServicePrice;
use App\Services\Core\Helpers\Helper;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Models\Interval;
use Throwable;

class IntervalController extends Controller
{
    /**
     * @param int $id
     * @return JsonResponse
     */
    public function getIntervals(int $id = 0): JsonResponse
    {
        if ($id > 0) {
            $agreementInterval = AgreementInterval::with(
                'agreementProductPrices',
                'agreementServicePrices',
                'agreementProductPrices.product.product',
                'agreementServicePrices.service.service'
            )->where('agreement_id', $id)
                ->orderBy('max_volume')
                ->get();
            $query = count($agreementInterval) > 0 ? $agreementInterval : [];
        } else {
            $query = Interval::get();
        }
        return response()->json(['data' => $query]);
    }

    /**
     * @todo refactor
     * Add new interval dimensions with related prices
     *
     * @param Request $request
     * @param int $id
     *
     * @return JsonResponse
     *
     */
    public function addNewInterval(Request $request, int $id = 0): JsonResponse
    {
        try {
            $requestData = $request->all();
            $max_volume = Helper::format($requestData['max_volume']);
            $max_weight = Helper::format($requestData['max_weight']);
            $requestData['max_volume'] = $max_volume;
            $requestData['max_weight'] = $max_weight;
            if ($id === 0) {
                // check if dimensions are correct
                //@todo move to helper
                $currentIntervals = Interval::all();
                if (count($currentIntervals) > 2) {
                    $measurementsVolume = [];
                    $measurementsWeight = [];
                    foreach ($currentIntervals as $v) {
                        $measurementsVolume[] = $v->max_volume;
                        $measurementsWeight[] = $v->max_weight;
                    }
                    if (($max_volume <= max($measurementsVolume) && $max_volume >= min($measurementsVolume))
                        ||
                        ($max_weight <= max($measurementsWeight) && $max_weight >= min($measurementsWeight))
                    ) {
                        return response()->json(['result' => 'error', 'message' => 'Wrong intervals!']);
                    }
                }
                $interval = Interval::create($requestData);

                //after products creating add prices to each existing interval
                $products = Product::all();
                $services = Service::all();

                foreach ($products as $product) {
                    $productPrice = new ProductPrice();
                    $productPrice->product_id = $product->id;
                    $productPrice->interval_id = $interval->id;
                    $productPrice->save();
                }
                foreach ($services as $service) {
                    $servicePrice = new ServicePrice();
                    $servicePrice->service_id = $service->id;
                    $servicePrice->interval_id = $interval->id;
                    $servicePrice->save();
                }
            } else {
                $requestData['agreement_id'] = $id;
                // check if dimensions are correct
                $checkIntervals = Helper::checkIntervals($id, $max_volume, $max_weight);

                if (!$checkIntervals) {
                    return response()->json(['result' => 'error', 'message' => 'Wrong intervals!']);
                }

                $interval = AgreementInterval::create($requestData);

                //after products creating add prices to each existing interval
                $products = AgreementProduct::where('agreement_id', $id)->get();
                $services = AgreementService::where('agreement_id', $id)->get();

                foreach ($products as $product) {
                    $productPrice = new AgreementProductPrice();
                    $productPrice->agreement_product_id = $product->id;
                    $productPrice->agreement_interval_id = $interval->id;
                    $productPrice->save();
                }
                foreach ($services as $service) {
                    $servicePrice = new AgreementServicePrice();
                    $servicePrice->agreement_service_id = $service->id;
                    $servicePrice->agreement_interval_id = $interval->id;
                    $servicePrice->save();
                }
            }
            return response()->json(['result' => 'success']);
        } catch (Throwable $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @param int $agreement_id
     *
     * @return JsonResponse
     */
    public function removeInterval(int $id, int $agreement_id = 0): JsonResponse
    {
        try {
            if ($agreement_id == 0) {
                $interval = Interval::findOrFail($id);
                $interval->productPrices()->delete();
                $interval->servicePrices()->delete();
            } else {
                $interval = AgreementInterval::findOrFail($id);
                $interval->agreementProductPrices()->delete();
                $interval->agreementServicePrices()->delete();
            }
            $interval->delete();
            return response()->json(['success' => true]);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }
}
