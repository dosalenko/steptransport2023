<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreBookingRequest;
use App\Http\Requests\UpdateBookingRequest;
use App\Http\Resources\BookingCollection;
use App\Http\Resources\BookingResource;
use App\Mail\BookingMail;
use App\Models\AgreementService;
use App\Models\Booking;
use App\Models\Colli;
use App\Models\Log;
use App\Models\Service;
use App\Models\Trip;
use App\Models\UserRole;
use App\Models\ZipCity;
use App\Repository\Interfaces\BookingRepositoryInterface;
use App\Services\Communication\Communication;
use App\Services\Core\Helpers\BookingHelper;
use App\Services\Core\Helpers\Helper;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\View\View;

class BookingController extends Controller
{
    /**
     * @var BookingRepositoryInterface
     */
    protected $bookingRepo;

    /**
     * @param BookingRepositoryInterface $bookingRepo
     */
    public function __construct(BookingRepositoryInterface $bookingRepo)
    {
        $this->middleware(['role:' . UserRole::ROLE_ADMIN])->only(['update', 'confirm']);
        $this->bookingRepo = $bookingRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        return view('bookings.index', [
            'userRole' => in_array('user', Auth::user()->roles) ? UserRole::ROLE_USER : UserRole::ROLE_ADMIN
        ]);
    }

    /**
     * @param Request $request
     * @param int $excludeDelivered
     * @return BookingCollection
     */
    public function getBookings(Request $request, int $excludeDelivered = 0): BookingCollection
    {
        $data = $request->all();
        $data['excludeDelivered'] = $excludeDelivered;
        $bookings = $this->bookingRepo->get($data);
        return new BookingCollection($bookings);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View|RedirectResponse
     */
    public function create()
    {
        if (Auth::user()->active === 0) {
            return redirect()->route('bookings.index');
        }
        $userRole = in_array('user', Auth::user()->roles) ? UserRole::ROLE_USER : UserRole::ROLE_ADMIN;

        return view('bookings.create', compact(['userRole']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreBookingRequest $request
     * @return JsonResponse
     */
    public function store(StoreBookingRequest $request): JsonResponse
    {
        try {
            $booking = $this->bookingRepo->create($request->all());
            return response()->json(['message' => 'success', 'bookingId' => $booking->id]);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return View|RedirectResponse
     */
    public function edit($id)
    {
        $booking = Booking::findOrFail($id);
        $userRole = in_array('user', Auth::user()->roles) ? UserRole::ROLE_USER : UserRole::ROLE_ADMIN;

        if ($userRole === UserRole::ROLE_USER && $booking->client_id !== Auth::user()->client_id) {
            return redirect()->route('bookings.index');
        }

        if (count($booking->trips) > 0) {
            $bookingTrip = $booking->trips[0]->booking_id;
            $this->bookingRepo->checkStatusUpdates($bookingTrip);
        }
        $booking->is_seen = Booking::BOOKING_IS_SEEN;
        $booking->save();
        return view('bookings.edit', compact(['booking', 'userRole']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateBookingRequest $request
     * @param int $id
     *
     * @return JsonResponse
     */
    public function update(UpdateBookingRequest $request, int $id): JsonResponse
    {
        try {
            $booking = Booking::findOrFail($id);
            $bookingUpd = $this->bookingRepo->update($request->all(), $booking);
            return response()->json(['message' => 'success', 'bookingId' => $bookingUpd->id]);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }


    /**
     * Update services and prices
     *
     * @param Request $request
     * @param int $id
     *
     * @return JsonResponse
     */
    public function updateServices(Request $request, int $id): JsonResponse
    {
        try {
            $requestData = $request->all();
            $booking = Booking::findOrFail($id);
            if (!empty($requestData)) {
                $agreementServiceIds = [];

                foreach ($requestData as $v) {
                    $agreementServiceIds[] = AgreementService::where('agreement_id', '=', $booking->agreement_id)
                        ->where('service_id', '=', $v)
                        ->first()
                        ->id;
                }

                $priceService = BookingHelper::getServicePriceByInterval(
                    json_encode($agreementServiceIds),
                    $booking->total_volume,
                    $booking->total_weight,
                    $booking->agreement_id
                );

                foreach ($requestData as $agreementServiceId) {
                    if (array_key_exists('priceServiceDefaultAgreements', $priceService)) {
                        if (!array_key_exists($agreementServiceId, $priceService['priceServiceDefaultAgreements'])) {
                            $priceService['priceServiceDefaultAgreements'][$agreementServiceId] = 0;
                        }
                    } else {
                        $priceService['priceServiceDefaultAgreements'][$agreementServiceId] = 0;
                    }
                }

                $booking->service_price = json_encode($priceService['priceServiceDefaultAgreements']);
                $booking->total_price = $priceService['priceServiceSum'] + $booking->product_price;
            } else {
                $booking->service_price = null;
                $booking->total_price = $booking->product_price;
            }
            $booking->save();
            return response()->json(['message' => 'success']);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * Get booking info.
     *
     * @param int $id
     * @return BookingResource
     *
     */
    public function getSingle(int $id): BookingResource
    {
        $booking = Booking::with(
            'client',
            'client.primaryContact',
            'collis',
            'collisByReference',
            'dropoffAddress',
            'dropoffAddress.country',
            'pickupAddress',
            'pickupAddress.country',
            'trips',
            'product',
            'logs',
            'logs.user'
        )->findOrFail($id);
        return BookingResource::make($booking);
    }

    /**
     * allow confirm by admins
     *
     * @param int $id
     * @return JsonResponse
     *
     */
    public function confirm(int $id): JsonResponse
    {
        try {
            $booking = Booking::findOrFail($id);
            if (Auth::user()->hasRole(UserRole::ROLE_ADMIN)) {
                $booking->status = Booking::BOOKING_TYPE_CONFIRMED;
                if ($booking->save()) {
                    $this->bookingRepo->saveHistoryLog(
                        Log::logActions()[Log::LOG_ACTION_CONFIRMED],
                        Auth::user()->email,
                        $booking
                    );
                    $this->bookingRepo->assignDate($booking, Trip::TRIP_TYPE_PICKUP);
                    //start communication flow
                    if ($booking->pickupTrip) {
                        $communication = new Communication($booking->pickupTrip, \App\Models\Communication::MESSAGE_TYPE_PU_CONFIRMATION);
                        $communication->start();
                    }
                }
            }
            return response()->json(['message' => 'success', 'bookingId' => $id]);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * check for specific service chosen
     *
     * @param Request $request
     * @return JsonResponse
     *
     */
    public function checkExchangeService(Request $request): JsonResponse
    {
        try {
            $services = AgreementService::with('service')->whereIn('id', $request->all())->get();
            $servicesPresent = [
                'exchange' => false,
                'return' => false,
            ];
            foreach ($services as $service) {
                if ($service->service->name === Service::EXCHANGE_SERVICE_NAME) {
                    $servicesPresent['exchange'] = true;
                }
                if ($service->service->name === Service::EXCHANGE_SERVICE_NAME ||
                    $service->service->name === Service::RETURN_SERVICE_NAME) {
                    $servicesPresent['return'] = true;
                }
            }
            return response()->json(['message' => true, 'services' => $servicesPresent]);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * allow confirm by admins
     * @param string $message
     * @param string $client
     *
     * @return JsonResponse
     */
    public function errorNotify(string $message, string $client): JsonResponse
    {
        try {
            $mailData = [
                'title' => 'Booking error from my.steptransport.dk',
                'error' => $message,
                'client' => $client,
                'user' => Auth::user()->name
            ];
            Mail::to(env('ERROR_NOTIFIER_RECIPIENT'))->send(new BookingMail($mailData));
            return response()->json(['message' => 'success notified']);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * Get  all possible linehauls combinations.
     *
     * @return JsonResponse
     */
    public function getLinehauls(): JsonResponse
    {
        $linehauls = Helper::getLinehauls();
        return response()->json(['data' => $linehauls]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        try {
            $this->bookingRepo->delete((int)$id);
            return response()->json(['message' => 'success']);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * Recreate booking with specified parameters
     * @param string $zip
     *
     */
    public function getCityByZip(string $zip)
    {
        try {
            $city = ZipCity::where('zip', 'LIKE', "%$zip%")->orderBy('id', 'desc')->first();
            if ($city) {
                return response()->json(['message' => 'success', 'data' => ['city' => $city->city, 'country_id' => $city->country_id]]);
            } else {
                return response()->json(['message' => 'success', 'data' => []]);
            }
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * @param string $role
     * @return int
     */
    public function statusesCounter(string $role): int
    {
        return Helper::bookingStatusesCounter($role);
    }

    /**
     * @param string $role
     * @return int
     */
    public function nullHoursCounter(string $role): int
    {
        return count(BookingHelper::nullHoursCounter($role));
    }


    /**
     * @param int $client
     * @param int $excludeDelivered
     * @return JsonResponse
     */
    public function getBookingsClient(int $client, int $excludeDelivered): JsonResponse
    {
        $q = Booking::with('client', 'dropoffAddress')->where('client_id', '=', $client)->whereNull('deleted_at');
        if ($excludeDelivered === 1) {
            $q->where('status', '<>', Booking::BOOKING_TYPE_DELIVERED);
        }

        return response()->json(['data' => $q->get()]);
    }

    /**
     * @param int $colli
     *
     * @return JsonResponse
     */
    public function getBookingsListByColli(int $colli): JsonResponse
    {
        $colli = Colli::findOrFail($colli);
        $bookings = Booking::with('client', 'dropoffAddress')
            ->where('client_id', '=', $colli->client_id)
            ->whereNull('deleted_at')->get();
        return response()->json(['data' => $bookings]);
    }

    /**
     *
     * @param int $clientId
     * @param string $reference
     * @return int
     */
    public function checkClientReference(int $clientId, string $reference): int
    {
        return Helper::getClientReferencesNumber($clientId, $reference);
    }


    /*
     *
     */
    public function recalculatePrices()
    {
        $bookings = Booking::all();
        return view('bookings.recalculate_prices', ['bookings' => $bookings]);
    }

    /*
     * only for test purposes
     */
    public function getRecalculatedPrices(Request $request)
    {
        $ids = explode(',', $request->get('ids'));
        $prices = [];
//        foreach ($request->all() as $k => $v) {
//            if (strstr($v['value'], 'B-') !== false) {
//                $ids[] = (int)substr($v['value'], 2);
//            } else {
//                $ids[] = (int)$v['value'];
//            }
//        }

        foreach (array_filter(array_unique($ids)) as $v) {
            $booking = Booking::where('id', '=', (int)trim($v))->first();
            if ($booking) {
                $priceProduct = (float)BookingHelper::getProductPriceByInterval(
                    $booking->agreement_product_id,
                    (float)$booking->total_volume,
                    (float)$booking->total_weight,
                    $booking->agreement_id,
                    1
                );

                $new_services = [];
                if ($booking->service_price) {
                    foreach (json_decode($booking->service_price) as $k => $service) {
                        $new_services[] = (int)$k;
                    }
                }

                $priceService = BookingHelper::getServicePriceByInterval(
                    json_encode($new_services),
                    $booking->total_volume,
                    $booking->total_weight,
                    $booking->agreement_id,
                    1
                );

                $prices[$booking->id] =
                    ['product' => $priceProduct,
                        'service' => $priceService,
                    ];
            }
        }
        return response()->json(['message' => 'success', 'data' => $prices]);
    }

    /*
  *
  */
    public function storeRecalculatedPrices(Request $request)
    {
        $prices = $request->all();

        foreach ($prices as $k => $price) {
            $booking = Booking::where('id', '=', $k)->first();
            if ($booking) {
                $previousProductPrice = $booking->product_price;
                $previousServicePrice = $booking->service_price;
                if (array_key_exists('service', $price) && array_key_exists('priceService', $price['service'])) {
                    if ((float)$price['service']['priceServiceSum'] > 0) {
                        $booking->service_price = (json_encode($price['service']['priceService']));
                        $this->bookingRepo->saveHistoryLog(
                            Log::logActions()[Log::LOG_ACTION_SERVICE_PRICE_RECALCULATED] . ' from: ' . $previousServicePrice . ' to:' . json_encode($price['service']['priceService']),
                            Auth::user()->email,
                            $booking
                        );
                    }
                }
                if ((float)$price['product'] > 0) {
                    $booking->product_price = ((float)$price['product']);
                    $this->bookingRepo->saveHistoryLog(
                        Log::logActions()[Log::LOG_ACTION_PRODUCT_PRICE_RECALCULATED] . ' from: ' . $previousProductPrice . ' to:' . (float)$price['product'],
                        Auth::user()->email,
                        $booking
                    );
                }
                $booking->total_price = ((float)$price['product'] + (float)$price['service']['priceServiceSum']);
                $booking->save();
            }
        }
    }
}
