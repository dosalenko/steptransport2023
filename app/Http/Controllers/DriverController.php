<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreDriverRequest;
use App\Http\Resources\DriverCollection;
use App\Models\Driver;
use App\Repository\Interfaces\DriverRepositoryInterface;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Throwable;

class DriverController extends Controller
{
    /**
     * @var DriverRepositoryInterface
     */
    protected $driverRepo;

    /**
     * @param DriverRepositoryInterface $driverRepo
     */
    public function __construct(DriverRepositoryInterface $driverRepo)
    {
        $this->driverRepo = $driverRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        return view('drivers.index');
    }

    /**
     * @param Request $request
     * @return DriverCollection
     */
    public function getDrivers(Request $request): DriverCollection
    {
        $drivers = $this->driverRepo->get($request->all());
        return new DriverCollection($drivers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        return view('drivers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreDriverRequest $request
     * @return JsonResponse
     */
    public function store(StoreDriverRequest $request): JsonResponse
    {
        try {
            $driver = $this->driverRepo->create($request->all());
            return response()->json(['message' => 'success', 'driverId' => $driver->id]);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }


    /**
     * Get $driver info.
     *
     * @param int $id
     *
     * @return JsonResponse
     */
    public function getSingle(int $id): JsonResponse
    {
        $driver = Driver::findOrFail($id);
        return response()->json($driver);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return View
     */
    public function edit(int $id): View
    {
        $driver = Driver::findOrFail($id);
        return view('drivers.edit', compact('driver'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreDriverRequest $request
     * @param int $id
     *
     * @return JsonResponse
     */
    public function update(StoreDriverRequest $request, int $id): JsonResponse
    {
        try {
            $requestData = $request->all();

            $driver = Driver::findOrFail($id);
            $driver->update($requestData);

            return response()->json(['result' => 'success', 'driverId' => $driver->id]);
        } catch (Throwable $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * Remove multiple drivers
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteMultipleDrivers(Request $request): JsonResponse
    {
        try {
            $requestData = array_filter($request->all());
            foreach ($requestData as $k => $v) {
                $driver = Driver::findOrFail((int)$k);
                $driver->delete();
            }
            return response()->json(['message' => 'success']);
        } catch (Throwable $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }
}
