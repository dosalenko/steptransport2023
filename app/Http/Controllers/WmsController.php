<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreColliRequest;
use App\Http\Resources\ColliResource;
use App\Models\Colli;
use App\Models\Location;
use App\Models\Log;
use App\Models\Picklist;
use App\Repository\Interfaces\BookingRepositoryInterface;
use App\Repository\Interfaces\ColliRepositoryInterface;
use App\Services\Core\Helpers\Helper;
use App\Services\Optimoroute\Optimoroute;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class WmsController extends Controller
{

    /**
     * @var ColliRepositoryInterface
     */
    protected $colliRepo;

    /**
     * @param ColliRepositoryInterface $colliRepo
     */
    public function __construct(ColliRepositoryInterface $colliRepo)
    {
        $this->colliRepo = $colliRepo;
    }

    /**
     *
     * @return View
     */
    public function receiveGoods(): View
    {
        return view('settings.receive_goods');
    }

    /**
     *
     * @return View
     */
    public function receiveGoodsMobile(): View
    {
        return view('mobile.receive_goods');
    }

    /**
     *
     * @return View
     */
    public function receiveColli(): View
    {
        return view('collis.receive_colli');
    }

    /**
     *
     * @return View
     */
    public function moveGoods(): View
    {
        return view('settings.move_goods');
    }

    /**
     *
     * @return View
     */
    public function packGoods(): View
    {
        return view('settings.pack_goods');
    }

    /**
     *
     * @param int $warehouse
     * @return View
     */
    public function createBarcode(int $warehouse = 1): View
    {
        $colli = Colli::orderBy('id', 'desc')->first();
        $lastColli = $colli->id;
        return view('settings.create_barcode', ['code' => $lastColli + 1 ?? 1, 'warehouse' => $warehouse]);
    }


    /**
     * Store the specified resource in storage.
     *
     * @param StoreColliRequest $request
     * @return JsonResponse
     */
    public function storeBarcode(StoreColliRequest $request): JsonResponse
    {
        try {
            $saveColli = $this->colliRepo->create($request->all());
            return response()->json(['message' => 'success',
                'colliId' => $saveColli->id,
                'colliReference' => $saveColli->reference]);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     *
     * @param int $code
     * @param int $warehouse
     *
     */
    public function scanBarcode($code, int $warehouse)
    {
        try {
            //$colli=$this->colliRepo->scanBarcode();

            //get collis from the picklist
            $colli = Colli::with('booking', 'client')->where('id', '=', (int)$code)->first();
            if ($colli) {
                $colli->status = Colli::COLLI_STATUS_ON_DISPATCH_TERMINAL;
                $colli->colli_packing_status = Colli::COLLI_PACKING_STATUS_NEW;
                $colli->received_at = Carbon::now();
                if ($colli->booking_id !== null) {
                    $bookingRepo = App::make(BookingRepositoryInterface::class);
                    $action = 'Colli [' . $colli->id . '] have been received';
                    $findPreparedColli = Log::where('booking_id', '=', $colli->booking_id)
                        ->where('action', '=', $action)
                        ->first();
                    if (!$findPreparedColli) {
                        $bookingRepo->saveHistoryLog($action, Auth::user()->email, $colli->booking);
                    }
                }
                if ($warehouse > 0) {
                    $receivingLocation = Helper::getReceivingLocation($warehouse);
                    if ($receivingLocation) {
                        $colli->location_id = $receivingLocation;
                    }
                }
                $colli->save();
                //colli log
                if ($warehouse > 0 && $colli->location) {
                    $this->colliRepo->saveHistoryLog(
                        Log::logActions()[LOG::LOG_ACTION_COLLI_LOCATION_CHANGED],
                        $colli->id,
                        '#' . $colli->location->name . ' (' . $colli->location->warehouse->nickname . ')',
                    );
                }
                return response()->json(['data' => ColliResource::make($colli)], 200);
            } else {
                return response()->json(['message' => 'not found'], 200);
            }
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     *
     * @param Request $request
     * @param int $warehouse
     */
    public function storeReceivedColli(Request $request, int $warehouse)
    {
        try {
            //get collis from the picklist
            $requestData = array_filter($request->all());
            $ids = [];
            foreach ($requestData as $k => $v) {
                if ((int)$k > 0) {
                    $ids[$k] = $k;
                }
            }
            $collis = Colli::with('booking', 'client')->whereIn('id', $ids)->get();
            foreach ($collis as $colli) {
                $getCollisByColliBatch = Colli::where('colli_batch', '=', $colli->colli_batch)->get();
                foreach ($getCollisByColliBatch as $batch) {
                    $batch->status = Colli::COLLI_STATUS_ON_DISPATCH_TERMINAL;
                    $batch->colli_packing_status = Colli::COLLI_PACKING_STATUS_NEW;
                    $batch->location_id = $batch->location_id ? $batch->location_id : Helper::getReceivingLocation($warehouse);
                    $batch->received_at = Carbon::now();
                    //booking log
                    if ($batch->booking_id !== null) {
                        $bookingRepo = App::make(BookingRepositoryInterface::class);
                        $action = 'Colli [' . $batch->id . '] have been received';
                        $findPreparedColli = Log::where('booking_id', '=', $batch->booking_id)
                            ->where('action', '=', $action)
                            ->first();
                        if (!$findPreparedColli) {
                            $bookingRepo->saveHistoryLog($action, Auth::user()->email, $batch->booking);
                        }
                    }
                    $batch->save();
                }
            }
            return response()->json(['message' => 'colli received'], 200);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     *
     * @param string $tour
     * @param int $type
     * @return JsonResponse
     *
     */
    public function scanTour(string $tour, int $type): JsonResponse
    {
        try {
            $pickList = Picklist::where('id', '=', $tour)->first();
            if (!$pickList) {
                return response()->json(['message' => ' Tour couldn\'t not be loaded']);
            } else {
                //find associated colli
                if ($pickList->orders !== null) {
                    $associatedCollis = Helper::getAssociatedCollis($pickList->orders, $type, 1);
                    $checkIfTourAlreadyPrepared = Helper::checkIfTourAlreadyPrepared($associatedCollis);

                    if ($checkIfTourAlreadyPrepared === 'loaded') {
                        return response()->json(['message' => 'loaded']);
                    }
                    if ($checkIfTourAlreadyPrepared === 'prepared') {
                        return response()->json(['message' => 'prepared']);
                    }
                } else {
                    return response()->json(['message' => 'empty']);
                }
                //get trip routes
                $optimoroute = new Optimoroute();
                $routes = $optimoroute->getRoutes(Carbon::createFromFormat('Y-m-d H:i:s', $pickList->date)
                    ->format('Y-m-d'));
                $pickListOrders = $pickList->orders !== null ?
                    array_flip(array_filter(json_decode($pickList->orders, true), function ($k) {
                        return is_numeric($k);
                    })) : [];


                return response()->json(['data' => $pickList,
                    'pickListOrders' => $pickListOrders,
                    'collis' => $associatedCollis,
                    'routes' => $routes['success'] == true ? $routes['routes'] : null]);
            }
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * @param int $tour
     * @param string $colliId
     * @param int $type
     */
    public function scanColli(int $tour, string $colliId, int $type)
    {
        try {
            //get collis from the picklist
            $associatedCollis = [];
            $colli = Colli::with('booking', 'client')->where('id', '=', (int)$colliId)->first();
            if ($colli) {
                //get associated collis to this picklist
                $pickList = Picklist::where('id', '=', $tour)->first();
                if (!$pickList) {
                    return response()->json(['message' => 'not found']);
                } else {
                    if ($pickList->orders !== null) {
                        $associatedCollis = Helper::getAssociatedCollis($pickList->orders, $type);
                    }
                }

                //check if colli is on current picklist
                foreach ($associatedCollis as $associatedColli) {
                    if ((int)$colliId === $associatedColli['id']) {
                        $bookingRepo = App::make(BookingRepositoryInterface::class);
                        $colli = Colli::findOrFail((int)$colliId);
                        if ($associatedColli['colliPackingStatus'] === Colli::COLLI_PACKING_STATUS_PREPARED
                            && $type === 2) {
                            if ($colli->booking_id !== null) {
                                $action = 'Colli [' . $colliId . '] have been prepared';
                                $findPreparedColli = Log::where('booking_id', '=', $colli->booking_id)
                                    ->where('action', '=', $action)
                                    ->first();
                                if (!$findPreparedColli) {
                                    $bookingRepo->saveHistoryLog($action, Auth::user()->email, $colli->booking);
                                }
                            }
                            return response()->json(['message' => 'prepared']);
                        } elseif ($associatedColli['colliPackingStatus'] === Colli::COLLI_PACKING_STATUS_LOADED) {
                            if ($colli->booking_id !== null) {
                                $action = 'Colli [' . $colliId . '] have been loaded';
                                $findPreparedColli = Log::where('booking_id', '=', $colli->booking_id)
                                    ->where('action', '=', $action)
                                    ->first();
                                if (!$findPreparedColli) {
                                    $bookingRepo->saveHistoryLog($action, Auth::user()->email, $colli->booking);
                                }
                            }
                            return response()->json(['message' => 'loaded']);
                        } else {
                            $colli->colli_packing_status = $type;
                            if ($type == 2) {
                                $colli->location_id = Location::DEFAULT_PREPARED_LOCATION_ID;
                            }
                            if ($type == 3) {
                                $colli->location_id = null;
                            }
                            $colli->received_at = Carbon::now();
                            $colli->save();
                            if ($colli->booking_id !== null) {
                                $bookingRepo = App::make(BookingRepositoryInterface::class);
                                $action = 'Colli [' . $colli->id . '] have been received';
                                $findPreparedColli = Log::where('booking_id', '=', $colli->booking_id)
                                    ->where('action', '=', $action)
                                    ->first();
                                if (!$findPreparedColli) {
                                    $bookingRepo->saveHistoryLog($action, Auth::user()->email, $colli->booking);
                                }
                            }
                            return ColliResource::make($colli);
                        }
                    }
                }
                return response()->json(['message' => 'wrong tour']);
            } else {
                return response()->json(['message' => 'not found']);
            }
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * @param Request $request
     * @param string $name
     * @param int $code
     *
     * @return JsonResponse
     */
    public function scanLocation(Request $request, string $name, int $code): JsonResponse
    {
        try {
            $data = $request->all();
//            $checkIfColliExists = $this->scanBarcode((int)$name, 0);
//            if ($checkIfColliExists->status() === 200) {
//                return response()->json(['colli' => (int)$name]);
//            } else {
            $location = Location::where('name', '=', $name)->first();
            if ($location) {
                foreach ($data as $k => $v) {
                    $colli = Colli::findOrFail((int)$v);

                    if ($colli->booking_id !== null) {
                        if ($colli->booking->linehaulTrip) {
                            $colli->status = Colli::COLLI_STATUS_ON_DISPATCH_TERMINAL;
                        } else {
                            $colli->status = Colli::COLLI_STATUS_ON_RECEIVER_TERMINAL;
                        }
                    }
                    $colli->location_id = $location->id;
                    $colli->colli_packing_status = Colli::COLLI_PACKING_STATUS_NEW;
                    $colli->received_at = Carbon::now();
                    $colli->save();
                    //colli log
                    $this->colliRepo->saveHistoryLog(
                        Log::logActions()[LOG::LOG_ACTION_COLLI_LOCATION_CHANGED],
                        $colli->id,
                        '#' . $colli->location->name . ' (' . $colli->location->warehouse->nickname . ')',
                    );
                }
                return response()->json(['location' => $location]);
            } else {
                return response()->json(['message' => 'not found']);
            }
            // }
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * @param int $id
     * @param Request $request
     * @return JsonResponse
     */
    public function moveLocation(int $id, Request $request): JsonResponse
    {
        try {
            $collisToMove = array_filter($request->all());
            if ($collisToMove) {
                foreach ($collisToMove as $k => $v) {
                    $colli = Colli::findOrFail((int)$k);
                    $getCollisByColliBatch = Colli::where('colli_batch', '=', $colli->colli_batch)->get();
                    foreach ($getCollisByColliBatch as $batch) {
                        $batch->location_id = $id;
                        $batch->colli_packing_status = Colli::COLLI_PACKING_STATUS_NEW;
                        $batch->save();
                        //colli log
                        if ($batch->location) {
                            $this->colliRepo->saveHistoryLog(
                                Log::logActions()[LOG::LOG_ACTION_COLLI_LOCATION_CHANGED],
                                $batch->id,
                                '#' . $batch->location->name . ' (' . $batch->location->warehouse->nickname . ')',
                            );
                        }
                    }
                }
            }
            return response()->json(['message' => 'success']);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }
}
