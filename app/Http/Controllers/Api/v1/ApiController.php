<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\BookingApiCollection;
use App\Models\Booking;
use App\Models\Client;
use App\Repository\Interfaces\BookingRepositoryInterface;
use App\Services\Core\Helpers\BookingHelper;
use App\Services\Core\Helpers\ColliHelper;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    /**
     * @var
     */
    protected $agreementRepo;

    /**
     * @var BookingRepositoryInterface
     */
    protected $bookingRepo;

    /**
     * @param BookingRepositoryInterface $bookingRepo
     */
    public function __construct(BookingRepositoryInterface $bookingRepo)
    {
        $this->bookingRepo = $bookingRepo;
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function get(Request $request)
    {
        $client = Client::with('agreements', 'agreements.products', 'keys')
            ->whereHas('keys', function ($q) use ($request) {
                $q->where('key', '=', $request['key']);
            })->first();

        if (!$client) {
            return response()->json('AUTH_KEY_UNKNOWN', 400);
        }

        $apiOrders = [];
        $response = json_decode($request->getContent(), true);
        if (array_key_exists('orders', $response)) {
            foreach ($response['orders'] as $v) {
                if (array_key_exists('orderNo', $v)) {
                    $apiOrders['ids'][] = (int)$v['orderNo'];
                }
                if (array_key_exists('start', $v)) {
                    $apiOrders['start'] = $v['start'];
                }
                if (array_key_exists('end', $v)) {
                    $apiOrders['end'] = $v['end'];
                }
            }
        }
        $apiOrders['client_id'] = $client->id;

        $bookings = $this->bookingRepo->get(['apiOrders' => $apiOrders]);
        return new BookingApiCollection($bookings);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $client = Client::with('agreements', 'agreements.products', 'keys')
            ->whereHas('keys', function ($q) use ($request) {
                $q->where('key', '=', $request['key']);
            })->first();

        if (!$client) {
            return response()->json('AUTH_KEY_UNKNOWN', 400);
        }

        $response = json_decode($request->getContent(), true);

        try {
            //user agreements
            $clientAgreements = [];
            $clientProducts = [];
            $clientServices = [];

            foreach ($client['agreements'] as $agreement) {
                $clientAgreements[$agreement->id] = $agreement->name;
            }
            if (!in_array($response['colli'], $response)) {
                return response()->json('ERR_COLLI_NOTFOUND', 400);
            } else {
                $newTotalVolume = 0;
                $newTotalWeight = 0;
                foreach ($response['colli'] as $v) {
                    $newTotalVolume += $v['volume'];
                    $newTotalWeight += $v['weight'];
                }
            }

            if (!in_array($response['agreement'], $clientAgreements)) {
                return response()->json('ERR_AGREEMENT_NOTFOUND', 400);
            } else {
                $agreementId = array_search($response['agreement'], $clientAgreements);
            }

            //user products
            foreach ($client['agreements'] as $agreement) {
                foreach ($agreement->products as $product) {
                    if ($product->agreement_id === $agreementId) {
                        $clientProducts[$product->id] = $product->product->name;
                    }
                }
            }

            if (!in_array($response['products'], $clientProducts)) {
                return response()->json('ERR_PRODUCT_NOTFOUND', 400);
            } else {
                $productId = array_search($response['products'], $clientProducts);
                $priceProduct = (float)BookingHelper::getProductPriceByInterval(
                    $productId,
                    (float)$newTotalVolume,
                    (float)$newTotalWeight,
                    $agreementId
                );
            }

            //user services
            foreach ($client['agreements'] as $agreement) {
                foreach ($agreement->services as $service) {
                    if ($service->agreement_id === $agreementId) {
                        $clientServices[$service->id] = $service->service->name;
                    }
                }
            }

            if (count(array_intersect($clientServices, $response['services'])) !== count($response['services'])) {
                return response()->json('ERR_SERVICE_NOTFOUND', 400);
            } else {
                $clientServices = array_intersect($clientServices, $response['services']);
                $priceService = BookingHelper::getServicePriceByInterval(
                    json_encode(array_values(array_flip($clientServices))),
                    (float)$newTotalVolume,
                    (float)$newTotalWeight,
                    $agreementId
                );
            }

            $booking_data = [
                'totalVolume' => (float)$newTotalVolume,
                'totalWeight' => (float)$newTotalWeight,

                'pickupAddressName' => $response['pickuplocation']['pickupLocationName'],
                'pickupAddressZip' => $response['pickuplocation']['pickupAddressZip'],
                'pickupAddressCity' => $response['pickuplocation']['pickupAddressCity'],
                'pickupAddressStreet' => $response['pickuplocation']['pickupAddressStreet'],
                'pickupPhone' => $response['pickuplocation']['pickupAllowedTime'][0]['pickupPhone'],
                'pickupAddressPhone' => $response['pickuplocation']['pickupAllowedTime'][0]['pickupPhone'],
                'pickupNote' => $response['pickuplocation']['pickupAllowedTime'][0]['pickupNote'],
                'pickupDate' => empty($response['pickuplocation']['pickupAllowedTime'][0]['from']) ? null
                    : Carbon::parse($response['pickuplocation']['pickupAllowedTime'][0]['from'])
                        ->setTimezone('UTC')
                        ->format('Y-m-d'),
                'pickupTimeStart' =>empty($response['pickuplocation']['pickupAllowedTime'][0]['from']) ? null
                    : Carbon::parse($response['pickuplocation']['pickupAllowedTime'][0]['from'])
                        ->setTimezone('UTC')
                        ->format('H:i'),
                'pickupTimeEnd' => empty($response['pickuplocation']['pickupAllowedTime'][0]['to']) ? null
                    : Carbon::parse($response['pickuplocation']['pickupAllowedTime'][0]['to'])
                        ->setTimezone('UTC')
                        ->format('H:i'),
                'dropoffAddressName' => $response['dropofflocation']['dropoffLocationName'],
                'dropoffAddressZip' => $response['dropofflocation']['dropoffAddressZip'],
                'dropoffAddressCity' => $response['dropofflocation']['dropoffAddressCity'],
                'dropoffAddressStreet' => $response['dropofflocation']['dropoffAddressStreet'],
                'dropoffPhone' => $response['dropofflocation']['dropoffPhone'],
                'dropoffAddressPhone' => $response['dropofflocation']['dropoffPhone'],
                'dropoffNote' => $response['dropofflocation']['dropoffNote'],
                'dropoffDate' => empty($response['dropofflocation']['dropoffAllowedTime'][0]['from']) ? null
                    : Carbon::parse($response['dropofflocation']['dropoffAllowedTime'][0]['from'])
                        ->setTimezone('UTC')
                        ->format('Y-m-d'),
                'dropoffTimeStart' => empty($response['dropofflocation']['dropoffAllowedTime'][0]['from']) ? null
                    : Carbon::parse($response['dropofflocation']['dropoffAllowedTime'][0]['from'])
                        ->setTimezone('UTC')
                        ->format('H:i'),
                'dropoffTimeEnd' => empty($response['dropofflocation']['dropoffAllowedTime'][0]['to']) ? null
                    : Carbon::parse($response['dropofflocation']['dropoffAllowedTime'][0]['to'])
                        ->setTimezone('UTC')
                        ->format('H:i'),
                'linehaulType' => '0-0',

                'productId' => $productId,
                'priceProduct' => $priceProduct,
                'priceService' => $priceService['priceServiceSum'],
                'newTotalPriceProductByPiece' => $priceProduct,
                'priceServiceSum' => $priceService['priceServiceSum'],
                'newIndividualPriceService' => $priceService['priceService'] ?? null,

                'clientId' => ['id' => $client->id],
                'clientAgreement' => $agreementId,
                'client' => $client,

                //'total_colli' => $response['colli'] ?? 1,
                'reference' => $response['reference'] ?? null,
                'note' => $response['notes'] ?? null,
                'colli' => $response['colli'] ?? null,
                'isApiOrder' => 1
            ];

            $booking = $this->bookingRepo->create($booking_data);

            //print labels
            $printData = ColliHelper::generateBase64Label($booking);

            return response()->json(
                [
                    'success' => true,
                    'price' => $priceProduct + $priceService['priceServiceSum'],
                    'status' => Booking::bookingStatuses()[$booking->status],
                    'bookingNo' => $booking->id,
                    'labels' => $printData
                ]
            );
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }
}
