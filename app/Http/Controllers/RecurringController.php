<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateRecurringRequest;
use App\Models\UserRole;
use App\Repository\Interfaces\RecurringRepositoryInterface;
use App\Http\Requests\StoreRecurringRequest;
use App\Http\Resources\RecurringCollection;
use App\Http\Resources\RecurringResource;
use App\Models\Recurring;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class RecurringController extends Controller
{
    /**
     * @var RecurringRepositoryInterface
     */
    protected $recurringRepo;

    /**
     * @param RecurringRepositoryInterface $recurringRepo
     */
    public function __construct(RecurringRepositoryInterface $recurringRepo)
    {
        $this->middleware(['role:' . UserRole::ROLE_ADMIN]);
        $this->recurringRepo = $recurringRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        return view('recurrings.index', ['userRole' => in_array('user', Auth::user()->roles) ?
            UserRole::ROLE_USER : UserRole::ROLE_ADMIN]);
    }

    /**
     * @param Request $request
     * @return RecurringCollection
     */
    public function getRecurrings(Request $request): RecurringCollection
    {
        $recurrings = $this->recurringRepo->get($request->all());
        return new RecurringCollection($recurrings);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        return view('recurrings.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRecurringRequest $request
     * @return JsonResponse
     */
    public function store(StoreRecurringRequest $request): JsonResponse
    {
        try {
            dd($request->all());
            $recurring = $this->recurringRepo->create($request->all());
            return response()->json(['message' => 'success', 'recurringId' => $recurring->id]);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return View
     */
    public function edit(int $id): View
    {
        $recurring = Recurring::findOrFail($id);
        return view('recurrings.edit', compact(['recurring']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRecurringRequest $request
     * @param int $id
     *
     * @return JsonResponse
     */
    public function update(UpdateRecurringRequest $request, int $id): JsonResponse
    {
        try {
            $recurring = Recurring::findOrFail($id);
            $recurringUpd = $this->recurringRepo->update($request->all(), $recurring);
            return response()->json(['message' => 'success', 'recurringId' => $recurringUpd->id]);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * Get recurring info.
     *
     * @param int $id
     * @return RecurringResource
     *
     */
    public function getSingle(int $id): RecurringResource
    {
        $recurring = Recurring::with('driver', 'client', 'agreement', 'product')->findOrFail($id);
        return RecurringResource::make($recurring);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        try {
            $recurring = Recurring::findOrFail($id);
            if ($recurring->delete()) {
                return response()->json(['message' => 'success']);
            } else {
                return response()->json(['message' => 'failed']);
            }
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }
}
