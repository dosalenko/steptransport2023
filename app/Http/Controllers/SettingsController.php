<?php

namespace App\Http\Controllers;

use App\Exports\BookingExport;
use App\Exports\TripExport;
use App\Http\Requests\StoreStepAddressRequest;
use App\Http\Requests\UpdateStepAddressRequest;
use App\Jobs\OptimoRouteGetOrdersJob;
use App\Models\Address;
use App\Models\Agreement;
use App\Models\Booking;
use App\Models\Communication;
use App\Models\Picklist;
use App\Models\Service;
use App\Models\StepAddress;
use App\Models\TourStop;
use App\Models\Trip;
use App\Services\Core\Helpers\Helper;
use App\Services\Optimoroute\Optimoroute;
use Arcanedev\LaravelSettings\Contracts\Store;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;
use Maatwebsite\Excel\Facades\Excel;
use Printer;
use Throwable;

class SettingsController extends Controller
{
    /** @var  \Arcanedev\LaravelSettings\Contracts\Store */
    protected $settings;

    public function __construct(Store $settings)
    {
        $this->settings = $settings;
    }

    /**
     *
     * @return View
     */
    public function reporting(): View
    {
        return view('settings.index');
    }

    /**
     *
     * @param Request $request
     * @param string $dateFrom
     * @param string $dateTo
     *
     */
    public function exportBookings(Request $request, string $dateFrom, string $dateTo)
    {
        try {
            $requestData = $request->all();
            $columns = Helper::getBookingExportColumns($requestData);
            return Excel::download(new BookingExport($columns, $dateFrom, $dateTo), 'booking_' . time() . '.xlsx');
        } catch (Throwable $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * @param Request $request
     */
    public function exportTrips(Request $request)
    {
        try {
            $requestData = $request->all();
            return Excel::download(new TripExport(array_filter($requestData)), 'trips.xlsx');
        } catch (Throwable $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     *
     * @param Request $request
     */
    public function exportDrivers(Request $request)
    {
        dispatch(new OptimoRouteGetOrdersJob(0));
        //sleep(150);
        try {
            $requestData = $request->all();
            Carbon::createFromFormat('Y-m-d', $requestData['date'])->format('d/m/Y');

            $routesStops = [];
            $pickListData = [];
            $barcodeData = [];
            $documentCreatedAt = $requestData['documentCreatedAt'];
            $showSingleDriverTrips = $requestData['driver'] != "0" && $requestData['driver'] != "1";
            $showAllTrips = $requestData['driver'] == "0";
            $showUnassignedTrips = $requestData['driver'] == "0" || $requestData['driver'] == "1";
            $driverName = $requestData['driver'];

            if ($showUnassignedTrips) {
                $unassignedTrips = Trip::with('booking', 'client')
                    ->whereDate('tw_start', $requestData['date'])
                    ->whereNull('driver')->get();

                $pickList = new Picklist();
                $pickList->driver = 'unassigned';
                $pickList->date = $requestData['date'];
                $pickList->orders = null;
                $pickList->file = 'report_trips_' . $documentCreatedAt;
                $pickList->status = Picklist::PICKLIST_STATUS_NEW;
                $pickList->save();
                $barcodeData[0] = $pickList->id;
            } else {
                $unassignedTrips = [];
            }

            if ($showSingleDriverTrips || $showAllTrips) {
                //get trip routes
                $optimoroute = new Optimoroute();
                $routes = $optimoroute->getRoutes($requestData['date']);

                if ($routes['result'] = true) {
                    //group routes by drivers
                    foreach ($routes['routes'] as $v) {
                        if ($showSingleDriverTrips && $v['driverName'] !== $requestData['driver']) {
                            continue;
                        }
                        $routesStops[$v['driverName']] = $v['stops'];
                    }
                    //find associated trip by OrderNo
                    foreach ($routesStops as $k => $driver) {
                        if ($showSingleDriverTrips && $k !== $requestData['driver']) {
                            continue;
                        }
                        foreach ($driver as $v => $stop) {
                            $isPrefixedOrder = strpos($stop['orderNo'], 'T') !== false;
                            $orderNo = $isPrefixedOrder ? (int)substr($stop['orderNo'], 2) : (int)($stop['orderNo']);

                            $trip = Trip::with('booking', 'client')
                                ->where('id', '=', $orderNo)
                                ->whereNotNull('driver')
                                ->first();
                            if ($trip) {
                                $routesStops[$k][$v]['isDepotExists'] = 0;
                                $routesStops[$k][$v]['trip'] = [
                                    'id' => $trip['id'],
                                    'type' => $trip['type'],
                                    'booking' => $trip['booking'],
                                    'collis' => $trip['booking']['collis'],
                                    'services' => Service::getServicesNames($trip['booking']['service_price']),
                                    'client' => $trip['client'],
                                    'total_colli' => Helper::getTotalBookingCollis($trip['booking']['id']),
                                    'colli_location' => Helper::getColliLocations($trip['booking']['id']),
                                    'note' => $trip['note'],
                                ];

                                TourStop::where('trip_id', '=', $trip['id'])->
                                where('driver', '=', $k)->
                                where('date', '=', $requestData['date'])->delete();

                                $pickListData[$k][$v] = $trip['id'];
                                $tourStop = new TourStop();
                                $tourStop->trip_id = $trip['id'];
                                $tourStop->driver = $k;
                                $tourStop->date = $requestData['date'];
                                $tourStop->stop_number = $v;
                                $tourStop->save();
                            } else {
                                if ((array_key_exists('type', $stop) && $stop['type'] === 'depot')
                                    || $stop['address'] === Address::DEPOT_ADDRESS) {
                                    $routesStops[$k][$v]['trip'] = [
                                        'type' => 'depot',
                                        'address' => $stop['address'],
                                    ];
                                    $routesStops[$k][$v]['isDepotExists'] = 1;
                                    $pickListData[$k][$v] = 'depot';
                                } else {
                                    $routesStops[$k][$v]['trip'] = [
                                        'id' => $stop['orderNo'],
                                        'client' => $stop['locationName'],
                                        'type' => 'unknown',
                                        'address' => $stop['address'],
                                    ];
                                    $routesStops[$k][$v]['isUnknownExists'] = 1;
                                    $pickListData[$k][$v] = $stop;
                                    //unset($routesStops[$k][$v]);
                                    //$routesStops[$k][$v]['trip'] = null;
                                }
                            }
                        }
                    }

                    foreach ($pickListData as $driver => $order) {
                        $pickList = new Picklist();
                        $pickList->driver = $driver;
                        $pickList->date = $requestData['date'];
                        $pickList->orders = json_encode($order);
                        $pickList->file = 'report_trips_' . $documentCreatedAt;
                        $pickList->status = Picklist::PICKLIST_STATUS_NEW;
                        $pickList->save();
                        $barcodeData[$driver] = $pickList->id;
                    }
                }
            }
            $routesStopsFormatted = [];
            $numPage = 0;
            $i = 0;
            foreach ($routesStops as $driver => $stop) {
                if (array_key_exists('isDepotExists', $stop) && in_array(1, array_column($stop, 'isDepotExists'))) {
                    foreach ($stop as $k => $v) {
                        if ($v['isDepotExists'] === 0) {
                            if ($i <= 6) {
                                $i++;
                                $routesStopsFormatted[$driver][$numPage][$k] = $v;
                            } else {
                                $numPage++;
                                $i = 0;
                                $routesStopsFormatted[$driver][$numPage][$k] = $v;
                            }
                        } else {
                            if ($i <= 6) {
                                $i = 0;
                                $routesStopsFormatted[$driver][$numPage][$k] = $v;
                                $numPage++;
                            } else {
                                $numPage++;
                                $i = 0;
                                $routesStopsFormatted[$driver][$numPage][$k] = $v;
                            }
                        }
                    }
                } else {
                    for ($n = 0; $n < count($stop); $n = $n + 7) {
                        $routesStopsFormatted[$driver][$n] = array_slice($stop, $n, 7, true);
                    }
                }
                $numPage = 0;
                $i = 0;
            }

            $pickListDate = Carbon::createFromFormat('Y-m-d', $requestData['date'])->format('d/m/Y');
            if ($showUnassignedTrips === true && count($unassignedTrips) > 0) {
                $view = 'settings.trip_pdf_report_all';
            } else {
                $view = 'settings.trip_pdf_report_empty_unassigned';
            }
            $pdf = \PDF::loadView($view, compact([
                'routesStopsFormatted',
                'pickListDate',
                'barcodeData',
                'showAllTrips',
                'showSingleDriverTrips',
                'showUnassignedTrips',
                'unassignedTrips',
                'driverName']));
            $pdf->setPaper('A4', 'landscape');
            $pdf->setOptions(['enable_php' => true, 'isRemoteEnabled' => true]);

            Storage::put('public/reports/report_trips_' . $documentCreatedAt . '.pdf', $pdf->output());

            //printnode
            if (env('APP_ENV', 'local') === 'prod') {
                $printerId = Printer::getTourId();
                $printer = Printer::getPrintnodePrinter($printerId);
                $printJobStatus = $printer->addPrintJob(
                    $printerId,
                    Storage::disk('public')->path('/reports/report_trips_' . $documentCreatedAt . '.pdf'),
                    'Report trips ' . $pickListDate,
                    config('services.printnode.options')
                );

                if (!isset($printJobStatus['success']) || !$printJobStatus['success']) {
                    Log::channel('printlog')->info("Print job has not been added (Report trip #$documentCreatedAt)");
                } else {
                    Log::channel('printlog')->info("Print job has been added (Report trip #$documentCreatedAt)");
                }
            }
            return $pdf->stream('report_trips.pdf');
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * Show step addresses
     *
     * @return View
     */
    public function stepAddresses(): View
    {
        return view('settings.step_addresses');
    }

    /**
     * New step address view
     *
     * @return View
     */
    public function newStepAddress(): View
    {
        return view('settings.step_address_create');
    }

    /**
     * Get step addresses list.
     *
     * @return JsonResponse
     */
    public function getStepAddresses(): JsonResponse
    {
        $addresses = StepAddress::with('receivingLocation')->get();
        return response()->json(['data' => $addresses]);
    }

    /**
     * Update step addresses
     *
     * @param UpdateStepAddressRequest $request
     * @return JsonResponse
     */
    public function updateStepAddresses(UpdateStepAddressRequest $request): JsonResponse
    {
        try {
            $requestData = $request->all();
            foreach ($requestData as $item) {
                $item['receiving_location_id'] = !empty($item['receiving_location']) ?
                    (int)$item['receiving_location']['id'] : null;
                $address = StepAddress::findOrFail($item['id']);
                $address->update($item);
            }

            return response()->json(['result' => 'success']);
        } catch (Throwable $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     *
     * @return View
     */
    public function showLockedProducts(): View
    {
        return view('settings.locked_products');
    }

    /**
     *
     * @return JsonResponse
     */
    public function getLockedProducts(): JsonResponse
    {
        $agreements = Agreement::with('client')
            ->orderByRaw('(SELECT name FROM clients WHERE clients.id = agreements.client_id)')
            ->get();
        return response()->json(['data' => $agreements]);
    }

    /**
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function updateLockedProducts(Request $request): JsonResponse
    {
        try {
            $requestData = $request->all();
            foreach ($requestData as $k => $item) {
                $agreement = Agreement::findOrFail((int)$k);
                $agreement->is_locked_products = (bool)$item;
                $agreement->save();
            }

            return response()->json(['result' => 'success']);
        } catch (Throwable $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * Store step addresses
     *
     * @param StoreStepAddressRequest $request
     *
     * @return JsonResponse
     */
    public function storeStepAddress(StoreStepAddressRequest $request): JsonResponse
    {
        try {
            $requestData = $request->all();
            StepAddress::create($requestData);
            return response()->json(['result' => 'success']);
        } catch (Throwable $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /*
     *
     */
    public function getPicklist()
    {
        $picklist = Picklist::where('created_at', '>=', Carbon::now()->subDays(30)
            ->toDateTimeString())
            ->latest()
            ->get();
        return view('settings.picklist', ['picklist' => $picklist]);
    }

    /*
     *
     */
    public function getLinks()
    {
        $bookings = Booking::with('trips')->where('created_at', '>=', Carbon::now()->subDays(30)
            ->toDateTimeString())
            ->latest()
            ->get();
        return view('settings.links', ['bookings' => $bookings]);
    }

    /*
     *
     */
    public function downloadFile($file)
    {
        return response()->download(storage_path('app/public/reports/' . $file . '.pdf'));
    }

    /*
     * test
     */
    public function communicationLog()
    {
        $communications = Communication::whereIn('message_type', [0, 1, 5])->latest()
            ->get();
        return view('settings.communication_log', ['communications' => $communications]);
    }

    public function showCommunicationLog($bookingId)
    {
        try {
            $communications = Communication::whereHas('trip', function ($q) use ($bookingId) {
                $q->where('booking_id', '=', $bookingId);
            })->get();
            return response()->json(['result' => $communications]);
        } catch (Throwable $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }
}
