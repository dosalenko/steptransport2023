<?php

namespace App\Http\Controllers;

use App\Models\Communication as Model;
use App\Models\Trip;
use App\Repository\Interfaces\DeliveryRepositoryInterface;
use App\Services\Communication\Communication;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DeliveryController extends Controller
{
    /**
     * @var DeliveryRepositoryInterface
     */
    protected $deliveryRepo;

    /**
     * @param DeliveryRepositoryInterface $deliveryRepo
     */
    public function __construct(DeliveryRepositoryInterface $deliveryRepo)
    {
        $this->deliveryRepo = $deliveryRepo;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $data = $request->all();
            $delivery = $this->deliveryRepo->create($request->all());
            if ($delivery) {
                $trip = Trip::findOrFail($data['trip']['id']);
                $trip->tw_start = $data['date']['date'] . ' ' . $data['date']['from'] . ':00';
                $trip->tw_end = $data['date']['date'] . ' ' . $data['date']['to'] . ':00';
                $trip->status = Trip::TRIP_STATUS_NEW;
                if ($trip->type === Trip::TRIP_TYPE_PICKUP) {
                    $trip->booking->pickup_start = $data['date']['date'] . ' ' . $data['date']['from'] . ':00';
                    $trip->booking->pickup_end = $data['date']['date'] . ' ' . $data['date']['to'] . ':00';
                    $trip->booking->save();
                    $trip->save();

                    //start communication flow
                    $communication = new Communication($trip, \App\Models\Communication::MESSAGE_TYPE_PU_RECONFIRMATION);
                    $communication->start();
                } elseif ($trip->type === Trip::TRIP_TYPE_DROPOFF) {
                    $trip->booking->dropoff_start = $data['date']['date'] . ' ' . $data['date']['from'] . ':00';
                    $trip->booking->dropoff_end = $data['date']['date'] . ' ' . $data['date']['to'] . ':00';
                    $trip->booking->save();
                    $trip->save();

                    //start communication flow
                    $communication = new Communication($trip, \App\Models\Communication::MESSAGE_TYPE_DO_RECONFIRMATION);
                    $communication->start();
                }
                $communicationData = [
                    'user_id' => Auth::user() ? Auth::user()->id : 1,
                    'communication_type' => Model::COMMUNICATION_TYPE_UNASSIGNED,
                    'message_type' => Model::MESSAGE_TYPE_DATE_ASSIGNED,
                    'message' => 'Date assigned',
                    'trip_id' => $trip->id,
                    'date_assigned' => $data['date']['date'] . ' ' . $data['date']['from'] . '-' . $data['date']['to'],
                ];
                Model::create($communicationData);
            }
            return response()->json(['message' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }
}
