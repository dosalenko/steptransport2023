<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRouteRequest;
use App\Http\Requests\UpdateRouteRequest;
use App\Http\Resources\RouteCollection;
use App\Models\Route;
use App\Models\UserRole;
use App\Repository\Interfaces\RouteRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class RouteController extends Controller
{
    /**
     * @var RouteRepositoryInterface
     */
    protected $routeRepo;

    /**
     * @param RouteRepositoryInterface $routeRepo
     */
    public function __construct(RouteRepositoryInterface $routeRepo)
    {
        $this->middleware(['role:' . UserRole::ROLE_ADMIN]);
        $this->routeRepo = $routeRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        return view('routes.index', ['userRole' => in_array('user', Auth::user()->roles) ?
            UserRole::ROLE_USER : UserRole::ROLE_ADMIN]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\StoreRouteRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRouteRequest $request)
    {
        try {
            $route = $this->routeRepo->create($request->all());
            return response()->json(['message' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param \int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(int $id)
    {
        try {
            Route::destroy($id);
            return response()->json(['message' => 'success']);
        } catch (\Throwable $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * @param Request $request
     * @return RouteCollection
     */
    public function getRoutes(Request $request): RouteCollection
    {
        $routes = $this->routeRepo->get($request->all());
        return new RouteCollection($routes);
    }
}
