<?php

namespace App\Http\Controllers;

use App\Http\Resources\ServiceCollection;
use App\Models\AgreementService;
use App\Models\Interval;
use App\Models\Service;
use App\Repository\Interfaces\ServiceRepositoryInterface;
use App\Models\ServicePrice;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Requests\StoreServiceRequest;
use App\Http\Requests\UpdateServiceRequest;
use Illuminate\View\View;
use Throwable;

class ServiceController extends Controller
{
    /**
     * @var ServiceRepositoryInterface
     */
    protected $serviceRepo;

    /**
     * @param ServiceRepositoryInterface $serviceRepo
     */
    public function __construct(ServiceRepositoryInterface $serviceRepo)
    {
        $this->serviceRepo = $serviceRepo;
    }

    /**
     * @param int|null $agreementId
     * @return ServiceCollection
     */
    public function getServices($agreementId = 0): ServiceCollection
    {
        if ($agreementId > 0) {
            $query = AgreementService::with('prices', 'service')
                ->where('agreement_id', '=', $agreementId)
                ->where('active', '=', 1);
        } else {
            $query = Service::with('prices');
        }
        $products = $query->get();

        return new ServiceCollection($products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        return view('services.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreServiceRequest $request
     * @return JsonResponse
     */
    public function store(StoreServiceRequest $request): JsonResponse
    {
        try {
            $service = $this->serviceRepo->create($request->all());
            //after products creating add prices to each existing interval
            $intervals = Interval::all();
            foreach ($intervals as $interval) {
                $servicePrice = new ServicePrice();
                $servicePrice->service_id = $service->id;
                $servicePrice->interval_id = $interval->id;
                $servicePrice->save();
            }

            return response()->json(['message' => 'success', 'serviceId' => $service->id]);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * Get product info.
     *
     * @param int $id
     *
     * @return JsonResponse
     */
    public function getSingle(int $id): JsonResponse
    {
        $service = Service::findOrFail($id);

        return response()->json($service);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateServiceRequest $request
     * @return JsonResponse
     */
    public function update(UpdateServiceRequest $request): JsonResponse
    {
        try {
            $requestData = $request->all();
            foreach ($requestData as $item) {
                $service = Service::findOrFail($item['id']);
                $service->update($item);
            }
            return response()->json(['result' => 'success']);
        } catch (Throwable $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        try {
            $this->serviceRepo->delete($id);
            return response()->json(['message' => 'success']);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * Save products related to concrete agreement
     *
     * @param Request $request
     * @param $id
     *
     * @return JsonResponse
     */
    public function saveAgreementServices(Request $request, $id): JsonResponse
    {
        try {
            $requestData = $request->all();
            if (!empty($requestData)) {
                foreach ($requestData as $serviceId) {
                    AgreementService::updateOrCreate(
                        ['agreement_id' => $id, 'id' => (int)$serviceId],
                        ['active' => 1]
                    );
                }
                //make other agreements services inactive
                AgreementService::where('agreement_id', '=', $id)->whereNotIn('id', $requestData)
                    ->update(['active' => 0]);
            } else {
                AgreementService::where('agreement_id', '=', $id)
                    ->update(['active' => 0]);
            }
            return response()->json(['result' => 'success']);
        } catch (Throwable $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function getAgreementServices(int $id): JsonResponse
    {
        $arr = [];
        $query = AgreementService::where('agreement_id', '=', $id)->where('active', '=', 1)->get();
        foreach ($query as $v) {
            $arr[] = $v->id;
        }
        return response()->json($arr);
    }
}
