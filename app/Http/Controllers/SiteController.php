<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Rating;
use App\Models\Trip;
use App\Services\Core\Helpers\BookingHelper;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Jenssegers\Agent\Agent;

class SiteController extends Controller
{
    public function index()
    {
        $agent = new Agent();
        if ($agent->isMobile()) {
            return redirect()->route('settings.packGoods');
        }
        if (in_array('user', Auth::user()->roles)) {
            return redirect()->route('bookings.index');
        } elseif (in_array('admin', Auth::user()->roles)) {
            return redirect()->route('bookings.index');
        }


        $linkToAssets = asset('assets/site_assets');

        $presetsConfig = config('presets', []);
        $presets = Arr::pluck($presetsConfig, 'name');
        $presets = array_merge([0 => 'Select preset'], $presets);

        $presetsWeight = Arr::pluck($presetsConfig, 'autosuggest_weight');

        return view(
            'site.index',
            compact(
                'presets',
                'linkToAssets',
                'presetsWeight',
            )
        );
    }

    /**
     *
     * @param int $id
     *
     * @return View|RedirectResponse
     */
    public function rate(string $link)
    {

        $booking = Booking::where('rate_link', '=', $link)->first();

//        if (Auth::user()) {
//            $userVotes = Rating::where('booking_id', '=', $booking->id)->where('user_id', '=', Auth::user()->id)->count();
//            $isUserAlreadyVoted = (int)$userVotes > 0 ? 1 : 0;
//        } else {
//            $isUserAlreadyVoted = 0;
//        }

        $userVotes = Rating::where('booking_id', '=', $booking->id)->count();
        $isUserAlreadyVoted = (int)$userVotes > 0 ? 1 : 0;

        if ($booking) {
            return view('site.rate', compact(['booking', 'isUserAlreadyVoted']));
        } else {
            return redirect()->route('bookings.index');
        }
    }

    /**
     *
     * @param Request $request
     * @param int $id
     *
     * @return JsonResponse
     */
    public function storeRate(Request $request, int $id): JsonResponse
    {
        try {
            $request->validate([
                'comment' => 'max:1000',
                'rating' => 'required|integer',
            ]);
            $data = $request->all();
            $rateData = [
                'booking_id' => $id,
                'comment' => $data['comment'],
                'rating' => $data['rating'],
            ];
            Rating::create($rateData);
            return response()->json(['message' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * @param string $link
     * @return View|RedirectResponse
     */
    public function checkDelivery(string $link)
    {
        $trip = Trip::with('address', 'client')->where('unique_link', '=', $link)->first();
        if ($trip) {
            return view('site.check_delivery', compact(['trip']));
        } else {
            return redirect()->route('bookings.index');
        }
    }


    /**
     * @param int $zip
     * @param int $tripId
     * @return JsonResponse
     */
    public function getAvailableDates(int $zip, int $tripId): JsonResponse
    {
        $availableDates = BookingHelper::getAvailableDates($zip, $tripId);

        try {
            return response()->json(['message' => 'success', 'data' => $availableDates]);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /*
     *
     */
    public function getCommunicationTemplates(string $date, int $tripId)
    {
        $trip = Trip::with('address', 'client', 'booking', 'booking.bookingServices')->where('id', '=', $tripId)->first();
        if ($trip) {
            return view('communications.test', compact(['trip', 'date']));
        } else {
            return redirect()->route('bookings.index');
        }
    }
}
