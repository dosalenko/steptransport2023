<?php

namespace App\Http\Controllers;

use App;
use App\Http\Requests\StoreTripRequest;
use App\Http\Resources\TripCollection;
use App\Http\Resources\TripResource;
use App\Jobs\OptimoRouteGetOrdersJob;
use App\Models\Booking;
use App\Models\Colli;
use App\Models\Trip;
use App\Models\UserRole;
use App\Repository\Interfaces\BookingRepositoryInterface;
use App\Repository\Interfaces\TripRepositoryInterface;
use App\Services\Core\Helpers\BookingHelper;
use App\Services\Core\Helpers\Helper;
use App\Services\Optimoroute\Optimoroute;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class TripController extends Controller
{
    /**
     * @var TripRepositoryInterface
     */
    protected $tripRepo;

    /**
     * @var BookingRepositoryInterface
     */
    protected $bookingRepo;

    /**
     * @param TripRepositoryInterface $tripRepo
     * @param BookingRepositoryInterface $bookingRepo
     */
    public function __construct(TripRepositoryInterface $tripRepo, BookingRepositoryInterface $bookingRepo)
    {
        $this->tripRepo = $tripRepo;
        $this->bookingRepo = $bookingRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        return view('trips.index');
    }

    /**
     * @param Request $request
     * @return TripCollection
     */
    public function getTrips(Request $request): TripCollection
    {
        $trips = $this->tripRepo->get($request->all());
        return new TripCollection($trips);
    }

    /**
     * Get trip info.
     *
     * @param int $id
     * @return TripResource
     *
     */
    public function getSingle(int $id): TripResource
    {
        $trip = Trip::with('address', 'address.country', 'booking', 'booking.linehaulTrip', 'booking.pickupAddress', 'booking.pickupAddress.country', 'booking.dropoffAddress', 'booking.dropoffAddress.country', 'client', 'client.primaryContact')->findOrFail($id);
        return TripResource::make($trip);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return View
     */
    public function edit(int $id): View
    {
        dispatch(new OptimoRouteGetOrdersJob($id));
        $userRole = in_array('user', Auth::user()->roles) ? UserRole::ROLE_USER : UserRole::ROLE_ADMIN;
        $trip = Trip::findOrFail($id);
        return view('trips.edit', compact('trip', 'userRole'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreTripRequest $request
     * @param int $id
     *
     * @return JsonResponse
     */
    public function update(StoreTripRequest $request, int $id): JsonResponse
    {
        try {
            $trip = Trip::findOrFail($id);
            $tripUpd = $this->tripRepo->update($request->all(), $trip);
            $trip = Trip::findOrFail($id);
            if ($trip->optimoroute_result) {
                $tripDataJson = [];
                $tripDataJson[] = json_decode(TripResource::make($trip)->toJson(), true);

                //create new external optimo route order based on current trip
                $optimoroute = new Optimoroute();
                if (!empty($tripDataJson)) {
                    $optimoroute->createOrder($tripDataJson, true);
                }
                //check if return booking exists then send also return trip to Optimoroute
                if ($trip->type === Trip::TRIP_TYPE_DROPOFF) {
                    $childBooking = BookingHelper::getChildBooking($trip->booking->id);
                    if ($childBooking) {
                        $childTrip = Trip::where('booking_id', '=', $childBooking)->where('type', '=', Trip::TRIP_TYPE_PICKUP)->first();
                        $tripDataJson = [];
                        $tripDataJson[] = json_decode(TripResource::make($childTrip)->toJson(), true);
                        //create new external optimo route order based on current trip
                        $optimoroute = new Optimoroute();
                        if (!empty($tripDataJson)) {
                            $optimoroute->createOrder($tripDataJson, true);
                        }
                    }
                }
            }
            return response()->json(['message' => 'success', 'tripId' => $tripUpd->id]);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * @return  JsonResponse
     */
    public function statusesCounter(): JsonResponse
    {
        $result = Helper::tripStatusesCounter();
        return response()->json($result);
    }

    /**
     * @param Request $request
     * @return  JsonResponse
     */
    public function getDimensionsSum(Request $request): JsonResponse
    {
        $requestData = $request->all();
        $filters = json_decode($requestData['filters'], true);

        $checkedTrips = array_filter(json_decode($requestData['trips'], true));

        $bookingListToCalculate = [];
        $dimensions = [
            'volume' => 0,
            'weight' => 0,
        ];

        $trips = $this->tripRepo->get($filters);

        foreach ($trips as $trip) {
            if (count($checkedTrips) > 0 && !array_key_exists($trip->id, $checkedTrips)) {
                continue;
            }

            $bookingListToCalculate[$trip->booking->id]['volume'] = round($trip->booking->collis->sum('volume'), 1);
            $bookingListToCalculate[$trip->booking->id]['weight'] = round($trip->booking->collis->sum('weight'));
        }

        foreach ($bookingListToCalculate as $v) {
            $dimensions['weight'] += $v['weight'];
            $dimensions['volume'] += $v['volume'];
        }
        return response()->json($dimensions);
    }

    /**
     * Get trip drivers.
     *
     * @param string $date
     * @return JsonResponse
     */
    public function getDrivers(string $date): JsonResponse
    {
        dispatch(new OptimoRouteGetOrdersJob(0));
        $drivers = Helper::updateDriversByDate($date);
        return response()->json($drivers);
    }

    /**
     * set linehaul delivered status
     *
     * @param int $id
     * @param int $type
     * @param string $date
     * @return JsonResponse
     *
     */
    public function setTripDelivered(int $id, int $type, string $date = null): JsonResponse
    {
        try {
            $trip = Trip::findOrFail($id);

            if ($trip->type === Trip::TRIP_TYPE_LINEHAUL) {
                $trip->status = Trip::TRIP_STATUS_DELIVERED;
                $trip->save();
                //when a linehaul is marked as delivered, we "receive" the goods in that specific location where
                // the linehaul ended.
                $linehaulWarehouse = (int)explode('-', $trip->booking->linehaul_type)[1];
                if ($linehaulWarehouse > 0) {
                    $receivingLocation = Helper::getReceivingLocation($linehaulWarehouse);
                    foreach ($trip->booking->collis as $colli) {
                        if ($colli->status === Colli::COLLI_STATUS_DELIVERED) {
                            continue;
                        }
                        if ($colli->location_id !== null && $colli->location->warehouse_id === $linehaulWarehouse) {
                            continue;
                        }
                        $colli->location_id = $receivingLocation;
                        $colli->save();
                    }
                }
            } else {
                $trip->status = Trip::TRIP_STATUS_DELIVERED;
                $trip->save();
                $pickupTrip = Trip::where('type', '=', Trip::TRIP_TYPE_PICKUP)
                    ->where('booking_id', '=', $trip->booking_id)
                    ->first();
                if ($pickupTrip) {
                    $pickupTrip->status = Trip::TRIP_STATUS_DELIVERED;
                    $pickupTrip->save();
                }
            }
            //check for booking updates
            $bookingRepo = App::make(BookingRepositoryInterface::class);
            $bookingRepo->checkStatusUpdates($trip->booking->id);
            return response()->json(['message' => 'success']);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * set linehaul delivered status
     *
     * @param int $id
     * @param string $date
     * @return JsonResponse
     */
    public function updateDropoffDate(int $id, string $date): JsonResponse
    {
        try {
            $formattedDate = Carbon::createFromFormat('Y-m-d', $date)->format('Y-m-d');
            $trip = Trip::findOrFail($id);
            $trip->tw_start = $formattedDate;
            $trip->tw_end = $formattedDate;
            $trip->status = $trip->status === Trip::TRIP_STATUS_AWAITING_DATE ? Trip::TRIP_STATUS_NEW : $trip->status;
            if ($trip->save()) {
                //get related booking
                $booking = Booking::findOrFail($trip->booking_id);
                $booking->dropoff_start = $formattedDate;
                $booking->dropoff_end = $formattedDate;
                $booking->save();
            }
            return response()->json(['message' => 'success']);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }
}
