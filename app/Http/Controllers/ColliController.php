<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateColliRequest;
use App\Mail\ListOfReceivedGoods;
use App\Models\Image;
use App\Models\Trip;
use App\Models\UserRole;
use App\Repository\Interfaces\ColliRepositoryInterface;
use App\Http\Resources\ColliCollection;
use App\Http\Resources\ColliResource;
use App\Models\Colli;
use App\Services\Core\Helpers\Helper;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Picqer\Barcode\BarcodeGeneratorHTML;
use Printer;
use Throwable;

class ColliController extends Controller
{
    /**
     * @var ColliRepositoryInterface
     */
    protected $colliRepo;

    /**
     * @param ColliRepositoryInterface $colliRepo
     */
    public function __construct(ColliRepositoryInterface $colliRepo)
    {
        $this->colliRepo = $colliRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        return view('collis.index', [
            'userRole' => in_array('user', Auth::user()->roles) ? UserRole::ROLE_USER : UserRole::ROLE_ADMIN
        ]);
    }

    /**
     * @param Request $request
     * @return ColliCollection
     */
    public function getCollis(Request $request): ColliCollection
    {
        $collis = $this->colliRepo->get($request->all());
        return new ColliCollection($collis);
    }

    /**
     * @param Request $request
     * @return ColliCollection
     */
    public function getCollisByReference(Request $request): ColliCollection
    {
        $collis = $this->colliRepo->getByReference($request->all()['data']);
        return new ColliCollection($collis);
    }

    /**
     * @param Request $request
     * @return ColliCollection
     */
    public function getCollisByColliBatch(Request $request): ColliCollection
    {
        $collis = $this->colliRepo->getByColliBatch($request->all());
        return new ColliCollection($collis);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function colliRegister(Request $request): JsonResponse
    {
        try {
            $collis = array_filter($request->all());
            foreach ($collis as $k => $v) {
                $colli = Colli::with('booking')->findOrFail($k);
                $colli->status = $colli->booking->linehaulTrip === null ?
                    Colli::COLLI_STATUS_ON_RECEIVER_TERMINAL : Colli::COLLI_STATUS_ON_DISPATCH_TERMINAL;
                $colli->save();
            }
            return response()->json(['message' => 'success']);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * Get colli info.
     *
     * @param int $id
     * @return ColliResource
     */
    public function getSingle(int $id): ColliResource
    {
        $colli = Colli::with('booking', 'client')->findOrFail($id);
        return ColliResource::make($colli);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return View
     */
    public function edit(int $id): View
    {
        $colli = Colli::findOrFail($id);
        return view('collis.edit', compact('colli'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateColliRequest $request
     * @param int $id
     *
     * @return JsonResponse
     */
    public function update(UpdateColliRequest $request, int $id): JsonResponse
    {
        try {
            $colli = Colli::findOrFail($id);
            $colliUpd = $this->colliRepo->update($request->all(), $colli);
            return response()->json(['message' => 'success', 'colliId' => $colliUpd->id]);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * Remove multiple collis
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteMultipleCollis(Request $request): JsonResponse
    {
        try {
            $ids = array_filter($request->all());
            $this->colliRepo->delete($ids);
            return response()->json(['message' => 'success']);
        } catch (Throwable $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * Update multiple collis
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function updateMultipleCollis(Request $request): JsonResponse
    {
        try {
            $this->colliRepo->massUpdate($request->all());
            return response()->json(['message' => 'success']);
        } catch (Throwable $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * print labels
     *
     * @param Request $request
     * @param int $isPrintByBooking
     * @return
     */
    public function printLabels(Request $request)
    {
        try {
            $requestData = $request->all();
            if (array_key_exists('isForClients', $requestData) && (int)$requestData['isForClients'] === 0) {
                $maxColliNumber = [];
                $requestData = array_filter($request->all());
                $ids = [];
                foreach ($requestData as $k => $v) {
                    if ((int)$k > 0) {
                        $ids[$k] = $k;
                    }
                }
                if (array_key_exists('isPrintByBooking', $requestData) && (int)$requestData['isPrintByBooking'] === 1) {
                    $collis = Colli::with(
                        'booking',
                        'client',
                        'booking.pickupAddress',
                        'booking.dropoffAddress',
                    )->whereIn('booking_id', $ids)
                        ->orderBy('reference', 'ASC')
                        ->get();
                } else {
                    $collisByColliBatchIds = [];
                    foreach ($ids as $v) {
                        $colliExisting = Colli::findOrFail((int)$v);
                        $getCollisByColliBatch = Colli::where('colli_batch', '=', $colliExisting->colli_batch)->get();
                        foreach ($getCollisByColliBatch as $batch) {
                            $collisByColliBatchIds[] = $batch->id;
                        }
                    }
                    $collis = Colli::with(
                        'booking',
                        'booking.pickupAddress',
                        'booking.dropoffAddress',
                        'booking.product',
                        'client',
                        'location'
                    )->whereIn('id', $collisByColliBatchIds)
                        ->orderBy('reference', 'ASC')
                        ->get();
                }
                foreach ($collis as $colli) {
                    $maxColliNumber[$colli->reference][] = Helper::checkColliStartingFromZero($colli);
                }
            } else {
                if (array_key_exists('isReprint', $requestData) && (int)$requestData['isReprint'] > 0) {
                    $maxColliNumber = [];
                    $receivingLocation = Helper::getReceivingLocation((int)$requestData['warehouseId']);

                    $collis = Colli::where('booking_id', '=', (int)$requestData['isReprint'])
                        ->with('booking',
                            'booking.pickupAddress',
                            'booking.dropoffAddress',
                            'booking.product',
                            'client',
                            'location')
                        ->orderBy('reference', 'ASC')->orderBy('id', 'ASC')
                        ->get();

                    if ($receivingLocation > 0) {
                        foreach ($collis as $colli) {
                            $colli->location_id = $receivingLocation;
                            $colli->save();
                            $maxColliNumber[$colli->reference][] = count($collis);
                        }
                    }
                } elseif (array_key_exists('printByReference', $requestData)) {
                    //$printPeriod = array_key_exists('isReprint', $requestData) && (int)$requestData['isReprint'] === 1
                    // ? 999999999 : 2;

                    $collis = Colli::where('reference', '=', $requestData['reference'])
                        ->with('booking',
                            'booking.pickupAddress',
                            'booking.dropoffAddress',
                            'booking.product',
                            'client',
                            'location')
                        ->orderBy('reference', 'ASC')->orderBy('id', 'ASC')
                        //->where('created_at', '>', Carbon::now()->subMinutes($printPeriod)->toDateTimeString())
                        ->get();

                    $maxColliNumber[$requestData['reference']][] = count($collis);
                } elseif (array_key_exists('printByLastPrintedAsUnknown', $requestData)) {
//                    $maxColliNumber = [];
//                    $lastPrintedAsUnknown = [];
//                    if ($requestData['driver'] == "0") {
//                        $tripsByDate = Trip::with('booking')->whereDate('tw_start', $requestData['date'])->get();
//                    } elseif ($requestData['driver'] == "1") {
//                        $tripsByDate = Trip::with('booking')->whereDate('tw_start', $requestData['date'])
//                            ->whereNull('driver')
//                            ->get();
//                    } else {
//                        $tripsByDate = Trip::with('booking')
//                            ->whereDate('tw_start', $requestData['date'])
//                            ->whereNotNull('driver')
//                            ->get();
//                    }
//                    foreach ($tripsByDate as $trip) {
//                        $lastPrintedAsUnknown[] = $trip->booking->reference;
//                    }
//                    $collis = Colli::where('lastprintedas_unknown', '=', 1)
//                        ->whereIn('reference', $lastPrintedAsUnknown)
//                        ->get();
//                    foreach ($collis as $colli) {
//                        $maxColliNumber[$colli->reference][] = Helper::checkColliStartingFromZero($colli);
//                        $colli->lastprintedas_unknown = 0;
//                        $colli->save();
//                    }
                } else {
                    $maxColliNumber = [];
                    $requestData = array_filter($request->all());
                    foreach ($requestData as $k => $v) {
                        if ((int)$k > 0) {
                            $ids[$k] = $k;
                        }
                    }
                    if (array_key_exists('isPrintByBooking', $requestData)
                        && (int)$requestData['isPrintByBooking'] === 1) {
                        $collis = Colli::with(
                            'booking',
                            'booking.pickupAddress', 'booking.dropoffAddress',
                            'booking.product',
                            'client',
                            'location'
                        )->whereIn('booking_id', $ids)
                            ->orderBy('reference', 'ASC')
                            ->get();
                    } else {
                        $collisByColliBatchIds = [];
                        foreach ($ids as $v) {
                            $colliExisting = Colli::findOrFail((int)$v);
                            $getCollisByColliBatch = Colli::where('colli_batch', '=', $colliExisting->colli_batch)
                                ->get();
                            foreach ($getCollisByColliBatch as $batch) {
                                $collisByColliBatchIds[] = $batch->id;
                            }
                        }

                        $collis = Colli::with(
                            'booking',
                            'booking.pickupAddress', 'booking.dropoffAddress',
                            'booking.product',
                            'client',
                            'location'
                        )->whereIn('id', $collisByColliBatchIds)
                            ->orderBy('reference', 'ASC')
                            ->orderBy('id', 'ASC')->get();
                    }
                    foreach ($collis as $colli) {
                        $maxColliNumber[$colli->reference][] = Helper::checkColliStartingFromZero($colli);
                    }
                }
            }
            if (count($collis) > 0) {
                $barcode = [];
                $generator = new BarcodeGeneratorHTML();
                foreach ($collis as $colli) {
                    $barcode[$colli->id] = $generator->getBarcode('00000000000000000000000000000' . $colli->id, $generator::TYPE_CODE_128, 3, 65);
                }
                $response = [];
                $documentCreatedAt = time();
                $pdf = \PDF::loadView('collis.print_label', compact(['collis', 'maxColliNumber', 'barcode']));
                $pdf->setPaper('A4');
                Storage::put(
                    'public/reports/print_labels_' . $documentCreatedAt . '.pdf',
                    $pdf->output()
                );
                if (!array_key_exists('isForClients', $requestData) ||
                    (array_key_exists('isForClients', $requestData) && $requestData['isForClients'] === 0)) {
                    if (env('APP_ENV', 'local') === 'prod') {
                        //printnode
                        $printerId = Printer::getLabelId();
                        if ($printerId) {
                            $printer = Printer::getPrintnodePrinter($printerId);
                            $printJobStatus = $printer->addPrintJob(
                                $printerId,
                                Storage::disk('public')->path('/reports/print_labels_' . $documentCreatedAt . '.pdf'),
                                'Label print ' . $documentCreatedAt,
                                config('services.printnode.options')
                            );
                            if (!isset($printJobStatus['success']) || !$printJobStatus['success']) {
                                Log::channel('printlog')
                                    ->info("Print job has not been added (Print label #$documentCreatedAt)"
                                        . json_encode($printJobStatus));
                                $response['printnode'] = 'failed to add printnode job';
                            } else {
                                Log::channel('printlog')
                                    ->info("Print job has been added (Print label #$documentCreatedAt)");
                                $response['printnode'] = 'success';
                            }
                        } else {
                            $response['printnode'] = 'missing printer';
                        }
                    }
                }
                $response['file'] = $documentCreatedAt;
            } else {
                $response['file'] = null;
            }
            return response()->json($response);
        } catch (Throwable $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * allow confirm by admins
     * @param Request $request
     * @return JsonResponse
     */
    public function sendReceivedCollis(Request $request)
    {
        $data = $request->all();
        try {
            $files = [];
            if (array_key_exists('selectedEmail', $data) && count($data['selectedEmail']) > 0) {
                if (array_key_exists('selectedClients', $data) && count($data['selectedClients']) > 0) {
                    foreach ($data['selectedEmail'] as $k => $email) {
                        if (array_key_exists($k, $data['selectedClients']) && $data['selectedClients'][$k] === true) {
                            $colliRequest = [
                                'receivedFrom' => $data['dateFrom'],
                                'receivedTo' => $data['dateTo'],
                                'clientIdSingle' => $k
                            ];
                            $collis = $this->colliRepo->get($colliRequest);
                            $pdf = \PDF::loadView('settings.received_goods', compact(['collis', 'colliRequest']));
                            $pdf->setPaper('A4', 'portrait');
                            $documentCreatedAt = time();
                            $file = 'public/received_goods/received_' . $documentCreatedAt . '.pdf';
                            Storage::put($file, $pdf->output());
                            $files[] = $file;

                            Mail::send(
                                'emails.receivedGoods',
                                compact(['collis', 'colliRequest']),
                                function ($message) use ($email, $file) {
                                    $message->to($email)->subject('Varer modtaget');
                                    $message->attach(storage_path('app/' . $file));
                                }
                            );
                        }
                    }
                }
            }
            if (count($files) > 0 && $data['sendCopyToOwner'] === true) {
                $collis = [];
                $colliRequest = [
                    'receivedFrom' => $data['dateFrom'],
                    'receivedTo' => $data['dateTo'],
                    'toOwnerName' => Colli::RECEIVED_GOODS_OWNER_NAME
                ];
                Mail::send(
                    'emails.receivedGoods',
                    compact(['collis', 'colliRequest']),
                    function ($message) use ($email, $files) {
                        $message->to(Colli::RECEIVED_GOODS_OWNER_EMAIL)->subject('Varer modtaget');
                        foreach ($files as $file) {
                            $message->attach(storage_path('app/' . $file));
                        }
                    }
                );
            }
            return response()->json(['message' => 'success']);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * @param Request $request
     * @return  JsonResponse
     */
    public function getDimensionsSum(Request $request): JsonResponse
    {
        $requestData = $request->all();
        $filters = $requestData['params']['filters'];
        $filters['length'] = 99999;

        $checkedCollis = array_filter($requestData['params']['collis']);
        $dimensions = [
            'volume' => 0,
            'weight' => 0,
        ];

        $collis = $this->colliRepo->get($filters);

        foreach ($collis as $v) {
            if (count($checkedCollis) > 0 && !array_key_exists($v->id, $checkedCollis)) {
                continue;
            }
            $dimensions['weight'] += Helper::getTotalColliMeasureByColliBatch($v->colli_batch, 'weight');
            $dimensions['volume'] += Helper::getTotalColliMeasureByColliBatch($v->colli_batch, 'volume');
        }
        return response()->json($dimensions);
    }

    /**
     * @param string $batch
     * @return  JsonResponse
     */
    public function getLog(string $batch)
    {
        try {
            $colli = Colli::where('colli_batch', '=', (int)$batch)->first();
            $data = $this->colliRepo->getLog($colli->id);
            return response()->json($data);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     *
     * @param Request $request
     *
     * @return
     *
     */
    public function storeImage(Request $request)
    {
        $request->validate([
            'file' => 'required|mimes:jpeg,png,jpg,gif,svg,pdf|max:10000',
        ]);

        if ($request->hasFile('file')) {
            $data = $request->all();
            $collis = explode(',', $data['collis']);

            foreach ($collis as $colli) {
                $colliExisting = Colli::where('id', '=', (int)$colli)->first();
                if ($colliExisting) {
                    if ((int)$data['isColliBatch'] === 1) {
                        $getCollisByColliBatch = Colli::where('colli_batch', '=', $colliExisting->colli_batch)->get();
                       // foreach ($getCollisByColliBatch as $batch) {
                            $extension = request()->file('file')->getClientOriginalExtension();
                            $image_name = Str::uuid() . '.' . $extension;
                            $path = $request->file('file')->storeAs(
                                'images',
                                $image_name,
                                's3'
                            );
                            Image::create([
                                'created_by' => 1,
                                'source_id' => (int)$colli,
                                'source' => 'colli',
                                'image_url' => $image_name,
                                'uuid' => (string)Str::uuid()
                            ]);
                       // }
                    } else {
                        $extension = request()->file('file')->getClientOriginalExtension();
                        $image_name = Str::uuid() . '.' . $extension;
                        $path = $request->file('file')->storeAs(
                            'images',
                            $image_name,
                            's3'
                        );
                        Image::create([
                            'created_by' => 1,
                            'source_id' => (int)$colli,
                            'source' => 'colli',
                            'image_url' => $image_name,
                            'uuid' => (string)Str::uuid()
                        ]);
                    }
                }
            }
            return true;
        }
    }

    /**
     *
     * @param int $batch
     *
     * @return
     *
     */
    public function getImages(int $batch)
    {
        try {
            $imagesList = [];
            $getCollisByColliBatch = Colli::where('colli_batch', '=', $batch)->get();
            if ($getCollisByColliBatch) {
                foreach ($getCollisByColliBatch as $colli) {
                    foreach ($colli->images as $image) {
                        $imagesList[] = Storage::disk('s3')->temporaryUrl('images/' . $image->image_url, now()->addMinutes(30));
                    }
                }
            }
            return response(['result' => 'success', 'data' => $imagesList]);
        } catch (Throwable $e) {
            return response($e->getMessage(), 400);
        }
    }
}
