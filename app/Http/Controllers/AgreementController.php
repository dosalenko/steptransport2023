<?php

namespace App\Http\Controllers;

use App\Repository\Interfaces\AgreementRepositoryInterface;
use App\Models\Agreement;
use App\Models\Client;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Requests\StoreAgreementRequest;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Throwable;
use Exception;

class AgreementController extends Controller
{
    /**
     * @var AgreementRepositoryInterface
     */
    protected $agreementRepo;

    /**
     * @param AgreementRepositoryInterface $agreementRepo
     */
    public function __construct(AgreementRepositoryInterface $agreementRepo)
    {
        $this->agreementRepo = $agreementRepo;
    }

    /**
     * Show the form for creating a new resource.
     * @param int $clientId
     * @return View
     */
    public function create(int $clientId): View
    {
        $clientData = Client::with('addresses')->findOrFail($clientId);
        return view('agreements.create', compact('clientData'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreAgreementRequest $request
     * @return Response
     */
    public function store(StoreAgreementRequest $request): Response
    {
        try {
            $agreement = $this->agreementRepo->create($request->all());
            return response(['result' => 'success', 'agreement_id' => $agreement->id]);
        } catch (Throwable $e) {
            return response($e->getMessage(), 400);
        }
    }

    /**
     * Get agreement info.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function getSingle(int $id): JsonResponse
    {
        $this->agreementRepo->getSingle(Agreement::findOrFail($id));
        $q = Agreement::query();
        $agreementPrices = $q->whereHas('products.prices.interval', function ($q) use ($id) {
            $q->where('agreement_id', '=', $id);
        });
        if ($agreementPrices->count() > 0) {
            $agreementPrices = $q->with([
                'products',
                'products.product',
                'products.prices',
                'products.prices.interval',
                'services',
                'services.service',
                'services.prices',
                'services.prices.interval',
                'pickupAddress',
                'dropoffAddress',
            ])->findOrFail($id);
        } else {
            $agreementPrices = Agreement::with([
                'products',
                'products.product',
                'products.prices',
                'products.prices.interval',
                'services',
                'services.service',
                'services.prices',
                'services.prices.interval',
                'pickupAddress',
                'dropoffAddress',
            ])->findOrFail($id);
        }

        return response()->json(['agreement' => $agreementPrices]);
    }

    /**
     * check if client primary agreement already exists
     *
     * @param int $id
     *
     * @return JsonResponse
     */
    public function checkPrimaryAgreement(int $id): JsonResponse
    {
        $primary = Agreement::where('client_id', $id)->where('primary_agreement', 1)->count();

        return response()->json(['result' => $primary > 0]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return View
     */
    public function edit(int $id): View
    {
        $agreement = Agreement::findOrFail($id);
        $clientData = Client::with('addresses')->findOrFail($agreement->client_id);

        return view('agreements.edit', compact('agreement', 'clientData'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     *
     * @return JsonResponse
     */
    public function update(Request $request, int $id): JsonResponse
    {
        try {
            $agreement = Agreement::findOrFail($id);
            $agreementUpd = $this->agreementRepo->update($request->all(), $agreement);
            return response()->json(['result' => 'success', 'agreementId' => $agreementUpd->id]);
        } catch (Throwable $e) {
            return response()->json($e->getMessage(), 400);
        }
    }

    /**
     *
     * @param int $id
     *
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        try {
            $this->agreementRepo->delete($id);
            return response()->json(['message' => 'success']);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }
}
