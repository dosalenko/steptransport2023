<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Repository\Interfaces\LocationRepositoryInterface;
use Illuminate\Http\Request;
use Twilio\Rest\Client;

class TestController extends Controller
{
    /**
     * @var LocationRepositoryInterface
     */
    protected $locationRepo;

    /**
     * @param LocationRepositoryInterface $locationRepo
     */
    public function __construct(LocationRepositoryInterface $locationRepo)
    {
        $this->locationRepo = $locationRepo;
    }


    /**
     * @throws \Twilio\Exceptions\ConfigurationException
     */
    public function mail()
    {
        $text = 'test 14.06.2023';
        $account_sid = env("TWILIO_SID");
        $auth_token = env("TWILIO_TOKEN");
        $twilio_from_name = env("TWILIO_FROM_NAME");

//        $client = new Client($account_sid, $auth_token);
//        $client->messages->create('+4520389588', [
//            'from' => $twilio_from_name,
//            'body' => $text
//        ]);
        dd('success');


        $b = Booking::where('status', '=', Booking::BOOKING_TYPE_DELIVERED)->get();
        foreach ($b as $v) {
            foreach ($v->collis as $c) {
                echo $c->id;
                echo '-';
                echo $c->colli_packing_status;
                  $c->colli_packing_status = 3;
                  $c->save();
                echo '<br/>';
            }
        }
        dd();
        $receiverNumber = "+34641008969";
        $receiverNumber1 = "+4561101129";
        //virtual number
        $receiverNumber2 = "+4593705916";
        $message = "Hej NAME\n
Vi har modtaget besked om at afhente din pakke til DROPOFF ADDRESS NAME.\n
Din afhentning vil ske d. DATE mellem kl. TIMEBEGIN og TIMEEND til ADDRESS.\n
Afhentningsservice er SERVICE \n
Hvis du ønsker at ændre afhentningen, kan du klikke her: LINK.\n
Mvh Transport";

        try {
            $account_sid = env("TWILIO_SID");
            $auth_token = env("TWILIO_TOKEN");
            $twilio_from = env("TWILIO_FROM_NUMBER");
            $twilio_from_name = env("TWILIO_FROM_NAME");

            $client = new Client($account_sid, $auth_token);
            $client->messages->create($receiverNumber1, [
                'from' => 'Step Transp',
                //'from' => $twilio_from,
                'body' => $message]);
            dd('SMS Sent Successfully.');
        } catch (\Exception $e) {
            dd("Error: " . $e->getMessage());
        }
    }


    public function upload()
    {

        //  return view('test.upload');
    }

    public function image(Request $request)
    {
        /*        $request->validate([
                    'file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                ]);
                if ($request->hasFile('image')) {
                    $extension = request()->file('image')->getClientOriginalExtension();
                    $image_name = time() . '_' . $request->title . '.' . $extension;
        //            $path = $request->file('image')->storeAs(
        //                'images',
        //                $image_name,
        //                's3'
        //            );
                    Image::create([
                        'created_by' => 1,
                        'source_id' => 11,
                        'source' => $image_name,
                        'image_url' => $request->title,
                        'uuid' => (string)Str::uuid()
                    ]);

                }*/
    }
}
