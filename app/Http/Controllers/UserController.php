<?php

namespace App\Http\Controllers;

use App\Repository\Interfaces\UserRepositoryInterface;
use App\Models\User;
use App\Models\Client;
use Exception;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use Illuminate\View\View;
use Throwable;

class UserController extends Controller
{

    /**
     * @var UserRepositoryInterface
     */
    protected $userRepo;

    /**
     * @param UserRepositoryInterface $userRepo
     */
    public function __construct(UserRepositoryInterface $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    /**
     * @param int $userId
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(int $userId): View
    {
        $clientData = Client::with('addresses')->findOrFail($userId);
        return view('users.create', compact('clientData'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreUserRequest $request
     * @return JsonResponse
     */
    public function store(StoreUserRequest $request): JsonResponse
    {
        try {
            $user = $this->userRepo->create($request->all());
            return response()->json(['message' => 'success', 'userId' => $user->id]);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     *
     * @param UpdateUserRequest $request *
     * @return JsonResponse
     * @todo validation
     *
     * Update the specified resource in storage.
     *
     */
    public function update(UpdateUserRequest $request): JsonResponse
    {
        try {
            $requestData = $request->all();

            foreach ($requestData as $item) {
                $user = User::findOrFail($item['id']);
                $user->name = $item['name'];
                $user->email = $item['email'];
                $user->password = isset($item['password']) ? bcrypt($item['password']) : $user->password;
                $user->agreements = is_array($item['agreements']) ?
                    json_encode($item['agreements']) : $item['agreements'];
                $user->save();
            }
            return response()->json(['result' => 'success']);
        } catch (Throwable $e) {
            return response()->json($e->getMessage(), 400);
        }
    }

    /**
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        try {
            $user = User::findOrFail($id);
            $user->active = 0;
            $user->save();
            return response()->json(['message' => 'success']);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }
}
