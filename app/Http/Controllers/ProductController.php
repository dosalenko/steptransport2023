<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProductCollection;
use App\Models\AgreementProduct;
use App\Models\AgreementProductPrice;
use App\Models\AgreementServicePrice;
use App\Models\Product;
use App\Models\ProductPrice;
use App\Repository\Interfaces\ProductRepositoryInterface;
use App\Models\ServicePrice;
use App\Services\Core\Helpers\BookingHelper;
use App\Services\Core\Helpers\Helper;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Http\Requests\PriceRequest;
use App\Models\Interval;
use Illuminate\View\View;
use Throwable;

class ProductController extends Controller
{
    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepo;

    /**
     * @param ProductRepositoryInterface $productRepo
     */
    public function __construct(ProductRepositoryInterface $productRepo)
    {
        $this->productRepo = $productRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        return view('products.index');
    }

    /**
     * @param int|null $agreementId
     * @return ProductCollection
     */
    public function getProducts($agreementId = 0): ProductCollection
    {
        if ($agreementId > 0) {
            $query = AgreementProduct::with('prices', 'product')
                ->where('agreement_id', '=', $agreementId)
                ->where('active', '=', 1);
        } else {
            $query = Product::with('prices');
        }
        $products = $query->get();

        return new ProductCollection($products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreProductRequest $request
     * @return JsonResponse
     */
    public function store(StoreProductRequest $request): JsonResponse
    {
        try {
            $product = $this->productRepo->create($request->all());

            //after products creating add prices to each existing interval
            $intervals = Interval::all();
            foreach ($intervals as $interval) {
                $productPrice = new ProductPrice();
                $productPrice->product_id = $product->id;
                $productPrice->interval_id = $interval->id;
                $productPrice->save();
            }

            return response()->json(['message' => 'success', 'productId' => $product->id]);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * Get product info.
     *
     * @param int $id
     *
     * @return JsonResponse
     */
    public function getSingle(int $id): JsonResponse
    {
        $product = Product::findOrFail($id);

        return response()->json($product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return View
     */
    public function edit(int $id): View
    {
        $product = Product::findOrFail($id);
        return view('products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateProductRequest $request
     * @return JsonResponse
     */
    public function update(UpdateProductRequest $request): JsonResponse
    {
        try {
            $requestData = $request->all();

            foreach ($requestData as $item) {
                $product = Product::findOrFail($item['id']);
                $product->update($item);
            }
            return response()->json(['result' => 'success']);
        } catch (Throwable $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        try {
            $product = Product::findOrFail($id);
            AgreementProductPrice::whereHas('product', function ($q) use ($id) {
                $q->where('product_id', '=', $id);
            })->delete();
            AgreementProduct::where('product_id', $id)->delete();
            $product->prices()->delete();
            $product->delete();
            return response()->json(['message' => 'success']);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * @return JsonResponse
     */
    public function getProductPrices(): JsonResponse
    {
        $query = ProductPrice::with('product', 'interval')->get();
        return response()->json(['data' => $query]);
    }

    /**
     * @param int $productId
     * @param float $totalVolume
     * @param float $totalWeight
     * @param string $type
     */
    public function getProductPriceByInterval($productId, $totalVolume, $totalWeight, $type, $agreementId)
    {
        $totalWeight = (float)str_replace(',', '.', $totalWeight);
        $totalVolume = (float)str_replace(',', '.', $totalVolume);

        if ($type === 'service') {
            return BookingHelper::getServicePriceByInterval($productId, $totalVolume, $totalWeight, $agreementId);
        } else {
            return BookingHelper::getProductPriceByInterval($productId, $totalVolume, $totalWeight, $agreementId);
        }
    }

    /**
     * @return JsonResponse
     */
    public function getServicePrices(): JsonResponse
    {
        $query = ServicePrice::with('service', 'interval')->get();
        return response()->json(['data' => $query]);
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function getAgreementProducts(int $id): JsonResponse
    {
        $arr = [];
        $query = AgreementProduct::where('agreement_id', '=', $id)->where('active', '=', 1)->get();
        foreach ($query as $v) {
            $arr[] = $v->id;
        }
        return response()->json($arr);
    }

    /**
     * Update product prices
     *
     * @param PriceRequest $request
     *
     * @return JsonResponse
     */
    public function savePrices(PriceRequest $request): JsonResponse
    {
        try {
            $requestData = $request->all();

            $productPrices = [];
            $servicePrices = [];
            foreach ($requestData as $item) {
                if (isset($item['product_id'])) {
                    $productPrices[$item['product_id']][$item['interval_id']] =
                        [
                            'product_id' => $item['product_id'],
                            'price' => Helper::format($item['price']),
                            'price_type' => $item['price_type'],
                            'interval_id' => $item['interval_id'],
                        ];
                }
                if (isset($item['service_id'])) {
                    $servicePrices[$item['service_id']][$item['interval_id']] =
                        [
                            'service_id' => $item['service_id'],
                            'price' => Helper::format($item['price']),
                            'price_type' => $item['price_type'],
                            'interval_id' => $item['interval_id'],
                        ];
                }
            }

            foreach ($productPrices as $id => $item) {
                foreach ($item as $type) {
                    $productPrice = ProductPrice::where('product_id', '=', $id)
                        ->where('interval_id', '=', $type['interval_id'])
                        ->first();
                    if ($productPrice) {
                        if ((int)$type['price_type'] === 1) {
                            $productPrice->standard_price = $type['price'];
                        }
                        if ((int)$type['price_type'] === 2) {
                            $productPrice->minimum_price = $type['price'];
                        }
                        if ((int)$type['price_type'] === 3) {
                            $productPrice->cost_price = $type['price'];
                        }
                        $productPrice->save();
                    }
                }
            }

            foreach ($servicePrices as $id => $item) {
                foreach ($item as $type) {
                    $servicePrice = ServicePrice::where('service_id', '=', $id)
                        ->where('interval_id', '=', $type['interval_id'])
                        ->first();
                    if ($servicePrice) {
                        if ((int)$type['price_type'] === 1) {
                            $servicePrice->standard_addon_price = $type['price'];
                        }
                        if ((int)$type['price_type'] === 2) {
                            $servicePrice->minimum_price = $type['price'];
                        }
                        if ((int)$type['price_type'] === 3) {
                            $servicePrice->cost_price = $type['price'];
                        }
                        $servicePrice->save();
                    }
                }
            }
            return response()->json(['result' => 'success']);
        } catch (Throwable $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * Update product prices
     *
     * @param PriceRequest $request
     *
     * @return JsonResponse
     */
    public function saveAgreementProductsPrices(PriceRequest $request): JsonResponse
    {
        try {
            $requestData = $request->all();

            foreach ($requestData as $product) {
                foreach ($product['prices'] as $price) {
                    $agreementsProductsPrice = AgreementProductPrice::where(
                        'agreement_interval_id',
                        $price['agreement_interval_id']
                    )->where('agreement_product_id', $price['agreement_product_id'])->first();
                    if ($agreementsProductsPrice) {
                        $agreementsProductsPrice->price = Helper::format($price['price']);
                        $agreementsProductsPrice->save();
                    } else {
                        $checkIfProductExists = AgreementProduct::find((int)$price['agreement_product_id']);
                        if ($checkIfProductExists) {
                            $agreementsProductsPriceNew = new AgreementProductPrice();
                            $agreementsProductsPriceNew->price = Helper::format($price['price']);
                            $agreementsProductsPriceNew->agreement_interval_id = (int)$price['agreement_interval_id'];
                            $agreementsProductsPriceNew->agreement_product_id = (int)$price['agreement_product_id'];
                            $agreementsProductsPriceNew->save();
                        }
                    }
                }
            }

            return response()->json(['result' => 'success']);
        } catch (Throwable $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * Update product prices
     *
     * @param PriceRequest $request
     *
     * @return JsonResponse
     */
    public function saveAgreementServicesPrices(PriceRequest $request): JsonResponse
    {
        try {
            $requestData = $request->all();

            foreach ($requestData as $product) {
                foreach ($product['prices'] as $price) {
                    $agreementsProductsPrice = AgreementServicePrice::where(
                        'agreement_interval_id',
                        $price['agreement_interval_id']
                    )->where('agreement_service_id', $price['agreement_service_id'])->first();
                    if ($agreementsProductsPrice) {
                        $agreementsProductsPrice->price = Helper::format($price['price']);
                        $agreementsProductsPrice->save();
                    } else {
                        $checkIfProductExists = AgreementProduct::find((int)$price['agreement_service_id']);
                        if ($checkIfProductExists) {
                            $agreementsProductsPriceNew = new AgreementServicePrice();
                            $agreementsProductsPriceNew->price = Helper::format($price['price']);
                            $agreementsProductsPriceNew->agreement_interval_id = (int)$price['agreement_interval_id'];
                            $agreementsProductsPriceNew->agreement_service_id = (int)$price['agreement_service_id'];
                            $agreementsProductsPriceNew->save();
                        }
                    }
                }
            }
            return response()->json(['result' => 'success']);
        } catch (Throwable $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * Save products related to concrete agreement
     *
     * @param Request $request
     * @param $id
     *
     * @return JsonResponse
     */
    public function saveAgreementProducts(Request $request, $id): JsonResponse
    {
        try {
            $requestData = $request->all();
            if ($requestData) {
                foreach ($requestData as $productId) {
                    AgreementProduct::updateOrCreate(
                        ['agreement_id' => $id, 'id' => (int)$productId],
                        ['active' => 1]
                    );
                }
                //make other agreements products inactive
                AgreementProduct::where('agreement_id', '=', $id)->whereNotIn('id', $requestData)
                    ->update(['active' => 0]);
            }
            return response()->json(['result' => 'success']);
        } catch (Throwable $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }
}
