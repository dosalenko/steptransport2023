<?php

namespace App\Http\Controllers;

use App\Services\Printnode\Printnode;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PrinterController extends Controller
{
    public function index()
    {
        $printer = new Printnode();
        $listPrinters = [];
        if ($printer->isAuth()) {
            $printers = $printer->getPrinters();
            if (is_array($printers) && !empty($printers)) {
                foreach ($printers as $pt) {
                    $listPrinters[$pt->id] = $pt->computer->name . ' (' . $pt->name . ')';
                }
            }
        }
        $cachePrinters = PrintNode::getCookiesPrinters();
        return view('printer.index', [
            'printers' => $listPrinters,
            'tourListPrinter' => $cachePrinters['tour_printer'] ?? '',
            'labelPrinter' => $cachePrinters['label_printer'] ?? '',
        ]);
    }

    public function update(Request $request): RedirectResponse
    {
        $request->validate([
            'delivery_note_printer' => 'required|max:255',
            'label_printer' => 'required|max:255',
        ]);

        $printers['tour_printer'] = $request->delivery_note_printer;
        $printers['label_printer'] = $request->label_printer;

        PrintNode::setCookiesPrinters($printers);

        if (Auth::check()) {
            PrintNode::setCachePrinters($printers, Auth::id());
        }

        return redirect()->route('printer');
    }
}
