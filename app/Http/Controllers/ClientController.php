<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreApiKeyClientRequest;
use App\Http\Resources\ClientCollection;
use App\Models\Address;
use App\Models\Agreement;
use App\Models\ApiKey;
use App\Models\Client;
use App\Models\User;
use App\Models\UserRole;
use App\Repository\Interfaces\ClientRepositoryInterface;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Requests\StoreClientRequest;
use App\Http\Requests\UpdateClientRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Throwable;

class ClientController extends Controller
{
    /**
     * @var ClientRepositoryInterface
     */
    protected $clientRepo;

    /**
     * @param ClientRepositoryInterface $clientRepo
     */
    public function __construct(ClientRepositoryInterface $clientRepo)
    {
        $this->clientRepo = $clientRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        return view('clients.index');
    }

    /**
     * @param Request $request
     * @return ClientCollection
     */
    public function getClients(Request $request): ClientCollection
    {
        if (Auth::user()->hasRole(UserRole::ROLE_USER)) {
            $request['role'] = UserRole::ROLE_USER;
        }
        $clients = $this->clientRepo->get($request->all());
        return new ClientCollection($clients);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        return view('clients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreClientRequest $request
     * @return JsonResponse
     */
    public function store(StoreClientRequest $request): JsonResponse
    {
        try {
            $client = $this->clientRepo->create($request->all());
            return response()->json(['message' => 'success', 'clientId' => $client->id]);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     *
     * @param StoreApiKeyClientRequest $request
     * @return JsonResponse
     */
    public function storeApiKey(StoreApiKeyClientRequest $request): JsonResponse
    {
        try {
            $this->clientRepo->createApiKey($request->all());
            return response()->json(['message' => 'success']);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    public function updateApiKey(Request $request): JsonResponse
    {
        try {
            $requestData = $request->all();

            foreach ($requestData as $v) {
                $api = ApiKey::findOrFail($v['id']);
                $api->name = $v['name'];
                $api->key = $v['key'];
                $api->save();
            }

            return response()->json(['message' => 'success']);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }


    /**
     * Get client info.
     *
     * @param int $id
     *
     * @return JsonResponse
     */
    public function getSingle(int $id): JsonResponse
    {
        $client = Client::with([
            'contacts',
            'users',
            'users.userAgreement',
            'primaryContact',
            'agreements.pickupAddress',
            'agreements.dropoffAddress',
            'addresses',
            'keys'
        ])->findOrFail($id);
        return response()->json($client);
    }


    /**
     * Get client addresses.
     *
     * @param int $id
     *
     * @return JsonResponse
     */
    public function getClientAddresses(int $id): JsonResponse
    {
        $addresses = Address::with('country')->where('client_id', $id)->get();
        return response()->json($addresses);
    }

    /**
     * Get client addresses.
     *
     * @param int $id
     *
     * @return JsonResponse
     */
    public function getClientAgreements(int $id): JsonResponse
    {
        if (Auth::user()->hasRole(UserRole::ROLE_USER)) {
            $agreements = Agreement::whereIn('id', json_decode(Auth::user()->agreements, true))
                ->whereNull('deleted_at')
                ->get();
        } else {
            $agreements = Agreement::where('client_id', $id)->whereNull('deleted_at')->get();
        }
        return response()->json($agreements);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return View
     */
    public function edit(int $id): View
    {
        $client = Client::with('agreements', 'contacts')->findOrFail($id);
        return view('clients.edit', compact('client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateClientRequest $request
     * @param int $id
     *
     * @return JsonResponse
     */
    public function update(UpdateClientRequest $request, int $id): JsonResponse
    {
        try {
            $requestData = $request->all();

            $client = Client::with('primaryContact')->findOrFail($id);
            $client->primaryContact->fill($requestData['primary_contact'])->push();
            $client->update($requestData);

            return response()->json(['result' => 'success', 'clientId' => $client->id]);
        } catch (Throwable $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        try {
            User::where('client_id', '=', $id)->delete();
            Client::destroy($id);
            return response()->json(['message' => 'success']);
        } catch (Throwable $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * Remove client api key
     *
     * @param int $id
     *
     * @return JsonResponse
     */
    public function deleteApiKey(int $id): JsonResponse
    {
        try {
            ApiKey::destroy($id);
            return response()->json(['message' => 'success']);
        } catch (Throwable $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }
}
