<?php

namespace App\Http\Controllers;

use App\Exports\LocationExport;
use App\Http\Requests\StoreLocationRequest;
use App\Http\Resources\LocationCollection;
use App\Imports\LocationImport;
use App\Models\Location;
use App\Models\StepAddress;
use App\Repository\Interfaces\LocationRepositoryInterface;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Maatwebsite\Excel\Facades\Excel;
use Throwable;

class LocationController extends Controller
{
    /**
     * @var LocationRepositoryInterface
     */
    protected $locationRepo;

    /**
     * @param LocationRepositoryInterface $locationRepo
     */
    public function __construct(LocationRepositoryInterface $locationRepo)
    {
        $this->locationRepo = $locationRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        return view('locations.index');
    }

    /**
     * @param Request $request
     * @return LocationCollection
     */
    public function getLocations(Request $request): LocationCollection
    {
        $locations = $this->locationRepo->get($request->all());
        return new LocationCollection($locations);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        return view('locations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreLocationRequest $request
     * @return JsonResponse
     */
    public function store(StoreLocationRequest $request): JsonResponse
    {
        try {
            $location = $this->locationRepo->create($request->all());
            return response()->json(['message' => 'success', '$locationId' => $location->id]);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }


    /**
     * Get $location info.
     *
     * @param int $id
     *
     * @return JsonResponse
     */
    public function getSingle(int $id): JsonResponse
    {
        $location = Location::findOrFail($id);
        return response()->json($location);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return View
     */
    public function edit(int $id): View
    {
        $location = Location::findOrFail($id);
        return view('locations.edit', compact('location'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreLocationRequest $request
     * @param int $id
     *
     * @return JsonResponse
     */
    public function update(StoreLocationRequest $request, int $id): JsonResponse
    {
        try {
            $requestData = $request->all();

            $location = Location::findOrFail($id);
            $location->update($requestData);

            return response()->json(['result' => 'success', 'locationId' => $location->id]);
        } catch (Throwable $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * Remove multiple locations
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteMultipleLocations(Request $request): JsonResponse
    {
        try {
            $requestData = array_filter($request->all());
            foreach ($requestData as $k => $v) {
                $location = Location::findOrFail((int)$k);
                $location->delete();
            }
            return response()->json(['message' => 'success']);
        } catch (Throwable $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     *
     * @return JsonResponse
     */
    public function getWarehouses(): JsonResponse
    {
        $warehouses = StepAddress::get();
        return response()->json($warehouses);
    }

    /**
     *
     * @return JsonResponse
     */
    public function getLocationsSelect(): JsonResponse
    {
        $locations = Location::with('warehouse')->get();
        return response()->json($locations);
    }

    /**
     *
     * @param Request $request
     *
     */
    public function export(Request $request)
    {
        try {
            $requestData = $request->all();
            return Excel::download(new LocationExport($requestData), 'location_' . time() . '.csv');
        } catch (Throwable $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }


    /**
     *
     * @param Request $request
     *
     * @return bool
     *
     */
    public function import(Request $request): bool
    {
        Excel::import(new LocationImport(), $request->file('file')->store('tmp'));
        return true;
    }
}
