<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|max:255',
            'billing_street' => 'required|string|max:255',
            'billing_zip' => 'required|string|max:255',
            'billing_city' => 'required|string|max:255',
            'billing_country_id' => 'integer',
            'vat' => 'required|string|max:255',
            'billing_email' => 'required|email|max:255',
            'credit_days' => 'required|integer',
        ];
    }
}
