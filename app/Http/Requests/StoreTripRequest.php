<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreTripRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'address.name' => 'required|string|max:255',
            'address.street' => 'required|string|max:255',
            'address.zip' => 'required|string|max:255',
            'address.city' => 'required|string|max:255',
        ];
    }
}
