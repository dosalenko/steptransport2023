<?php

namespace App\Http\Resources;

use App\Services\Core\Helpers\ColliHelper;
use App\Services\Core\Helpers\Helper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ColliResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $resourceData = [
            'id' => $this->id,
            'booking' => $this->booking,
            'client' => $this->client,
            'location' => $this->location ?? null,
            'reference' => $this->reference,
            'name' => $this->name,
            'colli' => Helper::checkColliStartingFromZero($this),
            'volume' => number_format($this->volume, 1),
            'weight' => number_format($this->weight, 0),
            'status' => $this->client,
            'created' => Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('Y-m-d'),
            'received' => $this->received_at ?
                Carbon::createFromFormat('Y-m-d H:i:s', $this->received_at)->format('Y-m-d') : null,
            'delivery' => $this->booking != null && $this->booking->dropoff_start ?
                Carbon::createFromFormat('Y-m-d H:i:s', $this->booking->dropoff_start)->format('Y-m-d') : null,
            'totalColliAmount' => Helper::getTotalColliAmountByReference($this->reference),
            'totalColliAmountByColliBatch' => Helper::getTotalColliAmountByColliBatch($this->colli_batch),
            'totalVolume' => number_format(Helper::getTotalColliMeasureByColliBatch($this->colli_batch, 'volume'), 1),
            'totalWeight' => number_format(Helper::getTotalColliMeasureByColliBatch($this->colli_batch, 'weight'), 0),
            'totalVolumeByColliBatch' => number_format(Helper::getTotalColliMeasureByColliBatch($this->colli_batch, 'volume'), 1),
            'totalWeightByColliBatch' => number_format(Helper::getTotalColliMeasureByColliBatch($this->colli_batch, 'weight')),
            'colliBatchLocations' => ColliHelper::getColliBatchLocationList($this->colli_batch),
            //'createdBy' => $this->created_by > 0 ? $this->user->email : null,
            'createdBy' => 'admin@gmail.com',
            'colli_batch' => $this->colli_batch,
            'grouppedByLocations' => ColliHelper::getGrouppedByLocations($this->colli_batch),
            'images' => $this->images,
        ];
        return $resourceData;
    }
}
