<?php

namespace App\Http\Resources;

use App\Models\Booking;
use App\Models\Service;
use Illuminate\Http\Resources\Json\JsonResource;

class BookingApiResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $resourceData = [
            //'success' => true,
            'booking' => $this->id,
            'agreement' => $this->agreement->name,
            'weight' => $this->collis->sum('weight'),
            'volume' => $this->collis->sum('volume'),
            'colli' => $this->collis->count('id'),
            'pickuplocation' => [
                'pickupLocationNo' => $this->pickupAddress->id,
                'pickupAddressZip' => $this->pickupAddress->zip,
                'pickupAddressStreet' => $this->pickupAddress->street,
                'pickupAddressCity' => $this->pickupAddress->city,
                'pickupAddressCountry' => 'Denmark',
                'pickupLocationName' => $this->pickupAddress->name,
                'pickupAllowedTime' => [
                    'from' => $this->pickup_start,
                    'to' => $this->pickup_end,
                ]
            ],
            'dropofflocation' => [
                'dropoffLocationNo' => $this->dropoffAddress->id,
                'dropoffAddressZip' => $this->dropoffAddress->zip,
                'dropoffAddressStreet' => $this->dropoffAddress->street,
                'dropoffAddressCity' => $this->dropoffAddress->city,
                'dropoffAddressCountry' => 'Denmark',
                'dropoffLocationName' => $this->dropoffAddress->name,
                'dropoffAllowedTime' => [
                    'from' => $this->dropoff_start,
                    'to' => $this->dropoff_end,
                ],
                'dropoffNote' => $this->dropoff_note,
                'dropoffPhone' => $this->dropoffAddress->phone,
            ],
            'notes' => $this->note,
            'reference' => $this->reference,
            'products' => $this->product->name,
            'services' => Service::getServicesNames($this->service_price),
        ];
        return $resourceData;
    }
}
