<?php

namespace App\Http\Resources;

use App\Services\Core\Helpers\Helper;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class RecurringResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $resourceData = [
            'id' => $this->id,
            'name' => $this->name,
            'client' => $this->client,
            'agreement' => $this->agreement,
            'driver' => $this->driver,
            'agreement_product' => $this->product,
            'recurring_dates' => json_decode($this->recurring_dates),
            'reference' => $this->reference,
            'internal_comment' => $this->internal_comment,
            'agreement_services_ids' => json_decode($this->agreement_services_ids),
            'total_price' => $this->total_price,
            'stops' => json_decode($this->stops),
            'driverJobs' => Helper::getRecurringDriverJobs($this->driver_id, $this->id),
        ];
        return $resourceData;
    }
}
