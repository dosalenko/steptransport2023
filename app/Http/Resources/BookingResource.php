<?php

namespace App\Http\Resources;

use App\Models\Booking;
use App\Models\Service;
use App\Services\Core\Helpers\BookingHelper;
use App\Services\Core\Helpers\Helper;
use Illuminate\Http\Resources\Json\JsonResource;
use PHPUnit\TextUI\Help;

class BookingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @param int $isForSingleBooking
     * @return array
     */
    public function toArray($request)
    {
        $pickupStart = $this->pickup_start === null ? null : \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->pickup_start)->format('H:i');
        $pickupEnd = $this->pickup_end === null ? null : \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->pickup_end)->format('H:i');

        $dropoffStart = $this->dropoff_start === null ? null : \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->dropoff_start)->format('H:i');
        $dropoffEnd = $this->dropoff_end === null ? null : \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->dropoff_end)->format('H:i');

        $resourceData = [
            'agreement_id' => $this->agreement_id,
            'client' => $this->client->name,
            'client_data' => $this->client,
            'created_at' => \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('Y-m-d H:i:s'),
            'deleted_at' => $this->deleted_at,
            'dropoff_address' => $this->dropoffAddress,
            'dropoff_address_name' => $this->dropoffAddress->name,
            'dropoff_date' => $dropoffStart === null ? null : \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->dropoff_start)->format('Y-m-d'),
            'dropoff_end' => $dropoffEnd === '00:00' ? null : $dropoffEnd,
            'dropoff_note' => $this->dropoff_note,
            'dropoff_start' => $dropoffStart === '00:00' ? null : $dropoffStart,
            'dropoff_trip' => $this->dropoffTrip,
            'id' => $this->id,
            'linehaul' => Booking::getLinehaulNames($this->linehaul_type),
            'linehaul_type' => $this->linehaul_type,
            'linehaul_trip' => $this->linehaulTrip,
            'logs' => $this->logs,
            'notSeenNumber' => Booking::getSeenNumber(),
            'note' => $this->note,
            'pickup_address' => $this->pickupAddress,
            'pickup_date' => $pickupStart === null ? null : \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->pickup_start)->format('Y-m-d'),
            'pickup_end' => $pickupEnd === '00:00' ? null : $pickupEnd,
            'pickup_note' => $this->pickup_note,
            'pickup_start' => $pickupStart === '00:00' ? null : $pickupStart,
            'pickup_trip' => $this->pickupTrip,
            'product' => $this->product,
            'product_price' => $this->product_price,
            'reference' => $this->reference,
            'service_price' => $this->service_price,
            'services' => Service::getServicesNames($this->service_price),
            'services_data' => Service::getServicesData($this),
            //  'services' => [],
            //  'services_data' => [],
            'total_service_price' => $this->service_price === null ? 0 : Service::getTotalServicesPrice($this->service_price),
            'status' => Booking::bookingStatuses()[$this->status],
            'status_id' => $this->status,
            'total_price' => $this->total_price,
            'trips' => $this->trips,
            'volume' => $this->collis && count($this->collis) > 0 ? number_format($this->collis->sum('volume'), 1) : number_format($this->total_volume, 1),
            'weight' => $this->collis && count($this->collis) > 0 ? number_format($this->collis->sum('weight'), 0) : number_format($this->total_weight, 0),
            'is_booking_pickup_exists' => (bool)$this->pickupTrip,
            'is_booking_linehaul_exists' => (bool)$this->linehaulTrip,
            'is_booking_dropoff_exists' => (bool)$this->dropoffTrip,
            'internal_comment' => $this->internal_comment,
            'colli' => $this->collis && count($this->collis) > 0 ? count($this->collis) : $this->total_colli,
            'colli_data' => [],
            'product_time_units' => $this->product_time_units,
            'service_time_units' => $this->service_time_units,
            'depends_on' => $this->depends_on,
            'child_booking' => BookingHelper::getChildBooking($this->id),
            'booking_services' => $this->bookingServices,
            'markAsNullHoursProduct' => BookingHelper::markAsNullHoursProduct($this),
            'markAsNullHoursService' => BookingHelper::markAsNullHoursServices($this),
        ];

        return $resourceData;
    }
}
