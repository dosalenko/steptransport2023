<?php

namespace App\Http\Resources;

use App\Models\Service;
use Illuminate\Http\Resources\Json\JsonResource;

class ServiceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $resourceData = [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'type' => $this->type,
            'type_readable' => $this->type ? Service::serviceTypes()[$this->type] : '',
            'price_type' => $this->price_type,
            'price_type_readable' => $this->price_type ? Service::servicePriceTypesReadable()[$this->price_type] : '',
            'prices' => $this->prices,
            'service' => $this->service,
            'duration' => $this->duration,
            'deleted_at' => $this->deleted_at,
        ];

        return $resourceData;
    }
}
