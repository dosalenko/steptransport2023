<?php

namespace App\Http\Resources;

use App\Models\Booking;
use App\Models\Service;
use App\Models\Trip;
use App\Services\Core\Helpers\BookingHelper;
use App\Services\Core\Helpers\Helper;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class TripResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $timeStart = $this->type == Trip::TRIP_TYPE_LINEHAUL || !$this->tw_start ?
            '' : Carbon::createFromFormat('Y-m-d H:i:s', $this->tw_start)->format('H:i');
        $timeEnd = $this->type == Trip::TRIP_TYPE_LINEHAUL || !$this->tw_end ?
            '' : Carbon::createFromFormat('Y-m-d H:i:s', $this->tw_end)->format('H:i');
        if ($timeStart === '00:00' || !$this->tw_start) {
            $timeStart = null;
        }
        if ($timeEnd === '00:00' || !$this->tw_end) {
            $timeEnd = null;
        }
        $resourceData = [
            'id' => $this->id,
            'booking' => $this->booking,
            'client' => $this->client,
            'address' => $this->address,
            'booking_id' => $this->booking->id,
            'booking_status_human' => Booking::bookingStatuses()[$this->booking->status] ?? $this->booking->status,
            'status' => Trip::tripStatuses()[$this->status] ?? $this->status,
            'status_id' => $this->status,
            'type' => Trip::tripTypes()[$this->type] ?? $this->type,
            'type_id' => $this->type,
            'address_data' => $this->addressData,
            'tw_start' => $this->tw_start ? Carbon::createFromFormat('Y-m-d H:i:s', $this->tw_start)->format('Y-m-d') : null,
            'time_data' => !$this->tw_start ? '' : $this->date,
            'time_day' => !$this->tw_start ? '' : Carbon::createFromFormat('Y-m-d H:i:s', $this->tw_start)->format('Y-m-d'),
            'time_start' => $timeStart,
            'time_end' => $timeEnd,
            'depending' => $this->depending,
            'volume' => $this->booking->collis && count($this->booking->collis) > 0 ?
                number_format(Helper::getTotalBookingColliMeasureByReference($this->booking->collis, 'volume'), 1) :
                number_format($this->booking->total_volume, 1),
            'weight' => $this->booking->collis && count($this->booking->collis) > 0 ?
                number_format(Helper::getTotalBookingColliMeasureByReference($this->booking->collis, 'weight'), 0) :
                number_format($this->booking->total_weight, 1),
            'services' => Service::getServicesNames($this->booking->service_price),
            'location_list' => Helper::getTripLocationList($this->booking),
            'note' => $this->note,
            'driver' => $this->driver ?? 'N/A',
            'vehicle' => $this->vehicle ?? 'N/A',
            'delivered_at' => $this->status === Trip::TRIP_STATUS_DELIVERED ? $this->tw_end : 'N/A',
            'spent_minutes' => $this->spent_minutes,
            'delivery_comment' => $this->delivery_comment ?? 'N/A',
            'image' => $this->image,
            'signature' => $this->signature,
            'pod' => $this->image,
            'optimoroute_result' => $this->optimoroute_result,
            'is_task' => $this->is_task,
            'internal_comment' => $this->booking->internal_comment,
            'barcode' => $this->barcode,
            'route_number' => $this->route_number,
            'markAsColliReceived' => Helper::markAsColliReceived($this),
            'markAsTransferred' => Helper::markAsTransferred($this),
            'markAsScheduled' => Helper::markAsScheduled($this),
            'markAsLinehauled' => Helper::markAsLinehauled($this),
            'child_trip' => BookingHelper::getChildTrip($this),
            'linehaulRowClass' => Helper::getLinehaulRowClass($this),
        ];

        return $resourceData;
    }
}
