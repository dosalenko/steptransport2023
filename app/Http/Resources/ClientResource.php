<?php

namespace App\Http\Resources;

use App\Models\Client;
use Illuminate\Http\Resources\Json\JsonResource;

class ClientResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $resourceData = [
            'id' => $this->id,
            'name' => $this->name,
            'agreements' => $this->agreements,
            'last_30_days_bookings' => Client::getLastMonthBookingsCount($this->id),
            'last_booking_date' => Client::getLastBookingDate($this->id),
            'billing_address' => $this->billing_address,
            'primary_contact' => $this->primaryContactData ?? '',
            'addresses' => $this->addresses,
            'billing_street' => $this->billing_street,
            'billing_zip' => $this->billing_zip,
            'billing_city' => $this->billing_city,
        ];
        return $resourceData;
    }
}
