<?php

namespace App\Http\Resources;

use App\Models\Location;
use Illuminate\Http\Resources\Json\JsonResource;

class DriverResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $resourceData = [
            'id' => $this->id,
            'name' => $this->name,
            'type' => $this->type,
            'optimoroute_driver_id' => $this->optimoroute_driver_id,
            'phone' => $this->phone,
        ];
        return $resourceData;
    }
}
