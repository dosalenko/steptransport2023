<?php

namespace App\Http\Resources;

use App\Models\Product;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $resourceData = [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'type' => $this->type,
            'type_readable' => $this->type ? Product::productTypes()[$this->type] : '',
            'price_type' => $this->price_type,
            'price_type_readable' => $this->price_type ? Product::productPriceTypesReadable()[$this->price_type] : '',
            'prices' => $this->prices,
            'product' => $this->product,
            'duration' => $this->duration,
        ];
        return $resourceData;
    }
}
