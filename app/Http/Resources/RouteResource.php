<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class RouteResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $resourceData = [
            'id' => $this->id,
            'name' => $this->name,
            'days' => json_decode($this->days),
            'intervals' => json_decode($this->intervals),
            'max_volume' => $this->max_volume,
            'max_weight' => $this->max_weight,
            'max_stops' => $this->max_stops,
            'zip_codes' => json_decode($this->zip_codes),
            'products' => $this->products,
            'services' => $this->services,
            'serviceIds' => $this->services->pluck('service_id')->toArray(),
            'productIds' => $this->products->pluck('product_id')->toArray(),
            'is_active' => $this->is_active
        ];
        return $resourceData;
    }
}
